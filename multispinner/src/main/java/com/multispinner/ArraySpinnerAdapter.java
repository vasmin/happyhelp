package com.multispinner;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ArraySpinnerAdapter extends ArrayAdapter {
    private Context activity;
    private List<Item> alSpinner=new ArrayList<>();
    public ArraySpinnerAdapter(@NonNull Context activity) {
        super(activity,0);
        this.activity = activity;

    }

    public  void setData(List<Item> alSpinner){
        this.alSpinner = alSpinner;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(activity).inflate(R.layout.item_spinner,parent,false);

        ((TextView)view.findViewById(R.id.tvName)).setText(alSpinner.get(position).getItemName());
        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(activity).inflate(R.layout.item_spinner,parent,false);
        ((TextView) view.findViewById(R.id.tvName)).setText(alSpinner.get(position).getItemName());
        return view;
    }

    @Override
    public int getCount() {
        return alSpinner.size();
    }
}
