package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.R;

public class WorkAdapter extends ArrayAdapter {
    private Activity activity;
    private List<String> alSpinner=new ArrayList<>();

    public WorkAdapter(@NonNull Activity activity) {
        super(activity,0);
        this.activity = activity;

    }

    public  void setData(List<String> alSpinner){
        this.alSpinner = alSpinner;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(activity).inflate(R.layout.item_spinner,parent,false);


        ((TextView)view.findViewById(R.id.tvName)).setText(alSpinner.get(position)+" years");


        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(activity).inflate(R.layout.item_spinner,parent,false);

        ((TextView) view.findViewById(R.id.tvName)).setText(alSpinner.get(position)+" years");

        return view;
    }

    @Override
    public int getCount() {
        return alSpinner.size();
    }
}
