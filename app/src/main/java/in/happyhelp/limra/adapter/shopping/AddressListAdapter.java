package in.happyhelp.limra.adapter.shopping;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.propertiesresponse.PropertyList;
import in.happyhelp.limra.adapter.AllPropertyAdapter;
import in.happyhelp.limra.shoppingresponse.addressresponse.Address;

public class AddressListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    List<Address> addressList = new ArrayList<>();
    private OnItemClickListner listner;
    public AddressListAdapter(Context context,OnItemClickListner itemClickListner) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.listner=itemClickListner;
    }

    public void setdata(List<Address> addressList) {
        this.addressList = addressList;
        Log.e("data", String.valueOf(addressList.size()));
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_address, viewGroup, false);
        return new AddressListAdapter.AddressHolder(v);
    }

    public interface OnItemClickListner {
        void onItemClick(Address d, View view) throws ParseException;

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        Address address = addressList.get(i);
        if(holder instanceof  AddressHolder){
            ((AddressHolder) holder).name.setText(address.getName());
            ((AddressHolder) holder).address.setText(address.getAddress1()+" "+address.getAddress2()+" "+address.getState()+" "+address.getCity()+" "+address.getLocality()+" "+address.getPincode());

            if(address.getPrefered()){
                ((AddressHolder) holder).checkBox.setChecked(true);
            }else{
                ((AddressHolder) holder).checkBox.setChecked(false);
            }

            ((AddressHolder) holder).bind(address,listner);
        }
    }

    @Override
    public int getItemCount() {
        return addressList.size();
    }

    public class AddressHolder extends RecyclerView.ViewHolder {
        TextView address, name;
        CheckBox checkBox;
        ImageView delete;
        public AddressHolder(View v) {
            super(v);
            address = itemView.findViewById(R.id.address);
            name = itemView.findViewById(R.id.name);
            checkBox=itemView.findViewById(R.id.checkbox);
            delete=itemView.findViewById(R.id.delete);
        }

        public void bind(final Address data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
