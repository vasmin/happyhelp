package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.savedjobresponse.SavedJob;

public class JobAdapter extends RecyclerView.Adapter {
    List<SavedJob> list=new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;
    OnItemClickListner onItemClickListner;

    public JobAdapter(Context context, OnItemClickListner listner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=listner;
    }

    public void setData(List<SavedJob> list){
        this.list=list;
        notifyDataSetChanged();
    }


    public interface OnItemClickListner {
        void onItemClick(SavedJob d, View view) throws ParseException;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_saved_job, viewGroup, false);
        return new JobAdapter.JobHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        SavedJob savedJob=list.get(i);
        if(holder instanceof JobHolder){

            ((JobHolder) holder).positionName.setText(savedJob.getJobTitle());
            ((JobHolder) holder).location.setText(savedJob.getLocation());
            ((JobHolder) holder).companyName.setText(savedJob.getCompanyName());
            ((JobHolder) holder).year.setText(savedJob.getExperience()+" Year");
            ((JobHolder) holder).date.setText(savedJob.getDate());

            ((JobHolder) holder).bind(savedJob,onItemClickListner);


        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class JobHolder extends RecyclerView.ViewHolder {
        TextView positionName,companyName,year,location,date;
        public JobHolder(View v) {
            super(v);
            positionName=v.findViewById(R.id.position);
            companyName=v.findViewById(R.id.companyname);
            year=v.findViewById(R.id.year);
            location=v.findViewById(R.id.location);
            date=v.findViewById(R.id.date);
        }
        public void bind(final SavedJob data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
