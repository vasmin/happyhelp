package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.adslistresponse.Datum;
import in.happyhelp.limra.activity.response.treeresponse.Result;
import in.happyhelp.limra.model.ServiceModel;

public class TreeAdapter extends RecyclerView.Adapter {
    List<Result> list=new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;
    OnItemClickListner onItemClickListner;

    public  TreeAdapter(Context context, OnItemClickListner onItemClickListner){
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }


    public void setData(List<Result> list){
        this.list=list;
        notifyDataSetChanged();
    }


    public interface OnItemClickListner {
        void onItemClick(Result d, View view) throws ParseException;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_treeview, viewGroup, false);
        return new TreeAdapter.TreeHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        final Result result=list.get(i);

        if(holder instanceof TreeHolder){
            ((TreeHolder) holder).name.setText(result.getText());
            ((TreeHolder) holder).referCount.setText("Total Refer : "+String.valueOf(result.getCount()));

            ((TreeHolder) holder).mobile.setText(result.getMobile());
            ((TreeHolder) holder).bind(result,onItemClickListner);

            if(result.getCount()>0){
                ((TreeHolder) holder).arrow.setVisibility(View.VISIBLE);
            }else{
                ((TreeHolder) holder).arrow.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class TreeHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.arrow)
        ImageView arrow;

        @BindView(R.id.refercount)
        TextView referCount;

        @BindView(R.id.mobile)
        TextView mobile;

        public TreeHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }

        public void bind(final Result data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
