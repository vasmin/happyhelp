package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.Jobs.JobApplicationsActivity;
import in.happyhelp.limra.activity.Jobs.MyJobActivity;
import in.happyhelp.limra.activity.Jobs.MyJobListActivity;
import in.happyhelp.limra.activity.Jobs.PostJobsActivity;
import in.happyhelp.limra.activity.property.PropertyDetailsActivity;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.joblistresponse.JobList;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import retrofit2.Call;
import retrofit2.Callback;

public class MyJobAdapter extends RecyclerView.Adapter {

    LayoutInflater layoutInflater;
    OnItemClickListner onItemClickListner;
    List<JobList> list;
    Activity context;

    public MyJobAdapter(Activity context, OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }

    public void setData(List<JobList> list){
        this.list=list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_job_detail, viewGroup, false);
        return new MyJobAdapter.JobHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        final JobList jobList=list.get(i);

        if(holder instanceof MyJobAdapter.JobHolder){
            ((MyJobAdapter.JobHolder) holder).position.setText(jobList.getTitle());
            ((MyJobAdapter.JobHolder) holder).location.setText(jobList.getLocation());
            ((MyJobAdapter.JobHolder) holder).time.setText(jobList.getDate());
            ((MyJobAdapter.JobHolder) holder).salary.setText(jobList.getMinSalary()+" - "+jobList.getMaxSalary());
            ((MyJobAdapter.JobHolder) holder).jobtype.setText(jobList.getJobType());
            ((MyJobAdapter.JobHolder) holder).experience.setText(jobList.getMinExp() +" - "+jobList.getMaxExp()+" Year");
            ((MyJobAdapter.JobHolder) holder).postby.setText(jobList.getCompanyName());
            ((MyJobAdapter.JobHolder) holder).role.setText(jobList.getRole());
            ((MyJobAdapter.JobHolder) holder).bind(jobList,onItemClickListner);
            ((JobHolder) holder).apply.setText("Delete");



            ((JobHolder) holder).apply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(context)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Are you sure you want to Delete Job ")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    deleteJob(jobList.getId());
                                }
                            })
                            .setNegativeButton("No", null)
                            .show();
                }
            });

            if(jobList.getCount()>0) {
                ((JobHolder) holder).countLinear.setVisibility(View.VISIBLE);
                ((JobHolder) holder).count.setText("Job Applications " + String.valueOf(jobList.getCount()));
            }else{
                ((JobHolder) holder).countLinear.setVisibility(View.GONE);
            }

            ((JobHolder) holder).countLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(jobList.getCount()!=0){
                        Intent intent=new Intent(context, JobApplicationsActivity.class);
                        intent.putExtra(Constants.JOBID,jobList.getId());
                        context.startActivity(intent);
                    }
                }
            });

            if(jobList.getStatus().equals("0")) {
                ((JobHolder) holder).view.setText("Active");
                ((JobHolder) holder).view.setBackgroundColor(Color.GREEN);
            }else{
                ((JobHolder) holder).view.setText("Deactive");
                ((JobHolder) holder).view.setBackgroundColor(Color.RED);
            }

            ((JobHolder) holder).linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, PostJobsActivity.class);
                    intent.putExtra(Constants.JOBID,jobList.getId());
                    intent.putExtra(Constants.JOB,true);
                    context.startActivity(intent);
                    context.finish();
                }
            });
            ((JobHolder) holder).linearLayout1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, PostJobsActivity.class);
                    intent.putExtra(Constants.JOBID,jobList.getId());
                    intent.putExtra(Constants.JOB,true);
                    context.startActivity(intent);
                    context.finish();
                }
            });


            ((JobHolder) holder).view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(jobList.getStatus().equals("1")) {
                        new AlertDialog.Builder(context)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Are you sure you want to de De Activate Job ")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        updateStatus(jobList.getId(), jobList.getStatus());
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                    }else{
                        new AlertDialog.Builder(context)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Are you sure you want to Activate Job ")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        updateStatus(jobList.getId(), jobList.getStatus());
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                    }
                }
            });


        }

    }

    public void updateStatus(String jobId,String status){
        HashMap<String,String> hashMap=new HashMap<>();

        if(status.equals("0")){
            hashMap.put("status","1");
        }else{
            hashMap.put("status","0");
        }
        hashMap.put("jobid",jobId);
        Call<Response> call= RestClient.get().updateJobStatus(SharedPreferenceHelper.getInstance(context).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @TargetApi(Build.VERSION_CODES.M)
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull retrofit2.Response<Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        Intent intent=new Intent(context, MyJobListActivity.class);
                        context.startActivity(intent);
                        context.finish();
                    }else{
                        View parentLayout =context.findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(context.getColor(android.R.color.holo_red_light ))
                                .show();
                    }

                }else if(response.code()==401){
                    AppUtils.logout(context);
                }else{
                    View parentLayout =context.findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(context.getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @TargetApi(Build.VERSION_CODES.M)
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout =context.findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(context.getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }



    public void deleteJob(String jobId){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("jobid",jobId);
        Call<Response> call= RestClient.get().deleteJob(SharedPreferenceHelper.getInstance(context).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull retrofit2.Response<Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        Intent intent=new Intent(context, MyJobActivity.class);
                        context.startActivity(intent);
                        context.finish();
                    }else{

                    }

                }else if(response.code()==401){
                    AppUtils.logout(context);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }



    public interface OnItemClickListner {
        void onItemClick(JobList d, View view) throws ParseException;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class JobHolder extends RecyclerView.ViewHolder {
        TextView position,location,time,salary,jobtype,experience,postby,role,view,apply,count;
        LinearLayout linearLayout,linearLayout1,countLinear;
        JobHolder(View v) {
            super(v);
            position=v.findViewById(R.id.position);
            location=v.findViewById(R.id.location);
            time=v.findViewById(R.id.time);
            salary=v.findViewById(R.id.salary);
            jobtype=v.findViewById(R.id.jobtype);
            experience=v.findViewById(R.id.experience);
            postby=v.findViewById(R.id.postby);
            role=v.findViewById(R.id.role);
            view=v.findViewById(R.id.view);
            apply=v.findViewById(R.id.apply);
            linearLayout=v.findViewById(R.id.linear);
            linearLayout1=v.findViewById(R.id.linear2);
            count=v.findViewById(R.id.count);
            countLinear=v.findViewById(R.id.countlinear);
        }
        public void bind(final JobList data, final MyJobAdapter.OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
