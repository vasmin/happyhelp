package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.donation.DonateEnquiryActivity;
import in.happyhelp.limra.activity.donation.DonationDetailsActivity;
import in.happyhelp.limra.activity.property.PropertyDetailsActivity;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.notificationresponse.Notification;
import in.happyhelp.limra.activity.service.MyServiceDetailsActivity;
import in.happyhelp.limra.activity.service.OrderDetailsActivity;
import in.happyhelp.limra.activity.service.ServiceEnquiryActivity;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import retrofit2.Call;
import retrofit2.Callback;

public class NotificationAdapter extends RecyclerView.Adapter {

    Context context;
    List<Notification> list=new ArrayList<>();
    LayoutInflater layoutInflater;


    public NotificationAdapter(Context context) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setData(List<Notification> list){
        this.list=list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_notification, viewGroup, false);
        return new NotificationAdapter.NotificationHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
            final Notification notification=list.get(i);

            if(holder instanceof NotificationHolder){
                ((NotificationHolder) holder).title.setText(notification.getTitle());
                ((NotificationHolder) holder).details.setText(notification.getDescription());
                ((NotificationHolder) holder).date.setText(notification.getRegDate()+" "+notification.getRegTime());


                if(notification.getReadBy().equals("0")){
                    ((NotificationHolder) holder).cardView.setCardBackgroundColor(Color.GRAY);
                }else{
                    ((NotificationHolder) holder).cardView.setCardBackgroundColor(Color.WHITE);
                }


                ((NotificationHolder) holder).cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(notification.getComponent().equals("newserviceenquiry")){
                            Intent intent=new Intent(context, MyServiceDetailsActivity.class);
                            intent.putExtra("id",notification.getTypeId());
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                            readNotification(notification.getId());

                        }else if(notification.getComponent().equals("newdonateenquiry")){
                            Intent intent=new Intent(context, DonationDetailsActivity.class);
                            intent.putExtra(Constants.DID,notification.getTypeId());
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                            readNotification(notification.getId());
                        }else if(notification.getComponent().equals("service_order_detail")){
                            Intent intent=new Intent(context, OrderDetailsActivity.class);
                            intent.putExtra(Constants.ORDERID,notification.getTypeId());
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                            readNotification(notification.getId());
                        }
                    }
                });
            }
    }


    public void readNotification(String notificationId){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(context).getString(Constants.USERID));
        hashMap.put("notification_id",notificationId);
        Call<Response> call= RestClient.get().update_notification_read_by(SharedPreferenceHelper.getInstance(context).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
             if(response.code()==401){
                    AppUtils.logout(context);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size() ;
    }

    public class NotificationHolder extends RecyclerView.ViewHolder {
        TextView title,details,date;
        CardView cardView;
        public NotificationHolder(View v) {
            super(v);
            title=v.findViewById(R.id.title);
            details=v.findViewById(R.id.details);
            date=v.findViewById(R.id.date);
            cardView=v.findViewById(R.id.card);
        }
    }
}
