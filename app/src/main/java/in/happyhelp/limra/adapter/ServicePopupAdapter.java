package in.happyhelp.limra.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.ParseException;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.model.ServiceModel;
import in.happyhelp.limra.network.RestClient;

public class ServicePopupAdapter extends RecyclerView.Adapter {
    RadioGroup lastCheckedRadioGroup = null;
    Context context;
    LayoutInflater layoutInflater;
    OnItemClickListner onItemClickListner;
    List<ServiceModel> serviceModels;



    public ServicePopupAdapter(Context context,OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }

    public void setData(List<ServiceModel> serviceModels){
        this.serviceModels=serviceModels;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_service_pop, viewGroup, false);
        return new ServicePopHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int i) {

        final ServiceModel model=serviceModels.get(i);


        if(holder instanceof ServicePopHolder){
           /* RadioButton rb = new RadioButton(ServicePopupAdapter.this.context);

            rb.setText(model.getServiceName());
            rb.setId(Integer.parseInt(model.getId()));
            ((ServicePopHolder) holder).radioGroup.addView(rb);
            ((ServicePopHolder) holder).bind(model,onItemClickListner);

            ((ServicePopHolder) holder).radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (lastCheckedRadioGroup != null
                            && lastCheckedRadioGroup.getCheckedRadioButtonId()
                            != radioGroup.getCheckedRadioButtonId()
                            && lastCheckedRadioGroup.getCheckedRadioButtonId() != -1) {
                        lastCheckedRadioGroup.clearCheck();

                        RadioButton radioButton =  radioGroup.findViewById(i);
                        String text = radioButton.getText().toString();
                        Toast.makeText(ServicePopupAdapter.this.context,
                                "Radio button clicked " + radioGroup.getCheckedRadioButtonId()+text,
                                Toast.LENGTH_SHORT).show();


                    }
                    lastCheckedRadioGroup = radioGroup;

                }
            });*/


           if(model.getImage()!=null)
           if(!model.getImage().equals("")) {


               RequestOptions requestOptions = new RequestOptions();
               requestOptions.placeholder(R.drawable.logo);
               requestOptions.error(R.drawable.logo);

               Glide.with(context)
                       .setDefaultRequestOptions(requestOptions)
                       .load(RestClient.base_image_url + model.getImage())
                       .into(((ServicePopHolder) holder).imageView);

           }

            ((ServicePopHolder) holder).textView.setText(model.getServiceName());
            ((ServicePopHolder) holder).bind(model,onItemClickListner);
         }
        }


    public interface OnItemClickListner {
        void onItemClick(ServiceModel d, View view) throws ParseException;
    }

    @Override
    public int getItemCount() {

        return serviceModels.size();
    }

    public class ServicePopHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView imageView;
         ServicePopHolder(View v) {
            super(v);
            textView=v.findViewById(R.id.ans);
            imageView=v.findViewById(R.id.icon);


        }

        public void bind(final ServiceModel data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
