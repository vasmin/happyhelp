package in.happyhelp.limra.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import customfont.MyTextView;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.propertiesresponse.PropertyList;
import in.happyhelp.limra.network.RestClient;

public class AllPropertyAdapter extends RecyclerView.Adapter{
    LayoutInflater layoutInflater;
    List<PropertyList> list=new ArrayList<>();
    Context context;
    private OnItemClickListner listner;

    public AllPropertyAdapter(Context context, OnItemClickListner listner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.listner=listner;
    }

    public void setData(List<PropertyList> list){
       this.list=list;
       notifyDataSetChanged();
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.item_property, parent, false);
        return new AllPropertyAdapter.PropertyHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {

        final PropertyList propertiesDetail=list.get(position);

        if(holder instanceof PropertyHolder){

            if(propertiesDetail.getImages().size()>0) {
                Glide.with(context)
                        .load(RestClient.base_image_url + propertiesDetail.getImages().get(0))
                        .thumbnail(0.5f)
                        .into(((PropertyHolder) holder).imageView);
            }

            ((PropertyHolder)holder).share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    sendIntent.putExtra(Intent.EXTRA_TEXT,"Client Name :"+propertiesDetail.getAddress()+
                            " Mobile Number :"+propertiesDetail.getMobile()+
                            " Address "+ propertiesDetail.getCity());
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);*/


                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    sendIntent.putExtra(Intent.EXTRA_TEXT,
                            "Hey check out Property at: "+RestClient.PROPERTY_BASE_URL+"propertydetails?propertyId="+propertiesDetail.getId());
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);
                }
            });

            ((PropertyHolder) holder).location.setText(propertiesDetail.getCity());
            ((PropertyHolder) holder).name.setText(propertiesDetail.getPropertyName());
            ((PropertyHolder) holder).price.setText("\u20B9 "+propertiesDetail.getOfferPrice());
            ((PropertyHolder) holder).area.setText(propertiesDetail.getBuiltArea()+" Square/ft");

            if(propertiesDetail.getBedroom().equals("0")) {
                ((PropertyHolder) holder).roomType.setText("1 RK");
            }else if(propertiesDetail.getBedroom().equals("1")) {
                ((PropertyHolder) holder).roomType.setText("1 BHK");
            }else if(propertiesDetail.getBedroom().equals("2")) {
                ((PropertyHolder) holder).roomType.setText("2 BHK");
            }else if(propertiesDetail.getBedroom().equals("3")) {
                ((PropertyHolder) holder).roomType.setText("3 BHK");
            }else if(propertiesDetail.getBedroom().equals("4")) {
                ((PropertyHolder) holder).roomType.setText("4 BHK");
            }else{
                ((PropertyHolder) holder).roomType.setText("1 RK");
            }

            ((PropertyHolder) holder).propertyType.setText(propertiesDetail.getPro_type());
            ((PropertyHolder) holder).dealType.setText(propertiesDetail.getDeal_type());

            ((PropertyHolder) holder).bind(propertiesDetail,listner);
            ((PropertyHolder) holder).address.setText(propertiesDetail.getAddress());
            ((PropertyHolder) holder).date.setText(propertiesDetail.getPostedDate());

            ((PropertyHolder) holder).call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + propertiesDetail.getMobile()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);


                }
            });
        }
    }

    public interface OnItemClickListner {
        void onItemClick(PropertyList d, View view) throws ParseException;

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    static class PropertyHolder extends RecyclerView.ViewHolder {

        MyTextView price,location,roomType,area,address,date,name,propertyType,dealType;
        ImageView imageView,call,share;



        public PropertyHolder(View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            price=itemView.findViewById(R.id.price);
            location=itemView.findViewById(R.id.location);
            roomType=itemView.findViewById(R.id.roomtype);
            area=itemView.findViewById(R.id.area);
            call=itemView.findViewById(R.id.call);
            share=itemView.findViewById(R.id.share);
            imageView=itemView.findViewById(R.id.imageView);
            address=itemView.findViewById(R.id.address);
            date=itemView.findViewById(R.id.date);
            propertyType=itemView.findViewById(R.id.propertytype);
            dealType=itemView.findViewById(R.id.dealtype);
        }
        public void bind(final PropertyList data, final AllPropertyAdapter.OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
