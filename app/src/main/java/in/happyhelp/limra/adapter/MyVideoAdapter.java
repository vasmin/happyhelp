package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.patient.MyPatientDetailActivity;
import in.happyhelp.limra.activity.response.myvideolistresponse.Datum;
import in.happyhelp.limra.activity.video.MyVideoEditActivity;
import in.happyhelp.limra.network.RestClient;

public class MyVideoAdapter  extends RecyclerView.Adapter {
    LayoutInflater layoutInflater;
    Context context;
    List<Datum> donateList=new ArrayList<>();

    public MyVideoAdapter(Context context) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setData(List<Datum> donateList){
        this.donateList=donateList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_dolationlist, viewGroup, false);
        return new MyVideoAdapter.MyVideoHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        final Datum video=donateList.get(i);

        if(holder instanceof MyVideoAdapter.MyVideoHolder){

            ((MyVideoHolder) holder).name.setText(video.getName());
            ((MyVideoHolder) holder).date.setText(video.getDate());
            ((MyVideoHolder) holder).service.setText("Expiry Date "+video.getPlanexpire());

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.isMemoryCacheable();
            Glide.with(context).setDefaultRequestOptions(requestOptions).load(getYoutubeThumbnailUrlFromVideoUrl(video.getYoutubeCode())).into(((MyVideoHolder) holder).imageView);

           ((MyVideoHolder) holder).date.setText(video.getDate());
           ((MyVideoHolder) holder).address.setText("Plan Amount "+video.getPlanamount());
           ((MyVideoHolder) holder).view.setVisibility(View.GONE);

            ((MyVideoHolder) holder).view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, MyVideoEditActivity.class);
                    intent.putExtra(Constants.VID,video.getId());
                    context.startActivity(intent);
                }
            });
        }
    }

    private static String getYoutubeThumbnailUrlFromVideoUrl(String videoUrl) {
        return "http://img.youtube.com/vi/"+getYoutubeVideoIdFromUrl(videoUrl) + "/0.jpg";
    }

    private static String getYoutubeVideoIdFromUrl(String inUrl) {
        inUrl = inUrl.replace("&feature=youtu.be", "");
        if (inUrl.toLowerCase().contains("youtu.be")) {
            return inUrl.substring(inUrl.lastIndexOf("/") + 1);
        }
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(inUrl);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return donateList.size();
    }

    public class MyVideoHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name,service,date,address,view;
        MyVideoHolder(View v) {
            super(v);
            imageView=v.findViewById(R.id.image);
            name=v.findViewById(R.id.name);
            service=v.findViewById(R.id.service);
            date=v.findViewById(R.id.date);
            address=v.findViewById(R.id.address);
            view=v.findViewById(R.id.view);
        }
    }
}

