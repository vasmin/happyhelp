package in.happyhelp.limra.adapter.shopping;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.shopping.WishList;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shopping.HomeShoppingActivity;
import in.happyhelp.limra.shopping.ProductDetailsActivity;
import in.happyhelp.limra.shoppingresponse.productresponse.Product;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmModel;
import retrofit2.Call;
import retrofit2.Callback;

public class ProductAdapter extends RecyclerView.Adapter {

    Context context;
    LayoutInflater layoutInflater;
    OnItemClickListner onItemClickListner;
    List<Product> list=new ArrayList<>();

    public ProductAdapter(Context context,OnItemClickListner onItemClickListner)
    {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;

    }

    public void setData(List<Product> list){
        this.list=list;
        notifyDataSetChanged();
    }

    public interface OnItemClickListner {
        void onItemClick(Product d, View view) throws ParseException;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_product, viewGroup, false);
        return new ProductAdapter.ProductHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int i) {

        final Product product=list.get(i);

        if(holder instanceof ProductHolder){
            Glide.with(context).load(RestClient.base_image_url+product.getImage())
                    .into(((ProductHolder) holder).imageView);
            ((ProductHolder) holder).name.setText(product.getName());
            ((ProductHolder) holder).discountPrice.setText("\u20B9 "+product.getDiscountprice());
            ((ProductHolder) holder).price.setText(product.getPrice());
            ((ProductHolder) holder).price.setText((Html.fromHtml("<strike>"+"\u20B9 "+product.getPrice()+"</strike>")));
            ((ProductHolder) holder).price.setPaintFlags(((ProductHolder) holder).price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


           // textview.setText((Html.fromHtml("<strike>hello world!</strike>")));

            ((ProductHolder) holder).linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context,ProductDetailsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.PID,list.get(i).getId());
                    context.startActivity(intent);
                }
            });

         /*   ((ProductHolder) holder).like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(list.get(i).getWishlist().equals("1")){
                        removefromWishLits(list.get(i).getId());
                    }else{
                        addWhishlist(d);
                    }
                }
            });*/

            if(product.getWishlist().equals("1")){
                ((ProductHolder) holder).like.setImageDrawable(context.getDrawable(R.drawable.like));
            }else{
                ((ProductHolder) holder).like.setImageDrawable(context.getDrawable(R.drawable.dislike));
            }

            ((ProductHolder) holder).like_linear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "Click on like image", Toast.LENGTH_SHORT).show();
                    ((ProductHolder) holder).like.setImageDrawable(context.getDrawable(R.drawable.like));

                }
            });

           /* Realm realm= RealmHelper.getRealmInstance();
            final WishList wishList=realm.where(WishList.class).equalTo("pid",product.getId()).findFirst();
            if(wishList!=null)
            if(wishList.isValid()&& wishList.isLoaded()){
                ((ProductHolder) holder).like.setImageDrawable(context.getDrawable(R.drawable.like));
            }else{
                ((ProductHolder) holder).like.setImageDrawable(context.getDrawable(R.drawable.dislike));
            }*/



           /* ((ProductHolder) holder).like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int i=0;i<list.size();i++){
                        if(list.contains(product)){
                            if(!product.isIsfav()){
                                product.setIsfav(true);
                                addWhishlist(product);
                            }
                        }
                    }
                    notifyDataSetChanged();
                }
            });*/


           // ((ProductHolder) holder).bind(product,onItemClickListner);

          /*  ((ProductHolder) holder).like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        onItemClickListner.onItemClick(product,v);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });*/


        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ProductHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView imageView;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.like)
        ImageView like;


        @BindView(R.id.price)
        TextView price;

        @BindView(R.id.discountprice)
        TextView discountPrice;

        @BindView(R.id.linear)
        LinearLayout linearLayout;

        @BindView(R.id.like_linear)
        LinearLayout like_linear;



        public ProductHolder(View v) {
            super(v);
            //v.setOnClickListener(this);
            ButterKnife.bind(this,v);
        }


/*
        public void bind(final Product data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
*/

       /* @Override
        public void onClick(View v) {

            String
            Intent intent=new Intent(context,ProductDetailsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Constants.PID, String.valueOf(list.get(getPosition())));
            context.startActivity(intent);


        }*/
    }
}
