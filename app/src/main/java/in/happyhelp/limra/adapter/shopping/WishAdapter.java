package in.happyhelp.limra.adapter.shopping;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.R;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.productresponse.Product;
import in.happyhelp.limra.shoppingresponse.wishlistresponse.Wishlist;

public class WishAdapter extends RecyclerView.Adapter {

    Context context;
    LayoutInflater layoutInflater;
    OnItemClickListner onItemClickListner;
    List<Wishlist> list=new ArrayList<>();

    public WishAdapter(Context context, OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;

    }

    public void setData(List<Wishlist> list){
        this.list=list;
        notifyDataSetChanged();
    }

    public interface OnItemClickListner {
        void onItemClick(Wishlist d, View view) throws ParseException;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_wish, viewGroup, false);
        return new WishAdapter.WishHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        Wishlist product=list.get(i);

        if(holder instanceof WishHolder){
            Glide.with(context)
                    .load(RestClient.base_image_url+product.getImage())
                    .into(((WishHolder) holder).imageView);
            ((WishHolder) holder).name.setText(product.getName());
            ((WishHolder) holder).price.setText("\u20B9 "+product.getPrice());
            ((WishHolder) holder).discountprice.setText("\u20B9 "+product.getDiscountprice());


            ((WishHolder) holder).discountprice.setText("\u20B9 "+product.getDiscountprice());

            ((WishHolder) holder).price.setText((Html.fromHtml("<strike>"+"\u20B9 "+product.getPrice()+"</strike>")));


            ((WishHolder) holder).bind(product,onItemClickListner);

        }

    }




    @Override
    public int getItemCount() {
        return list.size();
    }

    public class WishHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView imageView;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.remove)
        ImageView remove;


        @BindView(R.id.price)
        TextView price;

        @BindView(R.id.discountprice)
        TextView discountprice;

        public WishHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }


        public void bind(final Wishlist data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
