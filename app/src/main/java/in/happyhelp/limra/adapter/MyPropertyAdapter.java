package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import customfont.MyTextView;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.Jobs.MyJobListActivity;
import in.happyhelp.limra.activity.property.MyPropertyActivity;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.propertiesresponse.PropertyList;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import retrofit2.Call;
import retrofit2.Callback;

public class MyPropertyAdapter extends RecyclerView.Adapter{
    LayoutInflater layoutInflater;
    List<PropertyList> list=new ArrayList<>();
    Activity context;
    private OnItemClickListner listner;

    public MyPropertyAdapter(Activity context, MyPropertyAdapter.OnItemClickListner listner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.listner=listner;
    }

    public void setData(List<PropertyList> list){
        this.list=list;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.item_my_property, parent, false);
        return new MyPropertyAdapter.PropertyHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {

        final PropertyList propertiesDetail=list.get(position);

        if(holder instanceof MyPropertyAdapter.PropertyHolder){

            if(propertiesDetail.getImages().size()>0) {
                Glide.with(context)
                        .load(RestClient.base_image_url + propertiesDetail.getImages().get(0))
                        .thumbnail(0.5f)
                        .into(((PropertyHolder) holder).imageView);
            }

          /*  ((PropertyHolder)holder).share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT,"Client Name :"+propertiesDetail.getAddress()+
                            " Mobile Number :"+propertiesDetail.getMobile()+
                            " Address "+ propertiesDetail.getCity());
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);
                }
            });*/

            ((PropertyHolder) holder).location.setText(propertiesDetail.getCity());
            ((PropertyHolder) holder).name.setText(propertiesDetail.getPropertyName());
            ((PropertyHolder) holder).price.setText("\u20B9 "+propertiesDetail.getOfferPrice());
            ((PropertyHolder) holder).area.setText(propertiesDetail.getBuiltArea()+" Square/ft");
            ((PropertyHolder) holder).roomType.setText(propertiesDetail.getBedroom()+" BHK");
            ((PropertyHolder) holder).bind(propertiesDetail,listner);
            ((PropertyHolder) holder).address.setText(propertiesDetail.getAddress());
            ((PropertyHolder) holder).date.setText(propertiesDetail.getPostedDate());

            ((PropertyHolder) holder).dealType.setText(propertiesDetail.getDeal_type());
            ((PropertyHolder) holder).propertyType.setText(propertiesDetail.getPro_type());

            ((PropertyHolder) holder).active.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(propertiesDetail.getStatus().equals("1")){
                        new AlertDialog.Builder(context)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Are you sure you want to  De Activate Property ")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        updateStatus(propertiesDetail.getId(), propertiesDetail.getStatus());
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();

                    }else{
                        new AlertDialog.Builder(context)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Are you sure you want to  Activate Property ")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        updateStatus(propertiesDetail.getId(), propertiesDetail.getStatus());
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                    }
                }
            });


            if(propertiesDetail.getStatus().equals("1")){
                ((PropertyHolder) holder).active.setBackground(context.getDrawable(R.color.colorPrimary));
                ((PropertyHolder) holder).active.setText("De-Active");


            }else{
                ((PropertyHolder) holder).active.setBackground(context.getDrawable(R.color.green));
                ((PropertyHolder) holder).active.setText("Active");

            }

           /* ((PropertyHolder) holder).call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + propertiesDetail.getMobile()));
                    context.startActivity(intent);
                }
            });*/
        }
    }


    public void updateStatus(String propertyId,String status){
        HashMap<String,String> hashMap=new HashMap<>();

        if(status.equals("0")){
            hashMap.put("status","1");
        }else{
            hashMap.put("status","0");
        }
        hashMap.put("propertyid",propertyId);
        Call<Response> call= RestClient.get(RestClient.PROPERTY_BASE_URL).updatePropertyStatus(SharedPreferenceHelper.getInstance(context).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @TargetApi(Build.VERSION_CODES.M)
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull retrofit2.Response<Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        Intent intent=new Intent(context, MyPropertyActivity.class);
                        context.startActivity(intent);
                        context.finish();
                    }else{
                        View parentLayout =context.findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(context.getColor(android.R.color.holo_red_light ))
                                .show();
                    }

                }else if(response.code()==401){
                    AppUtils.logout(context);
                }else{
                    View parentLayout =context.findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(context.getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @TargetApi(Build.VERSION_CODES.M)
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout =context.findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(context.getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public interface OnItemClickListner {
        void onItemClick(PropertyList d, View view) throws ParseException;

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    static class PropertyHolder extends RecyclerView.ViewHolder {

        TextView price,location,roomType,area,address,date,name,propertyType,dealType,active,delete;
        ImageView imageView,call,share;



        public PropertyHolder(View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            price=itemView.findViewById(R.id.price);
            location=itemView.findViewById(R.id.location);
            roomType=itemView.findViewById(R.id.roomtype);
            area=itemView.findViewById(R.id.area);
            call=itemView.findViewById(R.id.call);
            share=itemView.findViewById(R.id.share);
            imageView=itemView.findViewById(R.id.imageView);
            address=itemView.findViewById(R.id.address);
            date=itemView.findViewById(R.id.date);
            delete=itemView.findViewById(R.id.delete);
            propertyType=itemView.findViewById(R.id.propertytype);
            dealType=itemView.findViewById(R.id.dealtype);
            active=itemView.findViewById(R.id.active);
        }
        public void bind(final PropertyList data, final MyPropertyAdapter.OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
