package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.joblistresponse.JobList;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import retrofit2.Call;
import retrofit2.Callback;

public class JobDetailAdapter extends RecyclerView.Adapter {

    LayoutInflater layoutInflater;
    OnItemClickListner onItemClickListner;
    List<JobList> list;
    Context context;

    public JobDetailAdapter(Context context, OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }

    public void setData(List<JobList> list){
        this.list=list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_job_detail, viewGroup, false);
        return new JobDetailAdapter.JobHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        JobList jobList=list.get(i);

        if(holder instanceof JobHolder){
            ((JobHolder) holder).position.setText(jobList.getTitle());
            ((JobHolder) holder).location.setText(jobList.getLocation());
            ((JobHolder) holder).time.setText(jobList.getDate());
            ((JobHolder) holder).salary.setText(jobList.getMinSalary() +" Lakh"+" - "+jobList.getMaxSalary()+" Lakh");
            ((JobHolder) holder).jobtype.setText(jobList.getJobType());
            ((JobHolder) holder).experience.setText(jobList.getMinExp() +" - "+jobList.getMaxExp()+" Year");
            ((JobHolder) holder).postby.setText(jobList.getCompanyName());
            ((JobHolder) holder).role.setText(jobList.getRole());
            ((JobHolder) holder).bind(jobList,onItemClickListner);

            if(jobList.isApplied()){
                ((JobHolder) holder).apply.setVisibility(View.GONE);
            }else{
                ((JobHolder) holder).apply.setVisibility(View.VISIBLE);
            }
        }
    }





    public interface OnItemClickListner {
        void onItemClick(JobList d, View view) throws ParseException;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class JobHolder extends RecyclerView.ViewHolder {
        TextView position,location,time,salary,jobtype,experience,postby,role,view,apply;
        public JobHolder(View v) {
            super(v);
            position=v.findViewById(R.id.position);
            location=v.findViewById(R.id.location);
            time=v.findViewById(R.id.time);
            salary=v.findViewById(R.id.salary);
            jobtype=v.findViewById(R.id.jobtype);
            experience=v.findViewById(R.id.experience);
            postby=v.findViewById(R.id.postby);
            role=v.findViewById(R.id.role);
            view=v.findViewById(R.id.view);
            apply=v.findViewById(R.id.apply);
        }
        public void bind(final JobList data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
