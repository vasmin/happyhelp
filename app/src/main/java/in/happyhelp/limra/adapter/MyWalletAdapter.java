package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.mywallet.Wallet;
import in.happyhelp.limra.adapter.shopping.MyProductAdapter;
import in.happyhelp.limra.shoppingresponse.orderdetailresponse.Product;
import io.realm.RealmList;

public class MyWalletAdapter extends RecyclerView.Adapter {

    Context context;
    RealmList<Wallet> list=new RealmList<>();
    LayoutInflater layoutInflater;
    OnItemClickListner mlistner;

    public MyWalletAdapter(Context context,OnItemClickListner mlistner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.mlistner=mlistner;
    }

    public void setData(RealmList<Wallet> list){
        this.list=list;
        notifyDataSetChanged();
    }

    public interface OnItemClickListner {
        void onItemClick(Wallet d, View view) throws ParseException;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_wallet, viewGroup, false);
        return new MyWalletAdapter.MyWalletHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        Wallet wallet=list.get(i);
        if(holder instanceof MyWalletHolder){
            ((MyWalletHolder) holder).date.setText(wallet.getDate());
            ((MyWalletHolder) holder).particular.setText(wallet.getParticular());
            ((MyWalletHolder) holder).total.setText("\u20B9 "+wallet.getTotal());
            ((MyWalletHolder) holder).credit.setText("\u20B9 "+wallet.getCredit());
            ((MyWalletHolder) holder).debit.setText("\u20B9 "+wallet.getDebit());

            ((MyWalletHolder) holder).bind(wallet,mlistner);

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyWalletHolder extends RecyclerView.ViewHolder {
        TextView date,particular,debit,credit,total;
        public MyWalletHolder(View v) {
            super(v);
            date=v.findViewById(R.id.date);
            particular=v.findViewById(R.id.particular);
            debit=v.findViewById(R.id.debit);
            credit=v.findViewById(R.id.credit);
            total=v.findViewById(R.id.total);
        }

        public void bind(final Wallet data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
