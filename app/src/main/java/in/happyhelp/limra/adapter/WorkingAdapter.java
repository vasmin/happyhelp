package in.happyhelp.limra.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.companydetailsresponse.WorkingHour;

public class WorkingAdapter extends RecyclerView.Adapter {

    List<WorkingHour> list=new ArrayList<>();
    LayoutInflater layoutInflater;
    Context context;
    public WorkingAdapter(Context context){
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public  void setData(List<WorkingHour> list){
        this.list=list;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_working
                , viewGroup, false);
        return new WorkingAdapter.WorkHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        WorkingHour workingHour=list.get(i);
                if(holder instanceof WorkHolder){
                    ((WorkHolder) holder).time.setText(workingHour.getTime());
                    ((WorkHolder) holder).day.setText(workingHour.getDays());
                }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class WorkHolder extends RecyclerView.ViewHolder {
        TextView day,time;
        public WorkHolder(View v) {
            super(v);
            day=v.findViewById(R.id.day);
            time=v.findViewById(R.id.time);

        }
    }
}
