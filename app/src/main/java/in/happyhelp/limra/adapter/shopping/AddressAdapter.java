package in.happyhelp.limra.adapter.shopping;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import in.happyhelp.limra.R;
import in.happyhelp.limra.shoppingresponse.addressresponse.Address;

public class AddressAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    List<Address> addressList = new ArrayList<>();
    List<String> listIDChoose;
    List<String> listIDRemove;

    private static CheckBox lastChecked = null;
    private static int lastCheckedPos = 0;

    public AddressAdapter(Context context, List<String> listIDChoose, List<String> listIDRemove) {
        this.context = context;
        this.listIDChoose = listIDChoose;
        this.listIDRemove = listIDRemove;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setdata(List<Address> addressList) {
        this.addressList = addressList;
        Log.e("data", String.valueOf(addressList.size()));
        notifyDataSetChanged();
    }

    @Override
    public ItemAddressHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.itemaddress, parent, false);
        return new AddressAdapter.ItemAddressHolder(v);

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemAddressHolder) {


            Address address = addressList.get(position);
            String addr = " ";
            ((ItemAddressHolder) holder).checkBox.setTag(position);

            if (address != null) {

                addr =  address.getCity() + ", " + address.getAddress1() + ", " + address.getAddress2() + ", " + address.getPincode();
                ((ItemAddressHolder) holder).address.setText(addr);
                final String id = String.valueOf(address.getId());




                //for default check in first item
                if (position == 0 && addressList.get(0).isSelected() && ((ItemAddressHolder) holder).checkBox.isChecked()) {
                    lastChecked = ((ItemAddressHolder) holder).checkBox;
                    lastCheckedPos = 0;
                }

                ((ItemAddressHolder) holder).relativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ((ItemAddressHolder) holder).checkBox.setChecked(true);


                        CheckBox cb = (CheckBox) ((ItemAddressHolder) holder).checkBox;


                        int clickedPos = (Integer) cb.getTag();

                        if (cb.isChecked()) {
                            if (lastChecked != null) {
                                lastChecked.setChecked(false);
                                addressList.get(lastCheckedPos).setSelected(false);
                            }

                            lastChecked = cb;
                            lastCheckedPos = clickedPos;
                        } else
                            lastChecked = null;

                        addressList.get(clickedPos).setSelected(cb.isChecked());

                    }
                });
                ((ItemAddressHolder) holder).checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("CLICK","click");
                        CheckBox cb = (CheckBox) v;
                        int clickedPos = (Integer) cb.getTag();

                        if (cb.isChecked()) {
                            if (lastChecked != null) {
                                lastChecked.setChecked(false);
                                addressList.get(lastCheckedPos).setSelected(false);
                            }

                            lastChecked = cb;
                            lastCheckedPos = clickedPos;
                        } else
                            lastChecked = null;

                        addressList.get(clickedPos).setSelected(cb.isChecked());
                    }
                });

                ((ItemAddressHolder) holder).checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {


                        if (b) {
                            listIDChoose.add(id);
                            listIDRemove.remove(id);
                        } else {
                            listIDRemove.add(id);
                            listIDChoose.remove(id);
                        }

                    }
                });
                ((ItemAddressHolder) holder).name.setText(address.getName());
            }
        }
    }

    @Override
    public int getItemCount() {
        return addressList.size();
    }

    static class ItemAddressHolder extends RecyclerView.ViewHolder {
        TextView address, name;
        public CheckBox checkBox;
        RelativeLayout relativeLayout;

        public ItemAddressHolder(View itemView) {
            super(itemView);
            address = itemView.findViewById(R.id.address);
            name = itemView.findViewById(R.id.name);
            checkBox = itemView.findViewById(R.id.checkbox);
            relativeLayout=itemView.findViewById(R.id.relative);
        }

       /* public void bind(final Category data, final CategoryListAdpter.OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }*/
    }
}
