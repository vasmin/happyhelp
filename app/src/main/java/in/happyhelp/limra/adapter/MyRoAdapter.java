package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.ro.MyRoDetailActivity;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.myroresponse.MyRo;

public class MyRoAdapter extends RecyclerView.Adapter {

    Context context;
    LayoutInflater layoutInflater;
    List<MyRo> list=new ArrayList<>();

    public MyRoAdapter(Context context) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setData(List<MyRo> list){
        this.list=list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_myro, viewGroup, false);
        return new MyRoAdapter.RoHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        final MyRo myRo=list.get(i);
        if(holder instanceof RoHolder){

            ((RoHolder) holder).name.setText(myRo.getRoname());
            ((RoHolder) holder).deposit.setText(myRo.getDeposit());
            ((RoHolder) holder).rent.setText("\u20B9 "+myRo.getRent());
            ((RoHolder) holder).startdate.setText(myRo.getDate());

            if(myRo.getMonth().equals("0")){

                ((RoHolder) holder).deposit.setText(myRo.getBuy_amount());
                ((RoHolder) holder).rent.setVisibility(View.GONE);
            }

            Glide.with(context)
                    .load(RestClient.base_image_url+myRo.getImage())
                    .into(((RoHolder) holder).imageView);

            ((RoHolder) holder).cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!myRo.getMonth().equals("0")) {
                        Intent intent = new Intent(context, MyRoDetailActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(Constants.MYRO, myRo.getId());
                        context.startActivity(intent);
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    private class RoHolder extends RecyclerView.ViewHolder {
        TextView name,startdate,rent,deposit;
        ImageView imageView;
        CardView cardView;
        RoHolder(View v) {
            super(v);
            cardView=v.findViewById(R.id.card);
            name=v.findViewById(R.id.name);
            startdate=v.findViewById(R.id.startdate);
            rent=v.findViewById(R.id.rent);
            deposit=v.findViewById(R.id.deposit);
            imageView=v.findViewById(R.id.image);
        }


    }
}
