package in.happyhelp.limra.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.donation.DonationDetailsActivity;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.donatelist.Donate;

public class DonationAdapter extends RecyclerView.Adapter {

    LayoutInflater layoutInflater;
    Context context;
    List<Donate> donateList=new ArrayList<>();
    public DonationAdapter(Context context) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setData(List<Donate> donateList){
        this.donateList=donateList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_dolationlist, viewGroup, false);
        return new DonationAdapter.DonationHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        final Donate donate=donateList.get(i);

        if(holder instanceof DonationHolder){

            ((DonationHolder) holder).name.setText(donate.getName());
            Glide.with(context)
                    .load(RestClient.base_image_url+donate.getImage())
                    .into(((DonationHolder) holder).imageView);
            ((DonationHolder) holder).date.setText(donate.getDate());

            ((DonationHolder) holder).service.setText(donate.getCategory());
            ((DonationHolder) holder).address.setText(donate.getAddress());

            ((DonationHolder) holder).view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context,DonationDetailsActivity.class);
                    intent.putExtra(Constants.DID,donate.getId());
                    context.startActivity(intent);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return donateList.size();
    }

    public class DonationHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name,service,date,address,view;
        DonationHolder(View v) {
            super(v);
            imageView=v.findViewById(R.id.image);
            name=v.findViewById(R.id.name);
            service=v.findViewById(R.id.service);
            date=v.findViewById(R.id.date);
            address=v.findViewById(R.id.address);
            view=v.findViewById(R.id.view);
        }
    }
}
