package in.happyhelp.limra.adapter.shopping;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.orhanobut.dialogplus.Holder;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.R;
import in.happyhelp.limra.model.shopping.CartItem;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.orderdetailresponse.Product;

public class MyProductAdapter extends RecyclerView.Adapter {

    List<Product> list=new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;
    OnItemClickListner mlistner;

    public MyProductAdapter(Context context,OnItemClickListner mlistner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.mlistner=mlistner;
    }

    public void setData( List<Product> list){
        this.list=list;
        notifyDataSetChanged();
    }


    public interface OnItemClickListner {
        void onItemClick(Product d, View view) throws ParseException;
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_my_product, viewGroup, false);
        return new MyProductAdapter.OrderHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        Product product=list.get(i);
        if(holder instanceof OrderHolder){
            ((OrderHolder) holder).name.setText(product.getName());
            ((OrderHolder) holder).price.setText(product.getPrice());
            ((OrderHolder) holder).qty.setText(product.getQty());
            ((OrderHolder) holder).total.setText(String.valueOf(product.getTotalPrice()));

            Glide.with(context)
                    .load(RestClient.base_image_url+product.getImage())
                    .into(((OrderHolder) holder).imageView);

            if(product.getReturn_status()!=null) {
                if(product.getReturn_status()==0){
                    ((OrderHolder) holder).returnproduct.setVisibility(View.VISIBLE);
                    ((OrderHolder) holder).returnMessage.setVisibility(View.GONE);
                }else {
                    ((OrderHolder) holder).returnproduct.setVisibility(View.GONE);
                    ((OrderHolder) holder).returnMessage.setVisibility(View.VISIBLE);
                    ((OrderHolder) holder).returnMessage.setText(product.getReturnData());
                }
            }else{
                ((OrderHolder) holder).returnMessage.setVisibility(View.VISIBLE);
                ((OrderHolder) holder).returnproduct.setVisibility(View.GONE);
            }
            ((OrderHolder) holder).bind(product,mlistner);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class OrderHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.qty)
        TextView qty;

        @BindView(R.id.image)
        ImageView imageView;


        TextView price;

        @BindView(R.id.total)
        TextView total;

        @BindView(R.id.returnproduct)
        TextView returnproduct;

        @BindView(R.id.returnmessage)
        TextView returnMessage;


        public OrderHolder(View v) {
            super(v);
            price=v.findViewById(R.id.price);
            ButterKnife.bind(this,v);
        }

        public void bind(final Product data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
