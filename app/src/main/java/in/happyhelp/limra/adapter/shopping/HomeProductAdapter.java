package in.happyhelp.limra.adapter.shopping;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.model.shopping.SubCat;
import in.happyhelp.limra.model.shopping.SubSubCat;
import in.happyhelp.limra.shopping.ShopingProductActivity;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class HomeProductAdapter extends ExpandableRecyclerViewAdapter<HomeProductAdapter.ParentViewHolder, HomeProductAdapter.ChildHolder> {
    Context context;

    public HomeProductAdapter(Context context,List list) {
        super(list);
        this.context=context;
    }

    @Override
    public ParentViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_genre, parent, false);
        return new ParentViewHolder(view);
    }

    @Override
    public ChildHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_artist, parent, false);
        return new ChildHolder(view);
    }


    @Override
    public void onBindChildViewHolder(ChildHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
            final SubSubCat subSubCat = ((SubCat) group).getItems().get(childIndex);
            holder.setChildName(subSubCat.getName());
            holder.bind(subSubCat);
    }

    @Override
    public void onBindGroupViewHolder(ParentViewHolder holder, int flatPosition, ExpandableGroup group) {
                holder.setParentName(group);
              //  holder.bind(group);
    }



    class ChildHolder extends ChildViewHolder {

        private TextView childTextView;

        public ChildHolder(View itemView) {
            super(itemView);
            childTextView = (TextView) itemView.findViewById(R.id.name);
        }


        public void setChildName(String name) {
            childTextView.setText(name);
        }

        public void bind(final SubSubCat data) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, ShopingProductActivity.class);
                    intent.putExtra(Constants.SUBCAT,data.getId());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }


    class ParentViewHolder extends GroupViewHolder {
        private TextView genreName;
        private ImageView arrow;

        public ParentViewHolder(View itemView) {
            super(itemView);
            genreName = (TextView) itemView.findViewById(R.id.name);
            arrow = (ImageView) itemView.findViewById(R.id.list_item_genre_arrow);


        }

        public void bind(final ExpandableGroup genre) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, ShopingProductActivity.class);
                    intent.putExtra(Constants.SUBCAT,genre.getTitle());
                    context.startActivity(intent);
                }
            });
        }






        public void setParentName(ExpandableGroup genre) {
            if (genre instanceof SubCat) {
                genreName.setText(genre.getTitle());
            }

        }

        @Override
        public void expand() {
            animateExpand();
        }

        @Override
        public void collapse() {
            animateCollapse();
        }

        private void animateExpand() {
            RotateAnimation rotate =
                    new RotateAnimation(360, 270, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(300);
            rotate.setFillAfter(true);
            arrow.setAnimation(rotate);
        }

        private void animateCollapse() {
            RotateAnimation rotate = new RotateAnimation(270, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(300);
            rotate.setFillAfter(true);
            arrow.setAnimation(rotate);
        }
    }






}
