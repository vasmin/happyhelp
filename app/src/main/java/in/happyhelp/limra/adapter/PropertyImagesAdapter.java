package in.happyhelp.limra.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;


import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.happyhelp.limra.R;
import in.happyhelp.limra.model.PropertyImageModel;

/**
 * Created by admin on 03-05-2017.
 */

public class PropertyImagesAdapter extends RecyclerView.Adapter<PropertyImagesAdapter.ViewHolder> {
    private Context context;
    private List<PropertyImageModel> propertyImages=new ArrayList<>();
    private static final int NEW_IMAGE = 0;
    private static final int OLD_IMAGE = 1;

    private int propertyId;

    public PropertyImagesAdapter(Context context) {
        this.context = context;

    }

    public void setData( List<PropertyImageModel> propertyImages){

        this.propertyImages = propertyImages;
        notifyDataSetChanged();
    }

    @Override
    public PropertyImagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_property_image, parent, false);
        return new ViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(final PropertyImagesAdapter.ViewHolder holder, int position) {
        final PropertyImageModel model = propertyImages.get(position);
        if (holder.getItemViewType() == NEW_IMAGE) {
            holder.uploadImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //uploadImage(model.getUrl(), holder.getItemViewType(), holder.propertyImageProgress, holder.uploadImage);
                }
            });
        }
        Glide.with(this.context).load(model.getUrl()).into(holder.propertyImage);
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }


    @Override
    public int getItemCount() {
        return propertyImages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView propertyImage, uploadImage;
        ProgressBar propertyImageProgress;

        public ViewHolder(View itemView, final int viewType) {
            super(itemView);
            propertyImage = (ImageView) itemView.findViewById(R.id.property_image);
            propertyImageProgress = (ProgressBar) itemView.findViewById(R.id.property_image_progress);
            uploadImage = (ImageView) itemView.findViewById(R.id.upload_image);

            if (viewType == NEW_IMAGE) {
                propertyImageProgress.setVisibility(View.VISIBLE);
                propertyImageProgress.setMax(100);
                propertyImageProgress.setProgress(0);
                uploadImage.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return propertyImages.get(position).isNewImage() ? NEW_IMAGE : OLD_IMAGE;
    }

    public int getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(int propertyId) {
        this.propertyId = propertyId;
    }

    private void uploadImage(final String path, int viewType, final ProgressBar propertyImageProgress, final ImageView uploadImage) {
        if (propertyId == 0) {
            Toast.makeText(context, "Please create property first.", Toast.LENGTH_SHORT).show();
            return;
        }

        String url = "";//ApiConstants.UPLOAD_PROPERTY_IMAGE;
        String authKey ="";// PrefsManager.getInstance(context).getAuthKey();

        byte[] data = new byte[0];
        try {
            data = authKey.getBytes("UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String base64 = Base64.encodeToString(data, Base64.NO_WRAP);

        final String filename = getFilename(path);
/*
        try {

            MultipartUploadRequest request = new MultipartUploadRequest(context, url)
                    .addFileToUpload(path, "photo")
                    .setNotificationConfig(getNotificationConfig(filename))
                    .addParameter("property_id", String.valueOf(propertyId))
                    .addHeader("Authorization", "Basic " + base64.replace("=", "6"))
                    .setMaxRetries(3);

            String uploadId = request.setDelegate(new UploadStatusDelegate() {
                @Override
                public void onProgress(UploadInfo uploadInfo) {
                    Log.i("Add Property", String.format(Locale.getDefault(), "ID: %1$s (%2$d%%) at %3$.2f Kbit/s",
                            uploadInfo.getUploadId(), uploadInfo.getProgressPercent(),
                            uploadInfo.getUploadRate()));
                    propertyImageProgress.setProgress(uploadInfo.getProgressPercent());
                    uploadImage.setVisibility(View.GONE);
                }

                @Override
                public void onError(UploadInfo uploadInfo, Exception exception) {
                    Toast.makeText(context, "Error: " + exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    propertyImageProgress.setProgress(100);
                    propertyImageProgress.setMax(0);
                    propertyImageProgress.setVisibility(View.VISIBLE);
                    uploadImage.setVisibility(View.VISIBLE);
                }

                @Override
                public void onCompleted(UploadInfo uploadInfo, ServerResponse serverResponse) {
                    Toast.makeText(context, "Image uploaded successfully.", Toast.LENGTH_SHORT).show();
                    propertyImageProgress.setProgress(100);
                    propertyImageProgress.setMax(0);
                    propertyImageProgress.setVisibility(View.GONE);
                    uploadImage.setVisibility(View.GONE);
                    //imagesToUpload.remove(imagesToUpload.indexOf(path));
                }

                @Override
                public void onCancelled(UploadInfo uploadInfo) {
                    Toast.makeText(context, "User cancelled image upload.", Toast.LENGTH_SHORT).show();
                    propertyImageProgress.setProgress(100);
                    propertyImageProgress.setMax(0);
                    propertyImageProgress.setVisibility(View.VISIBLE);
                    uploadImage.setVisibility(View.VISIBLE);
                }
            }).startUpload();
        } catch (FileNotFoundException | MalformedURLException e) {
            e.printStackTrace();
        }*/
    }


    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private PropertyImagesAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final PropertyImagesAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    private String getFilename(String filepath) {
        if (filepath == null)
            return null;

        final String[] filepathParts = filepath.split("/");

        return filepathParts[filepathParts.length - 1];
    }

   /* private UploadNotificationConfig getNotificationConfig(String fileName) {
        return new UploadNotificationConfig()
                .setIcon(R.drawable.ic_upload)
                .setCompletedIcon(R.drawable.ic_upload_success)
                .setErrorIcon(R.drawable.ic_upload_error)
                .setTitle(fileName)
                .setInProgressMessage("Uploading")
                .setCompletedMessage("Success")
                .setErrorMessage("Failed")
                .setAutoClearOnSuccess(true)
                //.setClickIntent(new Intent(this.getContext(), ProfileFragment.class))
                .setClearOnAction(true)
                .setRingToneEnabled(true);

    }*/
}
