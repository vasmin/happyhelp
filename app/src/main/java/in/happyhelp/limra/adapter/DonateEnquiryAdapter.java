package in.happyhelp.limra.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.donateenquiry.Donateenqiry;

public class DonateEnquiryAdapter extends RecyclerView.Adapter {
    List<Donateenqiry> list=new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;

    public DonateEnquiryAdapter(Context context) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setData(List<Donateenqiry> list){
        this.list=list;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_donate_enquiry, viewGroup, false);
        return new DonateEnquiryAdapter.DonationEnquiryHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        Donateenqiry donateenqiry=list.get(i);
        if(holder instanceof  DonationEnquiryHolder){

            ((DonationEnquiryHolder) holder).name.setText(donateenqiry.getName());
            ((DonationEnquiryHolder) holder).email.setText(donateenqiry.getEmail());
            ((DonationEnquiryHolder) holder).mobile.setText(donateenqiry.getMobile());
            ((DonationEnquiryHolder) holder).date.setText(donateenqiry.getDate());
            ((DonationEnquiryHolder) holder).message.setText(donateenqiry.getMessage());
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DonationEnquiryHolder extends RecyclerView.ViewHolder {
        TextView date,name,email,mobile,message;
        public DonationEnquiryHolder(View v) {
            super(v);
            date=v.findViewById(R.id.date);
            name=v.findViewById(R.id.name);
            email=v.findViewById(R.id.email);
            mobile=v.findViewById(R.id.mobile);
            message=v.findViewById(R.id.message);
        }
    }
}
