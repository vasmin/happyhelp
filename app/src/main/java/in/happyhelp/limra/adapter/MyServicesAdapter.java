package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.myservicesresponse.MyServices;

public class MyServicesAdapter extends RecyclerView.Adapter {

    List<MyServices> list=new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;
    OnItemClickListner onItemClickListner;


    public MyServicesAdapter(Context context, OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }

    public  void setData(List<MyServices> list){
        this.list=list;
        notifyDataSetChanged();
    }

    public interface OnItemClickListner {
        void onItemClick(MyServices d, View view) throws ParseException;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_myservices, viewGroup, false);
        return new MyServicesAdapter.ServiceHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        MyServices myServices=list.get(i);


        if(holder instanceof  ServiceHolder){
            ((ServiceHolder) holder).name.setText(myServices.getName());
            ((ServiceHolder) holder).email.setText(myServices.getEmail());
            ((ServiceHolder) holder).mobile.setText(myServices.getMobile());
            ((ServiceHolder) holder).message.setText(myServices.getMessage());
            ((ServiceHolder) holder).address.setText(myServices.getAddress());
            ((ServiceHolder) holder).amount.setText(myServices.getAmount());
            ((ServiceHolder) holder).date.setText(myServices.getDate());
            ((ServiceHolder) holder).orderId.setText(myServices.getId());
            String service="";
            for(int j=0;j<myServices.getServices().size();j++) {
                if(j==0){
                    service =  myServices.getServices().get(j).getName();
                }else {
                    service = service + "," + myServices.getServices().get(j).getName();
                }
            }
            ((ServiceHolder) holder).service.setText(service);

            if(myServices.getPaymentMode().equals("0")) {
                ((ServiceHolder) holder).paymentMode.setText("Cash On Delivery");
            }else{
                ((ServiceHolder) holder).paymentMode.setText("Online ");
            }

            if(myServices.getHour().equals("0")){
                ((ServiceHolder) holder).hour.setText("Enquiry");
            }else {
                ((ServiceHolder) holder).hour.setText(myServices.getHour() + " Hour");
            }

            ((ServiceHolder) holder).time.setText(myServices.getTime());

            if(myServices.getStatus().equals("0")){
                ((ServiceHolder) holder).status.setText("Processing");
                ((ServiceHolder) holder).status.setTextColor(context.getResources().getColor(R.color.accent_color));
            }
            else if(myServices.getStatus().equals("1")){

                ((ServiceHolder) holder).status.setText("Please Wait for User Confirmation ");
                ((ServiceHolder) holder).status.setTextColor(context.getResources().getColor(R.color.green));


            }
            else if(myServices.getStatus().equals("2")){
                ((ServiceHolder) holder).status.setText("Service Completed ");
                ((ServiceHolder) holder).status.setTextColor(context.getResources().getColor(R.color.green));

            }
            else if(myServices.getStatus().equals("3")){
                ((ServiceHolder) holder).status.setText("Service Confirm by Vendor ");
                ((ServiceHolder) holder).status.setTextColor(context.getResources().getColor(R.color.green));

            }
            else if(myServices.getStatus().equals("4")){
                ((ServiceHolder) holder).status.setText("Service Rejected by Vendor ");
                ((ServiceHolder) holder).status.setTextColor(context.getResources().getColor(R.color.accent_color));

            }
            else if(myServices.getStatus().equals("5")){
                ((ServiceHolder) holder).status.setText("Service Confirm by User");
                ((ServiceHolder) holder).status.setTextColor(context.getResources().getColor(R.color.green));

            }
            else if(myServices.getStatus().equals("6")){
                ((ServiceHolder) holder).status.setText("Service Cancel by user ");
                ((ServiceHolder) holder).status.setTextColor(context.getResources().getColor(R.color.accent_color));

            }
            else if(myServices.getStatus().equals("7")){
                ((ServiceHolder) holder).status.setText(" Reach to service location ");
                ((ServiceHolder) holder).status.setTextColor(context.getResources().getColor(R.color.green));
            }else if(myServices.getStatus().equals("8")){
                ((ServiceHolder) holder).status.setText(" Reach to service location confimation Pending ");
                ((ServiceHolder) holder).status.setTextColor(context.getResources().getColor(R.color.accent_color));
            }else if(myServices.getStatus().equals("9")){
                ((ServiceHolder) holder).status.setText(" Service Automatically Cancelled.. ");
                ((ServiceHolder) holder).status.setTextColor(context.getResources().getColor(R.color.accent_color));
            }
            ((ServiceHolder) holder).bind(myServices,onItemClickListner);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ServiceHolder extends RecyclerView.ViewHolder {
        TextView name,mobile,email,message,address,amount,date,status,time,orderId,hour,paymentMode,service;
        public ServiceHolder(View v) {
            super(v);
            name=v.findViewById(R.id.name);
            date=v.findViewById(R.id.date);
            mobile=v.findViewById(R.id.mob);
            address=v.findViewById(R.id.address);
            email=v.findViewById(R.id.email);
            message=v.findViewById(R.id.message);
            amount=v.findViewById(R.id.amount);
            status=v.findViewById(R.id.status);
            orderId=v.findViewById(R.id.orderid);
            time=v.findViewById(R.id.time);
            hour=v.findViewById(R.id.hour);
            paymentMode=v.findViewById(R.id.paymentmode);
            service=v.findViewById(R.id.service);
        }

        public void bind(final MyServices data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
