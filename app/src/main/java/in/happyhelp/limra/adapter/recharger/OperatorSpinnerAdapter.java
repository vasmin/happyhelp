package in.happyhelp.limra.adapter.recharger;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.statecityresponse.City;
import in.happyhelp.limra.rechargeresponse.operator.Operator;

public class OperatorSpinnerAdapter  extends ArrayAdapter {
    private Activity activity;
    private List<Operator> alSpinner=new ArrayList<>();

    public OperatorSpinnerAdapter(@NonNull Activity activity) {
        super(activity,0);
        this.activity = activity;

    }

    public  void setData(List<Operator> alSpinner){
        this.alSpinner = alSpinner;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(activity).inflate(R.layout.item_spinner,parent,false);


        ((TextView)view.findViewById(R.id.tvName)).setText(alSpinner.get(position).getValue());


        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(activity).inflate(R.layout.item_spinner,parent,false);

        ((TextView) view.findViewById(R.id.tvName)).setText(alSpinner.get(position).getValue());

        return view;
    }

    @Override
    public int getCount() {
        return alSpinner.size();
    }
}
