package in.happyhelp.limra.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.mypropertyenquiryresponse.Enquiry;

public class PropertyEnquiryAdapter extends RecyclerView.Adapter {
    List<Enquiry> list=new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;
    OnItemClickListner onItemClickListner;

    public PropertyEnquiryAdapter(Context context,OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }

    public void setData(List<Enquiry> list){
        this.list=list;
        notifyDataSetChanged();
    }

    public interface OnItemClickListner {
        void onItemClick(Enquiry d, View view) throws ParseException;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_property_enquiry, viewGroup, false);
        return new PropertyEnquiryAdapter.EnquiryHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        Enquiry enquiry=list.get(i);

        if(holder instanceof EnquiryHolder){
            ((EnquiryHolder) holder).name.setText(enquiry.getName());
            ((EnquiryHolder) holder).propertyName.setText(enquiry.getPropertyName());
            ((EnquiryHolder) holder).mobile.setText(enquiry.getMobile());
            ((EnquiryHolder) holder).message.setText(enquiry.getMessage());
            ((EnquiryHolder) holder).email.setText(enquiry.getEmail());
            ((EnquiryHolder) holder).date.setText(enquiry.getDate());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class EnquiryHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.date)
        TextView date;

        @BindView(R.id.mobile)
        TextView mobile;

        @BindView(R.id.email)
        TextView email;

        @BindView(R.id.message)
        TextView message;

        @BindView(R.id.propertyname)
        TextView propertyName;


        public EnquiryHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }
        public void bind(final Enquiry data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }

    }
}
