package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.orhanobut.dialogplus.Holder;

import java.text.ParseException;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.model.ServiceModel;

public class HourAdapter extends RecyclerView.Adapter {
    LayoutInflater layoutInflater;
    OnItemClickListner onItemClickListner;
    List<ServiceModel> serviceModels;
    Context context;

    public HourAdapter(Context context, OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }

    public void setData(List<ServiceModel> serviceModels){
        this.serviceModels=serviceModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_hours, viewGroup, false);
        return new HoursHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        ServiceModel serviceModel=serviceModels.get(i);

        if(holder instanceof HoursHolder){
            ((HoursHolder) holder).rps.setText("₹"+serviceModel.getRps());
            ((HoursHolder) holder).hours.setText(serviceModel.getHours()+"Hours");

            if(serviceModel.getHours().equals("48")){
                ((HoursHolder) holder).hours.setText(serviceModel.getServiceName());
            }

            ((HoursHolder) holder).bind(serviceModel,onItemClickListner);
            ((HoursHolder) holder).service.setClickable(false);

            if(serviceModel.isSelected()){
                ((HoursHolder) holder).servicetype.setText(serviceModel.getService());
                ((HoursHolder) holder).servicetype.setVisibility(View.VISIBLE);
                ((HoursHolder) holder).cardView.setCardBackgroundColor(Color.parseColor("#B3E5FC"));
                ((HoursHolder) holder).service.setChecked(true);
            }else{
                ((HoursHolder) holder).cardView.setCardBackgroundColor(Color.WHITE);
                ((HoursHolder) holder).service.setChecked(false);
                ((HoursHolder) holder).servicetype.setVisibility(View.GONE);
            }
        }
    }

    public interface OnItemClickListner {
        void onItemClick(ServiceModel d, View view) throws ParseException;
    }

    @Override
    public int getItemCount() {
        return serviceModels.size();
    }

    private class HoursHolder extends RecyclerView.ViewHolder {
        TextView hours,rps,servicetype;
        CardView cardView;
        CheckBox service;
        public HoursHolder(View v) {
            super(v);
            hours=v.findViewById(R.id.hours);
            rps=v.findViewById(R.id.rps);
            cardView=v.findViewById(R.id.card);
            service=v.findViewById(R.id.service);
            servicetype=v.findViewById(R.id.servicetype);
        }


        public void bind(final ServiceModel data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
