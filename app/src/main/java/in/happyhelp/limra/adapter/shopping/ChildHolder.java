package in.happyhelp.limra.adapter.shopping;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import in.happyhelp.limra.R;

public class ChildHolder extends ChildViewHolder {

  private TextView childTextView;

  public ChildHolder(View itemView) {
    super(itemView);
    childTextView = (TextView) itemView.findViewById(R.id.name);


  }




  public void setChildName(String name) {
    childTextView.setText(name);
  }
}
