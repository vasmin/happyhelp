package in.happyhelp.limra.adapter.shopping;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.shopping.MyOrdersActivity;
import in.happyhelp.limra.shopping.ShoppingOrderDetailActivity;
import in.happyhelp.limra.shoppingresponse.categoryresponse.Category;
import in.happyhelp.limra.shoppingresponse.myorderresponse.Myorder;
import in.happyhelp.limra.shoppingresponse.wishlistresponse.Wishlist;

public class MyOrderAdapter extends RecyclerView.Adapter {

    List<Myorder> list=new ArrayList<>();
    Context context;
    OnItemClickListner onItemClickListner;
    LayoutInflater layoutInflater;

    public MyOrderAdapter(Context context, OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }

    public void setData(List<Myorder> list){
        this.list=list;
        notifyDataSetChanged();
    }

    public interface OnItemClickListner {
        void onItemClick(Myorder d, View view) throws ParseException;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_my_order, viewGroup, false);
        return new MyOrderAdapter.OrderHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        final Myorder myorder=list.get(i);
        if(holder instanceof  OrderHolder){

            if(myorder.getStatus().equals("0")){
                ((OrderHolder) holder).status.setText("Processing");
                ((OrderHolder) holder).status.setTextColor(Color.RED);
            }else if(myorder.getStatus().equals("1")){
                ((OrderHolder) holder).status.setText("Shipped");
                ((OrderHolder) holder).status.setTextColor(Color.parseColor("#8BC34A"));
                ((OrderHolder) holder).cancel.setVisibility(View.GONE);
            }else if(myorder.getStatus().equals("2")){
                ((OrderHolder) holder).status.setText("Delivered");
                ((OrderHolder) holder).status.setTextColor(Color.parseColor("#8BC34A"));
                ((OrderHolder) holder).cancel.setVisibility(View.GONE);
            }else if(myorder.getStatus().equals("3")){
                ((OrderHolder) holder).status.setText("Order Failed");
                ((OrderHolder) holder).status.setTextColor(Color.RED);
                ((OrderHolder) holder).cancel.setVisibility(View.GONE);
            }else if(myorder.getStatus().equals("4")){
                ((OrderHolder) holder).status.setText("Cancelled");
                ((OrderHolder) holder).status.setTextColor(Color.RED);
                ((OrderHolder) holder).cancel.setVisibility(View.GONE);
            }else {
                ((OrderHolder) holder).status.setText("Processing");
                ((OrderHolder) holder).status.setTextColor(Color.parseColor("#FFEB3B"));
            }

            ((OrderHolder) holder).orderId.setText("HH00"+myorder.getId());
            ((OrderHolder) holder).total.setText(myorder.getAmount());
            ((OrderHolder) holder).bind(myorder,onItemClickListner);

            ((OrderHolder) holder).view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, ShoppingOrderDetailActivity.class);
                    intent.putExtra(Constants.ORDERID,myorder.getId());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class OrderHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.orderid)
        TextView orderId;

        @BindView(R.id.total)
        TextView total;

        @BindView(R.id.status)
        TextView status;

        @BindView(R.id.cancel)
        TextView cancel;

        @BindView(R.id.viewtext)
        TextView view;


        public OrderHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }

        public void bind(final Myorder data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);
                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
