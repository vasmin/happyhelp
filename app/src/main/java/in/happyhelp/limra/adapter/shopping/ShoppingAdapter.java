package in.happyhelp.limra.adapter.shopping;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.model.ServiceModel;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.categoryresponse.Category;

public class ShoppingAdapter extends RecyclerView.Adapter {

    private List<Category> list=new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;

    OnItemClickListner onItemClickListner;
    public ShoppingAdapter(Context context, OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }

    public void setData(List<Category>list){
        this.list=list;
        notifyDataSetChanged();
    }

    public interface OnItemClickListner {
        void onItemClick(Category d, View view) throws ParseException;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_shopping, viewGroup, false);
        return new ShoppingAdapter.ShoppingHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        Category category=list.get(i);

        if(holder instanceof ShoppingHolder) {
            ((ShoppingHolder) holder).textView.setText(category.getName());
            Glide.with(context)
                    .load(RestClient.base_image_url + category.getImage())
                    .into(((ShoppingHolder) holder).imageView);

            ((ShoppingHolder) holder).bind(category,onItemClickListner);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ShoppingHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView imageView;
        ShoppingHolder(View v) {
            super(v);
            textView=v.findViewById(R.id.name);
            imageView=v.findViewById(R.id.icon);


        }

        public void bind(final Category data, final ShoppingAdapter.OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}


