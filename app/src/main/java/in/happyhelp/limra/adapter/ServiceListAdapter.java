package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.List;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.service.ServiceDetailActivity;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.servicelistresponse.Datum;

public class ServiceListAdapter extends RecyclerView.Adapter {

    Context context;
    List<Datum> list;
    LayoutInflater layoutInflater;
    OnItemClickListner onItemClickListner;

    public ServiceListAdapter(Context context,OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }

    public void setData(List<Datum> list){
        this.list=list;

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_service, viewGroup, false);
        return new ServiceListAdapter.ServiceHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        final Datum datum=list.get(i);

        if(holder instanceof ServiceHolder){
            ((ServiceHolder) holder).name.setText(datum.getCompanyName());
            ((ServiceHolder) holder).mob.setText(datum.getMobile());
            ((ServiceHolder) holder).location.setText(datum.getLocation());
            ((ServiceHolder) holder).distance.setText(String.valueOf(datum.getDistance())+" Km");

           /* String serviceName="";
            for(int j=0;j<datum.getServices().size();j++){
                if(j==datum.getServices().size()-1){
                    serviceName=serviceName+datum.getServices().get(j).getName()+",";
                }
                else{
                    serviceName=serviceName+datum.getServices().get(j).getName()+",";
                }
            }
            ((ServiceHolder) holder).service.setText(serviceName);*/

            ((ServiceHolder) holder).mobileicon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + datum.getMobile()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

            ((ServiceHolder) holder).mob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + datum.getMobile()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

            ((ServiceHolder) holder).linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, ServiceDetailActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.SERVICEID,datum.getId());
                    context.startActivity(intent);
                }
            });




         //   ((ServiceHolder) holder).bind(datum,onItemClickListner);

            Glide.with(context)
                    .load(RestClient.base_image_url+datum.getImage())
                    .into(((ServiceHolder) holder).imageView);
        }

    }

    public interface OnItemClickListner {
        void onItemClick(Datum d, View view) throws ParseException;

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ServiceHolder extends RecyclerView.ViewHolder {
        ImageView imageView,mobileicon;
        LinearLayout linearLayout;
        TextView name,mob,location,distance,view,service,star;
        public ServiceHolder(View v) {
            super(v);
            imageView=v.findViewById(R.id.image);
            linearLayout=v.findViewById(R.id.linear);
            name=v.findViewById(R.id.name);
            mob=v.findViewById(R.id.mob);
            location=v.findViewById(R.id.location);
            distance=v.findViewById(R.id.distance);
            view=v.findViewById(R.id.view);
            service=v.findViewById(R.id.service);
            mobileicon=v.findViewById(R.id.mobileicon);
            star=v.findViewById(R.id.stars);
        }

        public void bind(final Datum data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
