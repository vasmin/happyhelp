package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.donation.MyDonationActivity;
import in.happyhelp.limra.activity.patient.MyPatientActivity;
import in.happyhelp.limra.activity.patient.MyPatientDetailActivity;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.mypatientresponse.Datum;
import retrofit2.Call;
import retrofit2.Callback;

public class MyPatientAdapter extends RecyclerView.Adapter {
    LayoutInflater layoutInflater;
    Activity context;
    List<Datum> donateList = new ArrayList<>();

    public MyPatientAdapter(Activity context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setData(List<Datum> donateList) {
        this.donateList = donateList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_dolationlist, viewGroup, false);
        return new MyPatientAdapter.MyPatientHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        final Datum donate = donateList.get(i);

        if (holder instanceof MyPatientHolder) {

            ((MyPatientHolder) holder).name.setText(donate.getPatientName());

            ((MyPatientHolder) holder).date.setText(donate.getDate());

            Glide.with(context)
                    .load(RestClient.base_image_url + donate.getImage())
                    .into(((MyPatientHolder) holder).imageView);

            Log.e("MyDate", donate.getDate());
            ((MyPatientHolder) holder).date.setText(donate.getDate());

            ((MyPatientHolder) holder).service.setText(donate.getBloodGroup());
            ((MyPatientHolder) holder).address.setText(donate.getPatientAddress());

            ((MyPatientHolder) holder).view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, MyPatientDetailActivity.class);
                    intent.putExtra(Constants.DID, donate.getId());
                    context.startActivity(intent);
                }
            });


            ((MyPatientHolder) holder).active.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (donate.getStatus().equals("1")) {
                        new AlertDialog.Builder(context)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Are you sure you want to De Activate Donate ")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        updateStatus(donate.getId(), donate.getStatus());
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                    } else {
                        new AlertDialog.Builder(context)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Are you sure you want to Activate Donate ")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        updateStatus(donate.getId(), donate.getStatus());
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                    }
                }
            });

            if (donate.getStatus().equals("0")) {
                ((MyPatientHolder) holder).active.setText("Active");
                ((MyPatientHolder) holder).active.setBackground(context.getDrawable(R.drawable.green_round));
            } else {
                ((MyPatientHolder) holder).active.setText("Deactivate");
                ((MyPatientHolder) holder).active.setBackground(context.getDrawable(R.drawable.red_round));
            }
        }
    }


    @Override
    public int getItemCount() {
        return donateList.size();
    }

    public void updateStatus(String jobId, String status) {
        HashMap<String, String> hashMap = new HashMap<>();

        if (status.equals("0")) {
            hashMap.put("status", "1");
        } else {
            hashMap.put("status", "0");
        }
        hashMap.put("patientid", jobId);
        Call<Response> call = RestClient.get().updatePatientStatus(SharedPreferenceHelper.getInstance(context).getAuthToken(), hashMap);
        call.enqueue(new Callback<Response>() {
            @TargetApi(Build.VERSION_CODES.M)
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull retrofit2.Response<Response> response) {
                in.happyhelp.limra.activity.response.Response response1 = response.body();
                if (response.code() == 200) {
                    if (response1.getStatus()) {
                        Intent intent = new Intent(context, MyPatientActivity.class);
                        context.startActivity(intent);
                        context.finish();
                    } else {
                        View parentLayout = context.findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(context.getColor(android.R.color.holo_red_light))
                                .show();
                    }

                } else if (response.code() == 401) {
                    AppUtils.logout(context);
                } else {
                    View parentLayout = context.findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(context.getColor(android.R.color.holo_red_light))
                            .show();
                }
            }

            @TargetApi(Build.VERSION_CODES.M)
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = context.findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(context.getColor(android.R.color.holo_red_light))
                        .show();
            }
        });
    }


    public class MyPatientHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name, service, date, address, view, active;

        MyPatientHolder(View v) {
            super(v);
            imageView = v.findViewById(R.id.image);
            name = v.findViewById(R.id.name);
            service = v.findViewById(R.id.service);
            date = v.findViewById(R.id.date);
            address = v.findViewById(R.id.address);
            view = v.findViewById(R.id.view);
            active = v.findViewById(R.id.active);
        }
    }
}

