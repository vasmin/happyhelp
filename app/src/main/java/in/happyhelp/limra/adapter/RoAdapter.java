package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.roresponse.Ro;

public class RoAdapter extends RecyclerView.Adapter {

    Context context;
    LayoutInflater layoutInflater;
    List<Ro> roList=new ArrayList<>();
    OnItemClickListner mlistener;

    public RoAdapter(Context context,OnItemClickListner listner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.mlistener=listner;
    }

    public  void setData(List<Ro> roList){
        this.roList=roList;
        Log.e("Size Ro", String.valueOf(roList.size()));
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_ro, viewGroup, false);
        return new RoAdapter.RoHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        Ro ro=roList.get(i);

        if(holder instanceof  RoHolder){

            ((RoHolder) holder).name.setText(ro.getName());

            Glide.with(context)
                    .load(RestClient.base_image_url+ro.getImage())
                    .into(((RoHolder) holder).imageView);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                ((RoHolder) holder).details.setText(Html.fromHtml(ro.getContent()+"...", Html.FROM_HTML_MODE_COMPACT));
            } else {
                ((RoHolder) holder).details.setText(Html.fromHtml(ro.getContent()+"..."));
            }

            ((RoHolder) holder).bind(ro,mlistener);


        }

    }

    @Override
    public int getItemCount() {
        return roList.size();
    }

    public interface OnItemClickListner {
        void onItemClick(Ro d, View view) throws ParseException;

    }

    private class RoHolder extends RecyclerView.ViewHolder {
        TextView name,details;
        ImageView imageView;

        RoHolder(View v) {
            super(v);
            name=v.findViewById(R.id.name);
            details=v.findViewById(R.id.details);
            imageView=v.findViewById(R.id.image);
        }

        public void bind(final Ro data, final RoAdapter.OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
