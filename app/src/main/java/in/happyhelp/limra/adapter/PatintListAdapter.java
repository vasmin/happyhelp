package in.happyhelp.limra.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.patient.PatientDetailsActivity;
import in.happyhelp.limra.activity.response.patientresponse.Patient;
import in.happyhelp.limra.network.RestClient;

public class PatintListAdapter extends RecyclerView.Adapter {
    List<Patient> list=new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;

    public PatintListAdapter(Context context) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setData(List<Patient> list){
        this.list=list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_patient, viewGroup, false);
        return new PatintListAdapter.PatientHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        Patient patient=list.get(i);
        if(holder instanceof  PatientHolder){
            ((PatientHolder) holder).date.setText(patient.getDate());
            ((PatientHolder) holder).patientName.setText(patient.getPatientName());
            ((PatientHolder) holder).mobile.setText(patient.getMobile());
            ((PatientHolder) holder).hospitalName.setText(patient.getHospitalName());
            ((PatientHolder) holder).drName.setText(patient.getDoctorName());

            ((PatientHolder) holder).cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, PatientDetailsActivity.class);
                    intent.putExtra(Constants.PID,patient.getId());
                    context.startActivity(intent);
                }
            });

            Glide.with(context)
                    .load(RestClient.base_image_url+patient.getImage())
                    .into(((PatientHolder) holder).image);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class PatientHolder extends RecyclerView.ViewHolder {
        TextView date,patientName,hospitalName,mobile,drName;
        ImageView image;
        CardView cardView;

        PatientHolder(View v) {
            super(v);
            date=v.findViewById(R.id.date);
            patientName=v.findViewById(R.id.name);
            hospitalName=v.findViewById(R.id.hospitalname);
            mobile=v.findViewById(R.id.mobile);
            drName=v.findViewById(R.id.drname);
            image=v.findViewById(R.id.image);
            cardView=v.findViewById(R.id.viewcard);
        }
    }
}
