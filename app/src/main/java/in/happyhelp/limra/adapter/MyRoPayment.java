package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.myrodetailresponse.Bill;

public class MyRoPayment extends RecyclerView.Adapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Bill> list=new ArrayList<>();
    OnItemClickListner listner;

    public MyRoPayment(Context context,OnItemClickListner listner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.listner=listner;
    }

    public void setData(List<Bill> list){
        this.list=list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_myro_payment, viewGroup, false);
        return new MyRoPayment.MyRoHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        Bill bill=list.get(i);
        if(holder instanceof MyRoHolder){
            ((MyRoHolder) holder).date.setText(bill.getDate());
            ((MyRoHolder) holder).rent.setText(bill.getAmount());
            ((MyRoHolder) holder).month.setText(bill.getMonth());

            ((MyRoHolder) holder).bind(bill,listner);

            if(bill.getStatus().equals("0")){
                ((MyRoHolder) holder).pay.setText("Payment");

            }else{
                ((MyRoHolder) holder).pay.setText("Paid");
                ((MyRoHolder) holder).pay.setBackgroundColor(Color.GREEN);
                ((MyRoHolder) holder).pay.setClickable(false);
            }
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickListner {
        void onItemClick(Bill d, View view) throws ParseException;

    }

    public class MyRoHolder extends RecyclerView.ViewHolder {
        TextView date,month,rent,pay;
        MyRoHolder(View v) {
            super(v);
            date=v.findViewById(R.id.date);
            month=v.findViewById(R.id.month);
            rent=v.findViewById(R.id.rent);
            pay=v.findViewById(R.id.pay);
        }

        public void bind(final Bill data, final MyRoPayment.OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
