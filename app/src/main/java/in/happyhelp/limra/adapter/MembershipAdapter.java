package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.jobplanresponse.JobPlan;

public class MembershipAdapter extends RecyclerView.Adapter  {
    List<JobPlan> list=new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;
    OnItemClickListner onItemClickListner;

    public MembershipAdapter(Context context, OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }

    public void setData(List<JobPlan> list){
        this.list=list;
        notifyDataSetChanged();
    }

    public interface OnItemClickListner {
        void onItemClick(JobPlan d, View view) throws ParseException;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_member, viewGroup, false);
        return new MembershipAdapter.MemberHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        JobPlan datum=list.get(i);
        if(holder instanceof MemberHolder){
            ((MemberHolder) holder).month.setText(datum.getMonths()+" Month");
            ((MemberHolder) holder).rates.setText(datum.getAmount());

            ((MemberHolder) holder).bind(datum,onItemClickListner);
            if(datum.isSelected()){
                ((MemberHolder) holder).cardView.setCardBackgroundColor(Color.parseColor("#B3E5FC"));

                ((MemberHolder) holder).checkBox.setChecked(true);
            }else{
                ((MemberHolder) holder).cardView.setCardBackgroundColor(Color.WHITE);

                ((MemberHolder) holder).checkBox.setChecked(false);

            }


        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MemberHolder extends RecyclerView.ViewHolder {
        TextView month,rates;
        CheckBox checkBox;
        CardView cardView;
        public MemberHolder(View v) {
            super(v);
            cardView=v.findViewById(R.id.card);
            month=v.findViewById(R.id.month);
            rates=v.findViewById(R.id.rps);
            checkBox=v.findViewById(R.id.checkbox);
        }

        public void bind(final JobPlan data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
