package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.jobapplicationresponse.JobApplication;

public class JobApplicationAdapter extends RecyclerView.Adapter {
    List<JobApplication> list=new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;


    public JobApplicationAdapter(Context context) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setData(List<JobApplication> list){
        this.list=list;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_job_applicant, viewGroup, false);
        return new JobApplicationAdapter.JobApplicationHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        JobApplication jobApplication=list.get(i);

        if(holder instanceof JobApplicationHolder){
            ((JobApplicationHolder) holder).name.setText(jobApplication.getUserName());
            ((JobApplicationHolder) holder).date.setText(jobApplication.getDate());
            ((JobApplicationHolder) holder).email.setText(jobApplication.getEmail());
            ((JobApplicationHolder) holder).mobile.setText(jobApplication.getMobile());
            ((JobApplicationHolder) holder).skill.setText(jobApplication.getSkills());
            ((JobApplicationHolder) holder).ctc.setText(jobApplication.getCurrentSalary());
            ((JobApplicationHolder) holder).location.setText(jobApplication.getJobLocation());
            ((JobApplicationHolder) holder).experience.setText(jobApplication.getJobExperience()+" Years");
            ((JobApplicationHolder) holder).role.setText(jobApplication.getJobRole());
            ((JobApplicationHolder) holder).qualification.setText(jobApplication.getQualification());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class JobApplicationHolder extends RecyclerView.ViewHolder {
        TextView date,name,email,mobile,experience,skill,ctc,location,role,qualification;
        JobApplicationHolder(View v) {
            super(v);
            date=v.findViewById(R.id.date);
            name=v.findViewById(R.id.name);
            email=v.findViewById(R.id.email);
            mobile=v.findViewById(R.id.mobile);
            experience=v.findViewById(R.id.experience);
            skill=v.findViewById(R.id.skill);
            ctc=v.findViewById(R.id.ctc);
            location=v.findViewById(R.id.location);
            role=v.findViewById(R.id.role);
            qualification=v.findViewById(R.id.qualification);
        }
    }
}
