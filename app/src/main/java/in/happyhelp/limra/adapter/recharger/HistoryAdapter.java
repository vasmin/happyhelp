package in.happyhelp.limra.adapter.recharger;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.adapter.AdsListAdapter;
import in.happyhelp.limra.rechargeresponse.rechargelist.History;

public class HistoryAdapter extends RecyclerView.Adapter {
    List<History> list=new ArrayList<>();
    LayoutInflater layoutInflater;
    Context context;


    public HistoryAdapter(Context context) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setData(List<History> list){
        this.list=list;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_recharge_history, viewGroup, false);
        return new HistoryAdapter.RechargeHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        History history=list.get(i);

        if(holder instanceof RechargeHolder){
            ((RechargeHolder) holder).mobile.setText("Mobile "+history.getMobile());
            ((RechargeHolder) holder).date.setText("Date "+history.getDate());
            ((RechargeHolder) holder).amount.setText("Amount "+history.getAmount());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RechargeHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.mobile)
        TextView mobile;

        @BindView(R.id.date)
        TextView date;

        @BindView(R.id.amount)
        TextView amount;

        public RechargeHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }
    }
}
