package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.Jobs.MyJobListActivity;
import in.happyhelp.limra.activity.donation.DonationDetailsActivity;
import in.happyhelp.limra.activity.donation.MyDonationActivity;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.mydonationresponse.Datum;
import retrofit2.Call;
import retrofit2.Callback;

public class MyDonationAdapter extends RecyclerView.Adapter {

    LayoutInflater layoutInflater;
    Activity context;
    List<Datum> donateList=new ArrayList<>();
    public MyDonationAdapter(Activity context) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setData(List<Datum> donateList){
        this.donateList=donateList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_dolationlist, viewGroup, false);
        return new MyDonationAdapter.MyDonationHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        final Datum donate=donateList.get(i);

        if(holder instanceof MyDonationHolder){

            ((MyDonationHolder) holder).name.setText(donate.getName());
            ((MyDonationHolder) holder).date.setText(donate.getDate());
            Glide.with(context)
                    .load(RestClient.base_image_url+donate.getImage())
                    .into(((MyDonationHolder) holder).imageView);

            Log.e("MyDate",donate.getDate());
            ((MyDonationHolder) holder).date.setText(donate.getDate());

            ((MyDonationHolder) holder).service.setText(donate.getCategory());
            ((MyDonationHolder) holder).address.setText(donate.getAddress());

            ((MyDonationHolder) holder).view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context,DonationDetailsActivity.class);
                    intent.putExtra(Constants.DID,donate.getId());
                    intent.putExtra(Constants.ENQUIRY,"true");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });


            ((MyDonationHolder) holder).active.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(donate.getStatus().equals("1")) {
                        new AlertDialog.Builder(context)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Are you sure you want to De Activate Patient ")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        updateStatus(donate.getId(), donate.getStatus());
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                    }else{
                        new AlertDialog.Builder(context)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Are you sure you want to Activate Patient ")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        updateStatus(donate.getId(), donate.getStatus());
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                    }
                }
            });

            if(donate.getStatus().equals("0")) {
                ((MyDonationHolder) holder).active.setText("Active");
                ((MyDonationHolder) holder).active.setBackground(context.getDrawable(R.drawable.green_round));
            }else{
                ((MyDonationHolder) holder).active.setText("Deactivate");
                ((MyDonationHolder) holder).active.setBackground(context.getDrawable(R.drawable.red_round));
            }


        }

    }


    public void updateStatus(String jobId,String status){
        HashMap<String,String> hashMap=new HashMap<>();

        if(status.equals("0")){
            hashMap.put("status","1");
        }else{
            hashMap.put("status","0");
        }
        hashMap.put("donateid",jobId);
        Call<Response> call= RestClient.get().updateDonateStatus(SharedPreferenceHelper.getInstance(context).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @TargetApi(Build.VERSION_CODES.M)
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull retrofit2.Response<Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        Intent intent=new Intent(context, MyDonationActivity.class);
                        context.startActivity(intent);
                        context.finish();
                    }else{
                        View parentLayout =context.findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(context.getColor(android.R.color.holo_red_light ))
                                .show();
                    }

                }else if(response.code()==401){
                    AppUtils.logout(context);
                }else{
                    View parentLayout =context.findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(context.getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @TargetApi(Build.VERSION_CODES.M)
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout =context.findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(context.getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return donateList.size();
    }

    public class MyDonationHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name,service,date,address,view,active;
        MyDonationHolder(View v) {
            super(v);
            active=v.findViewById(R.id.active);
            imageView=v.findViewById(R.id.image);
            name=v.findViewById(R.id.name);
            service=v.findViewById(R.id.service);
            date=v.findViewById(R.id.date);
            address=v.findViewById(R.id.address);
            view=v.findViewById(R.id.view);
        }
    }
}
