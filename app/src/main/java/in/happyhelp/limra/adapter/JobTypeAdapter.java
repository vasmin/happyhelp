package in.happyhelp.limra.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.jobcategory.JobCategory;

public class JobTypeAdapter extends RecyclerView.Adapter {

    Context context;
    LayoutInflater layoutInflater;
    List<JobCategory> list=new ArrayList<>();
    OnItemClickListner listner;


    public JobTypeAdapter(Context context, OnItemClickListner listner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.listner=listner;
    }

    public void setData(List<JobCategory> list)
    {
        this.list=list;
        notifyDataSetChanged();
    }

    public interface OnItemClickListner {
        void onItemClick(JobCategory d, View view) throws ParseException;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_job_type, viewGroup, false);
        return new JobTypeAdapter.JobHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        final JobCategory jobCategory=list.get(i);

        if(holder instanceof  JobHolder){
            ((JobHolder) holder).job.setText(jobCategory.getName());

            ((JobHolder) holder).bind(jobCategory,listner);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class JobHolder extends RecyclerView.ViewHolder {

        TextView job;
        public JobHolder(View v) {
            super(v);
            job=v.findViewById(R.id.job);
        }


        public void bind(final JobCategory data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }


}
