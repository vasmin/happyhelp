package in.happyhelp.limra.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.happyhelp.limra.R;

public class AppliedJobAdapter extends RecyclerView.Adapter {

    Context context;
    LayoutInflater layoutInflater;
    public AppliedJobAdapter(Context context) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);

    }

    public void setData(){
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_applied, viewGroup, false);
        return new AppliedJobAdapter.AppliedHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class AppliedHolder extends RecyclerView.ViewHolder {

        public AppliedHolder(View v) {
            super(v);
        }
    }
}
