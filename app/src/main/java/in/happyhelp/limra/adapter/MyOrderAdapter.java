package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.myservicesresponse.MyServices;

public class MyOrderAdapter extends RecyclerView.Adapter {

    Context context;
    List<MyServices> list;
    LayoutInflater layoutInflater;
    OnItemClickListner onItemClickListner;

    public MyOrderAdapter(Context context, OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;
    }

    public void setData(List<MyServices> list){
        this.list=list;
        notifyDataSetChanged();
    }

    public interface OnItemClickListner {
        void onItemClick(MyServices d, View view) throws ParseException;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_myorder, viewGroup, false);
        return new MyOrderAdapter.MyOrderHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        MyServices myServices=list.get(i);

        if(holder instanceof  MyOrderHolder) {
            if (myServices.getStatus().equals("0")) {
                ((MyOrderHolder) holder).status.setText("Processing");
            } else if (myServices.getStatus().equals("1")) {
                ((MyOrderHolder) holder).status.setText("Vendor Accepted ");
                ((MyOrderHolder) holder).status.setTextColor(context.getResources().getColor(R.color.green));


            } else if (myServices.getStatus().equals("2")) {
                ((MyOrderHolder) holder).status.setText("Completed");
                ((MyOrderHolder) holder).status.setTextColor(context.getResources().getColor(R.color.green));

            } else if (myServices.getStatus().equals("3")) {
                ((MyOrderHolder) holder).status.setText("Order Confirm by Vendor");
                ((MyOrderHolder) holder).status.setTextColor(context.getResources().getColor(R.color.green));

            } else if (myServices.getStatus().equals("4")) {
                ((MyOrderHolder) holder).status.setText("Order Rejected by Vendor ");
                ((MyOrderHolder) holder).status.setTextColor(Color.RED);

            } else if (myServices.getStatus().equals("5")) {
                ((MyOrderHolder) holder).status.setText("Order Confirm ");
                ((MyOrderHolder) holder).status.setTextColor(context.getResources().getColor(R.color.green));

            } else if (myServices.getStatus().equals("6")) {
                ((MyOrderHolder) holder).status.setText("Order Cancel by User");
                ((MyOrderHolder) holder).status.setTextColor(Color.RED);

            }else if (myServices.getStatus().equals("7")) {
                ((MyOrderHolder) holder).status.setText("Vendor Reach location ");
                ((MyOrderHolder) holder).status.setTextColor(context.getResources().getColor(R.color.green));

            }else if (myServices.getStatus().equals("8")) {
                ((MyOrderHolder) holder).status.setText("Vendor Reach location but Not Confirm by You");
                ((MyOrderHolder) holder).status.setTextColor(context.getResources().getColor(R.color.green));

            }else if (myServices.getStatus().equals("9")) {
                ((MyOrderHolder) holder).status.setText("Order Cancel due to Vendor not available Please wait we will contact you soon..");
                ((MyOrderHolder) holder).status.setTextColor(Color.RED);

            }else if (myServices.getStatus().equals("10")) {
                ((MyOrderHolder) holder).status.setText("Service request cancel due to service provider not available..! Please Select other or Please Try again..");
                ((MyOrderHolder) holder).status.setTextColor(Color.RED);
            }

            ((MyOrderHolder) holder).bind(myServices,onItemClickListner);


            ((MyOrderHolder) holder).amount.setText(myServices.getAmount());
            ((MyOrderHolder) holder).orderid.setText(myServices.getId());
            ((MyOrderHolder) holder).address.setText(myServices.getAddress());
            ((MyOrderHolder) holder).name.setText(myServices.getName());
            ((MyOrderHolder) holder).mobile.setText(myServices.getMobile());

            String service="";
            for(int j=0;j<myServices.getServices().size();j++) {
                if(j==0){
                    service =  myServices.getServices().get(j).getName();
                }else {
                    service = service + "," + myServices.getServices().get(j).getName();
                }
            }
            ((MyOrderHolder) holder).service.setText(service);


            ((MyOrderHolder) holder).date.setText(myServices.getDate());

        }



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyOrderHolder extends RecyclerView.ViewHolder {
        TextView status,date,amount,address,orderid,name,mobile,service;
        public MyOrderHolder(View v) {
            super(v);
            status=v.findViewById(R.id.status);
            amount=v.findViewById(R.id.amount);
            address=v.findViewById(R.id.address);
            orderid=v.findViewById(R.id.orderid);
            name=v.findViewById(R.id.name);
            date=v.findViewById(R.id.date);
            mobile=v.findViewById(R.id.mobile);
            service=v.findViewById(R.id.service);
        }

        public void bind(final MyServices data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);
                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
