package in.happyhelp.limra.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import in.happyhelp.limra.R;
import in.happyhelp.limra.model.WorkModel;
import in.happyhelp.limra.network.RestClient;

/**
 * Created by admin on 03-05-2017.
 */

public class WorkImagesAdapter extends RecyclerView.Adapter<WorkImagesAdapter.ViewHolder> {
    private Context context;
    private List<WorkModel> propertyImages=new ArrayList<>();

    OnItemClickListner listner;
    private int propertyId;

    public WorkImagesAdapter(Context context, OnItemClickListner onItemClickListner) {
        this.context = context;
        this.listner=onItemClickListner;

    }

    public void setData( List<WorkModel> propertyImages){
        this.propertyImages = propertyImages;
        notifyDataSetChanged();
    }

    @Override
    public WorkImagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_work_image, parent, false);
        return new ViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(final WorkImagesAdapter.ViewHolder holder, int position) {
        final WorkModel model = propertyImages.get(position);

        if(holder != null) {

            holder.bind(model,listner);


            if (model.isServer()) {
                Glide.with(this.context).load(RestClient.base_image_url + model.getUrl()).into(holder.propertyImage);
            } else {
                Glide.with(this.context).load(model.getUrl()).into(holder.propertyImage);
            }


        }
    }




    @Override
    public int getItemCount() {
        return propertyImages.size();
    }

    public interface OnItemClickListner {
        void onItemClick(WorkModel d, View view) throws ParseException;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView propertyImage, uploadImage,cancel;
        ProgressBar propertyImageProgress;

        public ViewHolder(View itemView, final int viewType) {
            super(itemView);
            propertyImage =  itemView.findViewById(R.id.property_image);
            propertyImageProgress =  itemView.findViewById(R.id.property_image_progress);
            uploadImage =  itemView.findViewById(R.id.upload_image);
            cancel=itemView.findViewById(R.id.cancel);


        }

        public void bind(final WorkModel data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);
                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }










}
