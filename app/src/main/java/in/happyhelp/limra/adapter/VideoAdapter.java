package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.videoresponse.Datum;
import in.happyhelp.limra.activity.video.VideoDetailActivity;

public class VideoAdapter  extends RecyclerView.Adapter {
    List<Datum> list=new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;

    public VideoAdapter(Context context) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setData(List<Datum> list){
        this.list=list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_video, viewGroup, false);
        return new VideoAdapter.VideoHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        final Datum video=list.get(i);
        if(holder instanceof VideoHolder){
            ((VideoAdapter.VideoHolder) holder).name.setText(video.getName());

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.isMemoryCacheable();
            Glide.with(context).setDefaultRequestOptions(requestOptions).load(getYoutubeThumbnailUrlFromVideoUrl(video.getYoutubeCode())).into(((VideoHolder) holder).video);

            ((VideoHolder) holder).view.setText("View "+video.getViews());
            ((VideoHolder) holder).duration.setText("Duration "+video.getDuration());
            ((VideoHolder) holder).description.setText("Description "+video.getDescription());

            ((VideoHolder) holder).videoPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, VideoDetailActivity.class);
                    intent.putExtra(Constants.VID,video.getId());
                    intent.putExtra(Constants.VURL,video.getYoutubeCode());
                    context.startActivity(intent);
                }
            });
        }
    }

    private static String getYoutubeThumbnailUrlFromVideoUrl(String videoUrl) {
        return "http://img.youtube.com/vi/"+getYoutubeVideoIdFromUrl(videoUrl) + "/0.jpg";
    }

    private static String getYoutubeVideoIdFromUrl(String inUrl) {
        inUrl = inUrl.replace("&feature=youtu.be", "");
        if (inUrl.toLowerCase().contains("youtu.be")) {
            return inUrl.substring(inUrl.lastIndexOf("/") + 1);
        }
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(inUrl);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VideoHolder extends RecyclerView.ViewHolder {
        TextView name,duration,description,view;
        ImageView video;
        LinearLayout videoPlay;

        VideoHolder(View v) {
            super(v);
            duration=v.findViewById(R.id.duration);
            description=v.findViewById(R.id.description);
            view=v.findViewById(R.id.view);
            name=v.findViewById(R.id.name);
            video=v.findViewById(R.id.video);
            videoPlay=v.findViewById(R.id.videoplay);
        }
    }
}
