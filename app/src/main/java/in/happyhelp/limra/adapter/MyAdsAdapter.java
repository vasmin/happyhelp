package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.myroresponse.MyRo;
import in.happyhelp.limra.activity.ro.MyRoDetailActivity;
import in.happyhelp.limra.myadsresponse.Data;
import in.happyhelp.limra.network.RestClient;

public class MyAdsAdapter extends RecyclerView.Adapter {

    Context context;
    LayoutInflater layoutInflater;
    List<Data> list=new ArrayList<>();

    public MyAdsAdapter(Context context) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setData(List<Data> list){
        this.list=list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_myads, viewGroup, false);
        return new MyAdsAdapter.AdsHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        final Data myRo=list.get(i);
        if(holder instanceof MyAdsAdapter.AdsHolder){

            ((AdsHolder) holder).name.setText(myRo.getSize());
            ((AdsHolder) holder).click.setText(myRo.getClickcount());
            ((AdsHolder) holder).days.setText(myRo.getDays()+" Days ");


            Glide.with(context)
                    .load(RestClient.base_image_url+myRo.getImage())
                    .into(((MyAdsAdapter.AdsHolder) holder).imageView);

            /*((MyAdsAdapter.AdsHolder) holder).cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!myRo.getMonth().equals("0")) {
                        Intent intent = new Intent(context, MyRoDetailActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(Constants.MYRO, myRo.getId());
                        context.startActivity(intent);
                    }
                }
            });*/
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    private class AdsHolder extends RecyclerView.ViewHolder {
        TextView name,click,days;
        ImageView imageView;
        CardView cardView;
        AdsHolder(View v) {
            super(v);

            click=v.findViewById(R.id.click);
            cardView=v.findViewById(R.id.card);
            name=v.findViewById(R.id.type);
            imageView=v.findViewById(R.id.image);
            days=v.findViewById(R.id.days);
        }


    }
}
