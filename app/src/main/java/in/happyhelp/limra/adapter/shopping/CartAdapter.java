package in.happyhelp.limra.adapter.shopping;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.R;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.model.shopping.CartItem;
import in.happyhelp.limra.model.shopping.WishList;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.productresponse.Product;
import io.realm.Realm;

public class CartAdapter extends RecyclerView.Adapter {

    Context context;
    LayoutInflater layoutInflater;
    OnItemClickListner onItemClickListner;
    List<CartItem> list=new ArrayList<>();

    public CartAdapter(Context context, OnItemClickListner onItemClickListner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.onItemClickListner=onItemClickListner;

    }

    public void setData(List<CartItem> list){
        this.list=list;
        notifyDataSetChanged();
    }

    public interface OnItemClickListner {
        void onItemClick(CartItem d, View view) throws ParseException;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_cart, viewGroup, false);
        return new CartAdapter.CartHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int i) {

        final CartItem cartItem=list.get(i);
        if(holder instanceof CartHolder){

            final Realm realm=RealmHelper.getRealmInstance();

            Glide.with(context)
                    .load(RestClient.base_image_url+cartItem.getImage())
                    .into(((CartHolder) holder).imageView);

            ((CartHolder) holder).price.setText("\u20B9 "+cartItem.getPrice());
            ((CartHolder) holder).name.setText(cartItem.getName());
            ((CartHolder) holder).count.setText(cartItem.getQty());

            if(cartItem.getColor()!=null)
            if(!cartItem.getColor().equals("Select Color")){
                ((CartHolder) holder).colorlinear.setVisibility(View.VISIBLE);
            }else{
                ((CartHolder) holder).colorlinear.setVisibility(View.GONE);
            }

            ((CartHolder) holder).size.setText(cartItem.getSize());
            ((CartHolder) holder).color.setText(cartItem.getColor());

            ((CartHolder) holder).bind(cartItem,onItemClickListner);

            /*((CartHolder) holder).increment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int counter= Integer.parseInt(cartItem.getQty());
                    int baseamount= Integer.parseInt(cartItem.getPrice());

                    if(baseamount>1){
                        counter++;
                        ((CartHolder) holder).count.setText(String.valueOf(counter));
                        cartItem.setQty(String.valueOf(counter));
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(cartItem);
                            }
                        });
                        int amount = counter*baseamount;

                    }
                }
            });

            ((CartHolder) holder).decrement.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int counter= Integer.parseInt(cartItem.getQty());
                    int baseamount= Integer.parseInt(cartItem.getPrice());

                    if(baseamount>1 && counter > 1){
                        counter--;
                        ((CartHolder) holder).count.setText(String.valueOf(counter));
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(cartItem);
                            }
                        });
                        //  int amount = amount-baseamount;
                    }
                }
            });*/


        }

    }




    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CartHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView imageView;

        @BindView(R.id.colorlinear)
        LinearLayout colorlinear;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.price)
        TextView price;

        @BindView(R.id.color)
        TextView color;

        @BindView(R.id.size)
        TextView size;

        @BindView(R.id.count)
        TextView count;

        @BindView(R.id.increment)
        ImageView increment;

        @BindView(R.id.decrement)
        ImageView decrement;


        public CartHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }


        public void bind(final CartItem data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
