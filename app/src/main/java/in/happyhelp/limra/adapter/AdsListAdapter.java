package in.happyhelp.limra.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import in.happyhelp.limra.R;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.adslistresponse.Datum;

public class AdsListAdapter extends RecyclerView.Adapter {

    Context context;
    LayoutInflater layoutInflater;
    List<Datum> list=new ArrayList<>();
    OnItemClickListner listner;
    public AdsListAdapter(Context context, OnItemClickListner listner) {
        this.context=context;
        this.layoutInflater = LayoutInflater.from(context);
        this.listner=listner;
    }

    public void setData(List<Datum> list){
        this.list=list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_ads, viewGroup, false);
        return new AdsListAdapter.AdsHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        Datum datum=list.get(i);
        if(holder instanceof AdsHolder){
            Glide.with(context)
                    .load(RestClient.base_image_url+datum.getImage())
                    .into(((AdsHolder) holder).imageView);


            ((AdsHolder) holder).bind(datum,listner);




        }
    }

    public interface OnItemClickListner {
        void onItemClick(Datum d, View view) throws ParseException;

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class AdsHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        AdsHolder(View v) {
            super(v);
            imageView=v.findViewById(R.id.adsbanner);

        }

        public void bind(final Datum data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }


}
