package in.happyhelp.limra.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.R;
import in.happyhelp.limra.membershipresponse.Membership;
import in.happyhelp.limra.model.ServiceModel;
import in.happyhelp.limra.network.RestClient;

public class MemberAdapter extends RecyclerView.Adapter {

    List<Membership> list=new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;
    OnItemClickListner onItemClickListner;

    public MemberAdapter(Context context, OnItemClickListner onItemClickListner) {
        this.context=context;
        this.onItemClickListner=onItemClickListner;
    }

    public void setData(List<Membership> list){
        this.list=list;
        this.layoutInflater = LayoutInflater.from(context);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_membership, viewGroup, false);
        return new MemberAdapter.MemberHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        Membership membership=list.get(i);
        if(viewHolder instanceof MemberHolder){

            Glide.with(context)
            .load(RestClient.base_image_url+membership.getImage())
            .into(((MemberHolder) viewHolder).imageView);

            ((MemberHolder) viewHolder).name.setText(membership.getProductName());
            ((MemberHolder) viewHolder).price.setText("\u20B9 "+membership.getPrice());
            ((MemberHolder) viewHolder).cashback.setText("\u20B9 "+membership.getCashbackValue());
            ((MemberHolder) viewHolder).use.setText(membership.getUsevalue()+" %");


            ((MemberHolder) viewHolder).bind(membership,onItemClickListner);

            if(membership.isSelected()){
                ((MemberHolder) viewHolder).cardView.setCardBackgroundColor(Color.parseColor("#B3E5FC"));
            }else{
                ((MemberHolder) viewHolder).cardView.setCardBackgroundColor(Color.WHITE);
            }

        }
    }

    public interface OnItemClickListner {
        void onItemClick(Membership d, View view) throws ParseException;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MemberHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView imageView;

        @BindView(R.id.card)
        CardView cardView;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.price)
        TextView price;

        @BindView(R.id.cashbackvalue)
        TextView cashback;

        @BindView(R.id.use)
        TextView use;

        public MemberHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }


        public void bind(final Membership data, final OnItemClickListner listner) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        listner.onItemClick(data, v);

                    }
                    catch (ParseException e) {

                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
