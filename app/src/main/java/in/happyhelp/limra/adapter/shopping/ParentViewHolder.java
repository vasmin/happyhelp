package in.happyhelp.limra.adapter.shopping;

import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import in.happyhelp.limra.R;
import in.happyhelp.limra.model.shopping.SubCat;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class ParentViewHolder extends GroupViewHolder {

  private TextView genreName;
  private ImageView arrow;

  public ParentViewHolder(View itemView) {
    super(itemView);
    genreName = (TextView) itemView.findViewById(R.id.name);
    arrow = (ImageView) itemView.findViewById(R.id.list_item_genre_arrow);
  }

  public void setParentName(ExpandableGroup genre) {
    if (genre instanceof SubCat) {
      genreName.setText(genre.getTitle());
//      icon.setBackgroundResource(((SubCat) genre).getIconResId());
    }

  }

  @Override
  public void expand() {
    animateExpand();
  }

  @Override
  public void collapse() {
    animateCollapse();
  }

  private void animateExpand() {
    RotateAnimation rotate =
        new RotateAnimation(360, 270, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
    rotate.setDuration(300);
    rotate.setFillAfter(true);
    arrow.setAnimation(rotate);
  }

  private void animateCollapse() {
    RotateAnimation rotate = new RotateAnimation(270, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
    rotate.setDuration(300);
    rotate.setFillAfter(true);
    arrow.setAnimation(rotate);
  }
}
