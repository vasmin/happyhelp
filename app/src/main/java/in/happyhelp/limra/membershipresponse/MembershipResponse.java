
package in.happyhelp.limra.membershipresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MembershipResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("membership")
    @Expose
    private List<Membership> membership = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Membership> getMembership() {
        return membership;
    }

    public void setMembership(List<Membership> membership) {
        this.membership = membership;
    }

}
