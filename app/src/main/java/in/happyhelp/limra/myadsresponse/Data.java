package in.happyhelp.limra.myadsresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("clickcount")
    @Expose
    private String clickcount;


    @SerializedName("business_name")
    @Expose
    private String business_name;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("payment_status")
    @Expose
    private String payment_status;

    @SerializedName("mobile")
    @Expose
    private String mobile;

    @SerializedName("gst")
    @Expose
    private String gst;


    @SerializedName("title")
    @Expose
    private String title;


    @SerializedName("type")
    @Expose
    private String type;


    @SerializedName("startdate")
    @Expose
    private String startdate;


    @SerializedName("userid")
    @Expose
    private String userid;


    @SerializedName("total")
    @Expose
    private String total;


    @SerializedName("enddate")
    @Expose
    private String enddate;


    @SerializedName("website_url")
    @Expose
    private String website_url;


    @SerializedName("size")
    @Expose
    private String size;


    @SerializedName("subtotal")
    @Expose
    private String subtotal;


    @SerializedName("name")
    @Expose
    private String name;


    @SerializedName("days")
    @Expose
    private String days;


    @SerializedName("id")
    @Expose
    private String id;


    @SerializedName("category")
    @Expose
    private String category;


    @SerializedName("email")
    @Expose
    private String email;


    @SerializedName("status")
    @Expose
    private String status;

    public String getDate ()
    {
        return date;
    }

    public void setDate (String date)
    {
        this.date = date;
    }

    public String getClickcount ()
    {
        return clickcount;
    }

    public void setClickcount (String clickcount)
    {
        this.clickcount = clickcount;
    }

    public String getBusiness_name ()
    {
        return business_name;
    }

    public void setBusiness_name (String business_name)
    {
        this.business_name = business_name;
    }

    public String getImage ()
    {
        return image;
    }

    public void setImage (String image)
    {
        this.image = image;
    }

    public String getPayment_status ()
    {
        return payment_status;
    }

    public void setPayment_status (String payment_status)
    {
        this.payment_status = payment_status;
    }

    public String getMobile ()
    {
        return mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    public String getGst ()
    {
        return gst;
    }

    public void setGst (String gst)
    {
        this.gst = gst;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getStartdate ()
    {
        return startdate;
    }

    public void setStartdate (String startdate)
    {
        this.startdate = startdate;
    }

    public String getUserid ()
    {
        return userid;
    }

    public void setUserid (String userid)
    {
        this.userid = userid;
    }

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

    public String getEnddate ()
    {
        return enddate;
    }

    public void setEnddate (String enddate)
    {
        this.enddate = enddate;
    }

    public String getWebsite_url ()
    {
        return website_url;
    }

    public void setWebsite_url (String website_url)
    {
        this.website_url = website_url;
    }

    public String getSize ()
    {
        return size;
    }

    public void setSize (String size)
    {
        this.size = size;
    }

    public String getSubtotal ()
    {
        return subtotal;
    }

    public void setSubtotal (String subtotal)
    {
        this.subtotal = subtotal;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getDays ()
    {
        return days;
    }

    public void setDays (String days)
    {
        this.days = days;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getCategory ()
    {
        return category;
    }

    public void setCategory (String category)
    {
        this.category = category;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }


}

