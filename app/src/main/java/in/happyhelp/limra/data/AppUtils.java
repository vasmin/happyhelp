package in.happyhelp.limra.data;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.LoginActivity;
import in.happyhelp.limra.activity.SplashActivity;
import in.happyhelp.limra.activity.service.OrderDetailsActivity;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.myservicesresponse.MyServices;
import in.happyhelp.limra.activity.response.myservicesresponse.MyServicesResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AppUtils {


    static DialogPlus dialog;
    static TextView decline,accept;
    static TextView serviceName,date;
    static TextView serviceAddress,serviceHours;
    static TextView serviceamt,vendorAmt,quantity,penalty;
    static LinearLayout penaltyLinear;
    static TextView duration;
    String Id="";
    private static final int REQUEST_PERMISSION = 123;
    public static String dateFormater(String dateFromJSON, String expectedFormat, String oldFormat) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat);
        Date date = null;
        String convertedDate = null;
        try {
            date = dateFormat.parse(dateFromJSON);
            @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(expectedFormat);
            convertedDate = simpleDateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertedDate;
    }

    public static int calculateNoOfColumns(Context context, float columnWidthDp) { // For example columnWidthdp=180
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (screenWidthDp / columnWidthDp + 0.5); // +0.5 for correct rounding to int.
        return noOfColumns;
    }

    public static int numDaysBetween(final Calendar c, String start, String end) {
        long startDate = 0,endDate = 0;
        try {

            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date startdate = sdf.parse(start);
            Date enddate=sdf.parse(end);
            startDate = startdate.getTime();
            endDate = enddate.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        int result = 0;
        if (startDate <= endDate) {
            return result;
        }

        c.setTimeInMillis(startDate);
        final int toYear = c.get(Calendar.YEAR);
        result += c.get(Calendar.DAY_OF_YEAR);

        c.setTimeInMillis(endDate);
        result -= c.get(Calendar.DAY_OF_YEAR);

        while (c.get(Calendar.YEAR) < toYear) {
            result += c.getActualMaximum(Calendar.DAY_OF_YEAR);
            c.add(Calendar.YEAR, 1);
        }
        return result;
    }

    private static void checkNetworkConnection(Activity activity){
        AlertDialog.Builder builder =new AlertDialog.Builder(activity);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static boolean isNetworkConnectionAvailable(Activity activity){
        ConnectivityManager cm =
                (ConnectivityManager)activity.getSystemService(activity.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if(isConnected) {
            Log.d("Network", "Connected");
            return true;
        }
        else{
            checkNetworkConnection(activity);
            Log.d("Network","Not Connected");
            return false;
        }
    }

    public static   boolean checkAndRequestPermissions(Activity activity) {
        int permissionSendMessage = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.SEND_SMS);
        int locationPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_PERMISSION);
            return false;
        }
        return true;
    }

    public static  void startPickImageDialog(final int GALLERY_PICTURE, final int CAMERA_REQUEST, final String cameraPath, final Activity activity) {
        final AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(
                activity);
        myAlertDialog.setTitle("Upload Pictures Option");
        myAlertDialog.setMessage("How do you want to set your picture?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent pictureActionIntent;

                        pictureActionIntent = new Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        activity.startActivityForResult(
                                pictureActionIntent,
                                GALLERY_PICTURE);


                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface arg0, int arg1) {
//                        Intent i =new Intent(fragment.activity,MainCameraActivity.class);
//                        i.putExtra(Keys.Arguments.camera_path,cameraPath);
//                        fragment.startActivityForResult(i,CAMERA_REQUEST);
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(activity.getApplicationContext(),activity.getPackageName()+".provider",new File(cameraPath)));

                        activity.startActivityForResult(intent,
                                CAMERA_REQUEST,new Bundle());


                    }
                });
        myAlertDialog.show();

    }

    public static String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        return date;
    }

    public static boolean isValidMail(String email, TextView txtEmail) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email);
        check = m.matches();

        if(!check) {
            txtEmail.setError("Not Valid Email");

        }
        return check;
    }

    public static  boolean isBankAccount(String account, TextView bankAcc){
        boolean check;
        Pattern p;
        Matcher m;

        String ACCOUNTSTRING = "[0-9]{9,18}";

        p = Pattern.compile(ACCOUNTSTRING);

        m = p.matcher(account);
        check = m.matches();

        if(!check) {
            bankAcc.setError("Not Valid ACCOUNT");

        }
        return check;

    }





//TODO for website validation

    public static boolean isValidURL(String web)
    {
        String WebUrl = "^((ftp|http|https):\\/\\/)?(www.)?(?!.*(ftp|http|https|www.))[a-zA-Z0-9_-]+(\\.[a-zA-Z]+)+((\\/)[\\w#]+)*(\\/\\w+\\?[a-zA-Z0-9_]+=\\w+(&[a-zA-Z0-9_]+=\\w+)*)?$";
        String website = web.trim();
        if (website.trim().length() > 0) {
            if (!website.matches(WebUrl)) {
                //validation msg
                return false;
            }
        }
        return true;

    }


    public static void logout(Context context) {
        if(SharedPreferenceHelper.getInstance(context).isLoggerIn()) {
            SharedPreferenceHelper.getInstance(context).setUserLoggedIn(false);
            Intent intent = new Intent(context, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        }
    }


    public static boolean isValidMobile(String phone) {
        if (phone.length()!=10) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(phone).matches();
        }
    }


    public static String getFilePathFromContentUri(Uri selectedVideoUri,
                                                   ContentResolver contentResolver) {
        String filePath;
        String[] filePathColumn = {MediaStore.MediaColumns.DATA};

        Cursor cursor = contentResolver.query(selectedVideoUri, filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        filePath = cursor.getString(columnIndex);
        cursor.close();
        return filePath;
    }

    public static void getMyServiceEnquiryDetail(final Activity activity, String id){

        HashMap<String ,String> hashMap=new HashMap<>();
        hashMap.put("id",id);
        Call<MyServicesResponse> call=RestClient.get().myServicesEnquiryDetails(SharedPreferenceHelper.getInstance(activity).getAuthToken(),hashMap);
        call.enqueue(new Callback<MyServicesResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<MyServicesResponse> call, @NonNull Response<MyServicesResponse> response) {
                MyServicesResponse myServicesResponse = response.body();

                if (response.code() == 200) {
                    if (myServicesResponse.getStatus()) {
                        if (myServicesResponse.getData().size() > 0) {
                            MyServices myServices = myServicesResponse.getData().get(0);
                            String service="";
                            for(int j=0;j<myServices.getServices().size();j++) {
                                if(j==0){
                                    service =  myServices.getServices().get(j).getName();
                                }else {
                                    service = service + "," + myServices.getServices().get(j).getName();
                                }
                            }
                            serviceName.setText(service);
                            serviceAddress.setText(myServices.getLocation());

                            serviceHours.setText(myServices.getHour());

                            if(myServices.getHour().equals("48")){
                                serviceHours.setText(" Schedule Service");
                            }else{
                                serviceHours.setText(myServices.getHour());
                            }

                            duration.setText(myServices.getHour());
                            date.setText(myServices.getDate()+" "+myServices.getTime());
                            Log.e("Service Amount" ,"is "+myServices.getAmount());
                            serviceamt.setText("Rs "+ myServices.getAmount());
                            vendorAmt.setText("Rs "+myServices.getVendorAmount());
                            quantity.setText(myServices.getQuantity());
                            if(myServices.getPenalty().equals("") || myServices.getPenalty()==null){
                                penaltyLinear.setVisibility(View.GONE);
                            }else{
                                penaltyLinear.setVisibility(View.VISIBLE);
                                penalty.setText(myServices.getPenalty()+" "+myServices.getWaiting());
                            }

                        } else {
                            Toast.makeText(activity, Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                    }
                    } else {
                        Toast.makeText(activity, Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(activity, Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<MyServicesResponse> call, @NonNull Throwable t) {
                Toast.makeText(activity,Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();

                t.printStackTrace();
            }
        });
    }

    public static void grantUriPermission(Activity ctx, Intent intent, Uri uri, int permissionFlags) {
        List<ResolveInfo> resolvedIntentActivities = ctx.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

        for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
            String packageName = resolvedIntentInfo.activityInfo.packageName;

            ctx.grantUriPermission(packageName, uri, permissionFlags);
        }
    }


    public static void setDialog(final Activity activity, final String Id) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService( activity.LAYOUT_INFLATER_SERVICE );
        View view = inflater.inflate(R.layout.service_pop, null);
        dialog = DialogPlus.newDialog(activity)
                .setContentHolder(new ViewHolder(view))
                .setGravity(Gravity.CENTER)
                .setCancelable(false)
                .setInAnimation(R.anim.abc_fade_in)
                .setMargin(50, 20, 50, 50)
                .setOutAnimation(R.anim.abc_fade_out)
                .setOverlayBackgroundResource(Color.TRANSPARENT)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogPlus dialog) {
                        dialog.dismiss();
                    }
                })
                .create();

        serviceName= (TextView) dialog.findViewById(R.id.servicename);
        serviceAddress= (TextView) dialog.findViewById(R.id.serviceaddress);
        serviceamt= (TextView) dialog.findViewById(R.id.serviceamt);
        vendorAmt= (TextView) dialog.findViewById(R.id.vendoramt);
        duration= (TextView) dialog.findViewById(R.id.duration);
        penalty= (TextView) dialog.findViewById(R.id.penalty);
        penaltyLinear= (LinearLayout) dialog.findViewById(R.id.penaltylinear);
        date= (TextView) dialog.findViewById(R.id.date);
        serviceHours= (TextView) dialog.findViewById(R.id.servicehours);
        quantity= (TextView) dialog.findViewById(R.id.quantity);

        decline= (TextView) dialog.findViewById(R.id.decline);
        accept= (TextView) dialog.findViewById(R.id.accept);

        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptService(activity,Id);
            }
        });

        dialog.show();


    }





    public static  String getCompleteAddressString(Context context, double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        if(geocoder!=null)
            try {
                List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
                if (addresses != null) {
                    Address returnedAddress = addresses.get(0);
                    StringBuilder strReturnedAddress = new StringBuilder("");

                    for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                    }
                    strAdd = strReturnedAddress.toString();
                } else {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        return strAdd;
    }

    private static void acceptService(final Activity activity, String Id){
        @SuppressLint("SimpleDateFormat") java.text.DateFormat time = new SimpleDateFormat("hh:mm a");
        String currentTime = time.format(Calendar.getInstance().getTime());
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",Id);
        hashMap.put("userid",SharedPreferenceHelper.getInstance(activity).getString(Constants.USERID));
        hashMap.put("accept_time",currentTime);
        hashMap.put("serviceid",SharedPreferenceHelper.getInstance(activity).getString(Constants.COMPANYCODE));
        hashMap.put("status","1");
        Call<in.happyhelp.limra.activity.response.Response> call= RestClient.get().acceptService(SharedPreferenceHelper.getInstance(activity).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {

                if(response.code()==200){
                    in.happyhelp.limra.activity.response.Response response1 =response.body();
                    if(response1.getStatus()) {
                        Toast.makeText(activity, response1.getMessage(), Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                    }else{
                        dialog.dismiss();
                        Toast.makeText(activity, response1.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(activity);
                }
            }

            @Override
            public void onFailure(Call<in.happyhelp.limra.activity.response.Response> call, Throwable t) {
                t.printStackTrace();
                dialog.dismiss();
                Toast.makeText(activity, Constants.SERVERTIMEOUT, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
