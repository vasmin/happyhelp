package in.happyhelp.limra.data;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by bhushan on 3/31/2017.
 */

public class RealmHelper {



    public static Realm getRealmInstance() {
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        return Realm.getInstance(config);
    }



}
