package in.happyhelp.limra.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import in.happyhelp.limra.Constants;


public class SharedPreferenceHelper {
    private static SharedPreferenceHelper instance = null;
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;
    private static String SHARE_USER_INFO = "userinfo";
    private static String SHARE_KEY_NAME = "name";
    private static String SHARE_KEY_EMAIL = "email";
    private static String SHARE_KEY_AVATA = "avata";
    private static String SHARE_KEY_UID = "uid";


    private SharedPreferenceHelper() {}

    @SuppressLint("CommitPrefEdits")
    public static SharedPreferenceHelper getInstance(Context context) {
        if (instance == null) {
            instance = new SharedPreferenceHelper();
            preferences = context.getSharedPreferences(SHARE_USER_INFO, Context.MODE_PRIVATE);
            editor = preferences.edit();
        }
        return instance;
    }

    public void saveAuthToken(String authToken){
        editor.putString(Constants.AuthToken,authToken);
        editor.apply();
    }

    public String getAuthToken(){
        return String.format("bearer %s",preferences.getString(Constants.AuthToken,null));
    }

    public void setUserLoggedIn(boolean bool){
        editor.putBoolean(Constants.isLoggedIn,bool);
        editor.apply();
    }


    public boolean isCompanyProfile(){
        return preferences.getBoolean(Constants.CompanyProfile,false);
    }

    public void setCompanyProfile(boolean bool){
        editor.putBoolean(Constants.CompanyProfile,bool);
        editor.apply();
    }



    public boolean isPropertyCompanyProfile(){
        return preferences.getBoolean(Constants.CompanyPropertyProfile,false);
    }

    public void setPropertyCompanyProfile(boolean bool){
        editor.putBoolean(Constants.CompanyPropertyProfile,bool);
        editor.apply();
    }




    public boolean isLoggerIn(){
        return preferences.getBoolean(Constants.isLoggedIn,false);
    }

    public void logout() {
        editor.clear();
        editor.apply();
    }

    public void saveCurrentCart(String cartID) {
        editor.putString(Constants.CARTID,cartID);
        editor.apply();
    }

    public String getCurrentCartID() {
        return preferences.getString(Constants.CARTID,null);
    }

    public void removeCartID(){
        editor.putString(Constants.CARTID,null);
        editor.apply();
    }

    public void saveLastDate(int lastDate){
        editor.putInt(Constants.LASTDATE,lastDate);
        editor.apply();
    }

    public int getLastDate() {
        return preferences.getInt(Constants.LASTDATE,0);
    }

    public int getCurrentIndex() {
        return preferences.getInt(Constants.CURRENTINDEX,0);
    }

    public void setCurrentIndex(int index){
        editor.putInt(Constants.CURRENTINDEX, index);
        editor.apply();
    }

    public void putString(String key, String value){
        editor.putString(key,value);
        editor.apply();
    }
    public String getString(String key){
        return  preferences.getString(key,null);
    }

    public  void setCategory(int[] arrayList){
        editor.putInt(Constants.CATEGORYID1,arrayList[0]);
        editor.putInt(Constants.CATEGORYID2,arrayList[1]);
        editor.putInt(Constants.CATEGORYID3,arrayList[2]);
        editor.apply();

    }
    public  int[] getCategory(){
        int[] array=new int[3];
        array[0]=preferences.getInt(Constants.CATEGORYID1,0);
        array[1]=preferences.getInt(Constants.CATEGORYID2,0);
        array[2]=preferences.getInt(Constants.CATEGORYID3,0);

        return array;


    }
}
