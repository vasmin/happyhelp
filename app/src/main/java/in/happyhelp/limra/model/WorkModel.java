package in.happyhelp.limra.model;

public class WorkModel {


    String url;
    boolean isServer;
    boolean newImage;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isServer() {
        return isServer;
    }

    public void setServer(boolean server) {
        isServer = server;
    }
    public boolean isNewImage() {
        return newImage;
    }

    public void setNewImage(boolean newImage) {
        this.newImage = newImage;
    }
}
