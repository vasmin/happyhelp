package in.happyhelp.limra.model;

public class ServiceModel  {
    String id;
    String serviceName;
    String hours;
    String rps;
    boolean selected=false;
    String vendor_amount;
    String image;
    String level;
    String subId;
    String visitingAmt;
    String service;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getRps() {
        return rps;
    }

    public void setRps(String rps) {
        this.rps = rps;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getVendor_amount() {
        return vendor_amount;
    }

    public void setVendor_amount(String vendor_amount) {
        this.vendor_amount = vendor_amount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public String getVisitingAmt() {
        return visitingAmt;
    }

    public void setVisitingAmt(String visitingAmt) {
        this.visitingAmt = visitingAmt;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }
}
