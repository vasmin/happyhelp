package in.happyhelp.limra.model;

import com.google.firebase.messaging.RemoteMessage;

public class NotificationEvent {
    private String body;
    RemoteMessage remoteMessage;

    public NotificationEvent(String body,RemoteMessage remoteMessage) {
        this.body = body;
        this.remoteMessage=remoteMessage;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public RemoteMessage getRemoteMessage() {
        return remoteMessage;
    }

    public void setRemoteMessage(RemoteMessage remoteMessage) {
        this.remoteMessage = remoteMessage;
    }
}
