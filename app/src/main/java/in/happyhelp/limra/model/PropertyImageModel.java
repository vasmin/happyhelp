package in.happyhelp.limra.model;

public class PropertyImageModel {


    String url;
    boolean newImage;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isNewImage() {
        return newImage;
    }

    public void setNewImage(boolean newImage) {
        this.newImage = newImage;
    }
}
