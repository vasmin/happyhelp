package in.happyhelp.limra.model.shopping;

import android.os.Parcel;
import android.os.Parcelable;

import in.happyhelp.limra.activity.response.statecityresponse.StateCityResponse;

public class SubSubCat implements Parcelable {

  String id;
  private String name;
  private boolean isFavorite;

  public SubSubCat(String id,String name, boolean isFavorite) {
    this.name = name;
    this.id=id;
    this.isFavorite = isFavorite;
  }

  protected SubSubCat(Parcel in) {
    name = in.readString();
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setFavorite(boolean favorite) {
    isFavorite = favorite;
  }

  public String getName() {
    return name;
  }

  public boolean isFavorite() {
    return isFavorite;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof SubSubCat)) return false;

    SubSubCat subSubCat = (SubSubCat) o;

    if (isFavorite() != subSubCat.isFavorite()) return false;
    return getName() != null ? getName().equals(subSubCat.getName()) : subSubCat.getName() == null;

  }

  @Override
  public int hashCode() {
    int result = getName() != null ? getName().hashCode() : 0;
    result = 31 * result + (isFavorite() ? 1 : 0);
    return result;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(name);
  }

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Creator<SubSubCat> CREATOR = new Creator<SubSubCat>() {
    @Override
    public SubSubCat createFromParcel(Parcel in) {
      return new SubSubCat(in);
    }

    @Override
    public SubSubCat[] newArray(int size) {
      return new SubSubCat[size];
    }
  };
}

