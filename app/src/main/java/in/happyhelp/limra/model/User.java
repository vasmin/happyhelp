package in.happyhelp.limra.model;

import android.support.annotation.NonNull;

public class User {
    private String id, name, text;
    private User[] user;
    private int count;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User[] getUser() {
        return user;
    }

    public void setUser(User[] user) {
        this.user = user;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @NonNull
    @Override
    public String toString() {
//        String str = "[" + id + ", " + name + ", " + text + ", " + count + ", users => ";
//
//        User[] user1 = getUser();
//        for (int i = 0; i < user1.length; i++) {
//            str += " \n***** " + user1[i];
//        }
//        str += "]";

//        return str;
        return "[" + id + ", " + name + ", " + text + ", " + count + "]";
    }
}
