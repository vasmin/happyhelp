package in.happyhelp.limra.model.shopping;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class SubCat extends ExpandableGroup<SubSubCat> {

  private int iconResId;

  public SubCat(String title, List<SubSubCat> items, int iconResId) {
    super(title, items);
    this.iconResId = iconResId;
  }

  public int getIconResId() {
    return iconResId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof SubCat)) return false;

    SubCat subCat = (SubCat) o;

    return getIconResId() == subCat.getIconResId();

  }

  @Override
  public int hashCode() {
    return getIconResId();
  }
}

