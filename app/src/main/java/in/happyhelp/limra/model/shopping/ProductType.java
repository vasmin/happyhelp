package in.happyhelp.limra.model.shopping;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ProductType extends RealmObject {
    @PrimaryKey
    String id;
    String size;
    String color;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
