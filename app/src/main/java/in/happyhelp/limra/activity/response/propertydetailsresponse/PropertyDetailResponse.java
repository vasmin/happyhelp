
package in.happyhelp.limra.activity.response.propertydetailsresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PropertyDetailResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("Property_details")
    @Expose
    private PropertyDetails propertyDetails;

    @SerializedName("is_my_property")
    @Expose
    private String is_my_property;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public PropertyDetails getPropertyDetails() {
        return propertyDetails;
    }

    public void setPropertyDetails(PropertyDetails propertyDetails) {
        this.propertyDetails = propertyDetails;
    }

    public String getIs_my_property() {
        return is_my_property;
    }

    public void setIs_my_property(String is_my_property) {
        this.is_my_property = is_my_property;
    }
}
