package in.happyhelp.limra.activity.wallet;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.HomeActivity;
import in.happyhelp.limra.activity.ads.AdsActivity;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.receiverresponse.ReceiverInfoResponse;
import in.happyhelp.limra.tdsresponse.TdsResponse;
import retrofit2.Call;
import retrofit2.Callback;

public class SendWalletActivity extends AppCompatActivity {

    @BindView(R.id.submit)
    TextView submit;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.amount)
    EditText amount;

    @BindView(R.id.mainwallet)
    RadioButton mainWallet;

    @BindView(R.id.cashbackwallet)
    RadioButton cashbackwallet;

    int walletType=0;
    int tds=0;
    ProgressDialog progressBar;

    @BindView(R.id.tdscharges)
    TextView tdsCharge;
    String totalamount="";

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.email)
    TextView email;

    @BindView(R.id.linear)
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_wallet);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        AppUtils.isNetworkConnectionAvailable(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);


        mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()>8){
                    getUserInfos(mobile.getText().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(SendWalletActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Happy help")
                        .setMessage("Are you sure you want send money ?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (TextUtils.isEmpty(mobile.getText().toString())) {
                                    mobile.setError("Enter mobile number ");
                                    mobile.requestFocus();
                                    return;
                                }

                                if(!AppUtils.isValidMobile(mobile.getText().toString())){
                                    mobile.requestFocus();
                                    mobile.setError("Enter valid mobile");
                                    return;

                                }

                                if (TextUtils.isEmpty(amount.getText().toString())) {
                                    amount.setError("Enter amount ");
                                    amount.requestFocus();
                                    return;
                                }


                                double result = (double) ((Integer.parseInt(amount.getText().toString().trim()) * tds) / 100);
                                totalamount= String.valueOf(Double.parseDouble(amount.getText().toString().trim())-result);

                                sendMoney();

                            }

                        })
                        .setNegativeButton("No", null)
                        .show();

            }
        });

        mainWallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    walletType=0;
                }else{
                    walletType=1;
                }
            }
        });


        cashbackwallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    walletType=1;
                }else{
                    walletType=0;
                }
            }
        });

        getTdsRate();

    }


    public void getUserInfos(String mobile){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("mobile_no",mobile);
        Call<ReceiverInfoResponse> call=RestClient.get().getMobileNumber(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<ReceiverInfoResponse>() {
            @Override
            public void onResponse(Call<ReceiverInfoResponse> call, retrofit2.Response<ReceiverInfoResponse> response) {
                ReceiverInfoResponse receiverInfoResponse=response.body();
                if(response.code()==200){
                    if(receiverInfoResponse.getStatus()){
                        if(receiverInfoResponse.getData().size()>0) {
                            linearLayout.setVisibility(View.VISIBLE);
                            name.setText(receiverInfoResponse.getData().get(0).getName());
                            email.setText(receiverInfoResponse.getData().get(0).getEmail());
                        }
                    }else{

                    }
                }else if(response.code()==401){
                    AppUtils.logout(getApplicationContext());
                }else{

                }
            }

            @Override
            public void onFailure(Call<ReceiverInfoResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getTdsRate(){
        Call<TdsResponse> call=RestClient.get().getTdsCharge(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<TdsResponse>() {
            @Override
            public void onResponse(Call<TdsResponse> call, retrofit2.Response<TdsResponse> response) {

                TdsResponse tdsResponse=response.body();
                if(response.code()==200){
                    if(tdsResponse.getStatus()){
                        if(tdsResponse.getTds().size()>0) {
                            tdsCharge.setText(tdsResponse.getTds().get(0).getName()+"%");
                            try {
                               // tds = Integer.parseInt(tdsResponse.getTds().get(0).getName());
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<TdsResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void sendMoney(){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("mobile_no",mobile.getText().toString());
        hashMap.put("amount",totalamount);
        hashMap.put("wallet_type",String.valueOf(walletType));
        Call<Response> call= RestClient.get().sendMoney(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Response response1=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(response1.getStatus()) {
                        new AlertDialog.Builder(SendWalletActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle("Money transfer succesfully")
                                .setMessage(response1.getMessage())
                                .setPositiveButton("Ok", null)
                                .show();
                        mobile.setText("");
                        amount.setText("");
                        mainWallet.setChecked(true);
                    }else{
                        new AlertDialog.Builder(SendWalletActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle("Money transfer failed")
                                .setMessage(response1.getMessage())
                                .setPositiveButton("Ok", null)
                                .show();
                        mobile.setText("");
                        amount.setText("");
                        mainWallet.setChecked(true);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getApplicationContext());

                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
