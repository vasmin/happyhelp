package in.happyhelp.limra.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.Jobs.MyJobActivity;
import in.happyhelp.limra.activity.ads.MyAdsActivity;
import in.happyhelp.limra.activity.donation.MyDonationActivity;
import in.happyhelp.limra.activity.patient.MyPatientActivity;
import in.happyhelp.limra.activity.ro.MyRoActivity;
import in.happyhelp.limra.activity.service.MyServicesActivity;
import in.happyhelp.limra.activity.video.MyVideoActivity;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.model.NotificationEvent;

public class MyAccountActivity extends AppCompatActivity {

    @BindView(R.id.myservice)
    LinearLayout myServices;

    @BindView(R.id.myro)
    LinearLayout myRo;

    @BindView(R.id.myproperty)
    LinearLayout myProperty;

    @BindView(R.id.myvideo)
    LinearLayout myVideo;

    @BindView(R.id.myjob)
    LinearLayout myJob;

    @BindView(R.id.mydonation)
    LinearLayout myDonation;

    @BindView(R.id.myorder)
    LinearLayout myOrder;

    @BindView(R.id.patient)
    LinearLayout myPatient;

    @BindView(R.id.myads)
    LinearLayout myAdsLinear;

    @BindView(R.id.myserviceenquiry)
    LinearLayout myServiceEnquiry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        AppUtils.isNetworkConnectionAvailable(this);

        myRo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MyAccountActivity.this,MyRoActivity.class);
                startActivity(intent);
            }
        });

        myAdsLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MyAccountActivity.this, MyAdsActivity.class);
                startActivity(intent);
            }
        });

        myDonation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MyAccountActivity.this,MyDonationActivity.class);
                startActivity(intent);
            }
        });

        myOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MyAccountActivity.this,MyOrderActivity.class);
                startActivity(intent);
            }
        });

        myServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MyAccountActivity.this,MyCompanyActivity.class);
                startActivity(intent);
            }
        });


        myServiceEnquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MyAccountActivity.this, MyServicesActivity.class);
                startActivity(intent);
            }
        });

        myPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MyAccountActivity.this, MyPatientActivity.class);
                startActivity(intent);
            }
        });

        myVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MyAccountActivity.this, MyVideoActivity.class);
                startActivity(intent);
            }
        });

        myJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            //    Intent intent=new Intent(MyAccountActivity.this, MyJobActivity.class);
             //   startActivity(intent);
            }
        });





    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MyAccountActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MyAccountActivity.this,Id);

            }
        });



    }
}
