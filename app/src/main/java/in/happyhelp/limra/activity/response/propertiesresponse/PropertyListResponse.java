
package in.happyhelp.limra.activity.response.propertiesresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PropertyListResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("Property_list")
    @Expose
    private List<PropertyList> propertyList = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<PropertyList> getPropertyList() {
        return propertyList;
    }

    public void setPropertyList(List<PropertyList> propertyList) {
        this.propertyList = propertyList;
    }

}
