package in.happyhelp.limra.activity.Jobs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.property.MembershipPropertyActivity;
import in.happyhelp.limra.activity.property.PostPropertyActivity;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.companyprofile.Company;
import in.happyhelp.limra.activity.response.companyprofile.CompanyDetailsResponse;
import in.happyhelp.limra.activity.response.jobplanresponse.JobPlan;
import in.happyhelp.limra.activity.response.jobplanresponse.JobPlanResponse;
import in.happyhelp.limra.adapter.MembershipAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.gstresponse.GstRateResponse;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.propertyprofileresponse.Datum;
import in.happyhelp.limra.shoppingresponse.propertyprofileresponse.PropertyProfileResponse;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;

public class MembershipJobActivity extends AppCompatActivity {

    List<JobPlan> jobPlanList=new ArrayList<>();
    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    MembershipAdapter membershipAdapter;

    @BindView(R.id.updateplan)
    TextView updatePlan;
    ProgressDialog progressBar;
    JobPlan selectedPlan;

    String email="";
    String mobile="";
    String companyName;



    boolean isSelected=false;
    int gstRate=0;
    double result=0;
    JobPlan jobPlan;
    String totalamount="0";


    @BindView(R.id.gstamt)
    TextView gstAmount;

    @BindView(R.id.gst)
    TextView gst;

    @BindView(R.id.gstlinear)
    LinearLayout gstLinear;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership_job);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);

        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);
        getJobCompanyProfile();

        membershipAdapter=new MembershipAdapter(getApplicationContext(), new MembershipAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(JobPlan d, View view) throws ParseException {
                jobPlan=d;
                isSelected=true;
                for(int i=0;i<jobPlanList.size();i++){
                    JobPlan jobPlan=jobPlanList.get(i);
                    if(d.getAmount().equals(jobPlan.getAmount())){
                        d.setSelected(true);
                        selectedPlan=d;
                    }else{
                        jobPlan.setSelected(false);
                    }
                }
                membershipAdapter.setData(jobPlanList);
                setDataVisible();
            }
        });
        updatePlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callInstamojoPay(email,mobile,totalamount,"Company MemberShip Plan", companyName);
            }
        });

        getMembershipPlan();
        getGstRate();
    }


    @SuppressLint("SetTextI18n")
    public void setDataVisible(){
        if(isSelected){
            gstLinear.setVisibility(View.VISIBLE);
            result = (double) ((Integer.parseInt(jobPlan.getAmount()) * gstRate) / 100);
            totalamount= String.valueOf(Double.parseDouble(jobPlan.getAmount())+result);
            gstAmount.setText("\u20B9 "+String.valueOf(result));
            gst.setText("Exclusive Gst ("+gstRate+"% )");
            updatePlan.setText("Pay for Membership  "+"\u20B9 "+totalamount);
        }else{
            gstLinear.setVisibility(View.GONE);

        }
    }


    public void getGstRate(){
        Call<GstRateResponse> call=RestClient.get().getGstRate(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<GstRateResponse>() {
            @Override
            public void onResponse(Call<GstRateResponse> call, retrofit2.Response<GstRateResponse> response) {

                if(response.code()==200){
                    GstRateResponse gstRateResponse=response.body();

                    if(gstRateResponse.getStatus()) {
                        if (gstRateResponse.getGst().size() >= 2) {
                            gstRate = Integer.parseInt(gstRateResponse.getGst().get(2).getName());

                        }
                    }else{
                        gstRate=0;
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getApplicationContext());
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<GstRateResponse> call, Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }
    public void getJobCompanyProfile(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<CompanyDetailsResponse> call= RestClient.get().jobscompany_details(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<CompanyDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<CompanyDetailsResponse> call, @NonNull retrofit2.Response<CompanyDetailsResponse> response) {
                final CompanyDetailsResponse companyDetailsResponse=response.body();
                if(response.code()==200){
                    if(companyDetailsResponse.getStatus()){
                        if(companyDetailsResponse.getData()!=null) {

                            if(companyDetailsResponse.getData().size()>0) {
                                if(companyDetailsResponse.getData().size()>0) {
                                    if(!companyDetailsResponse.getData().get(0).isPlanexpired()) {
                                        SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(true);
                                    }else{
                                        SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(false);
                                    }
                                }
                                Company company = companyDetailsResponse.getData().get(0);
                                companyName=company.getCompanyName();
                                email=company.getEmail();
                                mobile=company.getMobile();


                            }

                        } else
                        {
                            SharedPreferenceHelper.getInstance(MembershipJobActivity.this).setCompanyProfile(false);
                        }
                    }else{
                        SharedPreferenceHelper.getInstance(MembershipJobActivity.this).setCompanyProfile(false);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(MembershipJobActivity.this);
                }else{
                    SharedPreferenceHelper.getInstance(MembershipJobActivity.this).setCompanyProfile(false);

                }
            }

            @Override
            public void onFailure(@NonNull Call<CompanyDetailsResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                SharedPreferenceHelper.getInstance(MembershipJobActivity.this).setCompanyProfile(false);
                Toast.makeText(MembershipJobActivity.this,Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
            }
        });
    }


    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);
                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();

                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);
                updateMembership(payment);
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }

    public void updateMembership(String payment){
        progressBar.show();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String userID= SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID);
        builder.addFormDataPart("userid", userID);
        builder.addFormDataPart("payment_id", payment);
        builder.addFormDataPart("plan", selectedPlan.getId());
        builder.addFormDataPart("gst", String.valueOf(gstRate));
        builder.addFormDataPart("gstamt", String.valueOf(result));
        /*if(path!=null){
            File filepath = new File(path);
            builder.addFormDataPart("cv", filepath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), filepath));
        }*/

        final MultipartBody requestBody = builder.build();

        Call<Response> call= RestClient.get().updateJobMembership(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                progressBar.dismiss();
                Response response1=response.body();
                if(response.code()==200){

                    if(response1.getStatus()) {

                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();

                        new AlertDialog.Builder(MembershipJobActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(response1.getMessage())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        onBackPressed();
                                    }
                                })
                                .show();
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(MembershipJobActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void getMembershipPlan(){
        Call<JobPlanResponse> call= RestClient.get().getJobPlan(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<JobPlanResponse>() {
            @Override
            public void onResponse(@NonNull Call<JobPlanResponse> call, @NonNull retrofit2.Response<JobPlanResponse> response) {

                final JobPlanResponse jobPlanResponse=response.body();

                if(response.code()==200){
                    if(jobPlanResponse.getStatus()){
                        jobPlanList.clear();
                        jobPlanList.addAll(jobPlanResponse.getData());
                        membershipAdapter.setData(jobPlanList);
                        recyclerView.setAdapter(membershipAdapter);

                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(MembershipJobActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JobPlanResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getJobCompanyProfile();
        getMembershipPlan();
        EventBus.getDefault().unregister(this);
    }



    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MembershipJobActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MembershipJobActivity.this,Id);

            }
        });
    }
}
