package in.happyhelp.limra.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.bumptech.glide.Glide;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.multispinner.MultiSelectSpinner;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Time;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.GPSTracker;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import in.happyhelp.limra.activity.response.statecityresponse.City;
import in.happyhelp.limra.activity.service.MyServicesActivity;
import in.happyhelp.limra.activity.service.PromotionActivity;
import in.happyhelp.limra.activity.service.ServicePopActivity;
import in.happyhelp.limra.adapter.WorkImagesAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.model.WorkModel;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.companydetailsresponse.CompanyDetailsResponse;
import in.happyhelp.limra.activity.response.companydetailsresponse.Datum;
import in.happyhelp.limra.activity.response.companydetailsresponse.WorkingHour;
import in.happyhelp.limra.activity.response.servicecategoryresponse.ServiceCategoryResponse;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.v4.content.FileProvider.getUriForFile;

public class MyCompanyActivity extends AppCompatActivity   {

    private static final int AUTO_COMP_REQ_CODE = 865;
    @BindView(R.id.camera)
    RelativeLayout camera;

    @BindView(R.id.companyimage)
    ImageView companyImage;

    @BindView(R.id.uploadimage)
    TextView uploadImage;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.address)
    EditText add;

    @BindView(R.id.update)
    TextView update;

    @BindView(R.id.locality)
    TextView locality;

    @BindView(R.id.spinner)
    MultiSelectSpinner multiSelectSpinner;

    @BindView(R.id.services)
    TextView servicesNames;

    @BindView(R.id.editservice)
    TextView servicesEdit;
    List<String> serviceList = new ArrayList<>();
    List<String> selectedSeriveces = new ArrayList<>();
    List<WorkingHour> workingDay = new ArrayList<>();
    ArrayList<MultiSelectModel> selectModels = new ArrayList<>();
    ArrayList<Integer>preSelected= new ArrayList<>();
    String path="";
    List<WorkModel> list = new ArrayList<>();
    Realm realm;
    private static final int GALLERY_IMAGE =321;
    private int REQUEST_IMAGE_CAPTURE=123;
    private static final int GALLERY_WORK = 456;
    private static final int REQUEST_IMAGE_CAPTURE_WORK = 789;
    private static final int REQUEST_PHONE_CALL = 123;
    WorkImagesAdapter adapter;

    String latitude,longitude;
    GPSTracker gps;
    Location location;
    MultiSelectDialog multiSelectDialog;
    Menu mMenu;

    @BindView(R.id.sunclose)
    CheckBox sunClose;

    @BindView(R.id.sunstarttime)
    EditText sunStart;

    @BindView(R.id.sunendtime)
    EditText sunEnd;

    @BindView(R.id.companyname)
    EditText companyName;

    @BindView(R.id.monclose)
    CheckBox monClose;

    @BindView(R.id.monstarttime)
    EditText monStart;

    @BindView(R.id.monendtime)
    EditText monEnd;

    @BindView(R.id.tueclose)
    CheckBox tueClose;

    @BindView(R.id.tuestarttime)
    EditText tueStart;

    @BindView(R.id.tueendtime)
    EditText tueEnd;

    @BindView(R.id.wedclose)
    CheckBox wedClose;

    @BindView(R.id.wedstarttime)
    EditText wedStart;

    @BindView(R.id.wedendtime)
    EditText wedEnd;

    @BindView(R.id.thuclose)
    CheckBox thuClose;

    @BindView(R.id.thustarttime)
    EditText thusStart;

    @BindView(R.id.thuendtime)
    EditText thusEnd;

    @BindView(R.id.friclose)
    CheckBox friClose;

    @BindView(R.id.fristarttime)
    EditText friStart;

    @BindView(R.id.friendtime)
    EditText friEnd;

    @BindView(R.id.satclose)
    CheckBox satClose;

    @BindView(R.id.satstarttime)
    EditText satStart;

    @BindView(R.id.satendtime)
    EditText satEnd;

    @BindView(R.id.onehour)
    CheckBox onehour;

    @BindView(R.id.threehours)
    CheckBox threehours;

    ProgressDialog progressBar;
    Boolean isSubmit=false;
    String companyname="";
    String gallaryImage="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_company);
        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        realm=RealmHelper.getRealmInstance();

        list.clear();

        mobile.setFocusable(false);
        mobile.setEnabled(false);
        mobile.setCursorVisible(false);
        mobile.setKeyListener(null);
        mobile.setBackgroundColor(Color.TRANSPARENT);



        progressBar=new ProgressDialog(MyCompanyActivity.this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        getCompanyDetail();
        getServices();

        gps = new GPSTracker(this);

        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            gps.showSettingsAlert();
        }

        if(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.COMPANYCODE)!=null){
            isSubmit=true;
        }


        if(isSubmit){
            update.setText("Update");
        }else{
            update.setText("Next");
        }

        // Check if GPS enabled
        if(gps.canGetLocation()) {
            location=gps.getLocation();
            if(location!=null){

             //   latitude= String.valueOf(location.getLatitude());
              //  longitude= String.valueOf(location.getLongitude());


             //   SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.LATITUDE,latitude);
             //   SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.LONGITUDE,longitude);

            }
        } else {
            gps.showSettingsAlert();
        }

        RealmResults<UserData> userData=realm.where(UserData.class).findAllAsync();
        userData.addChangeListener(new RealmChangeListener<RealmResults<UserData>>() {
            @Override
            public void onChange(@NonNull RealmResults<UserData> userData) {
                if(userData.size()>0) {
                    mobile.setText(userData.get(0).getMobile());
                    email.setText(userData.get(0).getEmail());
                }
            }
        });

        sunClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sunClose.isChecked()){
                    sunStart.setError(null);
                    sunEnd.setError(null);
                    sunStart.setText("Closed");
                    sunEnd.setText("Closed");
                }else {
                    sunStart.setText("");
                    sunEnd.setText("");
                }
            }
        });

        monClose.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                if(monClose.isChecked()){
                    monStart.setText("Closed");
                    monEnd.setText("Closed");
                    monStart.setError(null);
                    monEnd.setError(null);
                }else {
                    monStart.setText("");
                    monEnd.setText("");
                }
            }
        });

        tueClose.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                if(tueClose.isChecked()){
                    tueStart.setText("Closed");
                    tueEnd.setText("Closed");
                    tueStart.setError(null);
                    tueEnd.setError(null);
                }else {
                    tueStart.setText("");
                    tueEnd.setText("");
                }
            }
        });

        wedClose.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                if(wedClose.isChecked()){
                    wedStart.setText("Closed");
                    wedEnd.setText("Closed");
                    wedStart.setError(null);
                    wedEnd.setError(null);
                }else {
                    wedStart.setText("");
                    wedEnd.setText("");
                }
            }
        });

        thuClose.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                if(thuClose.isChecked()){
                    thusStart.setText("Closed");
                    thusEnd.setText("Closed");
                    thusStart.setError(null);
                    thusEnd.setError(null);
                }else {
                    thusStart.setText("");
                    thusEnd.setText("");
                }
            }
        });

        friClose.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                if(friClose.isChecked()){
                    friStart.setText("Closed");
                    friEnd.setText("Closed");
                    friStart.setError(null);
                    friEnd.setError(null);
                }else {
                    friStart.setText("");
                    friEnd.setText("");
                }
            }
        });


        satClose.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                if(satClose.isChecked()){
                    satStart.setText("Closed");
                    satEnd.setText("Closed");
                    satStart.setError(null);
                    satEnd.setError(null);
                }else {
                    satStart.setText("");
                    satEnd.setText("");
                }
            }
        });

        sunStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!sunClose.isChecked())
                setTime(sunStart);
            }
        });
        sunEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!sunClose.isChecked())
                setTime(sunEnd);
            }
        });

        monStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!monClose.isChecked())
                setTime(monStart);
            }
        });
        monEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!monClose.isChecked())
                setTime(monEnd);
            }
        });
        tueStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!tueClose.isChecked())
                setTime(tueStart);
            }
        });
        tueEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!tueClose.isChecked())
                setTime(tueEnd);
            }
        });
        wedStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!wedClose.isChecked())
                setTime(wedStart);
            }
        });
        wedEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!wedClose.isChecked())
                setTime(wedEnd);
            }
        });
        thusStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!thuClose.isChecked())
                setTime(thusStart);
            }
        });

        thusEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!thuClose.isChecked())
                setTime(thusEnd);
            }
        });


        friStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!friClose.isChecked())
                setTime(friStart);
            }
        });

        friEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!friClose.isChecked())
                setTime(friEnd);
            }
        });

        satStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!satClose.isChecked())
                setTime(satStart);
            }
        });
        satEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!satClose.isChecked())
                setTime(satEnd);
            }
        });





        locality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .build(MyCompanyActivity.this);
                    startActivityForResult(intent, AUTO_COMP_REQ_CODE);
                } catch (Exception e) {
                   e.printStackTrace();
                }
            }
        });

       /* Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        List<Address> addresses = null;
        if(location!=null)
            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        Address address=null;
        String addr="";
        String zipcode="";
        String city="";
        String state="";
        if (addresses != null && addresses.size() > 0) {

            addr = addresses.get(0).getAddressLine(0) + "," + addresses.get(0).getSubAdminArea();
            city = addresses.get(0).getLocality();
            locality.setText(city);
            state = addresses.get(0).getAdminArea();

            for (int i = 0; i < addresses.size(); i++) {
                address = addresses.get(i);
                if (address.getPostalCode() != null) {
                    zipcode = address.getPostalCode();
                    break;
                }
            }
        }*/
        servicesEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if(serviceList.size()>0) {
                    multiSelectSpinner.performClick();
                }else{
                    Toast.makeText(getApplicationContext(),"No Arealist available",Toast.LENGTH_LONG).show();
                }*/
                getServices();
                multiSelectDialog.show(getSupportFragmentManager(),"Service Dialoge");
            }
        });


        multiSelectSpinner.setListener(new MultiSelectSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void selectedStrings(List<String> strings) {
                selectedSeriveces.clear();
                selectedSeriveces.addAll(strings);
                String services="";
                for(int i=0; i<selectedSeriveces.size();i++){

                    if(i==selectedSeriveces.size()-1){
                        services = services+selectedSeriveces.get(i) ;
                    }else {
                        services = services+selectedSeriveces.get(i) + ",";
                    }

                }
                servicesNames.setText(services);
            }
        });

        GridLayoutManager gridLayoutManager=new GridLayoutManager(getApplicationContext(), 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);


        adapter=new WorkImagesAdapter(getApplicationContext(), new WorkImagesAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(final WorkModel d, View view) throws ParseException {
                Toast.makeText(getApplicationContext(),"Delete call",Toast.LENGTH_LONG).show();

                if(d.isServer()) {
                    new AlertDialog.Builder(MyCompanyActivity.this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Delete")
                            .setMessage("Are you sure you want to Delete this Work Image ?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                   deleteGallaryImage(d);

                                }
                            })
                            .setNegativeButton("No", null)
                            .show();

                }else{
                    list.remove(d);
                    adapter.notifyDataSetChanged();
                }
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File filepath=new File(Environment.getExternalStorageDirectory()+Constants.imagePath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }

                path=filepath.getAbsolutePath()+"/"+System.currentTimeMillis()+"company_image.jpg";
                AppUtils.startPickImageDialog(GALLERY_IMAGE, REQUEST_IMAGE_CAPTURE, path,MyCompanyActivity.this);
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if(!isSubmit) {
                    postCompanyservices();
                }else{
                    updateCompanyServices();
                }

            }
        });




        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startDialog();
            }
        });
    }


    public void deleteGallaryImage(final WorkModel workModel){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("serviceid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.COMPANYCODE));
        hashMap.put("galleryimage",workModel.getUrl());
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().deleteGallary(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_SHORT).show();
                        list.remove(workModel);
                        adapter.notifyDataSetChanged();
                    }else{
                        Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(MyCompanyActivity.this);
                }else{
                    Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<in.happyhelp.limra.activity.response.Response> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setTime(final EditText editText){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MyCompanyActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                editText.setText( getTime(selectedHour, selectedMinute));
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    @SuppressLint("SimpleDateFormat")
    private String getTime(int hr, int min) {
        Time tme = new Time(hr,min,0);//seconds by default set to zero
        Format formatter;
        formatter = new SimpleDateFormat("h:mm a");
        return formatter.format(tme);
    }

    public void getCompanyDetail(){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        if(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.COMPANYCODE)!=null)
        hashMap.put("id",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.COMPANYCODE));

        Call<CompanyDetailsResponse> call=RestClient.get().getCompanyDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<CompanyDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<CompanyDetailsResponse> call, Response<CompanyDetailsResponse> response) {
                progressBar.dismiss();
                if(response.code()==200){
                    CompanyDetailsResponse companyDetailsResponse=response.body();
                    if(companyDetailsResponse.isStatus()) {
                        if (companyDetailsResponse.getData().size() > 0) {

                            Datum datum = companyDetailsResponse.getData().get(0);

                            if(datum!=null) {
                                Glide.with(getApplicationContext())
                                        .load(RestClient.base_image_url + datum.getImage())
                                        .into(companyImage);

                                name.setText(datum.getName());
                                companyName.setText(datum.getCompanyName());
                                add.setText(datum.getAddress());
                                locality.setText(datum.getLocation());
                                mobile.setText(datum.getMobile());
                                email.setText(datum.getEmail());
                                companyname = datum.getImage();
                                latitude=datum.getLatitude();
                                longitude=datum.getLongitude();


                                for (int i = 0; i < selectedSeriveces.size(); i++) {
                                    if (i == selectedSeriveces.size() - 1) {
                                        gallaryImage = gallaryImage + datum.getGalleryImages().get(i) + ",";
                                    } else {
                                        gallaryImage = gallaryImage + datum.getGalleryImages().get(i) + ",";
                                    }
                                }

                                if (datum.getServices() != null && datum.getServices().size() > 0) {
                                    selectedSeriveces.addAll(datum.getServices());
                                }
                                 if(datum.getWorkingHours()!=null) {
                                     if (datum.getWorkingHours().size() > 0) {
                                         workingDay.addAll(datum.getWorkingHours());
                                     }


                                     setStartEndTime(datum.getWorkingHours());
                                 }


                                if (datum.getOneHour().equals("1")) {
                                    onehour.setChecked(true);
                                } else {
                                    onehour.setChecked(false);
                                }

                                if(datum.getThreeHour()!=null)
                                if (datum.getThreeHour().equals("1")) {
                                    threehours.setChecked(true);
                                } else {
                                    threehours.setChecked(false);
                                }

                                String serviceName = "";
                                for (int i = 0; i < selectedSeriveces.size(); i++) {
                                    if (i == selectedSeriveces.size() - 1) {
                                        serviceName = serviceName + datum.getServices().get(i) ;
                                    } else {
                                        serviceName = serviceName + datum.getServices().get(i) + ",";
                                    }
                                }
                                servicesNames.setText(serviceName);


                                multiSelectSpinner.setSelection(selectedSeriveces);


                                if (datum.getGalleryImages().size() > 0) {

                                    for (int i = 0; i < datum.getGalleryImages().size(); i++) {
                                        if(!datum.getGalleryImages().get(i).equals("")) {
                                            WorkModel workModel = new WorkModel();
                                            workModel.setUrl(datum.getGalleryImages().get(i));
                                            workModel.setServer(true);
                                            list.add(workModel);
                                        }

                                    }
                                    adapter.setData(list);
                                    recyclerView.setAdapter(adapter);
                                }
                            }
                        }
                    }else{
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.COMPANYCODE,null);
                        Toast.makeText(getApplicationContext(),"No Company Found",Toast.LENGTH_SHORT).show();
                        if(isSubmit){
                            update.setText("Update");
                        }else{
                            update.setText("Next");
                        }
                    }
                }else if(response.code()==401){
                    AppUtils.logout(MyCompanyActivity.this);
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                   // Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CompanyDetailsResponse> call, Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
               // Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setStartEndTime(List<WorkingHour>  workingHour){
        for(int i=0;i<workingHour.size();i++){
            WorkingHour workingHours=workingHour.get(i);
            String[] separated = workingHour.get(i).getTime().split("-");
            String end=separated[1];
            String start=separated[0];
            if(workingHours.getDays().equals("Sunday")){
                if(start.equals("Closed")){
                    sunClose.setChecked(true);
                }
                sunStart.setText(start);
                sunEnd.setText(end);
            }else if(workingHours.getDays().equals("Monday")){
                if(start.equals("Closed")){
                    monClose.setChecked(true);
                }
                monStart.setText(start);
                monEnd.setText(end);

            }else if(workingHours.getDays().equals("Tuesday")){
                if(start.equals("Closed")){
                    tueClose.setChecked(true);
                }
                tueStart.setText(start);
                tueEnd.setText(end);

            }else if(workingHours.getDays().equals("Wednesday")){
                if(start.equals("Closed")){
                    wedClose.setChecked(true);
                }
                wedStart.setText(start);
                wedEnd.setText(end);

            }else if(workingHours.getDays().equals("Thursday")){
                if(start.equals("Closed")){
                    thuClose.setChecked(true);
                }
                thusStart.setText(start);
                thusEnd.setText(end);

            }else if(workingHours.getDays().equals("Friday")){
                if(start.equals("Closed")){
                    friClose.setChecked(true);
                }
                friStart.setText(start);
                friEnd.setText(end);

            }else if(workingHours.getDays().equals("Saturday")){
                if(start.equals("Closed")){
                    satClose.setChecked(true);
                }
                satStart.setText(start);
                satEnd.setText(end);

            }





        }

    }

    public void updateCompanyServices(){

        if(TextUtils.isEmpty(name.getText().toString())){
            name.setError("Enter Name ");
            name.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(companyName.getText().toString())){
            companyName.setError("Enter company Name ");
            companyName.requestFocus();
            return;
        }

        if(!AppUtils.isValidMobile(mobile.getText().toString())){
            mobile.setError("Enter valid mobile");
            mobile.requestFocus();
            return;
        }

        if(!AppUtils.isValidMail(email.getText().toString(),email)){
            email.setError("Enter valid Email");
            email.requestFocus();
            return;
        }


        if(TextUtils.isEmpty(add.getText().toString())){
            add.setError("Enter Address ");
            add.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(locality.getText().toString())){
            locality.setError("Enter Locality ");
            locality.requestFocus();
            return;
        }

        if(servicesNames.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Select atleast One services ",Toast.LENGTH_SHORT).show();
            return;
        }


       /* if(servicesNames.getText().equals("")){
            Toast.makeText(getApplicationContext(),"Select atleast One services ",Toast.LENGTH_SHORT).show();
            return;
        }*/


        if(!sunClose.isChecked()){
            if(TextUtils.isEmpty(sunStart.getText().toString())){
                sunStart.setError("Enter Start Time ");
                sunStart.requestFocus();
                return;
            }

            if(TextUtils.isEmpty(sunEnd.getText().toString())){
                sunEnd.setError("Enter End Time ");
                sunEnd.requestFocus();
                return;
            }
        }

        if(!monClose.isChecked()){
            if(TextUtils.isEmpty(monStart.getText().toString())){
                monStart.setError("Enter Start Time ");
                monStart.requestFocus();
                return;
            }

            if(TextUtils.isEmpty(monEnd.getText().toString())){
                monEnd.setError("Enter End Time ");
                monEnd.requestFocus();
                return;
            }

        }

        if(!tueClose.isChecked()){
            if(TextUtils.isEmpty(tueStart.getText().toString())){
                tueStart.setError("Enter Start Time ");
                return;
            }

            if(TextUtils.isEmpty(tueEnd.getText().toString())){
                tueEnd.setError("Enter End Time ");
                return;
            }

        }

        if(!wedClose.isChecked()){
            if(TextUtils.isEmpty(wedStart.getText().toString())){
                wedStart.setError("Enter Start Time ");
                return;
            }

            if(TextUtils.isEmpty(wedEnd.getText().toString())){
                wedEnd.setError("Enter End Time ");
                return;
            }

        }


        if(!thuClose.isChecked()){
            if(TextUtils.isEmpty(thusStart.getText().toString())){
                thusStart.setError("Enter Start Time ");
                return;
            }

            if(TextUtils.isEmpty(thusStart.getText().toString())){
                thusStart.setError("Enter End Time ");
                return;
            }

        }

        if(!friClose.isChecked()){
            if(TextUtils.isEmpty(friStart.getText().toString())){
                friStart.setError("Enter Start Time ");
                return;
            }

            if(TextUtils.isEmpty(friEnd.getText().toString())){
                friEnd.setError("Enter End Time ");
                return;
            }
        }

        if(!satClose.isChecked()){
            if(TextUtils.isEmpty(satStart.getText().toString())){
                satStart.setError("Enter Start Time ");
                return;
            }

            if(TextUtils.isEmpty(satEnd.getText().toString())){
                satEnd.setError("Enter End Time ");
                return;
            }

        }

        progressBar.show();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String userid=SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID);
        builder.addFormDataPart("userid", userid);
        builder.addFormDataPart("name", name.getText().toString());
        builder.addFormDataPart("email", email.getText().toString());
        builder.addFormDataPart("mobile", mobile.getText().toString());
        builder.addFormDataPart("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        builder.addFormDataPart("address", add.getText().toString());
        builder.addFormDataPart("company_name",companyName.getText().toString());
        builder.addFormDataPart("latitude",latitude);
        builder.addFormDataPart("longitude",longitude);
        builder.addFormDataPart("location",locality.getText().toString());
        builder.addFormDataPart("services",servicesNames.getText().toString());
        builder.addFormDataPart("sunday",sunStart.getText().toString()+"-"+sunEnd.getText().toString());
        builder.addFormDataPart("monday",monStart.getText().toString()+"-"+monEnd.getText().toString());
        builder.addFormDataPart("tuesday",tueStart.getText().toString()+"-"+tueEnd.getText().toString());
        builder.addFormDataPart("wednesday",wedStart.getText().toString()+"-"+wedEnd.getText().toString());
        builder.addFormDataPart("thursday",thusStart.getText().toString()+"-"+thusEnd.getText().toString());
        builder.addFormDataPart("friday",friStart.getText().toString()+"-"+friEnd.getText().toString());
        builder.addFormDataPart("saturday",satStart.getText().toString()+"-"+satEnd.getText().toString());

        builder.addFormDataPart("image1",companyname);
       // builder.addFormDataPart("gallery_images_old",gallaryImage);
        builder.addFormDataPart("serviceid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.COMPANYCODE));



        if(onehour.isChecked()) {
            builder.addFormDataPart("one_hour", "1");
        }else{
            builder.addFormDataPart("one_hour", "0");
        }

        if(threehours.isChecked()) {
            builder.addFormDataPart("three_hour", "1");
        }else{
            builder.addFormDataPart("three_hour", "0");
        }

        ArrayList<String> filePaths = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if(!list.get(i).isServer())
                filePaths.add(list.get(i).getUrl());
        }

        if(!path.equals("")) {
            File profilepath = new File(path);
            builder.addFormDataPart("image", profilepath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), profilepath));
        }

        for (int i = 0; i < filePaths.size(); i++) {
            File file = new File(filePaths.get(i));
            builder.addFormDataPart("gallery_images[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
        }

        final MultipartBody requestBody = builder.build();
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().updateCompanyServices(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                progressBar.dismiss();
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_SHORT).show();
                    getCompanyDetail();
                }else if(response.code()==401){
                    AppUtils.logout(MyCompanyActivity.this);
                }else{
                    Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_SHORT).show();
                progressBar.dismiss();
            }
        });
    }

    public void postCompanyservices(){

        if(path.equals("")){
            Toast.makeText(getApplicationContext(),"upload company image",Toast.LENGTH_SHORT).show();
            return;
        }


        if(TextUtils.isEmpty(name.getText().toString())){
            name.setError("Enter Name ");
            name.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(companyName.getText().toString())){
            companyName.setError("Enter company Name ");
            companyName.requestFocus();
            return;
        }

        if(!AppUtils.isValidMobile(mobile.getText().toString())){
            mobile.setError("Enter valid mobile");
            mobile.requestFocus();
            return;
        }

        if(!AppUtils.isValidMail(email.getText().toString(),email)){
            email.setError("Enter valid Email");
            email.requestFocus();
            return;
        }


        if(TextUtils.isEmpty(add.getText().toString())){
            add.setError("Enter Address ");
            add.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(locality.getText().toString())){
            locality.setError("Enter Locality ");
            locality.requestFocus();
            return;
        }

        if(servicesNames.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Select atleast One services ",Toast.LENGTH_SHORT).show();
            return;
        }

      /*  if(list.size()==0){
            Toast.makeText(getApplicationContext(),"Please Upload Atleast one Gallary picture ",Toast.LENGTH_SHORT).show();
            return;
        }*/

        if(!sunClose.isChecked()){
            if(TextUtils.isEmpty(sunStart.getText().toString())){
                sunStart.setError("Enter Start Time ");
                return;
            }

            if(TextUtils.isEmpty(sunEnd.getText().toString())){
                sunEnd.setError("Enter End Time ");
                return;
            }
        }

        if(!monClose.isChecked()){
            if(TextUtils.isEmpty(monStart.getText().toString())){
                monStart.setError("Enter Start Time ");
                return;
            }

            if(TextUtils.isEmpty(monEnd.getText().toString())){
                monEnd.setError("Enter End Time ");
                return;
            }

        }

        if(!tueClose.isChecked()){
            if(TextUtils.isEmpty(tueStart.getText().toString())){
                tueStart.setError("Enter Start Time ");
                return;
            }

            if(TextUtils.isEmpty(tueEnd.getText().toString())){
                tueEnd.setError("Enter End Time ");
                return;
            }

        }

        if(!wedClose.isChecked()){
            if(TextUtils.isEmpty(wedStart.getText().toString())){
                wedStart.setError("Enter Start Time ");
                return;
            }

            if(TextUtils.isEmpty(wedEnd.getText().toString())){
                wedEnd.setError("Enter End Time ");
                return;
            }

        }


        if(!thuClose.isChecked()){
            if(TextUtils.isEmpty(thusStart.getText().toString())){
                thusStart.setError("Enter Start Time ");
                return;
            }

            if(TextUtils.isEmpty(thusStart.getText().toString())){
                thusStart.setError("Enter End Time ");
                return;
            }

        }

        if(!friClose.isChecked()){
            if(TextUtils.isEmpty(friStart.getText().toString())){
                friStart.setError("Enter Start Time ");
                return;
            }

            if(TextUtils.isEmpty(friEnd.getText().toString())){
                friEnd.setError("Enter End Time ");
                return;
            }

        }

        if(!satClose.isChecked()){
            if(TextUtils.isEmpty(satStart.getText().toString())){
                satStart.setError("Enter Start Time ");
                return;
            }

            if(TextUtils.isEmpty(satEnd.getText().toString())){
                satEnd.setError("Enter End Time ");
                return;
            }

        }

        progressBar.show();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String userid=SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID);
        builder.addFormDataPart("userid", userid);
        builder.addFormDataPart("name", name.getText().toString());
        builder.addFormDataPart("email", email.getText().toString());
        builder.addFormDataPart("mobile", mobile.getText().toString());
        builder.addFormDataPart("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        builder.addFormDataPart("address", add.getText().toString());
        builder.addFormDataPart("company_name",companyName.getText().toString());
        builder.addFormDataPart("latitude",latitude);
        builder.addFormDataPart("longitude",longitude);
        builder.addFormDataPart("location",locality.getText().toString());
        builder.addFormDataPart("services",servicesNames.getText().toString());
        builder.addFormDataPart("sunday",sunStart.getText().toString()+"-"+sunEnd.getText().toString());
        builder.addFormDataPart("monday",monStart.getText().toString()+"-"+monEnd.getText().toString());
        builder.addFormDataPart("tuesday",tueStart.getText().toString()+"-"+tueEnd.getText().toString());
        builder.addFormDataPart("wednesday",wedStart.getText().toString()+"-"+wedEnd.getText().toString());
        builder.addFormDataPart("thursday",thusStart.getText().toString()+"-"+thusEnd.getText().toString());
        builder.addFormDataPart("friday",friStart.getText().toString()+"-"+friEnd.getText().toString());
        builder.addFormDataPart("saturday",satStart.getText().toString()+"-"+satEnd.getText().toString());

        if(onehour.isChecked()) {
            builder.addFormDataPart("one_hour", "1");
        }else{
            builder.addFormDataPart("one_hour", "0");
        }

        if(threehours.isChecked()){
            builder.addFormDataPart("three_hour", "1");
        }else{
            builder.addFormDataPart("three_hour", "0");
        }


        ArrayList<String> filePaths = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if(!list.get(i).isServer())
                filePaths.add(list.get(i).getUrl());
        }

        if(path!=null) {

            File profilepath = new File(path);
            builder.addFormDataPart("image", profilepath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), profilepath));
        }

        for (int i = 0; i < filePaths.size(); i++) {
            File file = new File(filePaths.get(i));
            builder.addFormDataPart("gallery_images[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
        }

        final MultipartBody requestBody = builder.build();
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().postCompanyServices(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                progressBar.dismiss();
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_SHORT).show();
                    SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.COMPANYCODE, String.valueOf(response1.getServiceid()));


                    Intent intent=new Intent(MyCompanyActivity.this,KycActivity.class);
                    startActivity(intent);
                    finish();
                }else if(response.code()==401){
                    AppUtils.logout(MyCompanyActivity.this);
                }else{
                    Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_SHORT).show();
                progressBar.dismiss();
            }
        });
    }

    public void deleteWorkImage(final WorkModel workModel){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("vendorid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.VENDORID));
        hashMap.put("workimage",workModel.getUrl());

       /* Call<Response> call=RestClient.get().deleteWork(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.feelhappydeal.limra.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.feelhappydeal.limra.response.Response> call, @NonNull Response<in.feelhappydeal.limra.response.Response> response) {
                if(response.code()==200){
                    in.feelhappydeal.limra.response.Response response1=response.body();
                    if(response1.getStatus()) {
                        Toast.makeText(getApplicationContext(), response1.getMessage(), Toast.LENGTH_SHORT).show();
                        list.remove(workModel);
                        adapter.notifyDataSetChanged();



                    }else{
                        Toast.makeText(getApplicationContext(), response1.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Image deletion Failed",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<in.feelhappydeal.limra.response.Response> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_SHORT).show();
            }
        });*/
    }




    public void getServices(){
        HashMap<String,String> hashMap=new HashMap<>();
        Call<ServiceCategoryResponse> call=RestClient.get().getServicesCategory(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<ServiceCategoryResponse>() {
            @Override
            public void onResponse(Call<ServiceCategoryResponse> call, Response<ServiceCategoryResponse> response) {
                if(response.code()==200){
                    final ServiceCategoryResponse serviceCategoryResponse=response.body();

                    serviceList.clear();
                    selectModels.clear();

                    for(int i=0;i<serviceCategoryResponse.getServiceCategoryList().size();i++){
                        serviceList.add(serviceCategoryResponse.getServiceCategoryList().get(i).getName());
                        selectModels.add(new MultiSelectModel(Integer.parseInt(serviceCategoryResponse.getServiceCategoryList().get(i).getId()),serviceCategoryResponse.getServiceCategoryList().get(i).getName()));

                        if (selectedSeriveces.contains(serviceCategoryResponse.getServiceCategoryList().get(i).getName()))
                        {
                            preSelected.add(Integer.parseInt(serviceCategoryResponse.getServiceCategoryList().get(i).getId()));
                        }
                    }
                    multiSelectSpinner.setItems(serviceList);
                    multiSelectSpinner.hasNoneOption(false);
                }else if(response.code()==401){
                    AppUtils.logout(MyCompanyActivity.this);
                }
            }

            @Override
            public void onFailure(Call<ServiceCategoryResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });

        multiSelectDialog = new MultiSelectDialog()
                .title("Please Select") //setting title for dialog
                .titleSize(25)
                .positiveText("Done")
                .negativeText("Cancel")
                .setMinSelectionLimit(0)
                .preSelectIDsList(preSelected)
                .setMaxSelectionLimit(1000)
                .multiSelectList(selectModels) // the multi select model list with ids and name
                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                    @Override
                    public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                        StringBuilder builder = new StringBuilder();
                        for (int i = 0; i < selectedIds.size(); i++) {
                            for (int j = 0; j < selectModels.size(); j++)
                           /* if(selectModels.get(j).getId()==selectedIds.get(i)){
                            builder.append(selectModels.get(j).getName()+",");
                            }*/
                            if (selectModels.get(j).getId() == selectedIds.get(i)) {
                                if (i == selectedIds.size() - 1) {
                                    builder.append(selectModels.get(j).getName());
                                } else {
                                    builder.append(selectModels.get(j).getName() + ",");
                                }
                            }
                        }
                        servicesNames.setText(builder);
                        System.out.println(builder);
                    }
                    @Override
                    public void onCancel() {
                        multiSelectDialog.dismiss();
                    }
                });

    }

    private void startDialog() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(
                MyCompanyActivity.this);
        myAlertDialog.setTitle("Upload Pictures Option");
        myAlertDialog.setMessage("How do you want to set your picture?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface arg0, int arg1) {


                        Intent pictureActionIntent = null;

                        pictureActionIntent = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        pictureActionIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        startActivityForResult(
                                pictureActionIntent,
                                GALLERY_WORK);

                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (ContextCompat.checkSelfPermission(MyCompanyActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(MyCompanyActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, REQUEST_PHONE_CALL);

                            Intent intent = new Intent(
                                    MediaStore.ACTION_IMAGE_CAPTURE);
                            File imagePath = new File(Environment.getExternalStorageDirectory(), Constants.imagePath);

                            File mypath = new File(imagePath.getAbsolutePath(), System.currentTimeMillis() + "Work.jpg");
                            // File newFile = new File(imagePath, "default_image.jpg");


                            WorkModel model = new WorkModel();
                            model.setUrl(mypath.getAbsolutePath());
                            list.add(model);

                            Uri contentUri = getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", mypath);
                            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, contentUri);
                            startActivityForResult(intent,
                                    REQUEST_IMAGE_CAPTURE_WORK);

                        } else {

                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File imagePath = new File(Environment.getExternalStorageDirectory(), Constants.imagePath);

                            File mypath = new File(imagePath.getAbsolutePath(), System.currentTimeMillis() + "Work.jpg");
                            WorkModel model = new WorkModel();
                            model.setUrl(mypath.getAbsolutePath());
                            list.add(model);
                            Uri contentUri = getUriForFile(MyCompanyActivity.this, getApplicationContext().getPackageName() + ".provider", mypath);
                            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, contentUri);
                            startActivityForResult(intent,
                                    REQUEST_IMAGE_CAPTURE_WORK);
                        }
                    }
                });
        myAlertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        MenuItem enquiry = menu.findItem(R.id.enquiry);
        enquiry.setVisible(false);

        MenuItem promo = menu.findItem(R.id.promo);
        promo.setVisible(true);

        this.mMenu=menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement

        if (id == R.id.enquiry) {
            Intent intent=new Intent(MyCompanyActivity.this,MyServicesActivity.class);
            startActivity(intent);
        }else if(id==R.id.promo){
            Intent intent=new Intent(MyCompanyActivity.this,PromotionActivity.class);
            intent.putExtra("email",email.getText().toString());
            intent.putExtra("mobile",mobile.getText().toString());
            intent.putExtra("company",companyName.getText().toString());
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( resultCode==RESULT_OK)
            if(requestCode==REQUEST_IMAGE_CAPTURE){
                File newFile=new File(path);
                setImageFromPath(companyImage,newFile.getPath());
            }else if(requestCode==GALLERY_IMAGE){
                Uri imageUri = data.getData();
                path = getPath(getApplicationContext(), imageUri);
                setImageFromPath(companyImage,path);
            }else  if(requestCode==REQUEST_IMAGE_CAPTURE_WORK)
            {
                adapter.setData(list);
                recyclerView.setAdapter(adapter);

            }else if(requestCode==GALLERY_WORK){
                if (data != null) {


                    if (data.getClipData() != null) {
                        int count = data.getClipData().getItemCount(); //evaluate the count before the for loop --- otherwise, the count is evaluated every loop.
                        for (int i = 0; i < count; i++) {
                            Uri imageUri = data.getClipData().getItemAt(i).getUri();
                            try {
                                InputStream is = getContentResolver().openInputStream(imageUri);
                                Bitmap bitmap = BitmapFactory.decodeStream(is);
                                savebitmap(bitmap);
                                is.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    } else if (data.getData() != null) {
                        String imagePath = data.getData().getPath();
                        try {
                            InputStream is = getContentResolver().openInputStream(Uri.parse(imagePath));
                            Bitmap bitmap = BitmapFactory.decodeStream(is);
                            savebitmap(bitmap);
                            is.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    adapter.setData(list);
                    recyclerView.setAdapter(adapter);

                } else {
                    Toast.makeText(getApplicationContext(), "Cancelled",
                            Toast.LENGTH_SHORT).show();
                }
            }else if(requestCode == AUTO_COMP_REQ_CODE){
                Place place = PlaceAutocomplete.getPlace(this, data);
                LatLng latLng=place.getLatLng();
                latitude= String.valueOf(latLng.latitude);
                longitude= String.valueOf(latLng.longitude);
                locality.setText(place.getAddress());
                locality.setTextColor(Color.BLACK);
            }
    }

    public  File savebitmap(Bitmap bmp) throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        File f = new File(Environment.getExternalStorageDirectory()+File.separator
                + Constants.imagePath + System.currentTimeMillis()+"Work.jpg");
        f.createNewFile();
        WorkModel model = new WorkModel();
        model.setUrl(f.getAbsolutePath());
        list.add(model);
        FileOutputStream fo = new FileOutputStream(f);
        fo.write(bytes.toByteArray());
        fo.close();
        return f;
    }


    private String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "";
            Toast.makeText(getApplicationContext(),"Sorry your records is not created",Toast.LENGTH_LONG).show();
        }
        return result;
    }

    private void setImageFromPath(ImageView image, String path){
        if(!TextUtils.isEmpty(path)&&!path.equals("")) {
            File imgFile = new File(path);
            if (imgFile.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize=8;      // 1/8 of original image
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(),options);
                image.setImageBitmap(myBitmap);
                image.setVisibility(View.VISIBLE);
            }else {
                image.setVisibility(View.GONE);
            }
        }else {
            image.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        getCompanyDetail();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MyCompanyActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MyCompanyActivity.this,Id);

            }
        });



    }

}
