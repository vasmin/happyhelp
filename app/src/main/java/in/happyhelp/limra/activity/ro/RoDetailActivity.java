package in.happyhelp.limra.activity.ro;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.adapter.MonthSpinnerAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.gstresponse.GstRateResponse;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.rodetails.Detail;
import in.happyhelp.limra.activity.response.rodetails.RoDetailResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoDetailActivity extends AppCompatActivity {

    @BindView(R.id.book)
    TextView booking;

    @BindView(R.id.scrollView)
    ScrollView scrollView;

    String roId;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.image)
    ImageView image;

    @BindView(R.id.detail)
    TextView details;

    @BindView(R.id.rentamt)
    TextView rent;

    @BindView(R.id.refundamt)
    TextView refundamt;

    @BindView(R.id.monthrentspinner)
    Spinner monthSpinner;

    @BindView(R.id.bookspiner)
    ImageView bookspinner;

    @BindView(R.id.imagespinner)
    ImageView imageSpinner;

    @BindView(R.id.buyrentspinner)
    Spinner buyRentSpinner;
    List<String> list=new ArrayList<>();
    MonthSpinnerAdapter monthSpinnerAdapter;

    @BindView(R.id.rent)
    LinearLayout rentLinear;

    @BindView(R.id.rentmonth)
    LinearLayout rentMonth;

    @BindView(R.id.buy)
    LinearLayout buyLinear;

    @BindView(R.id.buyamt)
    TextView buyAmt;

    List<Detail> listDetails=new ArrayList<>();

    ProgressDialog progressBar;
    Detail detail;
    String rentamt,deposit,month;
    String bookingType="Select Booking";
    ArraySpinnerAdapter arraySpinnerAdapter;

    boolean isSelected=false;
    int gstRate=0;
    double result=0;
    String totalamount="0";
    double buyamt=0;

    @BindView(R.id.monthLinear)
    LinearLayout monthLinear;

    @BindView(R.id.gst)
    TextView gst;

    @BindView(R.id.gstamt)
    TextView gstAmount;

    @BindView(R.id.gstlinear)
    LinearLayout gstLinear;


    boolean isBuy=false;
    boolean isRent=false;

    @BindView(R.id.cardselect)
    CardView cardSelect;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ro_detail);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);



        progressBar=new ProgressDialog(RoDetailActivity.this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            roId = extras.getString(Constants.ROID);
            Log.e("roId",roId);
            getRoDetail(roId);
        }

        getGstRate();

        list.clear();
        list.add("Select Booking");
        list.add("Rent");
        list.add("Buy");

        rentLinear.setVisibility(View.GONE);
        buyLinear.setVisibility(View.GONE);

        arraySpinnerAdapter=new ArraySpinnerAdapter(this);
        arraySpinnerAdapter.setData(list);
        buyRentSpinner.setAdapter(arraySpinnerAdapter);


        buyRentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                bookingType= list.get(i);
                if(bookingType.equals("Rent")){
                    rentLinear.setVisibility(View.VISIBLE);
                    buyLinear.setVisibility(View.GONE);
                    rentMonth.setVisibility(View.VISIBLE);
                    monthLinear.setVisibility(View.VISIBLE);
                    isBuy=false;
                    isRent=true;
                }else if(bookingType.equals("Buy")){
                    rentMonth.setVisibility(View.GONE);
                    rentLinear.setVisibility(View.GONE);
                    buyLinear.setVisibility(View.VISIBLE);

                    monthLinear.setVisibility(View.GONE);
                    isBuy=true;
                    isRent=false;
                    setDataVisible();
                }else{
                    rentLinear.setVisibility(View.GONE);
                    buyLinear.setVisibility(View.GONE);
                    monthLinear.setVisibility(View.GONE);
                    rentMonth.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.scrollTo(0, cardSelect.getBottom());
                    }
                });

                if(bookingType.equals("Rent")) {

                    if (detail.getTenure().equals("Select")) {
                        scrollView.post(new Runnable() {
                            @Override
                            public void run() {
                                scrollView.pageScroll(View.FOCUS_DOWN);
                            }
                        });
                        Toast.makeText(RoDetailActivity.this, "Please Select Month", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(RoDetailActivity.this, RoBookingActivity.class);
                        intent.putExtra(Constants.ROID, roId);
                        intent.putExtra(Constants.DEPOSIT, deposit);
                        intent.putExtra(Constants.BOOKINGTYPE, "0");
                        intent.putExtra(Constants.RENT, rentamt);
                        intent.putExtra(Constants.MONTH, month);
                        intent.putExtra(Constants.GST,String.valueOf(0));
                        intent.putExtra(Constants.GSTAMT,String.valueOf(0));
                        startActivity(intent);
                    }
                }
                else if(bookingType.equals("Buy")){
                    Intent intent = new Intent(RoDetailActivity.this, RoBookingActivity.class);
                    intent.putExtra(Constants.ROID, roId);
                    intent.putExtra(Constants.BOOKINGTYPE,"1");
                    intent.putExtra(Constants.BUY, totalamount);
                    intent.putExtra(Constants.BUYAMT, buyAmt.getText().toString());
                    intent.putExtra(Constants.GST,String.valueOf(gstRate));
                    intent.putExtra(Constants.GSTAMT,String.valueOf(result));
                    startActivity(intent);
                }
                else{
                    Toast.makeText(RoDetailActivity.this, "Please Select Booking Type", Toast.LENGTH_SHORT).show();
                }
            }
        });


        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                detail= listDetails.get(i);
                if(detail!=null) {
                    isRent=true;
                    if(detail.getTenure().equals("Select")){
                        monthLinear.setVisibility(View.GONE);
                    }else{
                        monthLinear.setVisibility(View.VISIBLE);
                    }

                    refundamt.setText("\u20B9 "+detail.getRefundableDeposit());
                    rentamt=detail.getRent();
                    month=detail.getTenure();
                    deposit=detail.getRefundableDeposit();
                    rent.setText("\u20B9 "+detail.getRent());
                   // setDataVisible();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        imageSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                monthSpinner.performClick();
            }
        });

        bookspinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buyRentSpinner.performClick();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    public void setDataVisible(){
        try {
            if (isBuy) {
                gstLinear.setVisibility(View.VISIBLE);
                result = buyamt * gstRate / 100;
                totalamount = String.valueOf(buyamt + result);
                gstAmount.setText("\u20B9 " + String.valueOf(result));
                gst.setText("Exclusive Gst (" + gstRate + "% )");
                booking.setText("Pay "+"\u20B9 " + totalamount);
            } else if (isRent) {
                gstLinear.setVisibility(View.VISIBLE);
                result = Double.parseDouble(detail.getRent()) * gstRate / 100;
                totalamount = String.valueOf(Double.parseDouble(totalamount) + result);
                gstAmount.setText("\u20B9 " + String.valueOf(result));
                gst.setText("Exclusive Gst (" + gstRate + "% )");
                booking.setText("Pay "+"\u20B9 " + totalamount);
            } else {
                gstLinear.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getGstRate(){
        Call<GstRateResponse> call=RestClient.get().getGstRate(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<GstRateResponse>() {
            @Override
            public void onResponse(Call<GstRateResponse> call, retrofit2.Response<GstRateResponse> response) {

                if(response.code()==200){
                    GstRateResponse gstRateResponse=response.body();

                    if(gstRateResponse.getStatus()) {
                        if (gstRateResponse.getGst().size() >= 1) {
                            gstRate = Integer.parseInt(gstRateResponse.getGst().get(1).getName());
                        }
                    }else{
                        gstRate=0;
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getApplicationContext());
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<GstRateResponse> call, Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void getRoDetail(final String roId){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("roid",roId);
        Call<RoDetailResponse> call=RestClient.get().roDetail(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<RoDetailResponse>() {
            @TargetApi(Build.VERSION_CODES.N)
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<RoDetailResponse> call, Response<RoDetailResponse> response) {
                RoDetailResponse roDetailResponse=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(roDetailResponse.getStatus()){

                        booking.setVisibility(View.VISIBLE);
                        name.setText(roDetailResponse.getData().getName());
                        Glide.with(getApplicationContext())
                                .load(RestClient.base_image_url+roDetailResponse.getData().getImage())
                                .into(image);
                        details.setText(roDetailResponse.getData().getContent());
                        buyAmt.setText(roDetailResponse.getData().getBuy_amount());
                        buyamt= Double.parseDouble(roDetailResponse.getData().getBuy_amount());


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            details.setText(Html.fromHtml(roDetailResponse.getData().getContent()+"...", Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            details.setText(Html.fromHtml(roDetailResponse.getData().getContent()+"..."));
                        }

                        listDetails.clear();
                        Detail detail=new Detail();
                        detail.setTenure("Select");
                        detail.setRent("Rent");
                        listDetails.add(detail);
                        listDetails.addAll(roDetailResponse.getDetails());
                        monthSpinnerAdapter=new MonthSpinnerAdapter(RoDetailActivity.this);
                        monthSpinnerAdapter.setData(listDetails);
                        monthSpinner.setAdapter(monthSpinnerAdapter);

                        if(roDetailResponse.getDetails().size()==0){
                            for(int i=0;i<list.size();i++){
                                if(list.get(i).equals("Rent")){
                                    list.remove(i);
                                }
                                //bookingType="Buy";
                            }
                        }
                        arraySpinnerAdapter.setData(list);

                    }
                }else if(response.code()==401){
                    AppUtils.logout(RoDetailActivity.this);
                }else{

                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RoDetailResponse> call, @NonNull Throwable t) {
                progressBar.dismiss();
                t.printStackTrace();
                booking.setVisibility(View.GONE);
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(RoDetailActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(RoDetailActivity.this,Id);

            }
        });



    }
}
