
package in.happyhelp.limra.activity.response.jobsubresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobSubcategoryResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<JobSubCategory> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<JobSubCategory> getData() {
        return data;
    }

    public void setData(List<JobSubCategory> data) {
        this.data = data;
    }

}
