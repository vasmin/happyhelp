
package in.happyhelp.limra.activity.response.companydetailsresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("plan")
    @Expose
    private String plan;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("km_limit")
    @Expose
    private String kmLimit;
    @SerializedName("planamount")
    @Expose
    private String planamount;
    @SerializedName("planstart")
    @Expose
    private String planstart;
    @SerializedName("planexpire")
    @Expose
    private String planexpire;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("gallery_images")
    @Expose
    private List<String> galleryImages = null;
    @SerializedName("banner")
    @Expose
    private String banner;
    @SerializedName("services")
    @Expose
    private List<String> services = null;
    @SerializedName("working_hours")
    @Expose
    private List<WorkingHour> workingHours = null;
    @SerializedName("one_hour")
    @Expose
    private String oneHour;

    @SerializedName("three_hour")
    @Expose
    private String threeHour;


    @SerializedName("show_button")
    @Expose
    private Boolean show_button;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getKmLimit() {
        return kmLimit;
    }

    public void setKmLimit(String kmLimit) {
        this.kmLimit = kmLimit;
    }

    public String getPlanamount() {
        return planamount;
    }

    public void setPlanamount(String planamount) {
        this.planamount = planamount;
    }

    public String getPlanstart() {
        return planstart;
    }

    public void setPlanstart(String planstart) {
        this.planstart = planstart;
    }

    public String getPlanexpire() {
        return planexpire;
    }

    public void setPlanexpire(String planexpire) {
        this.planexpire = planexpire;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<String> getGalleryImages() {
        return galleryImages;
    }

    public void setGalleryImages(List<String> galleryImages) {
        this.galleryImages = galleryImages;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public List<String> getServices() {
        return services;
    }

    public void setServices(List<String> services) {
        this.services = services;
    }

    public List<WorkingHour> getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(List<WorkingHour> workingHours) {
        this.workingHours = workingHours;
    }

    public String getOneHour() {
        return oneHour;
    }

    public void setOneHour(String oneHour) {
        this.oneHour = oneHour;
    }

    public String getThreeHour() {
        return threeHour;
    }

    public void setThreeHour(String threeHour) {
        this.threeHour = threeHour;
    }


    public Boolean getShow_button() {
        return show_button;
    }

    public void setShow_button(Boolean show_button) {
        this.show_button = show_button;
    }
}
