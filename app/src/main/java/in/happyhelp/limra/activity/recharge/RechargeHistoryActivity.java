package in.happyhelp.limra.activity.recharge;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.property.SearchPropertyActivity;
import in.happyhelp.limra.adapter.recharger.HistoryAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.rechargeresponse.rechargelist.RechargeListResponse;
import in.happyhelp.limra.shopping.HomeShoppingActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RechargeHistoryActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    HistoryAdapter historyAdapter;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.data)
    TextView data;

    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_history);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);


        historyAdapter=new HistoryAdapter(getApplicationContext());
        getRechargeList();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void getRechargeList(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<RechargeListResponse> call= RestClient.get(RestClient.RECHARGE_BASE_URL).getRechargeList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<RechargeListResponse>() {
            @Override
            public void onResponse(@NonNull Call<RechargeListResponse> call, @NonNull Response<RechargeListResponse> response) {
                RechargeListResponse rechargeListResponse=response.body();
                if(response.code()==200){

                    if(rechargeListResponse.getStatus()) {
                        if ( rechargeListResponse.getHistory().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }

                        historyAdapter.setData(rechargeListResponse.getHistory());
                        recyclerView.setAdapter(historyAdapter);
                    }else{
                        if ( rechargeListResponse.getHistory().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }

                        historyAdapter.setData(rechargeListResponse.getHistory());
                        recyclerView.setAdapter(historyAdapter);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(RechargeHistoryActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<RechargeListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onRefresh() {
        getRechargeList();
    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }


    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(RechargeHistoryActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(RechargeHistoryActivity.this,Id);

            }
        });
    }
}
