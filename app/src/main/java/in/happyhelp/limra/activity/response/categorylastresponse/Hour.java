
package in.happyhelp.limra.activity.response.categorylastresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hour {

    @SerializedName("hour")
    @Expose
    private String hour;
    @SerializedName("service_name")
    @Expose
    private String serviceName;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("vamount")
    @Expose
    private String vamount;

        @SerializedName("visitingamount")
    @Expose
    private String visitingamount;

    @SerializedName("services")
    @Expose
    private String services;

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getVamount() {
        return vamount;
    }

    public void setVamount(String vamount) {
        this.vamount = vamount;
    }


    public String getVisitingamount() {
        return visitingamount;
    }

    public void setVisitingamount(String visitingamount) {
        this.visitingamount = visitingamount;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }
}
