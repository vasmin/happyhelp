package in.happyhelp.limra.activity.ro;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.ConfirmationActivity;
import in.happyhelp.limra.adapter.MyRoAdapter;
import in.happyhelp.limra.adapter.MyRoPayment;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.gstresponse.GstRateResponse;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.myrodetailresponse.Bill;
import in.happyhelp.limra.activity.response.myrodetailresponse.MyRoDetailsResponse;
import in.happyhelp.limra.activity.response.myroresponse.MyRo;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyRoDetailActivity extends AppCompatActivity {

    String roId;

    MyRo myRo;
    Realm realm;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.detail)
    TextView detail;

    @BindView(R.id.rent)
    TextView rentAmt;

    @BindView(R.id.refundamt)
    TextView refundAmt;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.image)
    ImageView imageView;

    LinearLayoutManager linearLayoutManager;
    MyRoPayment myRoPaymentAdapter;
    ProgressDialog progressBar;
    String billId;

    DialogPlus dialog;
    Bill bill;
    String email,username,roName,mobile;

    int gstRate=0;
    double gstAmt=0;
    boolean isSelected=false;
    TextView rentamt,gstrate,gstamt,cancel,payment,date;

    double result=0;
    String totalamount="0";

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ro_detail);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        AppUtils.isNetworkConnectionAvailable(this);
        ButterKnife.bind(this);
        realm=RealmHelper.getRealmInstance();
        progressBar=new ProgressDialog(MyRoDetailActivity.this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            roId = extras.getString(Constants.MYRO);
            getRoDetail(roId);
        }
        getGstRate();
        setDialog(this);


        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // callInstamojoPay(myRo.getEmail(), myRo.getMobile(), totalamount, "Ro Rent", myRo.getName());

                callInstamojoPay(myRo.getEmail(), myRo.getMobile(), String.valueOf(Math.round(Double.parseDouble(totalamount))), "Ro Rent", myRo.getName());
               // callInstamojoPay(email, mobile, totalamount, "Ro Rent", username);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        myRoPaymentAdapter=new MyRoPayment(getApplicationContext(), new MyRoPayment.OnItemClickListner() {
            @Override
            public void onItemClick(final Bill d, View view) throws ParseException {
                    if(d.getStatus().equals("0")) {
                        try {
                            bill=d;
                            billId = d.getId();
                            isSelected=true;
                            setDataVisible();
                            dialog.show();
                          //  callInstamojoPay(myRo.getEmail(), myRo.getMobile(), d.getAmount(), "Ro Rent", myRo.getName());
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

            }
        });

        myRo=realm.where(MyRo.class).equalTo("id",roId).findFirst();
        if (myRo != null)
        if(myRo.isValid() && myRo.isLoaded()) {
            name.setText(myRo.getRoname());
            // detail.setText();
            roName=myRo.getRoname();
            username=myRo.getName();
            email=myRo.getEmail();
            mobile=myRo.getMobile();
            refundAmt.setText("\u20B9 " + myRo.getDeposit());
            rentAmt.setText("\u20B9 " + myRo.getRent());
            Glide.with(getApplicationContext())
                    .load(RestClient.base_image_url + myRo.getImage())
                    .into(imageView);
        }
    }


    @SuppressLint("SetTextI18n")
    public void setDataVisible(){
        if(isSelected){
            result =  Double.parseDouble(bill.getAmount()) * gstRate / 100;
            totalamount= String.valueOf(Double.parseDouble(bill.getAmount())+result);
            rentamt.setText(bill.getAmount());
            date.setText(bill.getDate());
            gstamt.setText("\u20B9 "+String.valueOf(result));
            gstrate.setText("Exclusive Gst ("+gstRate+"% )");
            payment.setText("Pay Rent "+"\u20B9 "+totalamount);
        }else{
        }
    }


    @SuppressLint("PrivateResource")
    public void setDialog(Activity activity) {
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.ro_rent_item, null);
        dialog = DialogPlus.newDialog(activity)
                .setContentHolder(new ViewHolder(view))
                .setGravity(Gravity.CENTER)
                .setInAnimation(R.anim.abc_fade_in)
                .setMargin(50, 20, 50, 50)
                .setOutAnimation(R.anim.abc_fade_out)
                .setOverlayBackgroundResource(Color.TRANSPARENT)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogPlus dialog) {
                        dialog.dismiss();
                    }
                })
                .create();

        rentamt= (TextView) dialog.findViewById(R.id.rentamt);
        gstamt= (TextView) dialog.findViewById(R.id.gstamt);
        gstrate= (TextView) dialog.findViewById(R.id.gst);
        cancel= (TextView) dialog.findViewById(R.id.cancel);
        payment= (TextView) dialog.findViewById(R.id.payment);
        date= (TextView) dialog.findViewById(R.id.date);





    }


    public void getGstRate(){
        Call<GstRateResponse> call=RestClient.get().getGstRate(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<GstRateResponse>() {
            @Override
            public void onResponse(Call<GstRateResponse> call, retrofit2.Response<GstRateResponse> response) {

                if(response.code()==200){
                    GstRateResponse gstRateResponse=response.body();

                    if(gstRateResponse.getStatus()) {
                        if (gstRateResponse.getGst().size() >= 3) {
                            gstRate = Integer.parseInt(gstRateResponse.getGst().get(3).getName());
                        }
                    }else{
                        gstRate=0;
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getApplicationContext());
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<GstRateResponse> call, Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }


    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);



                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();

                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);

                getRoPayment(payment);

            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: transaction" + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }



    public  void getRoPayment(String payment){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("robillid",billId);
        hashMap.put("amount",rentAmt.getText().toString());
        hashMap.put("payment_id",payment);
        hashMap.put("gst", String.valueOf(gstRate));
        hashMap.put("gstamt", String.valueOf(gstAmt));
        Call<Object> call=RestClient.get().myRoBill(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(@NonNull Call<Object> call, @NonNull Response<Object> response) {
                Intent intent=new Intent(
                        MyRoDetailActivity.this,ConfirmationActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    private void getRoDetail(String roId) {
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",roId);
       Call<MyRoDetailsResponse> call=RestClient.get().myRoDetail(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
       call.enqueue(new Callback<MyRoDetailsResponse>() {
           @Override
           public void onResponse(@NonNull Call<MyRoDetailsResponse> call, @NonNull Response<MyRoDetailsResponse> response) {
            progressBar.dismiss();

            if(response.code()==200){
                MyRoDetailsResponse myRoDetailsResponse=response.body();

                if(myRoDetailsResponse.getStatus()){
                    myRoPaymentAdapter.setData(myRoDetailsResponse.getBills());
                    recyclerView.setAdapter(myRoPaymentAdapter);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }else if(response.code()==401){
                AppUtils.logout(MyRoDetailActivity.this);
            }else{
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
           }

           @Override
           public void onFailure(@NonNull Call<MyRoDetailsResponse> call, @NonNull Throwable t) {
               progressBar.dismiss();
               t.printStackTrace();
               View parentLayout = findViewById(android.R.id.content);
               Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                       .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                       .show();
           }
       });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MyRoDetailActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MyRoDetailActivity.this,Id);

            }
        });



    }


}
