
package in.happyhelp.limra.activity.response.myroresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyRoResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<MyRo> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<MyRo> getData() {
        return data;
    }

    public void setData(List<MyRo> data) {
        this.data = data;
    }

}
