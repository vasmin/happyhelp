package in.happyhelp.limra.activity.property;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.Jobs.JobProfileActivity;
import in.happyhelp.limra.activity.LoginActivity;
import in.happyhelp.limra.activity.recharge.RechargeActivity;
import in.happyhelp.limra.activity.response.propertydetailsresponse.PropertyDetailResponse;
import in.happyhelp.limra.activity.response.propertydetailsresponse.PropertyDetails;
import in.happyhelp.limra.activity.video.MyVideoActivity;
import in.happyhelp.limra.adapter.AllPropertyAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PropertyDetailsActivity extends AppCompatActivity  {

    @BindView(R.id.propertyname)
    TextView name;

    @BindView(R.id.contactname)
    TextView contactName;

    @BindView(R.id.propertytype)
    TextView propertyType;

    @BindView(R.id.dealtype)
    TextView dealType;

    @BindView(R.id.floor)
    TextView noFloor;

    @BindView(R.id.building)
    TextView noBuilding;

    @BindView(R.id.expectedprice)
    TextView expectedPrice;

    ProgressDialog progressBar;

    @BindView(R.id.address)
    TextView address;

    @BindView(R.id.area)
    TextView buildArea;

    @BindView(R.id.carpetarea)
    TextView carpetArea;

    @BindView(R.id.mobile)
    TextView mobile;

    @BindView(R.id.bedroom)
    TextView bedRoom;

    @BindView(R.id.lift)
    TextView lift;

    @BindView(R.id.swimming)
    TextView swimingPool;

    @BindView(R.id.call)
    LinearLayout call;

    @BindView(R.id.price)
    TextView price;

    @BindView(R.id.propertyage)
    TextView propertyAge;

    @BindView(R.id.parking)
    TextView parking;

    @BindView(R.id.amenities)
    TextView amenities;
    List<String> listFooter=new ArrayList<>();

    @BindView(R.id.slider)
    SliderLayout slider;

    @BindView(R.id.enquiry)
    TextView enquiry;

    @BindView(R.id.postdate)
    TextView postDate;

    @BindView(R.id.city)
    TextView city;

    @BindView(R.id.weburl)
            TextView weburl;

    String propertyId="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);


        Intent intent = getIntent();
        if(intent.getData()!=null) {
            Uri uri = intent.getData();
            propertyId = uri.getQueryParameter(Constants.PROPERTYID);
            if (!propertyId.equals("")) {
                getPropertyDetails(propertyId);
            }
        }


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            propertyId = extras.getString(Constants.PROPERTYID);
            getPropertyDetails(propertyId);
        }

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mobile.getText().toString()));
                startActivity(intent);
            }
        });

        enquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PropertyDetailsActivity.this,PropertyEnquiryActivity.class);
                intent.putExtra(Constants.PROPERTYID,propertyId);
                startActivity(intent);
            }
        });


        slider.setPresetTransformer(com.daimajia.slider.library.SliderLayout.Transformer.Accordion);
        slider.setPresetIndicator(com.daimajia.slider.library.SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


    }

    public void getPropertyDetails(final String propertyId){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("property_id",propertyId);
        Call<PropertyDetailResponse> call= RestClient.get(RestClient.PROPERTY_BASE_URL).getPropertyDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<PropertyDetailResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<PropertyDetailResponse> call, @NonNull Response<PropertyDetailResponse> response) {
                PropertyDetailResponse propertyDetailResponse=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(propertyDetailResponse.getStatus()){
                        PropertyDetails propertyDetails=propertyDetailResponse.getPropertyDetails();
                        if(propertyDetails!=null) {

                            name.setText(propertyDetails.getPropertyName());
                            contactName.setText(propertyDetails.getName());
                            propertyType.setText(propertyDetails.getProType());
                            expectedPrice.setText("\u20B9 "+propertyDetails.getMinExpPrice());
                            dealType.setText(propertyDetails.getDealType());
                            noFloor.setText(propertyDetails.getFloorNo());
                            noBuilding.setText(propertyDetails.getNoOfBldngs());

                            weburl.setText(Html.fromHtml(propertyDetails.getWebsite_url()));
                            weburl.setMovementMethod(LinkMovementMethod.getInstance());

                            address.setText(propertyDetails.getAddress() +" "+propertyDetails.getBuildrName()+" "+propertyDetails.getBuildrName()+" "+propertyDetails.getCity()+" "+propertyDetails.getPincode());
                           // Glide.with(getApplicationContext()).load(RestClient.base_image_url + propertyDetails.getImages().get(0)).into(imageView);
                            buildArea.setText(propertyDetails.getBuiltArea()+" Square/ft");
                            carpetArea.setText(propertyDetails.getCarpArea()+" Square/ft");
                           // swimingPool.setText(propertyDetails.getSwmingPool());
                           // lift.setText(propertyDetails.getNoLift());
                            mobile.setText(propertyDetails.getMobile());

                            if(propertyDetails.getBedroom().equals("0")) {
                                bedRoom.setText("1 RK");
                            }else if(propertyDetails.getBedroom().equals("1")) {
                                bedRoom.setText("1 BHK");
                            }else if(propertyDetails.getBedroom().equals("2")) {
                                bedRoom.setText("2 BHK");
                            }else if(propertyDetails.getBedroom().equals("3")) {
                                bedRoom.setText("3 BHK");
                            }else if(propertyDetails.getBedroom().equals("4")) {
                                bedRoom.setText("4 BHK");
                            }else{
                                bedRoom.setText("1 RK");
                            }

                            postDate.setText(propertyDetails.getPostedDate());
                            price.setText("\u20B9 "+propertyDetails.getOfferPrice());
                            parking.setText(propertyDetails.getNoParking());
                            propertyAge.setText(propertyDetails.getNoOfYears()+" "+propertyDetails.getNoOfMonths());
                            amenities.setText(propertyDetails.getOtherAmenities());
                            city.setText(propertyDetails.getCity());

                            for(int i=0;i<propertyDetails.getImages().size();i++){
                                listFooter.clear();
                                listFooter.addAll(propertyDetails.getImages());
                            }

                            for(int i = 0; i<listFooter.size();i ++) {
                                DefaultSliderView defaultSliderView = new DefaultSliderView(getApplicationContext());
                                defaultSliderView.image(RestClient.base_image_url+listFooter.get(i));
                                slider.addSlider(defaultSliderView);
                            }

                            if(propertyDetailResponse.getIs_my_property().equals("1")){
                                enquiry.setVisibility(View.GONE);
                            }else{
                                enquiry.setVisibility(View.VISIBLE);
                            }

                        }else{
                            View parentLayout = findViewById(android.R.id.content);
                            Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                    .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                    .show();
                        }
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();

                    }

                }else if(response.code()==401){
                    AppUtils.logout(PropertyDetailsActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<PropertyDetailResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(PropertyDetailsActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(PropertyDetailsActivity.this,Id);

            }
        });
    }

}
