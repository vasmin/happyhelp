package in.happyhelp.limra.activity.Jobs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.jobapplicationresponse.JobApplication;
import in.happyhelp.limra.activity.response.jobapplicationresponse.JobApplicationResponse;
import in.happyhelp.limra.adapter.JobApplicationAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shopping.ShoppyAddressActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobApplicationsActivity extends AppCompatActivity implements  SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    String jobId="";
    JobApplicationAdapter adapter;

    LinearLayoutManager linearLayoutManager;
    @BindView(R.id.data)
    TextView data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_applications);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            jobId = extras.getString(Constants.JOBID);

            getJobApplications(jobId);


        }

        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter=new JobApplicationAdapter(getApplicationContext());
    }
    public void getJobApplications(String jobId)
    {
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("jobid",jobId);
        Call<JobApplicationResponse> call= RestClient.get().getJobApplications(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<JobApplicationResponse>() {
            @Override
            public void onResponse(@NonNull Call<JobApplicationResponse> call, @NonNull Response<JobApplicationResponse> response) {
                JobApplicationResponse jobApplicationResponse=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(jobApplicationResponse.getStatus()){
                        if(jobApplicationResponse.getData().size()>0) {
                            adapter.setData(jobApplicationResponse.getData());
                            recyclerView.setAdapter(adapter);
                            data.setVisibility(View.GONE);
                        }else{
                            data.setVisibility(View.VISIBLE);
                        }
                    }else{
                        data.setVisibility(View.VISIBLE);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(JobApplicationsActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JobApplicationResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }
    @Override
    public void onRefresh() {
        getJobApplications(jobId);
    }



    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(JobApplicationsActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(JobApplicationsActivity.this,Id);

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
