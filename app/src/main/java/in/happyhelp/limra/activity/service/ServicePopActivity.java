package in.happyhelp.limra.activity.service;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.GPSTracker;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.HomeActivity;
import in.happyhelp.limra.activity.MapsActivity;
import in.happyhelp.limra.activity.MyCompanyActivity;
import in.happyhelp.limra.activity.ro.RoRentActivity;
import in.happyhelp.limra.adapter.ServicePopupAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.model.ServiceModel;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.categoryresponse.CategoryResponse;
import in.happyhelp.limra.activity.response.serrviceresponse.ServiceBannerResponse;
import in.happyhelp.limra.activity.response.servicecategoryresponse.ServiceCategoryResponse;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServicePopActivity extends AppCompatActivity {

    @BindView(R.id.servicequestion)
    TextView serviceQuestion;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.slider)
    SliderLayout slider;

    ServicePopupAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    List<ServiceModel> serviceList=new ArrayList<>();

    int i=0;
    Realm realm;
    Menu mMenu;
    boolean isRemove=false;

    @BindView(R.id.card)
    CardView cardView;

    @BindView(R.id.skip)
    TextView skip;
    List<String> listBanner=new ArrayList<>();
    ArrayList<String> treeFlow=new ArrayList<>();

    ProgressDialog progressBar;

    GPSTracker gps;
    Location location;
    String latitude="";
    String longitude="";
    boolean isPopUp=false;

    String level="0";

    @BindView(R.id.location)
    LinearLayout locationLinear;

    boolean isSearbar=false;

    @BindView(R.id.address)
    TextView locationAddress;

    String catid="";

    @BindView(R.id.data)
    TextView data;

    @BindView(R.id.search_view)
    SearchView searchView;

    String address;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_pop);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().hide();
        AppUtils.isNetworkConnectionAvailable(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            latitude = extras.getString(Constants.LATITUDE);
            longitude=extras.getString(Constants.LONGITUDE);
            Log.e("Pop Lat",latitude+longitude);
            isPopUp=true;

            SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.LATITUDE,latitude);
            SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.LATITUDE,longitude);

            address=AppUtils.getCompleteAddressString(getApplicationContext(),Double.parseDouble(latitude),Double.parseDouble(longitude));
            locationAddress.setText(address);


        }


        locationLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ServicePopActivity.this, MapsActivity.class);
                intent.putExtra(Constants.LATITUDE,latitude);
                intent.putExtra(Constants.LONGITUDE,longitude);
                intent.putExtra(Constants.ISPOPUP,"0");
                startActivity(intent);
            }
        });
        realm=RealmHelper.getRealmInstance();
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        // recyclerView.setLayoutManager(linearLayoutManager);
        //recyclerView.setHasFixedSize(true);
        int mNoOfColumns = AppUtils.calculateNoOfColumns(getApplicationContext(),150);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, mNoOfColumns, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);

        adapter=new ServicePopupAdapter(getApplicationContext(), new ServicePopupAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(ServiceModel d, View view) throws ParseException {

                level=d.getLevel();
                if(level.equals("0")){
                    setQuestion(d.getId());
                } else if(level.equals("1")){
                    setQuestion(d.getId());
                }else if(level.equals("2")){
                    setQuestion(d.getSubId());
                }
            }
        });

        searchView.setQueryHint("Search Services");
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.e("onQueryTextSubmit",query);
                isSearbar=true;

                if(query.equals("")) {
                    isSearbar=false;
                    if (!catid.equals("")) {
                        getcategory(catid, "");
                    } else {
                        getServices("");
                    }
                }  else  {
                    if (!catid.equals("")) {
                        getcategory(catid, query);
                    } else {
                        getServices(query);
                    }
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                Log.e("onQueryTextChange",query);


                isSearbar=true;

                if(query.equals("")) {
                    isSearbar=false;
                    if (!catid.equals("")) {
                        getcategory(catid, "");
                    } else {
                        getServices("");
                    }
                }  else  {
                    if (!catid.equals("")) {
                        getcategory(catid, query);
                    } else {
                        getServices(query);
                    }
                }
                return false;
            }
        });

        gps = new GPSTracker(this);

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            gps.showSettingsAlert();
        }


        slider.movePrevPosition(false);
        slider.setPresetTransformer(com.daimajia.slider.library.SliderLayout.Transformer.Accordion);
        slider.setPresetIndicator(com.daimajia.slider.library.SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(3000);
        slider.startAutoCycle();

        getBanner();

        // Check if GPS enabled
        if(!isPopUp)
        if (gps.canGetLocation()) {
            location = gps.getLocation();
            if (location != null) {

                latitude = String.valueOf(location.getLatitude());
                longitude = String.valueOf(location.getLongitude());
                SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.LATITUDE, latitude);
                SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.LONGITUDE, longitude);
                address=AppUtils.getCompleteAddressString(getApplicationContext(),Double.parseDouble(latitude),Double.parseDouble(longitude));
                locationAddress.setText(address);

            }
        } else {
            gps.showSettingsAlert();
        }


        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);


        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ServicePopActivity.this,ServicesListActivity.class);
                intent.putExtra(Constants.LATITUDE,latitude);
                intent.putExtra(Constants.LONGITUDE,longitude);
                intent.putExtra(Constants.ISPOPUP,"1");
                startActivity(intent);
                finish();
            }
        });
        setQuestion("");
        serviceQuestion.setText("What service do you need?" );
    }

    public void getBanner(){
        Call<ServiceBannerResponse> call=RestClient.get().getServiceBanner(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<ServiceBannerResponse>() {
            @Override
            public void onResponse(@NonNull Call<ServiceBannerResponse> call, @NonNull Response<ServiceBannerResponse> response) {
                final ServiceBannerResponse serviceBannerResponse=response.body();

                if(response.code()==200){
                    if(serviceBannerResponse.getStatus()) {

                        listBanner.clear();
                        for(int i=0;i<serviceBannerResponse.getData().size();i++){
                            listBanner.add(serviceBannerResponse.getData().get(i).getImage());
                        }


                        for (int i = 0; i < listBanner.size(); i++) {
                            DefaultSliderView defaultSliderView = new DefaultSliderView(getApplicationContext());
                            final int finalI = i;
                            defaultSliderView.image(RestClient.base_image_url + listBanner.get(i))
                                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                        @Override
                                        public void onSliderClick(BaseSliderView slider) {
                                            String BannnerName = listBanner.get(finalI);
                            /*JobCategory banners=realm.where(JobCategory.class).equalTo("image",BannnerName).findFirst();
                            String url = banners.getLink();

                            if(!url.equals("")) {
                                Intent i = new Intent(Intent.ACTION_VIEW,
                                        Uri.parse(url));
                                startActivity(i);
                            }*/
                                        }
                                    });
                            slider.addSlider(defaultSliderView);
                        }
                        //slider.movePrevPosition(false);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                slider.startAutoCycle();
                            }
                        }, 5000);


                    }


                }else if(response.code()==401){
                    AppUtils.logout(ServicePopActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<ServiceBannerResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void setQuestion(String serviceModel){
        if(level.equals("0")){
            getServices("");
        } else if(level.equals("1")){
            isRemove=false;
            getcategory(serviceModel,"");
        }else if(level.equals("2")){
            isRemove=false;
            getcategory(serviceModel,"");
        }
    }

    public void getcategory(final String serviceModel, String search){
        if(!isSearbar) {
            progressBar.show();
        }
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("catid",serviceModel);
        hashMap.put("latitude",latitude);
        hashMap.put("longitude",longitude);
        if(!search.equals("")) {
            hashMap.put("search", search);
        }

        Call<CategoryResponse>call =RestClient.get().getSubCategory(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<CategoryResponse> call, @NonNull Response<CategoryResponse> response) {
                progressBar.dismiss();
                if(response.code()==200){
                    CategoryResponse categoryResponse=response.body();
                    int levels=1;
                    if(categoryResponse.getStatus()){
                        if(categoryResponse.getIsChild()) {
                            serviceList.clear();
                            for (int i = 0; i < categoryResponse.getData().size(); i++) {
                                ServiceModel serviceModel = new ServiceModel();
                                serviceModel.setId(categoryResponse.getData().get(i).getId());
                                serviceModel.setServiceName(categoryResponse.getData().get(i).getName());
                                serviceModel.setImage(categoryResponse.getData().get(i).getImage());

                                if(categoryResponse.getData().get(i).getLevel().equals("2")){
                                    serviceModel.setLevel("1");
                                    serviceModel.setSubId(categoryResponse.getData().get(i).getId());
                                }else if(categoryResponse.getData().get(i).getLevel().equals("3")){
                                    serviceModel.setLevel("2");
                                    serviceModel.setSubId(categoryResponse.getData().get(i).getId());
                                }

                              serviceList.add(serviceModel);
                            }

                            if(categoryResponse.getData().size()==0){
                                data.setVisibility(View.VISIBLE);
                                cardView.setVisibility(View.GONE);
                            }else{
                                data.setVisibility(View.GONE);
                                cardView.setVisibility(View.VISIBLE);
                            }

                            adapter.setData(serviceList);
                            recyclerView.setAdapter(adapter);

                            i--;

                            if(!isRemove) {
                                treeFlow.add(serviceModel);
                            }

                        }else{
                            treeFlow.add(serviceModel);
                            String servicesId="";
                            for(int i=0;i<treeFlow.size();i++){
                                if(i==treeFlow.size()-1){
                                    servicesId=servicesId+treeFlow.get(i);
                                }
                                else{
                                    servicesId=servicesId+treeFlow.get(i)+",";
                                }
                            }
                            Intent intent=new Intent(ServicePopActivity.this,RatesActivity.class);
                            intent.putStringArrayListExtra(Constants.SERVICELIST,treeFlow);
                            if(servicesId.equals("")) {
                                intent.putExtra(Constants.SERVICEID, serviceModel);
                            }else{
                                intent.putExtra(Constants.SERVICEID, servicesId);
                            }
                            intent.putExtra(Constants.CATEGORYID,serviceModel);
                            intent.putExtra(Constants.LATITUDE,latitude);
                            intent.putExtra(Constants.LONGITUDE,longitude);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    }else{
                        i--;

                    }
                }else if(response.code()==401){
                    AppUtils.logout(ServicePopActivity.this);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CategoryResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if(treeFlow.size()==0) {
            super.onBackPressed();
        }else{

            if(treeFlow.size()>0) {
                treeFlow.remove(treeFlow.size() - 1);
                isRemove = true;

                if(treeFlow.size()==0){
                    getServices("");
                }else {
                     catid = treeFlow.get(treeFlow.size() - 1);
                    getcategory(catid,"");
                }
            }else{
                String catid = treeFlow.get(treeFlow.size() );
                getcategory(catid,"");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        MenuItem skip = menu.findItem(R.id.skip);
        skip.setVisible(false);
        MenuItem map = menu.findItem(R.id.map);
        map.setVisible(false);

        MenuItem enquiryservice = menu.findItem(R.id.enquiryservice);
        enquiryservice.setVisible(false);
        this.mMenu=menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement

        if (id == R.id.skip) {
            Intent intent=new Intent(ServicePopActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.map) {

            Intent intent=new Intent(this, MapsActivity.class);
            intent.putExtra(Constants.LATITUDE,latitude);
            intent.putExtra(Constants.LONGITUDE,longitude);
            intent.putExtra(Constants.ISPOPUP,"0");
            startActivity(intent);

        }

        if (id == R.id.enquiryservice) {
            Intent intent=new Intent(this, ServiceEnquiryActivity.class);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);
    }




    public void getServices(String  search){
        HashMap<String,String> hashMap=new HashMap<>();
        if(!search.equals("")) {
            hashMap.put("search", search);
        }
        if(!isSearbar) {
            progressBar.show();
        }
        Call<ServiceCategoryResponse> call=RestClient.get().getMainServices(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<ServiceCategoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<ServiceCategoryResponse> call, @NonNull Response<ServiceCategoryResponse> response) {
                progressBar.dismiss();
                if(response.code()==200){
                    final ServiceCategoryResponse serviceCategoryResponse=response.body();
                    serviceList.clear();
                    for(int i=0;i<serviceCategoryResponse.getServiceCategoryList().size();i++){
                        ServiceModel serviceModel=new ServiceModel();
                        serviceModel.setId(serviceCategoryResponse.getServiceCategoryList().get(i).getId());
                        serviceModel.setLevel("1");
                        serviceModel.setServiceName(serviceCategoryResponse.getServiceCategoryList().get(i).getName());
                        serviceModel.setImage(serviceCategoryResponse.getServiceCategoryList().get(i).getImage());
                        serviceList.add(serviceModel);
                    }
                    adapter.setData(serviceList);
                    recyclerView.setAdapter(adapter);
                    if(serviceCategoryResponse.getServiceCategoryList().size()==0){
                        data.setVisibility(View.VISIBLE);
                        cardView.setVisibility(View.GONE);
                    }else{
                        data.setVisibility(View.GONE);
                        cardView.setVisibility(View.VISIBLE);
                    }

                }else if(response.code()==401){
                    AppUtils.logout(ServicePopActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<ServiceCategoryResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(ServicePopActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(ServicePopActivity.this,Id);

            }
        });



    }

    public void back(View view) {
        finish();
    }
}
