package in.happyhelp.limra.activity.Jobs;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.MyProfileActivity;
import in.happyhelp.limra.activity.RegisterActivity;
import in.happyhelp.limra.activity.response.jobdetail.JobDetail;
import in.happyhelp.limra.activity.response.jobdetail.JobDetailResponse;
import in.happyhelp.limra.activity.response.jobsubresponse.JobSubCategory;
import in.happyhelp.limra.activity.response.jobsubresponse.JobSubcategoryResponse;
import in.happyhelp.limra.activity.response.statecityresponse.City;
import in.happyhelp.limra.activity.response.statecityresponse.State;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.adapter.JobCategorySpinnerAdapter;
import in.happyhelp.limra.adapter.JobSubCategoryAdapter;
import in.happyhelp.limra.adapter.SalaryAdapter;
import in.happyhelp.limra.adapter.WorkAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.jobcategory.JobCategory;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;

public class PostJobsActivity extends AppCompatActivity {


    @BindView(R.id.jobtitle)
    EditText jobTitle;

    @BindView(R.id.jobrole)
    Spinner role;

    @BindView(R.id.jobcategory)
    Spinner jobcategory;

    @BindView(R.id.minsal)
    Spinner minSal;

    @BindView(R.id.maxsal)
    Spinner maxSal;

    JobSubCategoryAdapter jobSubCategoryAdapter;

    @BindView(R.id.companyname)
    EditText companyName;

    @BindView(R.id.description)
    EditText description;


    @BindView(R.id.address)
    EditText address;

    @BindView(R.id.email)
    EditText mail;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.company)
    Spinner companySpinner;

    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.postjob)
    LinearLayout postJob;

    @BindView(R.id.post)
    TextView post;
    ArraySpinnerAdapter adapter,jobTypeAdapter;

    List<String> list=new ArrayList<>();
    List<String> jobtype=new ArrayList<>();
    JobCategorySpinnerAdapter jobCategorySpinnerAdapter;


    String company="";
    String jobType="";
    String jobRole="";
    String jobCategory="";
    boolean isjob=false;

    String minsalary="",maxsalary="",minexp="",maxexp="";

    ProgressDialog progressBar;
    Realm realm;

    String jobId="";
    RealmResults<JobCategory> jobrole;
    RealmResults<JobSubCategory> jobsubCat;


    WorkAdapter minExpAdapter,maxExpAdapter;
    SalaryAdapter minSalaryAdapter,maxSalaryAdapter;

    @BindView(R.id.minexp)
            Spinner minExperience;

    @BindView(R.id.maxexp)
            Spinner maxExperience;


    @BindView(R.id.state)
    Spinner stateSpinner;


    String stateName,cityName,imageName,statename;

    @BindView(R.id.cityspinner)
    Spinner citySpinner;


    RealmResults<State> statesList;
    ArraySpinnerAdapter stateSpinnerAdapter,citySpinnerAdapter;

    List<String> listCity=new ArrayList<>();
    List<String> listState=new ArrayList<>();

    ArrayList<String> workList=new ArrayList<>();
    ArrayList<String> salaryList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_jobs);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        realm=RealmHelper.getRealmInstance();
        jobSubCategoryAdapter=new JobSubCategoryAdapter(this);
        stateSpinnerAdapter =new ArraySpinnerAdapter(this);
        citySpinnerAdapter =new ArraySpinnerAdapter(this);

        statesList=realm.where(State.class).findAllAsync();
        statesList.addChangeListener(new RealmChangeListener<RealmResults<State>>() {
            @Override
            public void onChange(RealmResults<State> states) {
                listState.clear();
                for(int i=0;i<statesList.size();i++){
                    listState.add(statesList.get(i).getState());
                }
                stateSpinnerAdapter.setData(listState);
                stateSpinner.setAdapter(stateSpinnerAdapter);
            }
        });

        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        jobtype.clear();
        jobtype.add("Full Time");
        jobtype.add("Part Time");
        jobtype.add("Work from Home");


        jobTypeAdapter=new ArraySpinnerAdapter(this);
        jobTypeAdapter.setData(jobtype);
        spinner.setAdapter(jobTypeAdapter);

        list.clear();
        list.add("Company");
        list.add("individual");
        list.add("Consultancy");

        workList.clear();
        workList.add("Experience");


        salaryList.clear();
        salaryList.add("Salary in ");


        for(int i=0;i<25;i++){
            workList.add(String.valueOf(i));
            salaryList.add(String.valueOf(i));
        }

        minExpAdapter=new WorkAdapter(this);
        maxExpAdapter=new WorkAdapter(this);

        minSalaryAdapter=new SalaryAdapter(this);
        maxSalaryAdapter=new SalaryAdapter(this);


        minExpAdapter.setData(workList);
        maxExpAdapter.setData(workList);

        minExperience.setAdapter(minExpAdapter);
        maxExperience.setAdapter(maxExpAdapter);


        minSalaryAdapter.setData(salaryList);
        maxSalaryAdapter.setData(salaryList);

        minSal.setAdapter(minSalaryAdapter);
        maxSal.setAdapter(maxSalaryAdapter);

        adapter=new ArraySpinnerAdapter(this);
        adapter.setData(list);
        companySpinner.setAdapter(adapter);

        jobCategorySpinnerAdapter=new JobCategorySpinnerAdapter(this);

        jobrole=realm.where(JobCategory.class).findAllAsync();
        jobrole.addChangeListener(new RealmChangeListener<RealmResults<JobCategory>>() {
            @Override
            public void onChange(RealmResults<JobCategory> jobCategories) {
                jobCategorySpinnerAdapter.setData(jobCategories);
                role.setAdapter(jobCategorySpinnerAdapter);

            }
        });

        jobsubCat=realm.where(JobSubCategory.class).findAllAsync();
        jobsubCat.addChangeListener(new RealmChangeListener<RealmResults<JobSubCategory>>() {
            @Override
            public void onChange(RealmResults<JobSubCategory> jobCategories) {
                jobSubCategoryAdapter.setData(jobCategories);
                jobcategory.setAdapter(jobSubCategoryAdapter);

            }
        });


        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stateName = String.valueOf(listState.get(i));
                final RealmResults<City> cityRealmResults = realm.where(City.class).equalTo("state", stateName).findAllAsync();
                cityRealmResults.addChangeListener(new RealmChangeListener<RealmResults<City>>() {
                    @Override
                    public void onChange(RealmResults<City> cities) {

                        listCity.clear();
                        listCity.add("Select City");
                        if (cityRealmResults.size() > 0) {
                            for (int i = 0; i < cityRealmResults.size(); i++) {
                                if (!listCity.contains(cities.get(i).getCity())) {
                                    listCity.add(cities.get(i).getCity());
                                }
                            }
                            citySpinnerAdapter.setData(listCity);
                            citySpinner.setAdapter(citySpinnerAdapter);
                        }
                    }


                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(listCity.size()>i) {
                    cityName = String.valueOf(listCity.get(i));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        minExperience.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(workList.size()>i) {
                    minexp = workList.get(i);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        maxExperience.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(workList.size()>i) {
                    maxexp = workList.get(i);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        minSal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(salaryList.size()>i) {
                    minsalary = salaryList.get(i);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        maxSal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(salaryList.size()>i) {
                    maxsalary = salaryList.get(i);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        companySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i<list.size()) {
                    company = list.get(i);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        role.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i<jobrole.size()) {
                    jobRole = jobrole.get(i).getId();
                    getJobSubcategory();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        jobcategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i<jobsubCat.size()) {
                    jobCategory = jobsubCat.get(i).getId();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i<jobtype.size()) {
                    jobType = jobtype.get(i);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        postJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(jobTitle.getText().toString())) {
                    jobTitle.setError("Enter jobTitle ");
                    return;
                }


                if(minsalary.equals("")||minsalary.equals("Salary in ")){
                    Toast.makeText(getApplicationContext(),"Select Min salary",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(maxsalary.equals("")||maxsalary.equals("Salary in ")){
                    Toast.makeText(getApplicationContext(),"Select  Max salary ",Toast.LENGTH_SHORT).show();
                    return;
                }

                Integer minsal= Integer.valueOf(minsalary);
                Integer maxsal= Integer.valueOf(maxsalary);

                if(minsal>maxsal){
                    Toast.makeText(getApplicationContext(),"Min Salary cannot be greater then Max salary",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(minsal==maxsal){
                    Toast.makeText(getApplicationContext(),"Min Salary and Max Salary cannot be same ",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(minexp.equals("")||minexp.equals("Experience")){
                    Toast.makeText(getApplicationContext(),"Select Min Work experience",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(maxexp.equals("")||maxexp.equals("Experience")){
                    Toast.makeText(getApplicationContext(),"Select Max Work experience",Toast.LENGTH_SHORT).show();
                    return;
                }

                Integer min= Integer.valueOf(minexp);
                Integer max= Integer.valueOf(maxexp);

                if(min>max){
                    Toast.makeText(getApplicationContext(),"Min Experience cannot be greater then Max Experience",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(min==max){
                    Toast.makeText(getApplicationContext(),"Min Experience and Max Experience cannot be same ",Toast.LENGTH_SHORT).show();
                    return;
                }


                if (TextUtils.isEmpty(companyName.getText().toString())) {
                    companyName.setError("Enter companyName ");
                    return;
                }

                if (TextUtils.isEmpty(description.getText().toString())) {
                    description.setError("Enter description ");
                    return;
                }

                if(stateName.equals("Select State")){
                    Toast.makeText(getApplicationContext(),"Select State",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(cityName.equals("Select City")){
                    Toast.makeText(getApplicationContext(),"Select City",Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(address.getText().toString())) {
                    address.setError("Enter address ");
                    return;
                }
                if(!AppUtils.isValidMail(mail.getText().toString(),mail)){
                    return;
                }

                if(AppUtils.isValidMobile(mobile.getText().toString())){

                }else{
                    mobile.setError("Enter valid mobile");
                    return;
                }

                if(!isjob) {
                    postJob();
                }else{
                    updateJob();
                }

            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            jobId = extras.getString(Constants.JOBID);
            isjob=extras.getBoolean(Constants.JOB);
            getJobDetail(jobId);

            if(isjob){
                post.setText("Update");
            }
        }



    }

    public void getJobSubcategory(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("catid",jobRole);
        Call<JobSubcategoryResponse> call=RestClient.get().jobSubcategory(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<JobSubcategoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<JobSubcategoryResponse> call, @NonNull retrofit2.Response<JobSubcategoryResponse> response) {
                final JobSubcategoryResponse jobSubcategoryResponse=response.body();
                if(response.code()==200){
                    if(jobSubcategoryResponse.getStatus()){
                        final JobSubCategory jobCategory=new JobSubCategory();
                        jobCategory.setName("Select Category");
                        jobCategory.setId("");
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {

                                realm.delete(JobSubCategory.class);
                                realm.copyToRealmOrUpdate(jobCategory);
                                realm.copyToRealmOrUpdate(jobSubcategoryResponse.getData());
                            }
                        });
                    }else{
                        final JobSubCategory jobCategory=new JobSubCategory();
                        jobCategory.setName("Select Category");
                        jobCategory.setId("");
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(JobSubCategory.class);
                                realm.copyToRealmOrUpdate(jobCategory);
                                realm.copyToRealmOrUpdate(jobSubcategoryResponse.getData());
                            }
                        });
                    }

                }else if(response.code()==401){
                    AppUtils.logout(PostJobsActivity.this);
                }else{

                }

            }

            @Override
            public void onFailure(@NonNull Call<JobSubcategoryResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getJobDetail(final String jobId){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("jobid",jobId);
        Call<JobDetailResponse> call= RestClient.get().jobdetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<JobDetailResponse>() {
            @Override
            public void onResponse(Call<JobDetailResponse> call, retrofit2.Response<JobDetailResponse> response) {
                progressBar.dismiss();
                JobDetailResponse jobDetailResponse=response.body();

                if(response.code()==200){
                    if(jobDetailResponse.getStatus()) {
                        JobDetail jobDetail=jobDetailResponse.getJobDetail();
                        if(jobDetail!=null) {
                            companyName.setText(jobDetail.getCompanyName());
                            mobile.setText(jobDetail.getMobile());
                            mail.setText(jobDetail.getEmail());
                            jobTitle.setText(jobDetail.getTitle());
                            description.setText(jobDetail.getDescription());


                            for(int i=0;i<workList.size();i++){
                                if(jobDetail.getMinExp().equals(workList.get(i))){
                                    minExperience.setSelection(i);
                                }


                                if(jobDetail.getMaxExp().equals(workList.get(i))){
                                    maxExperience.setSelection(i);
                                }

                            }

                            for(int i=0;i<salaryList.size();i++){
                                if(jobDetail.getMinSalary().equals(salaryList.get(i))){
                                    minSal.setSelection(i);
                                }


                                if(jobDetail.getMaxSalary().equals(salaryList.get(i))){
                                    maxSal.setSelection(i);
                                }

                            }


                                int pos=0;
                                for(int i=0;i<listState.size();i++){
                                    if(jobDetail.getState()!=null)
                                        if(jobDetail.getState().equals(listState.get(i))){
                                            pos=i;
                                        }
                                }

                               // stateSpinnerAdapter.setData(listState);
                                //stateSpinner.setAdapter(stateSpinnerAdapter);
                                stateSpinner.setSelection(pos);



                            for(int i=0;i<listCity.size();i++){
                                if(jobDetail.getCity()!=null)
                                    if(jobDetail.getCity().equals(listCity.get(i))){
                                      //  citySpinnerAdapter.setData(listCity);
                                       // citySpinner.setAdapter(citySpinnerAdapter);
                                        citySpinner.setSelection(i);
                                    }
                            }




                           // location.setText(jobDetail.getLocation());
                            address.setText(jobDetail.getAddress());

                            if(jobDetail.getRole().equals("0")) {
                                spinner.setSelection(0);
                            }else if(jobDetail.getRole().equals("1")) {
                                spinner.setSelection(1);
                            }else if(jobDetail.getRole().equals("2")) {
                                spinner.setSelection(2);
                            }

                            for(int i=0;i<list.size();i++){
                                if(jobDetail.getJobType().equals(list.get(i))) {
                                    role.setSelection(i);
                                }
                            }




                        }else{
                            View parentLayout = findViewById(android.R.id.content);
                            Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                    .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                    .show();
                        }

                    }
                    else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(PostJobsActivity.this);
                }
                else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<JobDetailResponse> call, Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void updateJob(){
        progressBar.show();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("jobid", jobId);
        builder.addFormDataPart("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        builder.addFormDataPart("title", jobTitle.getText().toString());
        builder.addFormDataPart("role", jobRole);
        builder.addFormDataPart("category", jobCategory);
        builder.addFormDataPart("min_salary",minsalary);
        builder.addFormDataPart("max_salary", maxsalary);
        builder.addFormDataPart("company_name", companyName.getText().toString());
        builder.addFormDataPart("description", description.getText().toString());
        builder.addFormDataPart("state", stateName);
        builder.addFormDataPart("location", cityName);
        builder.addFormDataPart("address", address.getText().toString());
        builder.addFormDataPart("email", mail.getText().toString());
        builder.addFormDataPart("mobile", mobile.getText().toString());
        builder.addFormDataPart("min_exp", minexp);
        builder.addFormDataPart("max_exp", maxexp);

        if(jobType.equals("Full Time")){
            builder.addFormDataPart("job_type", "0");
        }else if(jobType.equals("Part Time")){
            builder.addFormDataPart("job_type", "1");
        }else{
            builder.addFormDataPart("job_type", "2");
        }

     //   builder.addFormDataPart("i_am", company);


       /* if(path!=null){
            File filepath = new File(path);
            builder.addFormDataPart("cv", filepath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), filepath));
        }*/

        MultipartBody requestBody = builder.build();


        Call<Response> call= RestClient.get().updateJob(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                Response response1=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(response1.getStatus()){
                        new AlertDialog.Builder(PostJobsActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(response1.getMessage())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        onBackPressed();
                                    }

                                })
                                .show();
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(PostJobsActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void postJob(){
        progressBar.show();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        builder.addFormDataPart("title", jobTitle.getText().toString());
        builder.addFormDataPart("role", jobRole);
        builder.addFormDataPart("min_salary", minsalary);
        builder.addFormDataPart("max_salary", maxsalary);
        builder.addFormDataPart("company_name", companyName.getText().toString());
        builder.addFormDataPart("description", description.getText().toString());
        builder.addFormDataPart("state", stateName);
        builder.addFormDataPart("location", cityName);
        builder.addFormDataPart("address", address.getText().toString());
        builder.addFormDataPart("email", mail.getText().toString());
        builder.addFormDataPart("mobile", mobile.getText().toString());
        builder.addFormDataPart("min_exp", minexp);
        builder.addFormDataPart("max_exp", maxexp);

        if(jobType.equals("Full Time")){

            builder.addFormDataPart("job_type", "0");
        }else if(jobType.equals("Part Time")){
            builder.addFormDataPart("job_type", "1");
        }else{
            builder.addFormDataPart("job_type", "2");
        }

       // builder.addFormDataPart("i_am", company);


       /* if(path!=null){
            File filepath = new File(path);
            builder.addFormDataPart("cv", filepath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), filepath));
        }*/

        MultipartBody requestBody = builder.build();


        Call<Response> call= RestClient.get().postJob(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                Response response1=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(response1.getStatus()){
                        new AlertDialog.Builder(PostJobsActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(response1.getMessage())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent intent=new Intent(PostJobsActivity.this,JobHomeActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }

                                })
                                .show();
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(PostJobsActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }


}
