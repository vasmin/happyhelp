package in.happyhelp.limra.activity.service;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.ServiceRequestActivity;
import in.happyhelp.limra.activity.video.MyVideoActivity;
import in.happyhelp.limra.adapter.MyServicesAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.myservicesresponse.MyServices;
import in.happyhelp.limra.activity.response.myservicesresponse.MyServicesResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyServiceDetailsActivity extends AppCompatActivity {
    String id="";

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.date)
    TextView startDate;

    @BindView(R.id.mob)
    TextView mobile;

    @BindView(R.id.amount)
    TextView amount;

    @BindView(R.id.address)
    TextView address;

    @BindView(R.id.email)
    TextView email;

    @BindView(R.id.message)
    TextView message;
    ProgressDialog progressBar;

    @BindView(R.id.complete)
    TextView complete;

    DialogPlus dialog;

    @BindView(R.id.reject)
    TextView reject;

    @BindView(R.id.reach)
    TextView reachLocation;

    @BindView(R.id.status)
    TextView status;

    @BindView(R.id.confirm)
    TextView confirm;


    @BindView(R.id.paymentmode)
    TextView paymentMode;

    @BindView(R.id.service)
    TextView service;

    @BindView(R.id.paymentstatus)
    TextView paymentStatus;

    @BindView(R.id.walletlinear)
    LinearLayout walletLinear;

    @BindView(R.id.servicetype)
    TextView serviceType;

    @BindView(R.id.visitinglinear)
    LinearLayout visitingLinear;

    @BindView(R.id.visiting)
    TextView visiting;

    @BindView(R.id.schedulelinear)
    LinearLayout scheduleLinear;

    @BindView(R.id.walletamt)
    TextView walletAmt;


    @BindView(R.id.cashbackwallet)
    TextView cashbackwallet;

    @BindView(R.id.remainingamt)
    TextView remainingAmt;


    @BindView(R.id.quantity)
            TextView quantity;


    double serviceAmt=0;
    double cashwalletAmt=0;
    double cashbackWalletAmt=0;

    PinView pinView;

    String serviceStatus="0";


    TextView verify,resend;

    CountDownTimer countDownTimer;

    @BindView(R.id.paymentlinearstatus)
    LinearLayout paymentlinearstatus;

    @BindView(R.id.location)
    TextView location;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_service_details);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        AppUtils.isNetworkConnectionAvailable(this);
        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id = extras.getString("id");
            getMyServiceEnquiryDetail(id);
        }


        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(MyServiceDetailsActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Confirm Order ?")
                        .setMessage("Are you sure you want to Confirm Order ?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                setConfirmOrder();
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(MyServiceDetailsActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Reject Service ?")
                        .setMessage("Are you sure you reject service location ?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(serviceStatus.equals("7")) {
                                    setRejectService();
                                }else{
                                    Toast.makeText(getApplicationContext(),"Please first reach location",Toast.LENGTH_SHORT).show();
                                }
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });





        reachLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(MyServiceDetailsActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Reached Service Location ?")
                        .setMessage("Are you sure you reached service location ?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(serviceStatus.equals("3")||serviceStatus.equals("8")) {
                                    setReachLocation();
                                }else{
                                    Toast.makeText(getApplicationContext(),"Something Went Wrong",Toast.LENGTH_SHORT).show();
                                }
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MyServiceDetailsActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Complete Service ?")
                        .setMessage("Are you sure you want to Completed this Service ?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(serviceStatus.equals("7")) {
                                    setCompleteService();
                                }else{
                                    Toast.makeText(getApplicationContext(),"Please first reach location",Toast.LENGTH_SHORT).show();
                                }
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });
    }

    @SuppressLint("PrivateResource")
    public void setDialog(Activity activity) {
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.otp_verify, null);
        dialog = DialogPlus.newDialog(activity)
                .setContentHolder(new ViewHolder(view))
                .setGravity(Gravity.CENTER)
                .setCancelable(false)
                .setInAnimation(R.anim.abc_fade_in)
                .setMargin(50, 20, 50, 50)
                .setOutAnimation(R.anim.abc_fade_out)
                .setOverlayBackgroundResource(Color.TRANSPARENT)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogPlus dialog) {
                        dialog.dismiss();
                    }
                })
                .create();

        pinView= (PinView) dialog.findViewById(R.id.firstPinView);
        verify= (TextView) dialog.findViewById(R.id.verify);
        resend= (TextView) dialog.findViewById(R.id.resend);

        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendOtp();
            }
        });
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyReachOtp();
            }
        });

        dialog.show();
        if(countDownTimer!=null) {
            countDownTimer.cancel();
        }
        countDownTimer=new CountDownTimer(120000, 1000) {

            @SuppressLint("SetTextI18n")
            public void onTick(long millisUntilFinished) {
                resend.setText("Remaining: " + millisUntilFinished / 1000+"s");
                resend.setClickable(false);
                //here you can have your logic to set text to edittext
            }

            @SuppressLint("SetTextI18n")
            public void onFinish() {
                resend.setText("Resend Otp");
                resend.setClickable(true);
            }
        }.start();
    }



    public void setConfirmOrder(){
        progressBar.show();

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",id);
        hashMap.put("status","3");
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().setConfirmOrder(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(response1.getStatus()){
                        Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_LONG).show();
                       getMyServiceEnquiryDetail(id);
                    }else{
                        Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(MyServiceDetailsActivity.this);
                }else{
                    Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<in.happyhelp.limra.activity.response.Response> call, Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
            }
        });
    }

    public void resendOtp(){
        HashMap<String,String>hashMap=new HashMap<>();
        hashMap.put("id",id);
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().resendOtpService(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
                if(response.code()==200) {

                    if(countDownTimer!=null) {
                        countDownTimer.cancel();
                    }
                    countDownTimer=new CountDownTimer(120000, 1000) {

                        @SuppressLint("SetTextI18n")
                        public void onTick(long millisUntilFinished) {
                            resend.setText("Remaining: " + millisUntilFinished / 1000+"s");
                            resend.setClickable(false);
                            //here you can have your logic to set text to edittext
                        }

                        @SuppressLint("SetTextI18n")
                        public void onFinish() {

                            resend.setText("Resend Otp");
                            resend.setClickable(true);
                        }

                    }.start();
                }else if(response.code()==401){
                    AppUtils.logout(MyServiceDetailsActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }



    public void setReachLocation(){
        progressBar.show();
        @SuppressLint("SimpleDateFormat") DateFormat time = new SimpleDateFormat("hh:mm a");
        String currentTime = time.format(Calendar.getInstance().getTime());
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",id);
        hashMap.put("reach_time",currentTime);
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().setReachLocation(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                progressBar.dismiss();
                    if(response.code()==200){
                         if(response1.getStatus()){
                             Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_LONG).show();
                             setDialog(MyServiceDetailsActivity.this);
                         }else{
                             Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_LONG).show();
                         }
                    }else if(response.code()==401){
                        AppUtils.logout(MyServiceDetailsActivity.this);
                    }else{
                            Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                    }
                }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
            }
        });
    }

    public void verifyReachOtp(){
        progressBar.show();
        HashMap<String ,String> hashMap=new HashMap<>();
        hashMap.put("id",id);
        hashMap.put("otp",pinView.getText().toString());
        hashMap.put("status","7");

        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().verifyReachOtp(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(response1.getStatus()){
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_SHORT).show();
                        getMyServiceEnquiryDetail(id);
                    }else{
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(MyServiceDetailsActivity.this);
                }else{
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
            t.printStackTrace();
                progressBar.dismiss();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
            }
        });
    }

    public void setRejectService(){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",id);
        hashMap.put("status","4");

        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().serviceReject(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                progressBar.dismiss();
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()) {
                        Toast.makeText(getApplicationContext(), response1.getMessage(), Toast.LENGTH_LONG).show();
                        getMyServiceEnquiryDetail(id);
                    }else{
                        Toast.makeText(getApplicationContext(), response1.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(MyServiceDetailsActivity.this);
                }else{
                    Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
            }
        });
    }


    public void setCompleteService(){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",id);
        hashMap.put("status","2");

        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().completeService(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                progressBar.dismiss();
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()) {
                        getMyServiceEnquiryDetail(id);
                        Toast.makeText(getApplicationContext(),response1.getMessage(), Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(getApplicationContext(),response1.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(MyServiceDetailsActivity.this);
                }else{
                    Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<in.happyhelp.limra.activity.response.Response> call, Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
            }
        });
    }
    public void getMyServiceEnquiryDetail(String id){
        progressBar.show();
        HashMap<String ,String> hashMap=new HashMap<>();
        hashMap.put("id",id);
        Call<MyServicesResponse> call=RestClient.get().myServicesEnquiryDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<MyServicesResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<MyServicesResponse> call, Response<MyServicesResponse> response) {
                MyServicesResponse myServicesResponse = response.body();
                progressBar.dismiss();
                if (response.code() == 200) {
                    if (myServicesResponse.getStatus()) {
                        if (myServicesResponse.getData().size() > 0) {
                            MyServices myServices = myServicesResponse.getData().get(0);
                            name.setText(myServices.getName());
                            mobile.setText(myServices.getMobile());
                            email.setText(myServices.getEmail());
                            message.setText(myServices.getMessage());
                            startDate.setText(myServices.getDate()+" "+myServices.getTime());
                            address.setText(myServices.getAddress());
                            location.setText(myServices.getLocation());
                            amount.setText("Rs "+myServices.getAmount());
                            serviceStatus=myServices.getStatus();
                            walletAmt.setText("Rs "+myServices.getWallet());
                            cashbackwallet.setText("Rs "+myServices.getCashback_wallet());


                            serviceAmt= Double.parseDouble(myServices.getAmount());
                            cashwalletAmt= Double.parseDouble(myServices.getWallet());
                            cashbackWalletAmt= Double.parseDouble(myServices.getCashback_wallet());


                            serviceAmt=serviceAmt-cashwalletAmt-cashbackWalletAmt;
                            remainingAmt.setText("Rs "+String.valueOf(serviceAmt));
                            quantity.setText(myServices.getQuantity());

                            if(myServices.getVisitingCharge().equals("")|| myServices.getVisitingCharge()==null){
                                visitingLinear.setVisibility(View.GONE);
                            }else{
                                visitingLinear.setVisibility(View.GONE);
                                visiting.setText("Rs "+myServices.getVisitingCharge());
                            }



                            if(myServices.getHour().equals("48")){
                                serviceType.setText("Schedule Date ");
                                if(myServices.getShowdetails()){
                                    scheduleLinear.setVisibility(View.VISIBLE);
                                }else{
                                    scheduleLinear.setVisibility(View.GONE);
                                    reachLocation.setVisibility(View.GONE);
                                    complete.setVisibility(View.GONE);
                                }
                            }else {
                                serviceType.setText(myServices.getHour() );
                            }


                            switch (myServices.getStatus()) {
                                case "0":
                                    complete.setVisibility(View.GONE);
                                    reachLocation.setVisibility(View.GONE);
                                    reject.setVisibility(View.GONE);
                                    status.setText("Processing");
                                    status.setTextColor(getResources().getColor(R.color.accent_color));
                                    confirm.setVisibility(View.GONE);
                                    break;
                                case "1":
                                    reachLocation.setVisibility(View.GONE);
                                    reject.setVisibility(View.GONE);
                                    confirm.setVisibility(View.GONE);
                                    complete.setVisibility(View.GONE);
                                    status.setText("Please Wait for User Confirmation ");
                                    status.setTextColor(getResources().getColor(R.color.green));

                                    break;
                                case "2":
                                    status.setText("Service Completed ");
                                    status.setTextColor(getResources().getColor(R.color.green));
                                    complete.setVisibility(View.GONE);
                                    reachLocation.setVisibility(View.GONE);
                                    reject.setVisibility(View.GONE);
                                    confirm.setVisibility(View.GONE);
                                    break;
                                case "3":
                                    status.setText("Service Confirm by Vendor ");
                                    status.setTextColor(getResources().getColor(R.color.green));
                                    complete.setVisibility(View.VISIBLE);
                                    reachLocation.setVisibility(View.VISIBLE);
                                    reject.setVisibility(View.GONE);
                                    confirm.setVisibility(View.GONE);
                                    paymentlinearstatus.setVisibility(View.GONE);
                                    break;
                                case "4":
                                    status.setText("Service Order Rejected by Vendor");
                                    visitingLinear.setVisibility(View.VISIBLE);
                                    status.setTextColor(getResources().getColor(R.color.accent_color));
                                    complete.setVisibility(View.GONE);
                                    reachLocation.setVisibility(View.GONE);
                                    reject.setVisibility(View.GONE);
                                    confirm.setVisibility(View.GONE);
                                    visitingLinear.setVisibility(View.VISIBLE);
                                    break;
                                case "5":
                                    status.setText("Service Confirm by User ");
                                    status.setTextColor(getResources().getColor(R.color.green));
                                    complete.setVisibility(View.GONE);
                                    reachLocation.setVisibility(View.GONE);
                                    reject.setVisibility(View.GONE);
                                    confirm.setVisibility(View.VISIBLE);
                                    break;
                                case "6":
                                    status.setText("Service Cancel by user ");
                                    status.setTextColor(getResources().getColor(R.color.accent_color));
                                    complete.setVisibility(View.GONE);
                                    reachLocation.setVisibility(View.GONE);
                                    reject.setVisibility(View.GONE);
                                    confirm.setVisibility(View.GONE);
                                    break;
                                case "7":
                                    status.setText(" Reach to service location ");
                                    status.setTextColor(getResources().getColor(R.color.green));
                                    complete.setVisibility(View.VISIBLE);
                                    reachLocation.setVisibility(View.GONE);
                                    reject.setVisibility(View.VISIBLE);
                                    confirm.setVisibility(View.GONE);
                                    visitingLinear.setVisibility(View.VISIBLE);
                                    break;
                                case "8":
                                    status.setText("Service Confirm by Vendor ");
                                    status.setTextColor(getResources().getColor(R.color.green));
                                    complete.setVisibility(View.VISIBLE);
                                    reachLocation.setVisibility(View.VISIBLE);
                                    reject.setVisibility(View.GONE);
                                    confirm.setVisibility(View.GONE);
                                    break;
                                case "9":
                                    status.setText("Service cancel automatically due to vendor not Reach ");
                                    status.setTextColor(getResources().getColor(R.color.accent_color));
                                    complete.setVisibility(View.GONE);
                                    reachLocation.setVisibility(View.GONE);
                                    reject.setVisibility(View.GONE);
                                    confirm.setVisibility(View.GONE);
                                    break;
                            }

                            String services="";
                            for(int j=0;j<myServices.getServices().size();j++) {
                                if(j==0){
                                    services =  myServices.getServices().get(j).getName();
                                }else {
                                    services = services + "," + myServices.getServices().get(j).getName();
                                }
                            }
                            service.setText(services);

                            if(myServices.getPaid().equals("1")) {
                                paymentStatus.setText("Paid");
                                walletLinear.setVisibility(View.GONE);
                                paymentStatus.setTextColor(getResources().getColor(R.color.green));
                            }else{
                                paymentStatus.setText("Pending");
                                paymentStatus.setTextColor(getResources().getColor(R.color.accent_color));
                                walletLinear.setVisibility(View.GONE);
                            }

                            if(myServices.getPaymentMode().equals("0")) {
                                paymentMode.setText("Cash On Delivery");
                                walletLinear.setVisibility(View.VISIBLE);
                            }else if(myServices.getPaymentMode().equals("2")) {
                                paymentMode.setText("Wallet Used");
                            }else{
                                paymentMode.setText("Online ");
                            }

                        } else {
                            Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                            onBackPressed();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<MyServicesResponse> call, @NonNull Throwable t) {
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
                progressBar.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MyServiceDetailsActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MyServiceDetailsActivity.this,Id);

            }
        });



    }
}
