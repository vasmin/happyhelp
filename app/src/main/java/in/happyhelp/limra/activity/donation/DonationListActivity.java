package in.happyhelp.limra.activity.donation;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.KycActivity;
import in.happyhelp.limra.adapter.DonateEnquiryAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.donateenquiry.DonateEnquiryResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DonationListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    LinearLayoutManager linearLayoutManager;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.data)
    TextView data;
    DonateEnquiryAdapter donateEnquiryAdapter;
    String dId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donation_list);
        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            dId = extras.getString(Constants.DID);
            getDonationEnquiryList();
        }

        donateEnquiryAdapter=new DonateEnquiryAdapter(getApplicationContext());


        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
    }

    public void getDonationEnquiryList(){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",dId);
        Call<DonateEnquiryResponse> call= RestClient.get().getDonateEnquiry(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<DonateEnquiryResponse>() {
            @Override
            public void onResponse(Call<DonateEnquiryResponse> call, Response<DonateEnquiryResponse> response) {
                DonateEnquiryResponse donateEnquiryResponse=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){

                    if(donateEnquiryResponse.isStatus()) {
                        donateEnquiryAdapter.setData(donateEnquiryResponse.getDonateenqiry());
                        recyclerView.setAdapter(donateEnquiryAdapter);

                        if(donateEnquiryResponse.getDonateenqiry().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }
                    }else{
                        data.setVisibility(View.VISIBLE);
                        Toast.makeText(DonationListActivity.this, Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(DonationListActivity.this);
                }else{
                    Toast.makeText(DonationListActivity.this, Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DonateEnquiryResponse> call, Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(DonationListActivity.this, Constants.SERVERTIMEOUT, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRefresh() {

    }
}
