package in.happyhelp.limra.activity.service;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.HomeActivity;
import in.happyhelp.limra.adapter.MyServicesAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.myservicesresponse.MyServices;
import in.happyhelp.limra.activity.response.myservicesresponse.MyServicesResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyServicesActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.data)
    TextView data;

    MyServicesAdapter myServicesAdapter;

    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_services);
        ButterKnife.bind(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        AppUtils.isNetworkConnectionAvailable(this);

        swipeRefreshLayout.setOnRefreshListener(MyServicesActivity.this);
        swipeRefreshLayout.setRefreshing(true);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        myServicesAdapter=new MyServicesAdapter(getApplicationContext(), new MyServicesAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(MyServices d, View view) throws ParseException {
                Intent intent=new Intent(MyServicesActivity.this,MyServiceDetailsActivity.class);
                intent.putExtra("id",d.getId());
                startActivity(intent);
            }
        });

        getMyServices();
    }

    public void getMyServices(){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("serviceid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.COMPANYCODE));
       Call<MyServicesResponse> call=RestClient.get().myServicesEnquiry(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
       call.enqueue(new Callback<MyServicesResponse>() {
           @Override
           public void onResponse(Call<MyServicesResponse> call, Response<MyServicesResponse> response) {
               swipeRefreshLayout.setRefreshing(false);
               MyServicesResponse myServicesResponse=response.body();
               if(response.code()==200){
                   if(myServicesResponse.getStatus()){
                    if(myServicesResponse.getData().size()>0) {
                        myServicesAdapter.setData(myServicesResponse.getData());
                        recyclerView.setAdapter(myServicesAdapter);
                        data.setVisibility(View.GONE);
                    }else{
                        data.setVisibility(View.VISIBLE);
                    }
                   }else{
                       data.setVisibility(View.VISIBLE);
                   }
               }else if(response.code()==401){
                   AppUtils.logout(MyServicesActivity.this);
               }
           }

           @Override
           public void onFailure(Call<MyServicesResponse> call, Throwable t) {
               t.printStackTrace();
               swipeRefreshLayout.setRefreshing(false);

           }
       });
    }

    @Override
    public void onRefresh() {
        getMyServices();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MyServicesActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MyServicesActivity.this,Id);

            }
        });



    }
}
