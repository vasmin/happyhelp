package in.happyhelp.limra.activity.recharge;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Path;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.HomeActivity;
import in.happyhelp.limra.activity.LoginActivity;
import in.happyhelp.limra.activity.property.SearchPropertyActivity;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import in.happyhelp.limra.adapter.recharger.OperatorSpinnerAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.model.shopping.CartItem;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.rechargeresponse.RechargeResponse;
import in.happyhelp.limra.rechargeresponse.SecurreResponse;
import in.happyhelp.limra.rechargeresponse.operator.Operator;
import in.happyhelp.limra.rechargeresponse.operator.OperatorResponse;
import in.happyhelp.limra.shopping.CartActivity;
import in.happyhelp.limra.shopping.HomeShoppingActivity;
import in.happyhelp.limra.shoppingresponse.promodata.Product;
import in.happyhelp.limra.shoppingresponse.promodata.PromoData;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;

public class RechargeActivity extends AppCompatActivity {

    @BindView(R.id.contact)
    ImageView contact;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.amount)
    EditText amount;

    @BindView(R.id.operator)
    Spinner operatorSpinner;

    @BindView(R.id.recharge)
    TextView recharge;

    @BindView(R.id.promocodehave)
    TextView promoCode;

    @BindView(R.id.postpaid)
    RadioButton postPaid;

    @BindView(R.id.prepaid)
    RadioButton prePaid;

    String operator="";
    ProgressDialog progressBar;
    DialogPlus dialogPlus;
    EditText promocode;
    TextView apply,cancel;
    boolean isPromo=false;
    OperatorSpinnerAdapter adapter;

    String Operator="";

    List<Operator> list=new ArrayList<>();

    private static final int REQUEST_CODE = 1;
    String AuthKey="";
    String corporateId="";

    @BindView(R.id.history)
    TextView history;
    String provider="";

    Realm realm;
    @BindView(R.id.online)
    RadioButton onlineRb;

    @BindView(R.id.wallet)
    RadioButton walletRb;

    @BindView(R.id.walletamt)
            TextView walletamt;

    String name="";
    String email="";
    double wallet=0;

    boolean isRecharge=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        getOperator();

        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        realm= RealmHelper.getRealmInstance();
        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("content://contacts");
                Intent intent = new Intent(Intent.ACTION_PICK, uri);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
        getSecureCode();

        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        RealmResults<UserData> userData=realm.where(UserData.class).findAllAsync();
        userData.addChangeListener(new RealmChangeListener<RealmResults<UserData>>() {
            @Override
            public void onChange(@NonNull RealmResults<UserData> userData) {
                if(userData!=null) {
                    if (userData.size() > 0) {
                        name=userData.get(0).getName();
                        email=userData.get(0).getEmail();
                        wallet=userData.get(0).getWallet();
                        walletamt.setText(String.valueOf(wallet));
                    }
                }
            }
        });



        recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AppUtils.isValidMobile(mobile.getText().toString())){

                }else{
                    mobile.setError("Enter valid mobile");
                    return;
                }


                if(TextUtils.isEmpty(amount.getText().toString())){
                    amount.setError("Please Enter Amount");
                    return;
                }


                if(onlineRb.isChecked()){
                    callInstamojoPay(email,mobile.getText().toString(),amount.getText().toString(),"Recharge","Happy help Recharge");
                }if(walletRb.isChecked()){
                    if(wallet>Integer.parseInt(amount.getText().toString())) {
                        recharge("");
                    }else{
                        Toast.makeText(RechargeActivity.this, "Wallet Amount is less please enter less Amount", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }else{
                    Toast.makeText(RechargeActivity.this, "Server Failure ! Please try again ...", Toast.LENGTH_SHORT).show();
                }


            }
        });

        walletRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    walletamt.setVisibility(View.VISIBLE);
                }else{
                    walletamt.setVisibility(View.GONE);
                }
            }
        });
        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent=new Intent(RechargeActivity.this,RechargeHistoryActivity.class);
                    startActivity(intent);
            }
        });
        promoCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPromo){
                    isPromo=false;
                    setPromoCode();
                }else {
                    promoDialogue();
                }
            }
        });
        adapter=new OperatorSpinnerAdapter(this);

        operatorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Operator=list.get(position).getValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }
    InstapayListener listener;
    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);
                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);
               if(!isRecharge) {
                   recharge(payment);
               }

            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }

    public void getOperator(){
        Call<OperatorResponse> call=RestClient.get(RestClient.RECHARGE_BASE_URL).getOperator(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<OperatorResponse>() {
            @Override
            public void onResponse(@NonNull Call<OperatorResponse> call, @NonNull retrofit2.Response<OperatorResponse> response) {
                OperatorResponse operatorResponse=response.body();
                list.clear();
                Operator operator=new Operator();
                operator.setName("Select Operator");
                operator.setValue("Select Operator");
                list.add(operator);
                if(response.code()==200){
                    if(operatorResponse.getStatus()){


                        list.addAll(operatorResponse.getOperators());
                        adapter.setData(list);
                        operatorSpinner.setAdapter(adapter);
                    }else{
                        list.addAll(operatorResponse.getOperators());
                        adapter.setData(list);
                        operatorSpinner.setAdapter(adapter);
                    }

                }else if(response.code()==401){
                    AppUtils.logout(RechargeActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<OperatorResponse> call, @NonNull Throwable t) {
                t.printStackTrace();

            }
        });
    }

    @SuppressLint("SetTextI18n")
    public void setPromoCode(){
        if(isPromo) {
            promoCode.setText("Remove Promocode");
        }else{
            promoCode.setText("Have  Promocode ?");

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }



    public void promoDialogue(){
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.item_promocode, null);
        dialogPlus = DialogPlus.newDialog(RechargeActivity.this)
                .setContentHolder(new ViewHolder(view))
                .setGravity(Gravity.CENTER)
                .setCancelable(false)
                .setInAnimation(R.anim.abc_fade_in)
                .setMargin(50, 20, 50, 50)
                .setOutAnimation(R.anim.abc_fade_out)
                .setOverlayBackgroundResource(Color.TRANSPARENT)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {

                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogPlus dialog) {
                        dialog.dismiss();
                    }
                })
                .create();

        promocode= (EditText) dialogPlus.findViewById(R.id.promocode);
        apply= (TextView) dialogPlus.findViewById(R.id.apply);
        cancel= (TextView) dialogPlus.findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPlus.dismiss();
            }
        });

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(promocode.getText().toString())){
                    promocode.setError(" Enter promocode ");
                    return;
                }
                applyPromoCode();
            }
        });
        dialogPlus.show();
    }

    public void getSecureCode(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("SecurityKey","S743799");
        hashMap.put("EmailId","ecodelectronics@gmail.com");
        hashMap.put("Password","321123");
        hashMap.put("APIChkSum","1322b052df7a1a00e869919e1ac60a59");

        Call<SecurreResponse> call=RestClient.get("https://spi.justrechargeit.com/JRICorporateLogin.svc/").getSecureCode(hashMap);
        call.enqueue(new Callback<SecurreResponse>() {
            @Override
            public void onResponse(Call<SecurreResponse> call, retrofit2.Response<SecurreResponse> response) {
                SecurreResponse securreResponse=response.body();
                if(response.code()==200){
                    corporateId=securreResponse.getCorporateId();
                    AuthKey=securreResponse.getAuthenticationKey();
                }else if(response.code()==401){
                    AppUtils.logout(RechargeActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<SecurreResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void recharge(final String paymentId){
        isRecharge=true;
        String latitude= SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.LATITUDE);
        String longitude=SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.LONGITUDE);
        progressBar.show();

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("CorporateId",corporateId);
        hashMap.put("SecurityKey","S743799");
        hashMap.put("AuthKey",AuthKey);
        hashMap.put("Mobile",mobile.getText().toString().trim());
        hashMap.put("Amount",amount.getText().toString());
      //  hashMap.put("Provider",Operator);
        hashMap.put("Provider","");
        hashMap.put("Location","");

       // hashMap.put("Location",AppUtils.getCompleteAddressString(getApplicationContext(),Double.parseDouble(latitude),Double.parseDouble(longitude)));
        hashMap.put("ServiceType","M");
        String random=GenerateRandomString.randomString(30).toUpperCase();
        hashMap.put("SystemReference",random );


        hashMap.put("IsPostpaid", "N");
        String auth=getMd5(corporateId+AuthKey+mobile.getText().toString()+amount.getText().toString()+random+"ECOD@1");

        hashMap.put("APIChkSum",auth);
        hashMap.put("NickName","Happy Help");


        Call<RechargeResponse> call=RestClient.get("https://spi.justrechargeit.com/JRICorporateRecharge.svc/").rechargeApi(hashMap);
        call.enqueue(new Callback<RechargeResponse>() {
            @Override
            public void onResponse(@NonNull Call<RechargeResponse> call, @NonNull retrofit2.Response<RechargeResponse> response) {
                RechargeResponse rechargeResponse=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(rechargeResponse.getStatus().equals("0 | Recharge Successful")){
                        provider=rechargeResponse.getProvider();
                        rechargePostApi(rechargeResponse.getSystemReference(),rechargeResponse.getTransactionReference(),paymentId);
                    }else{
                        new AlertDialog.Builder(RechargeActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle("Recharge Failure")
                                .setCancelable(false)
                                .setMessage(rechargeResponse.getStatus())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        onBackPressed();
                                    }
                                })
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(RechargeActivity.this);
                }else{
                    progressBar.dismiss();
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RechargeResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }


    public static String getMd5(String input)
    {
        try {

            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");

            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }

        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }


    public static class GenerateRandomString {

        public  static String DATA = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public  static Random RANDOM = new Random();

        public static String randomString(int len) {
            StringBuilder sb = new StringBuilder(len);

            for (int i = 0; i < len; i++) {
                sb.append(DATA.charAt(RANDOM.nextInt(DATA.length())));
            }

            return sb.toString();
        }

    }

    public void applyPromoCode(){
        PromoData promoData=new PromoData();
        promoData.setAmount(amount.getText().toString());
        promoData.setCode(promocode.getText().toString());
        List<Product> products=new ArrayList<>();

        promoData.setProducts(products);
        promoData.setUserid(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());

        Call<Response> call= RestClient.get(RestClient.SHOPPING_BASE_URL).applyPrmoCode(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),promoData);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull retrofit2.Response<Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();

                if(response.code()==200) {
                    if (dialogPlus != null) {
                        dialogPlus.dismiss();
                    }

                /*if(response.code()==200){
                    if(response1.getStatus()) {
                        promoCode.setVisibility(View.VISIBLE);
                        promoAmt=response1.getPromo_discount();
                        grandtotal=grandtotal-promoAmt;
                        grandTotal.setText(String.valueOf(grandtotal));
                        promo.setText(String.valueOf(response1.getPromo_discount()));
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout,response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();
                        isPromo=true;
                        setPromoCode();
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout,response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();
                    }
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }*/
                }else if(response.code()==401){
                    AppUtils.logout(RechargeActivity.this);
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                if(dialogPlus!=null) {
                    dialogPlus.dismiss();
                }
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void rechargePostApi(String system_ref,String transaction,String paymentid){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("amount",amount.getText().toString());
        hashMap.put("is_postpaid", "N");
        hashMap.put("mobile",mobile.getText().toString());
        hashMap.put("provider",provider);
        hashMap.put("service_type","M");
        hashMap.put("system_ref",system_ref);
        hashMap.put("transaction_ref",transaction);
        if(walletRb.isChecked()) {
            hashMap.put("payment_method","0" );
            hashMap.put("payment_id","" );
        }else{
            hashMap.put("payment_method","1" );
            hashMap.put("payment_id",paymentid );
        }

        Call<Response> call=RestClient.get(RestClient.RECHARGE_BASE_URL).rechargePostApi(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                Response response1=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(response1.getStatus()) {
                        new AlertDialog.Builder(RechargeActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle("Recharge")
                                .setCancelable(false)
                                .setMessage(response1.getMessage())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                       amount.setText("");
                                       mobile.setText("");
                                       onBackPressed();

                                    }

                                })
                                .show();
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(RechargeActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        getSecureCode();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Uri uri = intent.getData();
                String[] projection = { ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME };

                Cursor cursor = getContentResolver().query(uri, projection,
                        null, null, null);
                cursor.moveToFirst();

                int numberColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(numberColumnIndex);

/*
                if(number.startsWith("+"))
                {
                    if(number.length()==13)
                    {
                        String str_getMOBILE=number.substring(3);
                        mobile.setText(str_getMOBILE);
                    }
                    else if(number.length()==14)
                    {
                        String str_getMOBILE=number.substring(4);
                        mobile.setText(str_getMOBILE);
                    }
                }
                else
                {
                    mobile.setText(number);
                }*/



                int nameColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                number = number.replaceAll("[^0-9]+", "");
                String name = cursor.getString(nameColumnIndex);
                if(number.length()>10){
                    number = number.replace("91", "");
                    mobile.setText(number);
                }else{
                    mobile.setText(number);
                }
            }
        }
    };




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(RechargeActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(RechargeActivity.this,Id);

            }
        });
    }
}
