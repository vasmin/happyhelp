package in.happyhelp.limra.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;

public class AddWalletActivity extends AppCompatActivity {

    @BindView(R.id.payment)
    TextView payment;

    @BindView(R.id.amount)
    EditText amount;
    ProgressDialog progressBar;
    Realm realm;
    String email="",phone="",name="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_wallet);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        realm= RealmHelper.getRealmInstance();
        AppUtils.isNetworkConnectionAvailable(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);
        RealmResults<UserData> userData=realm.where(UserData.class).findAllAsync();
        userData.addChangeListener(new RealmChangeListener<RealmResults<UserData>>() {
            @Override
            public void onChange(@NonNull RealmResults<UserData> userData) {
                if(userData!=null) {
                    if (userData.size() > 0) {
                        name=userData.get(0).getName();
                        email=userData.get(0).getEmail();
                        phone=userData.get(0).getMobile();
                    }


                }
            }
        });

        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(name.equals("")){
                    Toast.makeText(AddWalletActivity.this, "Please Update your Profile", Toast.LENGTH_SHORT).show();
                    return;                }

                if(email.equals("")){
                    Toast.makeText(AddWalletActivity.this, "Please Update your Profile", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(phone.equals("")){
                    Toast.makeText(AddWalletActivity.this, "Please Update your Profile", Toast.LENGTH_SHORT).show();
                    return;
                }
                callInstamojoPay(email,phone,amount.getText().toString(), name);
            }
        });
    }


    private void callInstamojoPay(String email, String phone, String amount, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", "Add Wallet Money");
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);

                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();

                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);
                addWallet(amount.getText().toString(),payment);
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }

    public void addWallet(String amount,String payment){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("amount",amount);
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("payment_id",payment);
        Call<Response> call= RestClient.get().addWalleyMoney(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                progressBar.dismiss();
                Response response1=response.body();
                if(response.code()==200){
                    new AlertDialog.Builder(AddWalletActivity.this)
                            .setIcon(R.drawable.logo)
                            .setTitle(response1.getMessage())
                            .setMessage("Successfully")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    onBackPressed();
                                }

                            })
                            .show();
                }else if(response.code()==401){
                    AppUtils.logout(AddWalletActivity.this);
                }else{
                    Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
                progressBar.dismiss();
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(AddWalletActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(AddWalletActivity.this,Id);

            }
        });

    }

}
