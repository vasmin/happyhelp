package in.happyhelp.limra.activity.property;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.Jobs.JobProfileActivity;
import in.happyhelp.limra.activity.KycActivity;
import in.happyhelp.limra.activity.response.joblistresponse.JobList;
import in.happyhelp.limra.activity.response.joblistresponse.JobListResponse;
import in.happyhelp.limra.activity.response.mypropertyenquiryresponse.Enquiry;
import in.happyhelp.limra.activity.response.mypropertyenquiryresponse.MyPropertyEnquiryResponse;
import in.happyhelp.limra.adapter.MyJobAdapter;
import in.happyhelp.limra.adapter.PropertyEnquiryAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPropertyEnquiryActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.data)
    TextView data;
    LinearLayoutManager linearLayoutManager;

    PropertyEnquiryAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_property_enquiry);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        adapter=new PropertyEnquiryAdapter(this, new PropertyEnquiryAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(final Enquiry d, View view) throws ParseException {


            }
        });
        getMyPropertyEnquiry();
    }
    public void getMyPropertyEnquiry()
    {
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<MyPropertyEnquiryResponse> call= RestClient.get(RestClient.PROPERTY_BASE_URL).getMyPropertyEnquiry(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<MyPropertyEnquiryResponse>() {
            @Override
            public void onResponse(@NonNull Call<MyPropertyEnquiryResponse> call, @NonNull Response<MyPropertyEnquiryResponse> response) {
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    MyPropertyEnquiryResponse myPropertyEnquiryResponse=response.body();
                    if(myPropertyEnquiryResponse.getStatus()){
                        adapter.setData(myPropertyEnquiryResponse.getEnquiries());
                        recyclerView.setAdapter(adapter);
                        if(myPropertyEnquiryResponse.getEnquiries().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }
                    }else{
                        if(myPropertyEnquiryResponse.getEnquiries().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }
                    }



                }else if(response.code()==401){
                    AppUtils.logout(MyPropertyEnquiryActivity.this);
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<MyPropertyEnquiryResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @Override
    public void onRefresh() {
        getMyPropertyEnquiry();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MyPropertyEnquiryActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MyPropertyEnquiryActivity.this,Id);

            }
        });
    }
}
