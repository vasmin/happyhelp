
package in.happyhelp.limra.activity.response.jobcategory;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobCategoryResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<JobCategory> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<JobCategory> getData() {
        return data;
    }

    public void setData(List<JobCategory> data) {
        this.data = data;
    }

}
