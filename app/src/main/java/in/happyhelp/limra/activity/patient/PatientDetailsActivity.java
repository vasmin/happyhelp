package in.happyhelp.limra.activity.patient;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.property.PostPropertyActivity;
import in.happyhelp.limra.activity.response.mydetailresponse.MyPatientDetailResponse;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import retrofit2.Call;
import retrofit2.Callback;

public class PatientDetailsActivity extends AppCompatActivity {

    private static final int GALLERY_DONATEIMAGE = 123;
    private static final int REQUEST_IMAGE_CAPTURE_DONATEIMAGE = 321;
    private static final int REQUEST_IMAGE_CAPTURE_PRECREPTION = 954;
    private static final int GALLERY_PRECREPTION=541;

    @BindView(R.id.patientname)
    TextView patientName;

    @BindView(R.id.patientaddress)
    TextView patientAddress;

    @BindView(R.id.email)
    TextView email;

    @BindView(R.id.mobile)
    TextView mobile;

    @BindView(R.id.hospitalname)
    TextView hospitalName;

    @BindView(R.id.doctorname)
    TextView drName;

    @BindView(R.id.call)
    TextView call;


    @BindView(R.id.message)
    TextView message;

    @BindView(R.id.image)
    ImageView profileImage;

    @BindView(R.id.precription)
    ImageView precreption;

    String profilePath="";
    String precriptionPath="";

    ProgressDialog progressBar;

    String pId="";
    String pricriptionName="";
    String profileName="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            pId = extras.getString(Constants.PID);
            getPatientDetails(pId);
        }


        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mobile.getText().toString()));
                startActivity(intent);
            }
        });

        mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mobile.getText().toString()));
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    public void getPatientDetails(String pId){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("patientid",pId);
        Call<MyPatientDetailResponse> call= RestClient.get().getMyPatientDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<MyPatientDetailResponse>() {
            @Override
            public void onResponse(@NonNull Call<MyPatientDetailResponse> call, @NonNull retrofit2.Response<MyPatientDetailResponse> response) {
                MyPatientDetailResponse myPatientDetailResponse=response.body();
                if(response.code()==200){
                    patientName.setText(myPatientDetailResponse.getData().getPatientName());
                    patientAddress.setText(myPatientDetailResponse.getData().getPatientAddress());
                    email.setText(myPatientDetailResponse.getData().getEmail());
                    mobile.setText(myPatientDetailResponse.getData().getMobile());
                    hospitalName.setText(myPatientDetailResponse.getData().getHospitalName());
                    drName.setText(myPatientDetailResponse.getData().getDoctorName());
                  //  amount.setText(myPatientDetailResponse.getData().getAmount());
                    message.setText(myPatientDetailResponse.getData().getMessage());
                    pricriptionName=myPatientDetailResponse.getData().getPrescription();
                    profileName=myPatientDetailResponse.getData().getImage();


                    Glide.with(getApplicationContext())
                            .load(RestClient.base_image_url+myPatientDetailResponse.getData().getImage())
                            .into(profileImage);

                    Glide.with(getApplicationContext())
                            .load(RestClient.base_image_url+myPatientDetailResponse.getData().getPrescription())
                            .into(precreption);

                }else if(response.code()==401){
                    AppUtils.logout(PatientDetailsActivity.this);
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<MyPatientDetailResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }
}
