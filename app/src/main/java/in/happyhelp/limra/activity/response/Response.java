
package in.happyhelp.limra.activity.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("serviceid")
    @Expose
    private int serviceid;

    @SerializedName("count")
    @Expose
    private int count;

    @SerializedName("promo_discount")
    @Expose
    private int promo_discount;


    @SerializedName("lastid")
    @Expose
    private int lastid;

    @SerializedName("usevalue")
    @Expose
    private  String usevalue;



    @SerializedName("cashback")
    @Expose
    private  String cashback;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getServiceid() {
        return serviceid;
    }

    public void setServiceid(int serviceid) {
        this.serviceid = serviceid;
    }

    public int getLastid() {
        return lastid;
    }

    public void setLastid(int lastid) {
        this.lastid = lastid;
    }


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPromo_discount() {
        return promo_discount;
    }

    public void setPromo_discount(int promo_discount) {
        this.promo_discount = promo_discount;
    }

    public String getUsevalue() {
        return usevalue;
    }

    public void setUsevalue(String usevalue) {
        this.usevalue = usevalue;
    }

    public String getCashback() {
        return cashback;
    }

    public void setCashback(String cashback) {
        this.cashback = cashback;
    }
}
