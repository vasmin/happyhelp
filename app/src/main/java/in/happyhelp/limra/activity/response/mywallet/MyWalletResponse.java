
package in.happyhelp.limra.activity.response.mywallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class MyWalletResponse extends RealmObject {

    @PrimaryKey
    int id;

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private RealmList<Wallet> data = null;

    @SerializedName("wallet_total")
    @Expose
    private double walletTotal;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public RealmList<Wallet> getData() {
        return data;
    }

    public void setData(RealmList<Wallet> data) {
        this.data = data;
    }

    public double getWalletTotal() {
        return walletTotal;
    }

    public void setWalletTotal(double walletTotal) {
        this.walletTotal = walletTotal;
    }

}
