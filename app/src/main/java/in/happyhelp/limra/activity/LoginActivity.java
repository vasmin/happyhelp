package in.happyhelp.limra.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.loginresponse.LoginResponse;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.create)
    TextView createAccount;

    @BindView(R.id.submit)
    TextView submit;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.forgetpassword)
    TextView forgetPassword;

    @BindView(R.id.password)
    EditText password;
    ProgressDialog progressBar;
    Realm realm;
    DialogPlus dialog;
    private static final int REQUEST_PERMISSION = 123;
    CountDownTimer countDownTimer;
    Boolean isresend=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        AppUtils.isNetworkConnectionAvailable(this);
        ButterKnife.bind(this);
        realm=RealmHelper.getRealmInstance();
        getWindow().setBackgroundDrawableResource(R.drawable.loginbg);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        AppUtils.checkAndRequestPermissions(this);

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });

        progressBar=new ProgressDialog(LoginActivity.this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);




        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                login();
            }
        });

        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setDialog();
                final EditText email= (EditText) dialog.findViewById(R.id.mobile);
                TextView submit= (TextView) dialog.findViewById(R.id.submit);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String s = email.getText().toString();

                        if(TextUtils.isEmpty(email.getText().toString())){
                            email.setError("Please Enter Email/Mobile ");
                            return;
                        }

                            countDownTimer=new CountDownTimer(10000, 1000) {

                                @SuppressLint("SetTextI18n")
                                public void onTick(long millisUntilFinished) {
                                    isresend=false;
                                }

                                @SuppressLint("SetTextI18n")
                                public void onFinish() {
                                    isresend=true;

                                    dialog.dismiss();
                                }

                            }.start();
                            if(isresend) {
                                forgetPassword(s);
                            }

                    }
                });
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void forgetPassword(String mobile){
        HashMap<String ,String> hashMap=new HashMap<>();
        hashMap.put("mobile",mobile);
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().forgetPassword(hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }else{
                    Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void setDialog() {
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.forget_password, null);
        dialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(view))
                .setGravity(Gravity.CENTER)
                .setInAnimation(R.anim.abc_fade_in)
                .setMargin(50, 20, 50, 50)
                .setOutAnimation(R.anim.abc_fade_out)
                .setOverlayBackgroundResource(Color.TRANSPARENT)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogPlus dialog) {
                        dialog.dismiss();
                    }
                })
                .create();

        dialog.show();
    }

    public void login(){

        if(TextUtils.isEmpty(mobile.getText().toString())){
            mobile.setError("Enter Email/mobile  ");
            return;
        }


        if(TextUtils.isEmpty(password.getText().toString())){
            password.setError("Enter Password ");
            return;
        }

        progressBar.show();

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("mobile",mobile.getText().toString());
        hashMap.put("password",password.getText().toString());
        Call<LoginResponse> call=RestClient.get().signInUser(hashMap);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                progressBar.dismiss();
                if(response.code()==200) {

                    final LoginResponse loginResponse=response.body();
                    if(loginResponse.getStatus()) {

                        SharedPreferenceHelper.getInstance(getApplicationContext()).saveAuthToken(loginResponse.getToken());
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.USERID,loginResponse.getData().get(0).getId());
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setUserLoggedIn(true);
                        Toast.makeText(getApplicationContext(),loginResponse.getMessage(),Toast.LENGTH_LONG).show();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                realm.copyToRealmOrUpdate(loginResponse.getData());
                            }
                        });
                        if(loginResponse.getData().size()>0) {
                            SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.REFERALCODE, loginResponse.getData().get(0).getUse_ref_code());
                            SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.COMPANYCODE,loginResponse.getData().get(0).getServiceid());
                        }
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, loginResponse.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
                t.printStackTrace();
            }
        });
    }

}
