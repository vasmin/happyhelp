
package in.happyhelp.limra.activity.response.savedjobresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SavedListResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<SavedJob> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<SavedJob> getData() {
        return data;
    }

    public void setData(List<SavedJob> data) {
        this.data = data;
    }

}
