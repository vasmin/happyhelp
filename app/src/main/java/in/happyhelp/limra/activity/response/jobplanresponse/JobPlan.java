
package in.happyhelp.limra.activity.response.jobplanresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobPlan  {


    @SerializedName("id")
    @Expose
    private String id;


    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("months")
    @Expose
    private String months;
    @SerializedName("amount")
    @Expose
    private String amount;

    boolean selected=false;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMonths() {
        return months;
    }

    public void setMonths(String months) {
        this.months = months;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
