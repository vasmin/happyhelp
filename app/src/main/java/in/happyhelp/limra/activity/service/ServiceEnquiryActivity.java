package in.happyhelp.limra.activity.service;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.HomeActivity;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceEnquiryActivity extends AppCompatActivity {
    String serviceId;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.message)
    EditText message;

    @BindView(R.id.enquiry)
    TextView enquiry;

    ProgressDialog progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_enquiry);

        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            serviceId = extras.getString(Constants.SERVICEID);
        }
        progressBar=new ProgressDialog(ServiceEnquiryActivity.this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);


        enquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(name.getText().toString())) {
                    name.setError("Enter Name ");
                    return;
                }

                if (AppUtils.isValidMobile(mobile.getText().toString())) {

                } else {
                    mobile.setError("Enter valid mobile");
                    return;
                }


                if (!AppUtils.isValidMail(email.getText().toString(), email)) {
                    email.setError("Enter Valid Email");
                    return;
                }

                if (TextUtils.isEmpty(message.getText().toString())) {
                    message.setError("Enter Message ");
                    return;
                }
                serviceEnquiry();
            }
        });
    }


    public void serviceEnquiry(){
        progressBar.show();
        HashMap<String,String>hashMap=new HashMap<>();
        hashMap.put("serviceid",serviceId);
        hashMap.put("name",name.getText().toString());
        hashMap.put("email",email.getText().toString());
        hashMap.put("mobile",mobile.getText().toString());
        hashMap.put("message",message.getText().toString());
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().serviceEnquiry(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {

                progressBar.dismiss();
                if(response.code()==200){
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response1.getStatus()){
                    Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }else{
                    Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }else if(response.code()==401){
                    AppUtils.logout(ServiceEnquiryActivity.this);
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
               // Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
            }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, Throwable t) {
                //Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_SHORT).show();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
                t.printStackTrace();
                progressBar.dismiss();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(ServiceEnquiryActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(ServiceEnquiryActivity.this,Id);

            }
        });



    }
}
