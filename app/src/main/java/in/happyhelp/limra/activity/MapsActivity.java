package in.happyhelp.limra.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.BinderThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.GPSTracker;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.donation.DonateEnquiryActivity;
import in.happyhelp.limra.activity.service.ServicePopActivity;
import in.happyhelp.limra.activity.service.ServicesListActivity;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMarkerClickListener  {

    private GoogleMap mMap;
    GPSTracker gps;
    Location location;
    double latitude;
    double longitude;
    boolean isSet=false;

    @BindView(R.id.done)
    TextView Done;

    LatLng latlng;

    private static final String TAG = "MapsActivity";

    String isPopUp="";

    private Button btnSubmit;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        AppUtils.isNetworkConnectionAvailable(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            latitude = Double.parseDouble(extras.getString(Constants.LATITUDE));
            longitude = Double.parseDouble(extras.getString(Constants.LONGITUDE));
            isPopUp=extras.getString(Constants.ISPOPUP);

            if(mMap!=null)
                if(!isSet){
                    LatLng currentlocation = new LatLng(latitude, longitude);
                    mMap.addMarker(new MarkerOptions().position(currentlocation).title("Your Here"));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(currentlocation));
                    mMap.setMinZoomPreference(16f);
                    mMap.getUiSettings().setZoomControlsEnabled(true);
                    latlng=currentlocation;
                    isSet=true;
                }
        }


        Done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(latlng!=null){

                    if(isPopUp.equals("0")) {
                        Intent intent = new Intent(MapsActivity.this, ServicePopActivity.class);
                        intent.putExtra(Constants.LATITUDE, String.valueOf(latitude));
                        intent.putExtra(Constants.LONGITUDE, String.valueOf(longitude));
                        startActivity(intent);
                        finish();
                    }else{
                        Intent intent = new Intent(MapsActivity.this, ServicesListActivity.class);
                        intent.putExtra(Constants.LATITUDE, String.valueOf(latitude));
                        intent.putExtra(Constants.LONGITUDE, String.valueOf(longitude));
                        intent.putExtra(Constants.ISPOPUP,"1");
                        startActivity(intent);
                        finish();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Select Location",Toast.LENGTH_SHORT).show();
                }
            }
        });


        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap=googleMap;
                if(mMap!=null)
                    if(!isSet){
                        LatLng currentlocation = new LatLng(latitude, longitude);
                        mMap.addMarker(new MarkerOptions().position(currentlocation).title("Your Here"));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentlocation));
                        mMap.setMinZoomPreference(16f);
                        mMap.getUiSettings().setZoomControlsEnabled(true);
                        latlng=currentlocation;
                        isSet=true;
                    }


                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                    @Override
                    public void onMapClick(LatLng latLng) {

                        // Creating a marker
                        latlng=latLng;
                        MarkerOptions markerOptions = new MarkerOptions();

                        // Setting the position for the marker
                        markerOptions.position(latLng);

                        // Setting the title for the marker.
                        // This will be displayed on taping the marker
                        markerOptions.title(latLng.latitude + " : " + latLng.longitude);

                        // Clears the previously touched position
                        mMap.clear();

                        // Animating to the touched position
                        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                        // Placing a marker on the touched position
                        mMap.addMarker(markerOptions.title("Your Location"));


                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        mMap.setMinZoomPreference(16f);
                        mMap.getUiSettings().setZoomControlsEnabled(true);

                    }
                });
            }
        });

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.

                mMap.clear();
                Log.e(TAG, "Place: " + place.getName());

                Toast.makeText(getApplicationContext(), place.getName(), Toast.LENGTH_SHORT).show();
                LatLng sydney = place.getLatLng();
                latitude=sydney.latitude;
                longitude=sydney.longitude;
                isSet=false;
                Log.e(TAG, "Place: " + place.getLatLng());
                mMap.addMarker(new MarkerOptions().position(sydney).title("selected Location"));
                CameraPosition camera = CameraPosition.builder().target(sydney).zoom(18).bearing(90).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera), 2000, null);
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


            }

            @Override
            public void onError(Status status) {
                Log.i(TAG, "An error occurred: " + status);
            }


        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MapsActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MapsActivity.this,Id);

            }
        });



    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
       /* LatLng sydney = new LatLng(19.2183, 72.9781);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in thane"));
        //mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.map)).position(sydney).title("thane").snippet("hiii welcome to thane"));
        //  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,18));
        CameraPosition camera=CameraPosition.builder().target(sydney).zoom(18).bearing(90).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera),2000,null);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);*/


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {

                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(point).title("Marker in thane"));


                Toast.makeText(getApplicationContext(), point.toString(), Toast.LENGTH_SHORT).show();
                CameraPosition camera = CameraPosition.builder().target(point).zoom(18).bearing(90).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera), 2000, null);
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            }
        });

    }




    private void moveMap() {
        /**
         * Creating the latlng object to store lat, long coordinates
         * adding marker to map
         * move the camera with animation
         */
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        if(mMap!=null) {
            mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .draggable(true)
                    .title("Marker in India"));

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            mMap.getUiSettings().setZoomControlsEnabled(true);
        }


    }



    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        // mMap.clear();
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        Toast.makeText(MapsActivity.this, "onMarkerDragStart", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        Toast.makeText(MapsActivity.this, "onMarkerDrag", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        // getting the Co-ordinates
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;

        //move to current position
        moveMap();
    }




    @Override
    public boolean onMarkerClick(Marker marker) {
        Toast.makeText(MapsActivity.this, "onMarkerClick", Toast.LENGTH_SHORT).show();
        return true;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

}
