package in.happyhelp.limra.activity.service;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.adapter.MonthAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.servicechargesresponse.Datum;
import in.happyhelp.limra.activity.response.servicechargesresponse.ServiceChargeResponse;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import me.himanshusoni.quantityview.QuantityView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PromotionActivity extends AppCompatActivity {
    String serviceCharges="";

    @BindView(R.id.month)
    Spinner spinner;

    @BindView(R.id.total)
    TextView total;

    String totalPrice="";
    int month=1;
    String selectMonth;
    int quantity=1;

    @BindView(R.id.promotion)
            TextView promotion;

    String mobile="",email="",companyName="";

    MonthAdapter monthAdapter;
    List<Datum> list=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            email = extras.getString("email");
            mobile = extras.getString("mobile");
            companyName = extras.getString("company");

        }

        if(email.equals("")){
            Toast.makeText(this, "Please Update Your Profile Before Promotion", Toast.LENGTH_SHORT).show();
        }


        monthAdapter=new MonthAdapter(this);
        final QuantityView quantityViewDefault =  findViewById(R.id.quantityView_default);

        quantityViewDefault.setOnQuantityChangeListener(new QuantityView.OnQuantityChangeListener() {
            @Override
            public void onQuantityChanged(int oldQuantity, int newQuantity, boolean programmatically) {
                Log.e("Quanity", String.valueOf(newQuantity));
                quantity=newQuantity;
                    setTotal();
            }

            @Override
            public void onLimitReached() {
            }
        });
        promotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callInstamojoPay(email,mobile,totalPrice,"Company Promotion",companyName);
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                selectMonth=list.get(position).getMonths();
                serviceCharges=list.get(position).getAmount();
                    setTotal();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
             
            }
        });








        AppUtils.isNetworkConnectionAvailable(this);
        getServiceCharges();


    }

    @SuppressLint("SetTextI18n")
    public void setTotal(){
        if(quantity!=0 && !serviceCharges.equals("") && month!=0) {
            totalPrice = String.valueOf(month * quantity * Integer.parseInt(serviceCharges));
            total.setText("Total Price " + totalPrice);
        }else{
            total.setText("Total Price 0 ");
        }
    }

    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);

                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();

                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);
                payForPromotion(payment);

            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void getServiceCharges(){
        Call<ServiceChargeResponse> call=RestClient.get().getServiceCharges(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<ServiceChargeResponse>() {
            @Override
            public void onResponse(Call<ServiceChargeResponse> call, Response<ServiceChargeResponse> response) {
                ServiceChargeResponse serviceChargeResponse=response.body();
                if(response.code()==200){
                    if(serviceChargeResponse.isStatus()){
                        //serviceCharges= serviceChargeResponse.getData().get(0).getAmount();
                        list.clear();
                        list.addAll(serviceChargeResponse.getData());

                        monthAdapter.setData(list);
                        spinner.setAdapter(monthAdapter);

                    }else{

                    }
                }else if(response.code()==401){
                    AppUtils.logout(PromotionActivity.this);
                }else{
                    Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ServiceChargeResponse> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_SHORT).show();
            }
        });
    }



    public void payForPromotion(String payment){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("serviceid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.COMPANYCODE));
        hashMap.put("month", String.valueOf(month));
        hashMap.put("km", String.valueOf(quantity));
        hashMap.put("amount",totalPrice);
        hashMap.put("plan","1");
        hashMap.put("payment_id",payment);
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().payforPrmotion(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        new AlertDialog.Builder(PromotionActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(response1.getMessage())
                                .setMessage("Promotion Successfully")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        onBackPressed();
                                    }

                                })
                                .show();
                    }else{
                        Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(PromotionActivity.this);
                }else{
                    Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<in.happyhelp.limra.activity.response.Response> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_SHORT).show();

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(PromotionActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(PromotionActivity.this,Id);

            }
        });



    }
}
