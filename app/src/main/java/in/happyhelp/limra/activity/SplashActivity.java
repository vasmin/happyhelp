package in.happyhelp.limra.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.R;
import in.happyhelp.limra.data.SharedPreferenceHelper;

public class SplashActivity extends AppCompatActivity {
    private static final long SPLASH_TIME_OUT = 4000;

    @BindView(R.id.background)
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        Glide.with(getApplicationContext())
                .load(getResources().getDrawable(R.drawable.people))
                .into(imageView);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
//******change activity here*******
        setAnimation();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                if(SharedPreferenceHelper.getInstance(getApplicationContext()).isLoggerIn()){
                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                }
                else{
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    private void setAnimation() {

        @SuppressLint("CutPasteId") ObjectAnimator fadeOut = ObjectAnimator.ofFloat(findViewById(R.id.logo), "alpha",  1f, .3f);
        fadeOut.setDuration(2000);
        @SuppressLint("CutPasteId") ObjectAnimator fadeIn = ObjectAnimator.ofFloat(findViewById(R.id.logo), "alpha", .3f, 1f);
        fadeIn.setDuration(2000);

        final AnimatorSet mAnimationSet = new AnimatorSet();

        mAnimationSet.play(fadeIn).after(fadeOut);

        mAnimationSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mAnimationSet.start();
            }
        });
        mAnimationSet.start();
    }
}
