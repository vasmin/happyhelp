package in.happyhelp.limra.activity.Jobs;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.KycActivity;
import in.happyhelp.limra.activity.RegisterActivity;
import in.happyhelp.limra.activity.response.jobcategory.JobCategory;
import in.happyhelp.limra.activity.response.jobcategory.JobCategoryResponse;
import in.happyhelp.limra.activity.response.jobdetailresponse.JobDetailsResponse;
import in.happyhelp.limra.activity.response.jobsubresponse.JobSubCategory;
import in.happyhelp.limra.activity.response.jobsubresponse.JobSubcategoryResponse;
import in.happyhelp.limra.activity.response.statecityresponse.City;
import in.happyhelp.limra.activity.response.statecityresponse.State;
import in.happyhelp.limra.activity.video.MyVideoActivity;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.adapter.JobCategorySpinnerAdapter;
import in.happyhelp.limra.adapter.JobDetailAdapter;
import in.happyhelp.limra.adapter.JobSubCategoryAdapter;
import in.happyhelp.limra.adapter.SalaryAdapter;
import in.happyhelp.limra.adapter.WorkAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.joblistresponse.JobList;
import in.happyhelp.limra.activity.response.joblistresponse.JobListResponse;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.data)
    TextView data;

    @BindView(R.id.search_view)
    SearchView searchView;

    @BindView(R.id.createprofile)
    LinearLayout createProfile;

    @BindView(R.id.create)
            TextView create;

    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.filter)
    LinearLayout filter;

    List<String> list=new ArrayList<>();
    JobSubCategoryAdapter jobCategorySpinnerAdapter;
    LinearLayoutManager linearLayoutManager;
    Realm realm;
    JobDetailAdapter adapter;
    String jobId="";
    String jobSubId="";
    String jobType="";
    String startsalary,endsalary;
    String startexp,endexp;
    ArraySpinnerAdapter arraySpinnerAdapter;
    DialogPlus dialogPlus;

    Spinner startSalary,endSalary,startExp,endExp,stateSpinner,citySpinner;
    Spinner jobTypeSpinner;
    String filterSalary="0";
    boolean isUpdate=false;
    RealmResults<JobSubCategory> jobrole;

    ArrayList<String> workList=new ArrayList<>();
    ArrayList<String> salaryList=new ArrayList<>();

    WorkAdapter workAdapter,workend;
    SalaryAdapter startAdapter,endAdapter;

    String stateName,cityName;
    List<String> listCity=new ArrayList<>();
    List<String> listState=new ArrayList<>();

    ArraySpinnerAdapter stateSpinnerAdapter,citySpinnerAdapter;
    RealmResults<State> statesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);


        Objects.requireNonNull(getSupportActionBar()).setElevation(0);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getJobDetails();
        jobCategorySpinnerAdapter=new JobSubCategoryAdapter(this);
        realm= RealmHelper.getRealmInstance();

        list.clear();

        list.add("Select Job Type");
        list.add("Full Time");
        list.add("Part Time");
        list.add("Work from Home");

        setDialog();

        arraySpinnerAdapter=new ArraySpinnerAdapter(this);
        arraySpinnerAdapter.setData(list);
        jobTypeSpinner.setAdapter(arraySpinnerAdapter);


        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        AppUtils.isNetworkConnectionAvailable(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            jobId = extras.getString(Constants.JOBID);

            getJobList(jobId,jobType,jobSubId);
            getJobSubcategory();
        }


        jobrole=realm.where(JobSubCategory.class).findAllAsync();
        jobrole.addChangeListener(new RealmChangeListener<RealmResults<JobSubCategory>>() {
            @Override
            public void onChange(RealmResults<JobSubCategory> jobCategories) {
                jobCategorySpinnerAdapter.setData(jobCategories);
                spinner.setAdapter(jobCategorySpinnerAdapter);

            }
        });

        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPlus.show();
            }
        });

        createProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isUpdate){
                    Intent intent = new Intent(JobListActivity.this, JobProfileActivity.class);
                    intent.putExtra(Constants.JOBID,"1");
                    startActivity(intent);
                }else {
                    Intent intent = new Intent(JobListActivity.this, JobProfileActivity.class);
                    startActivity(intent);
                }
            }
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                jobSubId=jobrole.get(position).getId();


                getJobList(jobId,jobType,jobSubId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        jobTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                 String job=list.get(position);

                switch (job) {
                    case "Full Time":
                        jobType = "0";
                        break;
                    case "Part Time":
                        jobType = "1";
                        break;

                    case "Work from Home":
                        jobType = "2";
                        break;
                    default:
                        jobType = "";
                        break;
                }
                getJobList(jobId,jobType,jobSubId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        adapter=new JobDetailAdapter(getApplicationContext(), new JobDetailAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(final JobList d, View view) {

                LinearLayout linearLayout=view.findViewById(R.id.linear);

                LinearLayout linearLayout1=view.findViewById(R.id.linear2);

                linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(JobListActivity.this,JobDescriptionActivity.class);
                        intent.putExtra(Constants.JOBID,d.getId());
                        startActivity(intent);
                    }
                });

                linearLayout1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(JobListActivity.this,JobDescriptionActivity.class);
                        intent.putExtra(Constants.JOBID,d.getId());
                        startActivity(intent);
                    }
                });

                TextView apply=view.findViewById(R.id.apply);
                TextView viewJob=view.findViewById(R.id.view);

                viewJob.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(JobListActivity.this,JobDescriptionActivity.class);
                        intent.putExtra(Constants.JOBID,d.getId());
                        startActivity(intent);
                    }
                });
                apply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        jobApply(d.getId());

                    }
                });



            }
        });

        searchView.setQueryHint("Search Job");

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if(query!=null && !query.equals("")) {

                    RealmResults<JobList> realmResults=realm.where(JobList.class).contains("title", query, Case.INSENSITIVE)
                            .or().contains("companyName", query, Case.INSENSITIVE)
                            .or().contains("location", query, Case.INSENSITIVE)
                            .or().contains("address", query, Case.INSENSITIVE)
                            .or().contains("role", query, Case.INSENSITIVE)
                            .findAllAsync();
                    realmResults.addChangeListener(new RealmChangeListener<RealmResults<JobList>>() {
                        @Override
                        public void onChange(@NonNull RealmResults<JobList> jobLists) {
                            adapter.setData(jobLists);
                            recyclerView.setAdapter(adapter);
                            if(jobLists.size()==0)
                            {
                                data.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                data.setVisibility(View.GONE);
                            }
                        }
                    });



                }else{
                    RealmResults<JobList> realmResults=realm.where(JobList.class).findAllAsync();
                    realmResults.addChangeListener(new RealmChangeListener<RealmResults<JobList>>() {
                        @Override
                        public void onChange(@NonNull RealmResults<JobList> jobLists) {
                            adapter.setData(jobLists);
                            recyclerView.setAdapter(adapter);
                            if(jobLists.size()==0)
                            {
                                data.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }


                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if(newText!=null && !newText.equals("")) {

                    RealmResults<JobList> realmResults=realm.where(JobList.class).contains("title", newText, Case.INSENSITIVE)
                                                                                .or().contains("companyName", newText, Case.INSENSITIVE)
                                                                                .or().contains("location", newText, Case.INSENSITIVE)
                                                                                .or().contains("address", newText, Case.INSENSITIVE)
                                                                                .or().contains("role", newText, Case.INSENSITIVE)
                                                                                .findAllAsync();
                    realmResults.addChangeListener(new RealmChangeListener<RealmResults<JobList>>() {
                        @Override
                        public void onChange(@NonNull RealmResults<JobList> jobLists) {
                            adapter.setData(jobLists);
                            recyclerView.setAdapter(adapter);
                            if(jobLists.size()==0)
                            {
                                data.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                data.setVisibility(View.GONE);
                            }
                        }
                    });

                }else{
                    RealmResults<JobList> realmResults=realm.where(JobList.class).findAllAsync();
                    realmResults.addChangeListener(new RealmChangeListener<RealmResults<JobList>>() {
                        @Override
                        public void onChange(@NonNull RealmResults<JobList> jobLists) {
                            adapter.setData(jobLists);
                            recyclerView.setAdapter(adapter);
                            if(jobLists.size()==0)
                            {
                                data.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }

                return false;
            }
        });

        RealmResults<JobList> realmResults=realm.where(JobList.class).findAllAsync();
        realmResults.addChangeListener(new RealmChangeListener<RealmResults<JobList>>() {
            @Override
            public void onChange(@NonNull RealmResults<JobList> jobLists) {
                adapter.setData(jobLists);
                recyclerView.setAdapter(adapter);
                if(jobLists.size()==0)
                {
                    data.setVisibility(View.VISIBLE);
                }
                else
                {
                    data.setVisibility(View.GONE);
                }
            }
        });
    }


    public void jobApply(final String jobid){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("jobid",jobid);
        //   hashMap.put("location",location.getText().toString());
        //   hashMap.put("current_salary",)
        Call<in.happyhelp.limra.activity.response.Response> call= RestClient.get().jobapply(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, retrofit2.Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(response1.getStatus()){
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                        getJobList(jobId,jobType,jobSubId);
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(JobListActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }



    @SuppressLint("PrivateResource")
    public void setDialog() {
        LayoutInflater inflater = getLayoutInflater();
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.filter_item, null);

        dialogPlus = DialogPlus.newDialog(JobListActivity.this)
                .setContentHolder(new ViewHolder(view))
                .setGravity(Gravity.CENTER)
                .setInAnimation(R.anim.abc_fade_in)
                .setMargin(50, 20, 50, 100)
                .setPadding(20,20,20,20)
                .setOutAnimation(R.anim.abc_fade_out)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogPlus dialog) {
                        dialog.dismiss();
                    }
                })
                .create();

        startSalary= (Spinner) dialogPlus.findViewById(R.id.startsalary);
        endSalary= (Spinner) dialogPlus.findViewById(R.id.endsalary);
        jobTypeSpinner= (Spinner) dialogPlus.findViewById(R.id.spinner);
        startExp= (Spinner) dialogPlus.findViewById(R.id.startexperience);
        endExp= (Spinner) dialogPlus.findViewById(R.id.endexperience);

        stateSpinner= (Spinner) dialogPlus.findViewById(R.id.state);
        citySpinner= (Spinner) dialogPlus.findViewById(R.id.cityspinner);



        stateSpinnerAdapter =new ArraySpinnerAdapter(JobListActivity.this);
        citySpinnerAdapter =new ArraySpinnerAdapter(JobListActivity.this);

        statesList=realm.where(State.class).findAllAsync();
        statesList.addChangeListener(new RealmChangeListener<RealmResults<State>>() {
            @Override
            public void onChange(RealmResults<State> states) {
                listState.clear();
                for(int i=0;i<statesList.size();i++){
                    listState.add(statesList.get(i).getState());
                }
                stateSpinnerAdapter.setData(listState);
                stateSpinner.setAdapter(stateSpinnerAdapter);
            }
        });



        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stateName = String.valueOf(listState.get(i));


                final RealmResults<City> cityRealmResults = realm.where(City.class).equalTo("state", stateName).findAllAsync();
                cityRealmResults.addChangeListener(new RealmChangeListener<RealmResults<City>>() {
                    @Override
                    public void onChange(RealmResults<City> cities) {

                        listCity.clear();
                        listCity.add("Select City");
                        if (cityRealmResults.size() > 0) {
                            for (int i = 0; i < cityRealmResults.size(); i++) {
                                if (!listCity.contains(cities.get(i).getCity())) {
                                    listCity.add(cities.get(i).getCity());
                                }
                            }
                            citySpinnerAdapter.setData(listCity);
                            citySpinner.setAdapter(citySpinnerAdapter);


                        }
                    }


                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(listCity.size()>i) {
                    cityName = String.valueOf(listCity.get(i));

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        workList.clear();
        workList.add("Experience");


        salaryList.clear();
        salaryList.add("Salary in ");


        for(int i=0;i<25;i++){
            workList.add(String.valueOf(i));
            salaryList.add(String.valueOf(i));
        }


        workAdapter=new WorkAdapter(this);
        workend=new WorkAdapter(this);

        startAdapter=new SalaryAdapter(this);
        endAdapter=new SalaryAdapter(this);

        startAdapter.setData(salaryList);
        endAdapter.setData(salaryList);

        startSalary.setAdapter(startAdapter);
        endSalary.setAdapter(startAdapter);


        workAdapter.setData(workList);
        startExp.setAdapter(workAdapter);


        workend.setData(workList);
        endExp.setAdapter(workend);

        startSalary.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(salaryList.size()>i) {
                    startsalary = salaryList.get(i);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        endSalary.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(salaryList.size()>i) {
                    endsalary = salaryList.get(i);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        startExp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(workList.size()>i) {
                    startexp = workList.get(i);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        endExp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(workList.size()>i) {
                    endexp = workList.get(i);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });






        TextView filter= (TextView) dialogPlus.findViewById(R.id.filter);


        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  filterSalary=salary.getText().toString();
                dialogPlus.dismiss();
                getJobList(jobId,jobType,jobSubId);

            }
        });

    }

    public void getJobSubcategory(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("catid",jobId);
        Call<JobSubcategoryResponse> call=RestClient.get().jobSubcategory(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<JobSubcategoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<JobSubcategoryResponse> call, @NonNull Response<JobSubcategoryResponse> response) {
                final JobSubcategoryResponse jobSubcategoryResponse=response.body();
                if(response.code()==200){
                    if(jobSubcategoryResponse.getStatus()){
                        final JobSubCategory jobCategory=new JobSubCategory();
                        jobCategory.setName("Select Category");
                        jobCategory.setId("");
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {

                                realm.delete(JobSubCategory.class);
                                realm.copyToRealmOrUpdate(jobCategory);
                                realm.copyToRealmOrUpdate(jobSubcategoryResponse.getData());
                            }
                        });
                    }else{
                        final JobSubCategory jobCategory=new JobSubCategory();
                        jobCategory.setName("Select Category");
                        jobCategory.setId("");
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(JobSubCategory.class);
                                realm.copyToRealmOrUpdate(jobCategory);
                                realm.copyToRealmOrUpdate(jobSubcategoryResponse.getData());
                            }
                        });
                    }

            }else if(response.code()==401){
                    AppUtils.logout(JobListActivity.this);
            }else{

            }

            }

            @Override
            public void onFailure(@NonNull Call<JobSubcategoryResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getJobDetails(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<JobDetailsResponse> call=RestClient.get().jobProfileDetail(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<JobDetailsResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<JobDetailsResponse> call, @NonNull Response<JobDetailsResponse> response) {

                JobDetailsResponse jobDetailsResponse=response.body();
                if(response.code()==200){

                    if(jobDetailsResponse.getStatus()){
                        create.setText("Update Profile");
                        isUpdate=true;
                    }else{
                        isUpdate=false;
                        create.setText("Create Profile");
                    }
                }else if(response.code()==401){
                    AppUtils.logout(JobListActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<JobDetailsResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getJobList(String jobId,String jobType,String subCat){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("role",jobId);
        hashMap.put("job_type",jobType);
        hashMap.put("subcat",subCat);
        if(!filterSalary.equals("0")) {
            hashMap.put("min_salary", startsalary);
            hashMap.put("max_salary", endsalary);
        }


        hashMap.put("state",stateName);
        hashMap.put("city",cityName);


        hashMap.put("min_exp",startexp);
        hashMap.put("max_exp",endexp);


        Call<JobListResponse> call= RestClient.get().getJobList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<JobListResponse>() {
            @Override
            public void onResponse(@NonNull Call<JobListResponse> call, @NonNull Response<JobListResponse> response) {
                final JobListResponse jobListResponse=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(jobListResponse.getStatus()){
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                realm.delete(JobList.class);
                                realm.copyToRealmOrUpdate(jobListResponse.getData());
                            }
                        });
                    }else{
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                realm.delete(JobList.class);
                                realm.copyToRealmOrUpdate(jobListResponse.getData());
                            }
                        });
                    }
                }else if(response.code()==401){
                    AppUtils.logout(JobListActivity.this);
                }else{

                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<JobListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        getJobList(jobId,jobType,jobSubId);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getJobDetails();
        getJobList(jobId,jobType,jobSubId);
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(JobListActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(JobListActivity.this,Id);

            }
        });
    }



}
