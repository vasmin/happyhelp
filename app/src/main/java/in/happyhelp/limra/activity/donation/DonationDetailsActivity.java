package in.happyhelp.limra.activity.donation;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.donatelist.Donate;
import in.happyhelp.limra.activity.response.mydonationdetails.MyDonationDetailResponse;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DonationDetailsActivity extends AppCompatActivity {
    String dId;
    Realm realm;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.detail)
    TextView detail;

    @BindView(R.id.category)
    TextView category;

    @BindView(R.id.image)
    ImageView imageView;

    @BindView(R.id.enquiry)
    TextView enquiry;

    @BindView(R.id.city)
    TextView city;

    @BindView(R.id.email)
    TextView email;

    @BindView(R.id.mobile)
    TextView mobile;

    @BindView(R.id.address)
    TextView address;

    @BindView(R.id.dob)
    TextView dob;

    boolean myservice=false;
    String isenquiry;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donation_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        realm=RealmHelper.getRealmInstance();
        AppUtils.isNetworkConnectionAvailable(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            dId = extras.getString(Constants.DID);
            isenquiry=extras.getString(Constants.ENQUIRY);
        }

        if(isenquiry!=null){
            myservice=true;
            enquiry.setText("My Service Enquiries");
        }else{
            myservice=false;
        }


        mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mobile.getText().toString()));
                startActivity(intent);
            }
        });

        if(!dId.equals("")) {

            final Donate donate = realm.where(Donate.class).equalTo("id", dId).findFirstAsync();
            donate.addChangeListener(new RealmChangeListener<RealmModel>() {
                @Override
                public void onChange(@NonNull RealmModel realmModel) {

                    if (donate.isValid() && donate.isManaged()) {
                        name.setText(donate.getName());
                        category.setText(donate.getCategory());
                        detail.setText(donate.getDetail());
                        Glide.with(getApplicationContext())
                                .load(RestClient.base_image_url + donate.getImage())
                                .into(imageView);
                        enquiry.setVisibility(View.VISIBLE);
                        city.setText(donate.getCity());
                        address.setText(donate.getAddress());
                        dob.setText(donate.getDate());
                        email.setText(donate.getEmail());
                        mobile.setText(donate.getMobile());

                    }else{
                        getDonationDetails();

                    }
                }
            });
        }

        enquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!myservice) {
                    Intent intent = new Intent(DonationDetailsActivity.this, DonateEnquiryActivity.class);
                    intent.putExtra(Constants.DID, dId);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(DonationDetailsActivity.this, DonationListActivity.class);
                    intent.putExtra(Constants.DID, dId);
                    startActivity(intent);
                }
            }
        });
    }

    public void getDonationDetails(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("donateid",dId);
        Call<MyDonationDetailResponse> call=RestClient.get().getMyDonationDetail(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<MyDonationDetailResponse>() {
            @Override
            public void onResponse(@NonNull Call<MyDonationDetailResponse> call, @NonNull Response<MyDonationDetailResponse> response) {
                MyDonationDetailResponse myDonationDetailResponse=response.body();
                if(response.code()==200){
                    if(myDonationDetailResponse.getStatus()){
                        name.setText(myDonationDetailResponse.getData().getName());
                        category.setText(myDonationDetailResponse.getData().getCategory());
                        detail.setText(myDonationDetailResponse.getData().getDetail());
                        dob.setText(myDonationDetailResponse.getData().getDate());
                        city.setText(myDonationDetailResponse.getData().getCity());
                        mobile.setText(myDonationDetailResponse.getData().getMobile());
                        email.setText(myDonationDetailResponse.getData().getEmail());
                        address.setText(myDonationDetailResponse.getData().getAddress());
                        Glide.with(getApplicationContext())
                                .load(RestClient.base_image_url + myDonationDetailResponse.getData().getImage())
                                .into(imageView);


                    }else{
                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(DonationDetailsActivity.this);
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<MyDonationDetailResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(DonationDetailsActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(DonationDetailsActivity.this,Id);

            }
        });



    }
}
