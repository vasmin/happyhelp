package in.happyhelp.limra.activity.property;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.Jobs.CompanyMembershipActivity;
import in.happyhelp.limra.activity.Jobs.JobApplicationsActivity;
import in.happyhelp.limra.activity.Jobs.JobProfileActivity;
import in.happyhelp.limra.activity.Jobs.PostJobsActivity;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.jobdetailresponse.JobDetailsResponse;
import in.happyhelp.limra.activity.response.jobplanresponse.JobPlan;
import in.happyhelp.limra.activity.response.jobplanresponse.JobPlanResponse;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import in.happyhelp.limra.activity.response.propertydetailsresponse.PropertyDetails;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.adapter.MembershipAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.propertyprofileresponse.Datum;
import in.happyhelp.limra.shoppingresponse.propertyprofileresponse.PropertyProfileResponse;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class PropertyProfileActivity extends AppCompatActivity {

    @BindView(R.id.name)
    EditText companyName;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.description)
    EditText description;

    @BindView(R.id.location)
    EditText location;

    @BindView(R.id.address)
    EditText address;

    @BindView(R.id.weburl)
    EditText webUrl;

    @BindView(R.id.company)
    Spinner companySpinner;

    @BindView(R.id.createprofile)
    TextView createProfile;


    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.membership)
    TextView membership;

    @BindView(R.id.memberlinear)
            LinearLayout memberLinear;

    ArraySpinnerAdapter adapter;
    List<String> list=new ArrayList<>();
    List<JobPlan> propertyList=new ArrayList<>();
    ProgressDialog progressBar;
    String company="";
    String planId="";
    MembershipAdapter membershipAdapter;
    Realm realm;
    JobPlan selectedPlan;
    boolean isUpdate=false;
    boolean isMembershipUpdate=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_profile);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);

        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        realm= RealmHelper.getRealmInstance();

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        getMembershipPlan();
        getPropertyProfile();


        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);

        membershipAdapter=new MembershipAdapter(getApplicationContext(), new MembershipAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(JobPlan d, View view) throws ParseException {
                for(int i=0;i<propertyList.size();i++){
                    JobPlan jobPlan=propertyList.get(i);
                    if(d.getAmount().equals(jobPlan.getAmount())){
                        d.setSelected(true);
                        selectedPlan=d;
                        if(jobPlan.equals(d.getId())){
                            isMembershipUpdate=false;
                        }else{
                            isMembershipUpdate=true;
                        }
                    }else{
                        jobPlan.setSelected(false);
                    }
                }
                membershipAdapter.setData(propertyList);
            }
        });

        membership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isMembershipUpdate) {
                    Intent intent = new Intent(PropertyProfileActivity.this, MembershipPropertyActivity.class);
                    startActivity(intent);
                }
            }
        });

        createProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(companyName.getText().toString())) {
                    companyName.setError("Enter companyName ");
                    companyName.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(email.getText().toString())) {
                    email.setError("Enter email ");
                    email.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(mobile.getText().toString())) {
                    mobile.setError("Enter mobile ");
                    mobile.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(location.getText().toString())) {
                    location.setError("Enter location ");
                    location.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(address.getText().toString())) {
                    address.setError("Enter address ");
                    address.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(description.getText().toString())) {
                    description.setError("Enter description ");
                    description.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(webUrl.getText().toString())) {
                    webUrl.setError("Enter webUrl ");
                    webUrl.requestFocus();
                    return;
                }

               /* if(selectedPlan==null){
                    Toast.makeText(PropertyProfileActivity.this, "Please Select Member Ship Plan", Toast.LENGTH_SHORT).show();
                    return;
                }*/

                if(!isUpdate) {
                    callInstamojoPay(email.getText().toString(), mobile.getText().toString(), selectedPlan.getAmount(), "Company MemberShip Plan", companyName.getText().toString());
                }else{
                    updatePropertyProfile();
                }
            }
        });

        list.add("Company");
        list.add("Individual");
        adapter=new ArraySpinnerAdapter(this);
        adapter.setData(list);
        companySpinner.setAdapter(adapter);

        companySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                company=list.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    public void updatePropertyProfile(){
        progressBar.show();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String userID= SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID);
        builder.addFormDataPart("userid", userID);
        builder.addFormDataPart("company_name", companyName.getText().toString());
        builder.addFormDataPart("mobile", mobile.getText().toString());
        builder.addFormDataPart("email", email.getText().toString());
        builder.addFormDataPart("description", description.getText().toString());
        builder.addFormDataPart("location",location.getText().toString());

        if(company.equals("Company")) {
            builder.addFormDataPart("i_am", "1");
        }else{
            builder.addFormDataPart("i_am", "0");
        }
        builder.addFormDataPart("address", address.getText().toString());
        builder.addFormDataPart("web_url", webUrl.getText().toString());
       // builder.addFormDataPart("plan", selectedPlan.getId());
        /*if(path!=null){
            File filepath = new File(path);
            builder.addFormDataPart("cv", filepath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), filepath));
        }*/

        final MultipartBody requestBody = builder.build();

        Call<Response> call= RestClient.get(RestClient.PROPERTY_BASE_URL).updatePropertyCompany(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                progressBar.dismiss();
                Response response1=response.body();
                if(response.code()==200){

                    if(response1.getStatus()) {

                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();

                        new AlertDialog.Builder(PropertyProfileActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(response1.getMessage())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .show();
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(PropertyProfileActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }



    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);
                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();

                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);
                companyMembership(payment);
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }


    public void getMembershipPlan(){
        Call<JobPlanResponse> call=RestClient.get(RestClient.PROPERTY_BASE_URL).getPropertyPlan(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<JobPlanResponse>() {
            @Override
            public void onResponse(@NonNull Call<JobPlanResponse> call, @NonNull retrofit2.Response<JobPlanResponse> response) {

                final JobPlanResponse jobPlanResponse=response.body();

                if(response.code()==200){
                    if(jobPlanResponse.getStatus()){
                        propertyList.clear();
                        propertyList.addAll(jobPlanResponse.getData());
                        membershipAdapter.setData(propertyList);
                        recyclerView.setAdapter(membershipAdapter);

                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(PropertyProfileActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JobPlanResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void companyMembership(String payment){
        progressBar.show();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String userID= SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID);
        builder.addFormDataPart("userid", userID);
        builder.addFormDataPart("company_name", companyName.getText().toString());
        builder.addFormDataPart("mobile", mobile.getText().toString());
        builder.addFormDataPart("email", email.getText().toString());
        //  builder.addFormDataPart("location", );
        builder.addFormDataPart("description", description.getText().toString());
        builder.addFormDataPart("location",location.getText().toString());

        if(company.equals("Company")) {
            builder.addFormDataPart("i_am", "1");
        }else{
            builder.addFormDataPart("i_am", "0");
        }
        builder.addFormDataPart("address", address.getText().toString());
        builder.addFormDataPart("web_url", webUrl.getText().toString());
        builder.addFormDataPart("payment_id", payment);
        builder.addFormDataPart("plan", selectedPlan.getId());
        /*if(path!=null){
            File filepath = new File(path);
            builder.addFormDataPart("cv", filepath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), filepath));
        }*/

        final MultipartBody requestBody = builder.build();

        Call<Response> call= RestClient.get(RestClient.PROPERTY_BASE_URL).companyPropertyMemberShip(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                progressBar.dismiss();
                Response response1=response.body();
                if(response.code()==200){

                    if(response1.getStatus()) {

                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();

                        new AlertDialog.Builder(PropertyProfileActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(response1.getMessage())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .show();
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(PropertyProfileActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void getPropertyProfile(){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<PropertyProfileResponse> call=RestClient.get(RestClient.PROPERTY_BASE_URL).getPropertyProfile(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<PropertyProfileResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<PropertyProfileResponse> call, @NonNull retrofit2.Response<PropertyProfileResponse> response) {
                progressBar.dismiss();
                PropertyProfileResponse propertyProfileResponse=response.body();
                if(response.code()==200){
                    if(propertyProfileResponse.getStatus()){
                        Datum profileProperty=propertyProfileResponse.getData().get(0);
                        if(profileProperty!=null) {
                            SharedPreferenceHelper.getInstance(getApplicationContext()).setPropertyCompanyProfile(true);
                            companyName.setText(profileProperty.getCompanyName());
                            email.setText(profileProperty.getEmail());
                            mobile.setText(profileProperty.getMobile());
                            address.setText(profileProperty.getAddress());
                            location.setText(profileProperty.getLocation());
                            description.setText(profileProperty.getDescription());
                            webUrl.setText(profileProperty.getWebUrl());
                            isUpdate=true;
                            if(profileProperty.getIAm().equals("0")){
                                companySpinner.setSelection(1);
                            }else{
                                companySpinner.setSelection(0);
                            }
                            planId=profileProperty.getPlan();
                            for(int i=0;i<propertyList.size();i++) {
                                if (profileProperty.getPlan().equals(propertyList.get(i).getId())){
                                    JobPlan jobPlan=propertyList.get(i);
                                    jobPlan.setSelected(true);
                                    membershipAdapter.setData(propertyList);
                                }
                            }
                            if(profileProperty.isPlanexpired()){
                                SharedPreferenceHelper.getInstance(getApplicationContext()).setPropertyCompanyProfile(false);
                                membership.setText("Membership plan Expire Please Renew plan");
                                isMembershipUpdate=true;
                            }else{
                                isMembershipUpdate=false;
                                SharedPreferenceHelper.getInstance(getApplicationContext()).setPropertyCompanyProfile(true);
                                membership.setText("Membership plan Expire Date "+profileProperty.getPlanexpire()+"\n Your plan is "+profileProperty.getPlan());
                            }
                            createProfile.setText("Update Profile");
                            memberLinear.setVisibility(View.GONE);
                        }
                    }else{
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setPropertyCompanyProfile(false);
                        membership.setVisibility(View.GONE);
                        memberLinear.setVisibility(View.VISIBLE);
                    }

                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<PropertyProfileResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPropertyProfile();
        EventBus.getDefault().unregister(this);
    }


    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(PropertyProfileActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(PropertyProfileActivity.this,Id);

            }
        });
    }
}
