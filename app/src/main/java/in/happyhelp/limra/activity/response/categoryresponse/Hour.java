
package in.happyhelp.limra.activity.response.categoryresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hour {

    @SerializedName("hour")
    @Expose
    private String hour;
    @SerializedName("amount")
    @Expose
    private String amount;



    @SerializedName("vamount")
    @Expose
    private String vamount;


    @SerializedName("service_name")
    @Expose
    private String service_name;

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getVamount() {
        return vamount;
    }

    public void setVamount(String vamount) {
        this.vamount = vamount;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

}
