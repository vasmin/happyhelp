
package in.happyhelp.limra.activity.response.myrodetailresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.happyhelp.limra.activity.response.myroresponse.MyRo;

public class MyRoDetailsResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private MyRo data;
    @SerializedName("bills")
    @Expose
    private List<Bill> bills = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public MyRo getData() {
        return data;
    }

    public void setData(MyRo data) {
        this.data = data;
    }

    public List<Bill> getBills() {
        return bills;
    }

    public void setBills(List<Bill> bills) {
        this.bills = bills;
    }

}
