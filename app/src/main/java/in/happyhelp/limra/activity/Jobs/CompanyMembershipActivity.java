package in.happyhelp.limra.activity.Jobs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.property.PostPropertyActivity;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.adapter.MembershipAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.gstresponse.GstRateResponse;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.jobplanresponse.JobPlan;
import in.happyhelp.limra.activity.response.jobplanresponse.JobPlanResponse;
import in.happyhelp.limra.shopping.ShoppyAddressActivity;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import io.realm.Realm;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;

public class CompanyMembershipActivity extends AppCompatActivity {

    @BindView(R.id.name)
    EditText companyName;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.description)
    EditText description;

    @BindView(R.id.location)
    EditText location;

    @BindView(R.id.address)
    EditText address;

    @BindView(R.id.weburl)
    EditText webUrl;

    @BindView(R.id.company)
    Spinner companySpinner;

    @BindView(R.id.createprofile)
    TextView createProfile;


    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    ArraySpinnerAdapter adapter;
    List<String> list=new ArrayList<>();
    List<JobPlan> jobList=new ArrayList<>();
    ProgressDialog progressBar;
    String company="";
    LinearLayoutManager linearLayoutManager;
    MembershipAdapter membershipAdapter;
    Realm realm;
    JobPlan selectedPlan;
    int gstRate=0;
    boolean isSelected=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_membership);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        realm= RealmHelper.getRealmInstance();

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getMembershipPlan();
        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);

        membershipAdapter=new MembershipAdapter(getApplicationContext(), new MembershipAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(JobPlan d, View view) throws ParseException {
                for(int i=0;i<jobList.size();i++){
                    JobPlan jobPlan=jobList.get(i);
                    if(d.getAmount().equals(jobPlan.getAmount())){

                        d.setSelected(true);
                        selectedPlan=d;


                    }else{
                        jobPlan.setSelected(false);
                    }
                }
                membershipAdapter.setData(jobList);
            }
        });

        createProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(companyName.getText().toString())) {
                    companyName.setError("Enter companyName ");
                    return;
                }

                if (TextUtils.isEmpty(email.getText().toString())) {
                    email.setError("Enter email ");
                    return;
                }

                if (TextUtils.isEmpty(mobile.getText().toString())) {
                    mobile.setError("Enter mobile ");
                    return;
                }

                if (TextUtils.isEmpty(location.getText().toString())) {
                    location.setError("Enter location ");
                    return;
                }

                if (TextUtils.isEmpty(address.getText().toString())) {
                    address.setError("Enter address ");
                    return;
                }

                if (TextUtils.isEmpty(description.getText().toString())) {
                    description.setError("Enter description ");
                    return;
                }

                if (TextUtils.isEmpty(webUrl.getText().toString())) {
                    webUrl.setError("Enter web Url ");
                    return;
                }

               /* if( !AppUtils.isValidURL(webUrl.getText().toString())){
                    webUrl.setError("Enter valid web Url ");
                    return;
                }*/

                if(selectedPlan==null){
                    Toast.makeText(CompanyMembershipActivity.this, "Please Select Member Ship Plan", Toast.LENGTH_SHORT).show();
                    return;
                }

                callInstamojoPay(email.getText().toString(),mobile.getText().toString(),selectedPlan.getAmount(),"Company MemberShip Plan",companyName.getText().toString());

            }
        });

        list.add("I am Company");
        list.add("Individual");
        list.add("Consultancy");
        adapter=new ArraySpinnerAdapter(this);
        adapter.setData(list);
        companySpinner.setAdapter(adapter);




        companySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                company=list.get(i);


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


    }


    public void getGstRate(){
        Call<GstRateResponse> call=RestClient.get().getGstRate(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<GstRateResponse>() {
            @Override
            public void onResponse(Call<GstRateResponse> call, retrofit2.Response<GstRateResponse> response) {

                if(response.code()==200){
                    GstRateResponse gstRateResponse=response.body();

                    if(gstRateResponse.getStatus()) {
                        if (gstRateResponse.getGst().size() >= 6) {
                            gstRate = Integer.parseInt(gstRateResponse.getGst().get(6).getName());

                        }
                    }else{
                        gstRate=0;
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getApplicationContext());
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<GstRateResponse> call, Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }


    @SuppressLint("SetTextI18n")
    public void setDataVisible(){
        if(isSelected){
            /*pay.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.VISIBLE);
            double result = (double) ((Integer.parseInt(membershipAmt) * gstRate) / 100);
            totalamount= String.valueOf(Double.parseDouble(membershipAmt)+result);
            gstAmount.setText(String.valueOf(result));
            gst.setText("Exclusive Gst ("+gstRate+"% )");
            pay.setText("Pay for Membership  "+"\u20B9 "+totalamount);*/
        }
    }



    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        Log.e("data","is "+email+phone+amount+purpose+buyername);
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);
                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();

                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);
                companyMembership(payment);
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }


    public void getMembershipPlan(){
        Call<JobPlanResponse> call=RestClient.get().getJobPlan(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<JobPlanResponse>() {
            @Override
            public void onResponse(Call<JobPlanResponse> call, retrofit2.Response<JobPlanResponse> response) {

                final JobPlanResponse jobPlanResponse=response.body();

                if(response.code()==200){
                    if(jobPlanResponse.getStatus()){
                        jobList.clear();
                        jobList.addAll(jobPlanResponse.getData());
                        membershipAdapter.setData(jobList);
                        recyclerView.setAdapter(membershipAdapter);

                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(CompanyMembershipActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<JobPlanResponse> call, Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void companyMembership(String payment){
        progressBar.show();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String userID= SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID);
        builder.addFormDataPart("userid", userID);
        builder.addFormDataPart("company_name", companyName.getText().toString());
        builder.addFormDataPart("mobile", mobile.getText().toString());
        builder.addFormDataPart("email", email.getText().toString());
        //  builder.addFormDataPart("location", );
        builder.addFormDataPart("description", description.getText().toString());
        builder.addFormDataPart("location",location.getText().toString());

       /* if(company.equals("I am Company")){
            builder.addFormDataPart("i_am", "0");
        }else if(company.equals("Individual")){
            builder.addFormDataPart("i_am", "1");
        }else if(company.equals("Consultancy")){
            builder.addFormDataPart("i_am", "2");
        }else{
            builder.addFormDataPart("i_am", "");
        }*/


        builder.addFormDataPart("i_am", company);
        builder.addFormDataPart("address", address.getText().toString());
        builder.addFormDataPart("web_url", webUrl.getText().toString());
        builder.addFormDataPart("payment_id", payment);
        builder.addFormDataPart("plan", selectedPlan.getId());


       /* if(path!=null){
            File filepath = new File(path);
            builder.addFormDataPart("cv", filepath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), filepath));
        }*/

        MultipartBody requestBody = builder.build();





        Call<Response> call= RestClient.get().companyMemberShip(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {

                progressBar.dismiss();
                Response response1=response.body();
                if(response.code()==200){
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                    SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(true);
                    Intent intent=new Intent(CompanyMembershipActivity.this,PostJobsActivity.class);
                    startActivity(intent);
                    finish();

                }else if(response.code()==401){
                    AppUtils.logout(CompanyMembershipActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(CompanyMembershipActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(CompanyMembershipActivity.this,Id);

            }
        });
    }

}
