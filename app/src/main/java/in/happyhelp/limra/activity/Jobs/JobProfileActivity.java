package in.happyhelp.limra.activity.Jobs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.MyProfileActivity;
import in.happyhelp.limra.activity.response.jobdetailresponse.JobDetailsResponse;
import in.happyhelp.limra.activity.response.statecityresponse.City;
import in.happyhelp.limra.activity.response.statecityresponse.State;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.adapter.SalaryAdapter;
import in.happyhelp.limra.adapter.WorkAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class JobProfileActivity extends AppCompatActivity {

    private static final int GALLERY_PROFILE =321;
    private int REQUEST_IMAGE_CAPTURE=123;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.upload)
    TextView upload;

    @BindView(R.id.industry)
    EditText industry;

    @BindView(R.id.experience)
    Spinner experience;

    @BindView(R.id.salary)
    Spinner salarySpinner;


    @BindView(R.id.role)
    EditText role;



    @BindView(R.id.qualification)
    EditText qualification;

    @BindView(R.id.skill)
    EditText skill;



    @BindView(R.id.crateprofile)
    CardView createProfile;

    @BindView(R.id.create)
    TextView create;



    Realm realm;
    String stateName,cityName;
    String statename="",cityname="";


    List<String> listCity=new ArrayList<>();
    List<String> listState=new ArrayList<>();

    @BindView(R.id.uploadresume)
    LinearLayout uploadResume;
    ArrayList<String> workList=new ArrayList<>();
    ArrayList<String> salaryList=new ArrayList<>();
    String path="";
    ProgressDialog progressBar;
    String workExp="0";
    String salary="0";

    WorkAdapter workAdapter;
    SalaryAdapter salaryAdapter;

    @BindView(R.id.state)
    Spinner stateSpinner;


    @BindView(R.id.cityspinner)
    Spinner citySpinner;

    ArraySpinnerAdapter stateSpinnerAdapter,citySpinnerAdapter;

    RealmResults<State> statesList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_profile);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);

        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);
        getJobDetails();
        realm= RealmHelper.getRealmInstance();


        RealmResults<UserData> userData=realm.where(UserData.class).findAllAsync();
        userData.addChangeListener(new RealmChangeListener<RealmResults<UserData>>() {
            @Override
            public void onChange(@NonNull RealmResults<UserData> userData) {
                if(userData.size()>0) {
                    name.setText(userData.get(0).getName());
                    name.setEnabled(false);
                    email.setText(userData.get(0).getEmail());
                    email.setEnabled(false);
                    mobile.setText(userData.get(0).getMobile());
                    mobile.setEnabled(false);

                }


            }
        });


        stateSpinnerAdapter =new ArraySpinnerAdapter(JobProfileActivity.this);
        citySpinnerAdapter =new ArraySpinnerAdapter(JobProfileActivity.this);

        statesList=realm.where(State.class).findAllAsync();
        statesList.addChangeListener(new RealmChangeListener<RealmResults<State>>() {
            @Override
            public void onChange(RealmResults<State> states) {
                listState.clear();
                for(int i=0;i<statesList.size();i++){
                    listState.add(statesList.get(i).getState());
                }
                stateSpinnerAdapter.setData(listState);
                stateSpinner.setAdapter(stateSpinnerAdapter);
            }
        });



        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stateName = String.valueOf(listState.get(i));


                final RealmResults<City> cityRealmResults = realm.where(City.class).equalTo("state", stateName).findAllAsync();
                cityRealmResults.addChangeListener(new RealmChangeListener<RealmResults<City>>() {
                    @Override
                    public void onChange(RealmResults<City> cities) {

                        listCity.clear();
                        listCity.add("Select City");
                        if (cityRealmResults.size() > 0) {
                            for (int i = 0; i < cityRealmResults.size(); i++) {
                                if (!listCity.contains(cities.get(i).getCity())) {
                                    listCity.add(cities.get(i).getCity());
                                }
                            }
                            citySpinnerAdapter.setData(listCity);
                            citySpinner.setAdapter(citySpinnerAdapter);

                            for(int j=0;j<listCity.size();j++) {
                                if(!cityname.equals(""))
                                if (cityname.equals(listCity.get(j))){
                                    citySpinner.setSelection(j);
                                }
                            }
                        }
                    }


                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(listCity.size()>i) {
                    cityName = String.valueOf(listCity.get(i));

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });




        workList.clear();
        workList.add("Experience");


        salaryList.clear();
        salaryList.add("Salary in ");


        for(int i=0;i<25;i++){
            workList.add(String.valueOf(i));
            salaryList.add(String.valueOf(i));
        }
        workAdapter=new WorkAdapter(this);

        salaryAdapter=new SalaryAdapter(this);


        workAdapter.setData(workList);
        experience.setAdapter(workAdapter);

        salaryAdapter.setData(salaryList);
        salarySpinner.setAdapter(salaryAdapter);

        experience.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(workList.size()>i) {
                    workExp = workList.get(i);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        salarySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(salaryList.size()>i) {
                    salary = salaryList.get(i);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        createProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               /* if (TextUtils.isEmpty(location.getText().toString())) {
                    location.setError("Enter location ");
                    return;
                }*/

                if (TextUtils.isEmpty(industry.getText().toString())) {
                    industry.setError("Enter industry ");
                    return;
                }

              /*  if (TextUtils.isEmpty(experience.getText().toString())) {
                    experience.setError("Enter experience ");
                    return;
                }*/

                if(stateName.equals("Select State")){
                    Toast.makeText(getApplicationContext(),"Select State",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(cityName.equals("Select City")){
                    Toast.makeText(getApplicationContext(),"Select City",Toast.LENGTH_SHORT).show();
                    return;
                }



                if(workExp.equals("")||workExp.equals("Experience")){
                    Toast.makeText(getApplicationContext(),"Select Work experience",Toast.LENGTH_SHORT).show();
                    return;
                }


                if (TextUtils.isEmpty(role.getText().toString())) {
                    role.setError("Enter role ");
                    return;
                }

                if (TextUtils.isEmpty(qualification.getText().toString())) {
                    qualification.setError("Enter qualification ");
                    return;
                }

                if (TextUtils.isEmpty(skill.getText().toString())) {
                    skill.setError("Enter skill ");
                    return;
                }


                if(salary.equals("")||salary.equals("Salary in")){
                    Toast.makeText(getApplicationContext(),"Select salary",Toast.LENGTH_SHORT).show();
                    return;
                }


              /*  if (TextUtils.isEmpty(salary.getText().toString())) {
                    salary.setError("Enter salary ");
                    return;
                }*/

               /* if(path.equals("")){
                    Toast.makeText(JobProfileActivity.this, "Please Upload Your Resume", Toast.LENGTH_SHORT).show();
                    return;
                }*/


                addJobProfile();
            }
        });

        uploadResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseFile;
                Intent intent;
                chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                chooseFile.setType( "*/ *");
                intent = Intent.createChooser(chooseFile, "Choose a file");
                startActivityForResult(intent, GALLERY_PROFILE);
            }
        });

    }


    public void getJobDetails(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<JobDetailsResponse> call=RestClient.get().jobProfileDetail(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<JobDetailsResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<JobDetailsResponse> call, @NonNull retrofit2.Response<JobDetailsResponse> response) {

                JobDetailsResponse jobDetailsResponse=response.body();
                if(response.code()==200){

                    if(jobDetailsResponse.getStatus()){
                        create.setText("Update Profile");

                       // location.setText(jobDetailsResponse.getData().getJobLocation());
                        industry.setText(jobDetailsResponse.getData().getIndustry());
                        //experience.setText(jobDetailsResponse.getData().getJobExperience());

                        for(int i=0;i<workList.size();i++){
                            if(jobDetailsResponse.getData().getJobExperience().equals(workList.get(i))){
                                experience.setSelection(i);
                            }
                        }

                        statename=jobDetailsResponse.getData().getState();
                        cityname=jobDetailsResponse.getData().getCity();
                        if(!statename.equals(""))
                        statesList=realm.where(State.class).findAllAsync();
                        statesList.addChangeListener(new RealmChangeListener<RealmResults<State>>() {
                            @Override
                            public void onChange(RealmResults<State> states) {
                                listState.clear();
                                for(int i=0;i<statesList.size();i++){
                                    listState.add(statesList.get(i).getState());
                                }

                                stateSpinnerAdapter.setData(listState);
                                stateSpinner.setAdapter(stateSpinnerAdapter);

                                for(int j=0;j<listState.size();j++) {
                                    if (statename.equals(listState.get(j))){
                                        stateSpinner.setSelection(j);
                                    }
                                }
                            }
                        });


                        for(int i=0;i<salaryList.size();i++) {
                            if (jobDetailsResponse.getData().getCurrentSalary().equals(salaryList.get(i))) {
                                salarySpinner.setSelection(i);
                            }
                        }
                        role.setText(jobDetailsResponse.getData().getJobRole());
                        qualification.setText(jobDetailsResponse.getData().getQualification());
                        skill.setText(jobDetailsResponse.getData().getSkills());
                       // salary.setText(jobDetailsResponse.getData().getCurrentSalary());

                    }else{
                        create.setText("Create Profile");
                    }
                }else if(response.code()==401){
                    AppUtils.logout(JobProfileActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<JobDetailsResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) return;

        if(requestCode == GALLERY_PROFILE)
        {
            Uri uri = data.getData();
             path = getRealPathFromURI(uri); // should the path be here in this string
            System.out.print("Path  = " + path);
            upload.setText(path);
        }


       /* if( resultCode==RESULT_OK)
        if(requestCode==GALLERY_PROFILE) {
            Uri uri = data.getData();
             path = uri.getPath();

             upload.setText(path);
           // path = getPath(getApplicationContext(), uri);

        }*/

    }




    public String getRealPathFromURI(Uri contentUri) {
        String [] proj      = {MediaStore.Images.Media.DATA};
        Cursor cursor       = getContentResolver().query( contentUri, proj, null, null,null);
        if (cursor == null) return null;
        int column_index    = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void addJobProfile(){

        progressBar.show();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String userID= SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID);
        builder.addFormDataPart("userid", userID);
        builder.addFormDataPart("name", name.getText().toString());
        builder.addFormDataPart("mobile", mobile.getText().toString());
        builder.addFormDataPart("email", email.getText().toString());
       // builder.addFormDataPart("location", location.getText().toString());
        builder.addFormDataPart("state",stateName);
        builder.addFormDataPart("city",cityName);
        builder.addFormDataPart("industry", industry.getText().toString());
        builder.addFormDataPart("skills",skill.getText().toString());
        builder.addFormDataPart("experience", workExp);
        builder.addFormDataPart("role", role.getText().toString());
        builder.addFormDataPart("current_salary", salary);
        builder.addFormDataPart("qualification", qualification.getText().toString());


     /*   if(path!=null) {
            File filepath = new File(path);
            if (filepath.exists()) {
                builder.addFormDataPart("cv", filepath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), filepath));
            }
        }*/

        MultipartBody requestBody = builder.build();

        Call<Response> call= RestClient.get().jobProfile(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                Response response1=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(response1.getStatus()){


                        new AlertDialog.Builder(JobProfileActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(response1.getMessage())
                                .setMessage("Successfully")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        onBackPressed();
                                    }

                                })
                                .show();



                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }

                }else if(response.code()==401){
                    AppUtils.logout(JobProfileActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(JobProfileActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(JobProfileActivity.this,Id);

            }
        });
    }




}
