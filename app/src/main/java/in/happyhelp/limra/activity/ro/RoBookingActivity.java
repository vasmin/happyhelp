package in.happyhelp.limra.activity.ro;

import android.annotation.SuppressLint;

import in.happyhelp.limra.activity.response.loginresponse.UserData;
import in.happyhelp.limra.model.NotificationEvent;

import android.app.Activity;

import in.happyhelp.limra.activity.ConfirmationActivity;
import instamojo.library.Instamojo;
import instamojo.library.InstapayListener;
import instamojo.library.InstamojoPay;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONObject;
import org.json.JSONException;
import android.content.IntentFilter;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.statecityresponse.City;
import in.happyhelp.limra.activity.response.statecityresponse.State;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;

public class RoBookingActivity extends AppCompatActivity {

    String roId,rent,deposit,month,bookingtype,buyamt,gst,gstAmt;
    boolean roBook=false;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.address)
    EditText address;

    @BindView(R.id.locality)
    EditText locality;

    @BindView(R.id.pincode)
    EditText pinCode;

    @BindView(R.id.booknow)
    TextView bookNow;

    @BindView(R.id.statespinner)
    Spinner stateSpinner;

    @BindView(R.id.cityspinner)
    Spinner citySpinner;



    Realm realm;

    List<String> stateList=new ArrayList<>();
    List<String> cityList=new ArrayList<>();

    AwesomeValidation awesomeValidation;

    RealmResults<State> statesList;

    ArraySpinnerAdapter stateAdapter,cityAdapter;

    String state,city;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ro_booking);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);
        realm=RealmHelper.getRealmInstance();

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            roId = extras.getString(Constants.ROID);
            rent = extras.getString(Constants.RENT);
            deposit = extras.getString(Constants.DEPOSIT);
            month=extras.getString(Constants.MONTH);
            bookingtype=extras.getString(Constants.BOOKINGTYPE);
           // buyamt = extras.getString(Constants.BUY);
            buyamt=extras.getString(Constants.BUYAMT);

            gst=extras.getString(Constants.GST);
            gstAmt=extras.getString(Constants.GSTAMT);


            Log.e("roId",roId+deposit+rent+buyamt);

        }

        RealmResults<UserData> userData=realm.where(UserData.class).findAllAsync();
        userData.addChangeListener(new RealmChangeListener<RealmResults<UserData>>() {
            @Override
            public void onChange(@NonNull RealmResults<UserData> userData) {
                if(userData!=null) {
                    if (userData.size() > 0) {
                        name.setText(userData.get(0).getName());
                        email.setText(userData.get(0).getEmail());
                        locality.setText(userData.get(0).getLocality());
                        pinCode.setText(userData.get(0).getPincode());
                        address.setText(userData.get(0).getAddress());
                        mobile.setText(userData.get(0).getMobile());
                    }
                }
            }
        });

        stateAdapter=new ArraySpinnerAdapter(this);
        cityAdapter=new ArraySpinnerAdapter(this);

        statesList=realm.where(State.class).findAllAsync();
        statesList.addChangeListener(new RealmChangeListener<RealmResults<State>>() {
            @Override
            public void onChange(RealmResults<State> states) {
                stateList.clear();
                for(int i=0;i<statesList.size();i++){
                    stateList.add(statesList.get(i).getState());
                }
                stateAdapter.setData(stateList);
                stateSpinner.setAdapter(stateAdapter);
            }
        });

        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                state= stateList.get(i);
                if(state!=null) {
                    RealmResults<City> cities=realm.where(City.class).equalTo("state",state).findAllAsync();
                    cities.addChangeListener(new RealmChangeListener<RealmResults<City>>() {
                        @Override
                        public void onChange(RealmResults<City> cities) {
                            cityList.clear();
                            for(int i=0;i<cities.size();i++){
                                cityList.add(cities.get(i).getCity());
                            }
                            cityAdapter.setData(cityList);
                            citySpinner.setAdapter(cityAdapter);
                        }
                    });
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                city=cityList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.email, Patterns.EMAIL_ADDRESS, R.string.emailerror);

        bookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(name.getText().toString())) {
                    name.setError("Enter Name ");
                    name.requestFocus();
                    return;
                }



                    if (!AppUtils.isValidMail(email.getText().toString(),email)) {
                        email.setError("Enter Valid Email");
                        email.requestFocus();
                        return;
                    }


                    if (!AppUtils.isValidMobile(mobile.getText().toString())) {
                        mobile.setError("Enter Valid mobile");
                        mobile.requestFocus();
                        return;
                    }

                    if (TextUtils.isEmpty(address.getText().toString())) {
                        address.setError("Enter Address ");
                        address.requestFocus();
                        return;
                    }


                    if (TextUtils.isEmpty(locality.getText().toString())) {
                        locality.setError("Enter Locality ");
                        locality.requestFocus();
                        return;
                    }
                    if (TextUtils.isEmpty(pinCode.getText().toString())) {
                        pinCode.setError("Enter Pincode ");
                        pinCode.requestFocus();
                        return;
                    }

                    if(pinCode.getText().length()!=6){
                        pinCode.setError("Enter valid pincode ");
                        pinCode.requestFocus();
                        return;
                    }

                    if(state.equals("Select State")){
                        Toast.makeText(getApplicationContext(),"Select State",Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if(city.equals("Select City")){
                        Toast.makeText(getApplicationContext(),"Select City",Toast.LENGTH_SHORT).show();
                        return;
                    }

                    String mail=email.getText().toString();
                    String mob= mobile.getText().toString();
                    String Name=name.getText().toString();

                    Log.e(mail,mob+" deposit "+ deposit+" buyamt "+buyamt+" Name "+Name+" bookingtype "+bookingtype);


                    if (bookingtype.equals("0")) {
                        callInstamojoPay(mail,mob, deposit, "RoRent",Name );
                    } else {
                        double totalamt= Double.parseDouble(buyamt)+Double.parseDouble(gstAmt);
                        callInstamojoPay(mail,mob, String.valueOf(totalamt), "RoBuy",Name );
                    }

            }
        });
    }


    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }
    InstapayListener listener;
    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);
                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);
                if(!roBook) {
                    roBooking(payment);
                }
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }


  /*  private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);

                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();

                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);
                roBooking(payment);
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }*/

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void roBooking(String paymentId){
        roBook=true;
        HashMap<String,String>hashMap=new HashMap<>();
        hashMap.put("roid",roId);
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("name",name.getText().toString());
        hashMap.put("email",email.getText().toString());
        hashMap.put("mobile",mobile.getText().toString());
        hashMap.put("address",address.getText().toString());
        hashMap.put("locality",locality.getText().toString());
        hashMap.put("pincode",pinCode.getText().toString());
        hashMap.put("state",state);
        hashMap.put("city",city);
        hashMap.put("gst",gst);
        hashMap.put("gstamt",gstAmt);



        if(bookingtype.equals("0")) {
            hashMap.put("rent", rent);
            hashMap.put("deposit", deposit);
            hashMap.put("month", month);
            hashMap.put("buy_amount", "0");
            hashMap.put("type",bookingtype);
        }else{
            hashMap.put("rent", "0");
            hashMap.put("deposit", "0");
            hashMap.put("month", "0");
            hashMap.put("buy_amount", buyamt);
            hashMap.put("type",bookingtype);
        }

        hashMap.put("payment_id",paymentId);

        Call<Response> call=RestClient.get().roBooking(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();


                        Intent intent=new Intent(RoBookingActivity.this,ConfirmationActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getApplicationContext());

                }else{

                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(RoBookingActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(RoBookingActivity.this,Id);
            }
        });
    }

}
