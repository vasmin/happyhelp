package in.happyhelp.limra.activity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.orhanobut.dialogplus.DialogPlus;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.ForceUpdateAsync;
import in.happyhelp.limra.GPSTracker;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.Jobs.JobActivity;
import in.happyhelp.limra.activity.Jobs.JobHomeActivity;
import in.happyhelp.limra.activity.ads.AdsListActivity;
import in.happyhelp.limra.activity.donation.DonationActivity;
import in.happyhelp.limra.activity.patient.PatientActivity;
import in.happyhelp.limra.activity.property.PropertyActivity;
import in.happyhelp.limra.activity.property.PropertyHomeActivity;
import in.happyhelp.limra.activity.recharge.RechargeActivity;
import in.happyhelp.limra.activity.response.jobcategory.JobCategory;
import in.happyhelp.limra.activity.response.jobcategory.JobCategoryResponse;
import in.happyhelp.limra.activity.ro.RoRentActivity;
import in.happyhelp.limra.activity.service.ServicePopActivity;
import in.happyhelp.limra.activity.video.VideoActivity;
import in.happyhelp.limra.activity.wallet.ScrollingWalletActivity;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.bannerresponse.Banner;
import in.happyhelp.limra.activity.response.bannerresponse.BannerResponse;
import in.happyhelp.limra.activity.response.companyprofile.CompanyDetailsResponse;
import in.happyhelp.limra.activity.response.companyprofile.Company;
import in.happyhelp.limra.activity.response.couponresponse.CouponsResponse;
import in.happyhelp.limra.activity.response.couponresponse.Datum;
import in.happyhelp.limra.activity.response.loginresponse.LoginResponse;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import in.happyhelp.limra.activity.response.statecityresponse.City;
import in.happyhelp.limra.activity.response.statecityresponse.State;
import in.happyhelp.limra.activity.response.statecityresponse.StateCityResponse;
import in.happyhelp.limra.shopping.HomeShoppingActivity;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,BaseSliderView.OnSliderClickListener {
    @BindView(R.id.slider)
    SliderLayout slider;

    @BindView(R.id.slider2)
    SliderLayout slider2;

    @BindView(R.id.shopping)
    LinearLayout shopping;

    @BindView(R.id.services)
    LinearLayout services;

    @BindView(R.id.recharge)
    LinearLayout recharge;

    @BindView(R.id.rorent)
    LinearLayout rorent;

    @BindView(R.id.property)
    LinearLayout property;

    TextView textCartItemCount;

    @BindView(R.id.ads)
    LinearLayout ads;

    @BindView(R.id.video)
    LinearLayout video;

    @BindView(R.id.job)
    LinearLayout job;

    @BindView(R.id.donate)
    LinearLayout donate;

    @BindView(R.id.patient)
    LinearLayout patient;

    @BindView(R.id.share)
    LinearLayout share;

    @BindView(R.id.referalcode)
    TextView referalCode;

    @BindView(R.id.coupon1)
    ImageView coupon1;

    @BindView(R.id.coupon2)
    ImageView coupon2;

    @BindView(R.id.membership)
    LinearLayout membership;

    private static final int REQUEST_PERMISSION = 123;
    Realm realm;
    TextView wallet,username;
    GPSTracker gps;
    Location location;
    String latitude,longitude;
    DialogPlus dialog;
    TextView decline,accept;
    TextView serviceName,serviceaddress,serviceamt,duration;
    String Id="";
    List<String> listBanner=new ArrayList<>();
    List<String> listFooter=new ArrayList<>();
    LinkedHashMap<String,String> hashMap,footer;
    String currentVersion;
    TextView version;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        realm=RealmHelper.getRealmInstance();
        wallet=toolbar.findViewById(R.id.wallet);
        username=toolbar.findViewById(R.id.username);

        AppUtils.checkAndRequestPermissions(this);

        RealmResults<UserData> userData=realm.where(UserData.class).findAllAsync();
        userData.addChangeListener(userData1 -> {
            if (userData1.size() > 0) {
                username.setText(userData1.get(0).getName());
                Log.e("Wallet " ,"is"+ userData1.get(0).getWallet());
                referalCode.setText(userData1.get(0).getUse_ref_code());
            }
            if (userData1.size() == 0) {
                wallet.setText("0");
            }
        });

        AppUtils.isNetworkConnectionAvailable(this);

        wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomeActivity.this, ScrollingWalletActivity.class);
                startActivity(intent);
            }
        });

        referalCode.setText(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.REFERALCODE));

      /*  Random rand = new Random();

        referalCode.setText("HH"+String.valueOf(rand.nextInt(10000))+SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));

*/
        //   setDialog(this);


        coupon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        coupon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        getStateCity();
        getBanner();
        getCoupons();
        getFooter();
        getUserProfile();
        getNotificationCount();


        gps = new GPSTracker(this);

        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            gps.showSettingsAlert();
        }

        // Check if GPS enabled
        if(gps.canGetLocation()) {
            location=gps.getLocation();
            if(location!=null){

                latitude= String.valueOf(location.getLatitude());
                longitude= String.valueOf(location.getLongitude());

                SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.LATITUDE,latitude);
                SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.LONGITUDE,longitude);

            }
        } else {
            gps.showSettingsAlert();
        }

        getJobCompanyProfile();
        getPropertyCompanyProfile();
        getJobCategory();
        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView =  findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.inflateHeaderView(R.layout.nav_header_home);
        version=header.findViewById(R.id.version);



        shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(HomeActivity.this, HomeShoppingActivity.class);
                startActivity(intent);
                //Toast.makeText(getApplicationContext(),"Coming Soon",Toast.LENGTH_LONG).show();
            }
        });

        membership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomeActivity.this, MemberShipActivity.class);
                startActivity(intent);
            }
        });

        services.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent=new Intent(HomeActivity.this,ServicesListActivity.class);
                startActivity(intent);
                finish();*/
                Intent intent=new Intent(HomeActivity.this,ServicePopActivity.class);
                startActivity(intent);
            }
        });

        rorent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(HomeActivity.this,RoRentActivity.class);
                startActivity(intent);
            }
        });

        property.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(HomeActivity.this, PropertyHomeActivity.class);
                startActivity(intent);
            }
        });

        ads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(HomeActivity.this,AdsListActivity.class);
                startActivity(intent);
            }
        });

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(HomeActivity.this, VideoActivity.class);
                startActivity(intent);
            }
        });

        job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(HomeActivity.this, JobHomeActivity.class);
                startActivity(intent);
            }
        });


        getCurrentVersion();


        donate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(HomeActivity.this,DonationActivity.class);
                startActivity(intent);
            }
        });

        patient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(HomeActivity.this, PatientActivity.class);
                startActivity(intent);
            }
        });



        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: https://play.google.com/store/apps/details?id="+getPackageName()+"&referrer="+referalCode.getText().toString());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomeActivity.this, RechargeActivity.class);
                startActivity(intent);
            }
        });

        setfcmToken();
    }

    public void getJobCategory(){
        Call<JobCategoryResponse> call= RestClient.get().getJobCategory(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<JobCategoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<JobCategoryResponse> call, @NonNull Response<JobCategoryResponse> response) {
                final JobCategoryResponse jobCategoryResponse=response.body();
                if(response.code()==200) {
                    if (jobCategoryResponse.getStatus()) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                realm.delete(JobCategory.class);
                                realm.copyToRealmOrUpdate(jobCategoryResponse.getData());
                            }
                        });
                    } else {
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(HomeActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JobCategoryResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getStateCity();
        getBanner();
        getCoupons();
        getFooter();
        getUserProfile();
        getJobCompanyProfile();
        getPropertyCompanyProfile();
        getNotificationCount();
        getJobCategory();

        RealmResults<UserData> userData=realm.where(UserData.class).findAllAsync();
        userData.addChangeListener(userData1 -> {
            if (userData1.size() > 0) {
                username.setText(userData1.get(0).getName());
                Log.e("Wallet " ,"is"+ userData1.get(0).getWallet());
                referalCode.setText(userData1.get(0).getUse_ref_code());
            }
            if (userData1.size() == 0) {
                wallet.setText("0");
            }
        });


        try {
            EventBus.getDefault().register(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onRestart() {
        super.onRestart();
        //When BACK BUTTON is pressed, the activity on the stack is restarted
        //Do what you want on the refresh procedure here

        getPropertyCompanyProfile();
        getStateCity();
        getBanner();
        getCoupons();
        getFooter();
        getUserProfile();
        getJobCompanyProfile();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Id=event.getBody();
                Log.e("Service ID",Id);
                if(!Id.equals(""))
                    AppUtils.setDialog(HomeActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(HomeActivity.this,Id);
            }
        });

        /*Notification notification = new NotificationCompat.Builder(HomeActivity.this)
                .setContentTitle("Fixtown")
                .setContentText(event.getBody())
                .setSmallIcon(R.drawable.splash_logo)
                .build();
        NotificationManagerCompat manager = NotificationManagerCompat.from(getApplicationContext());
        manager.notify(123, notification);*/
        // sendNotification(event.getBody());
    }
/*

    public void acceptService(){
        @SuppressLint("SimpleDateFormat") DateFormat time = new SimpleDateFormat("hh:mm a");
        String currentTime = time.format(Calendar.getInstance().getTime());
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",Id);
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("accept_time",currentTime);
        hashMap.put("serviceid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.COMPANYCODE));
        hashMap.put("status","1");
        Call<Object> call=RestClient.get().acceptService(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if(response.code()==200){
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                t.printStackTrace();
            }
        });
        }


    public void getMyServiceEnquiryDetail(String id){
        HashMap<String ,String> hashMap=new HashMap<>();
        hashMap.put("id",id);
        Call<MyServicesResponse> call=RestClient.get().myServicesEnquiryDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<MyServicesResponse>() {
            @Override
            public void onResponse(Call<MyServicesResponse> call, Response<MyServicesResponse> response) {
                MyServicesResponse myServicesResponse = response.body();
                if (response.code() == 200) {
                    if (myServicesResponse.isStatus()) {
                        if (myServicesResponse.getData().size() > 0) {
                            MyServices myServices = myServicesResponse.getData().get(0);
                            serviceName.setText(myServices.getName());
                            serviceaddress.setText(myServices.getAddress());
                            duration.setText(myServices.getHour());
                        } else {
                            Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                            onBackPressed();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<MyServicesResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });
    }

    @SuppressLint("PrivateResource")
    public void setDialog(Activity activity) {
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.service_pop, null);
        dialog = DialogPlus.newDialog(activity)
                .setContentHolder(new ViewHolder(view))
                .setGravity(Gravity.CENTER)
                .setCancelable(false)
                .setInAnimation(R.anim.abc_fade_in)
                .setMargin(50, 20, 50, 50)
                .setOutAnimation(R.anim.abc_fade_out)
                .setOverlayBackgroundResource(Color.TRANSPARENT)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogPlus dialog) {
                        dialog.dismiss();
                    }
                })
                .create();

        serviceName= (TextView) dialog.findViewById(R.id.servicename);
        serviceaddress= (TextView) dialog.findViewById(R.id.serviceaddress);
        serviceamt= (TextView) dialog.findViewById(R.id.serviceamt);
        duration= (TextView) dialog.findViewById(R.id.duration);

        decline= (TextView) dialog.findViewById(R.id.decline);
        accept= (TextView) dialog.findViewById(R.id.accept);

        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptService();
            }
        });


    }


    private void startNotification(String messageBody,Activity activity) {
        Log.i("NextActivity", "startNotification");

        // Sets an ID for the notification
        int mNotificationId = 001;

        // Build Notification , setOngoing keeps the notification always in status bar
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.splash_logo)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimary))
                        .setSound(defaultSoundUri)
                        .setOngoing(true);

        // Create pending intent, mention the Activity which needs to be
        //triggered when user clicks on notification(StopScript.class in this case)
        Intent intent=new Intent(this,ServiceRequestActivity.class);
        intent.putExtra(Constants.SERVICEID,Id);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);


        mBuilder.setContentIntent(contentIntent);


        // Gets an instance of the NotificationManager service
        NotificationManager mNotificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);


        // Builds the notification and issues it.
        mNotificationManager.notify(mNotificationId, mBuilder.build());


    }

    private void sendNotification(String messageBody) {
        PendingIntent pendingIntent = null;
        Intent intent = new Intent(this, ServiceRequestActivity.class);
        if (SharedPreferenceHelper.getInstance(getApplicationContext()).isLoggerIn()) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_ONE_SHOT);
        }

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.splash_logo)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimary))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());



    }

    @TargetApi(Build.VERSION_CODES.N)
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void showNotificationMessage(String message) {
        Intent intent = new Intent(this, ServiceRequestActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
        //| Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = 1;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        @SuppressLint("InlinedApi") int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);

            assert notificationManager != null;
            notificationManager.createNotificationChannel(mChannel);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.splash_logo)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND|Notification.DEFAULT_VIBRATE)
                .setContentIntent(pendingIntent);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_ONE_SHOT
        );
        mBuilder.setContentIntent(resultPendingIntent);

        notificationManager.notify((int) System.currentTimeMillis(), mBuilder.build());

    }*/

    public void getJobCompanyProfile(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<CompanyDetailsResponse> call=RestClient.get().jobscompany_details(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<CompanyDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<CompanyDetailsResponse> call, @NonNull Response<CompanyDetailsResponse> response) {
                final CompanyDetailsResponse companyDetailsResponse=response.body();
                if(response.code()==200){
                    if(companyDetailsResponse.getStatus()){
                        if(companyDetailsResponse.getData()!=null) {
                            if(companyDetailsResponse.getData().size()>0) {
                                if(!companyDetailsResponse.getData().get(0).isPlanexpired()) {
                                    SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(true);
                                }else{
                                    SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(false);
                                }
                            }

                        }else
                        {
                            SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(false);
                        }
                    }else{
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(false);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(HomeActivity.this);
                }else{
                    SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(false);

                }
            }

            @Override
            public void onFailure(@NonNull Call<CompanyDetailsResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(false);
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getPropertyCompanyProfile(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<CompanyDetailsResponse> call=RestClient.get(RestClient.PROPERTY_BASE_URL).propertycompany_details(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<CompanyDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<CompanyDetailsResponse> call, @NonNull Response<CompanyDetailsResponse> response) {
                final CompanyDetailsResponse companyDetailsResponse=response.body();
                if(response.code()==200){
                    if(companyDetailsResponse.getStatus()){
                        if(companyDetailsResponse.getData()!=null) {
                            SharedPreferenceHelper.getInstance(getApplicationContext()).setPropertyCompanyProfile(true);

                        }else
                        {
                            SharedPreferenceHelper.getInstance(getApplicationContext()).setPropertyCompanyProfile(false);
                        }
                    }else{
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setPropertyCompanyProfile(false);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(HomeActivity.this);
                }else{
                    SharedPreferenceHelper.getInstance(getApplicationContext()).setPropertyCompanyProfile(false);

                }
            }

            @Override
            public void onFailure(@NonNull Call<CompanyDetailsResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(false);
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
            }
        });
    }


    public void setfcmToken(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("fcmtoken",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.FCMTOKEN));
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<Object> call=RestClient.get().updatefcm(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(@NonNull Call<Object> call, @NonNull Response<Object> response) {
                if(response.code()==200){

                }else if(response.code()==401){
                    AppUtils.logout(HomeActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Exit")
                    .setMessage("Are you sure you want to close this Application?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

        }
    }

    public void getCoupons(){
        Call<CouponsResponse> call=RestClient.get().getCoupons(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<CouponsResponse>() {
            @Override
            public void onResponse(@NonNull Call<CouponsResponse> call, @NonNull Response<CouponsResponse> response) {
                if(response.code()==200){
                    CouponsResponse couponsResponse=response.body();
                    if(couponsResponse.getStatus()){

                        Glide.with(getApplicationContext())
                                .load(RestClient.base_image_url+couponsResponse.getData().get(0).getImage())
                                .into(coupon1);


                        Glide.with(getApplicationContext())
                                .load(RestClient.base_image_url+couponsResponse.getData().get(1).getImage())
                                .into(coupon2);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(HomeActivity.this);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CouponsResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getFooter(){
        Call<CouponsResponse> call=RestClient.get().getFooter(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<CouponsResponse>() {
            @Override
            public void onResponse(@NonNull Call<CouponsResponse> call, @NonNull Response<CouponsResponse> response) {
                if(response.code()==200){

                    final CouponsResponse couponsResponse=response.body();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.delete(Datum.class);
                        }
                    });
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(couponsResponse.getData());
                        }
                    });

                    if(couponsResponse.getStatus()){
                        listFooter.clear();
                        for(int i=0;i<couponsResponse.getData().size();i++){
                            listFooter.add(couponsResponse.getData().get(i).getImage());
                        }

                        setFooter();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(HomeActivity.this);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CouponsResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
            }
        });
    }


    @SuppressLint("SetTextI18n")
    private void getCurrentVersion() {
        PackageManager pm = getPackageManager();
        PackageInfo pInfo = null;
        try {
            pInfo = pm.getPackageInfo(getPackageName(), 0);

        } catch (PackageManager.NameNotFoundException e1) {
            e1.printStackTrace();
        }
        currentVersion = pInfo.versionName;
        version.setText("Version "+currentVersion);
        forceUpdate();

        // new GetLatestVersion().execute();

    }

    public void forceUpdate() {
        PackageManager packageManager = getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
        new ForceUpdateAsync(currentVersion, HomeActivity.this).execute();
    }

    public  void setFooter(){
        footer=new LinkedHashMap<>();

        for(int i=0;i<listFooter.size();i++){
            footer.put("",RestClient.base_image_url+listFooter.get(i));
        }

        for(int i = 0; i<listFooter.size();i ++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(getApplicationContext());
            final int finalI = i;
            defaultSliderView.image(RestClient.base_image_url+listFooter.get(i))
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            String BannnerName=listFooter.get(finalI);
                            Datum banners=realm.where(Datum.class).equalTo("image",BannnerName).findFirst();
                            String url = banners.getLink();

                            if(!url.equals("")) {
                                Intent intent = new Intent(Intent.ACTION_VIEW,
                                        Uri.parse(url));
                                if (null != intent.resolveActivity(getPackageManager())) {
                                    startActivity(intent);
                                }
                            }

                        }
                    });

            slider2.addSlider(defaultSliderView);
        }

        slider2.movePrevPosition(false);
        slider2.setPresetTransformer(com.daimajia.slider.library.SliderLayout.Transformer.Accordion);
        slider2.setPresetIndicator(com.daimajia.slider.library.SliderLayout.PresetIndicators.Center_Bottom);
        slider2.setCustomAnimation(new DescriptionAnimation());
        slider2.setDuration(5000);
        slider2.startAutoCycle();

    }

    public void getBanner(){
        Call<BannerResponse> call=RestClient.get().getBanner(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<BannerResponse>() {
            @Override
            public void onResponse(@NonNull Call<BannerResponse> call, @NonNull Response<BannerResponse> response) {
                final BannerResponse bannerResponse=response.body();

                if(response.code()==200){
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(@NonNull Realm realm) {
                            realm.delete(Banner.class);
                        }
                    });
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(@NonNull Realm realm) {
                            realm.copyToRealmOrUpdate(bannerResponse.getData());
                        }
                    });
                    if(bannerResponse.getStatus()){
                        listBanner.clear();
                        for(int i=0;i<bannerResponse.getData().size();i++){
                            listBanner.add(bannerResponse.getData().get(i).getImage());
                        }
                        setBanner();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(HomeActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<BannerResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
            }
        });
    }

    public  void setBanner(){
        hashMap=new LinkedHashMap<>();


        for(int i = 0; i<listBanner.size();i ++) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(getApplicationContext());
            final int finalI = i;
            defaultSliderView.image(RestClient.base_image_url+listBanner.get(i))
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            String BannnerName=listBanner.get(finalI);
                            Banner banners=realm.where(Banner.class).equalTo("image",BannnerName).findFirst();
                            String url = banners.getLink();

                            if(!url.equals("")) {
                                Intent i = new Intent(Intent.ACTION_VIEW,
                                        Uri.parse(url));
                                startActivity(i);
                            }

                        }
                    });
            slider.addSlider(defaultSliderView);
        }

        slider.movePrevPosition(false);
        slider.setPresetTransformer(com.daimajia.slider.library.SliderLayout.Transformer.Accordion);
        slider.setPresetIndicator(com.daimajia.slider.library.SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(3000);
        slider.startAutoCycle();


    }


    public void getUserProfile(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<LoginResponse> call=RestClient.get().getUserDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                final LoginResponse loginResponse=response.body();
                if(response.code()==200) {

                    if(loginResponse.getStatus()) {

                        if(wallet!=null && loginResponse.getData().size()>0 ) {
                            wallet.setText(String.valueOf(loginResponse.getData().get(0).getWallet()));
                        }else{
                            wallet.setText("0");
                        }

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                realm.delete(UserData.class);
                                realm.copyToRealmOrUpdate(loginResponse.getData());
                            }
                        });
                    }else{
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                realm.delete(UserData.class);
                                realm.copyToRealmOrUpdate(loginResponse.getData());
                            }
                        });
                    }
                }else if(response.code()==401){
                    AppUtils.logout(HomeActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }


    public void getStateCity(){
        Call<StateCityResponse> call=RestClient.get().getStateCity(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<StateCityResponse>() {
            @Override
            public void onResponse(@NonNull Call<StateCityResponse> call, @NonNull Response<StateCityResponse> response) {

                final StateCityResponse stateCityResponse=response.body();
                if(response.code()==200){
                    final State state=new State();
                    state.setState("Select State");
                    state.setId("");

                    final City city=new City();
                    city.setCity("Select City");
                    city.setId("");
                    city.setState("Select State");
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.delete(City.class);
                            realm.delete(State.class);

                            realm.copyToRealmOrUpdate(state);
                            realm.copyToRealmOrUpdate(city);
                            realm.copyToRealmOrUpdate(stateCityResponse.getCity());
                            realm.copyToRealmOrUpdate(stateCityResponse.getState());
                        }
                    });
                }else if(response.code()==401){
                    AppUtils.logout(HomeActivity.this);
                }
            }

            @Override
            public void onFailure(@NonNull Call<StateCityResponse> call, @NonNull Throwable t) {
                t.printStackTrace();

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.home, menu);
        MenuItem notification=menu.findItem(R.id.notification);
        notification.setVisible(true);

        final MenuItem menuItem = menu.findItem(R.id.notification);

        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

        setupBadge();

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });

        return true;
    }

    private void setupBadge() {

        if (textCartItemCount != null) {
            if(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.NOTIFICATION)!=null) {
                int size = Integer.parseInt(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.NOTIFICATION));
                if (size == 0) {
                    if (textCartItemCount.getVisibility() != View.GONE) {
                        textCartItemCount.setVisibility(View.GONE);
                    }
                }
                else {
                    textCartItemCount.setText(String.valueOf(Math.min(size, 99)));
                    if (textCartItemCount.getVisibility() != View.VISIBLE) {
                        textCartItemCount.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    public void getNotificationCount()
    {
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().getNotificationCount(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {

                if(response.code()==200){
                    in.happyhelp.limra.activity.response.Response response1=response.body();
                    if(response1.getStatus()){
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.NOTIFICATION, String.valueOf(response1.getCount()));
                        setupBadge();
                    }else{
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.NOTIFICATION, String.valueOf(response1.getCount()));
                        setupBadge();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(HomeActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.redeem) {
            Intent intent=new Intent(HomeActivity.this,RedeemActivity.class);
            startActivity(intent);
        }

        if (id == R.id.notification) {
            Intent intent=new Intent(HomeActivity.this,NotificationActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if(id==R.id.logout){


            logoutUser();



        }else if(id==R.id.wallet){
            Intent intent=new Intent(HomeActivity.this,ScrollingWalletActivity.class);
            startActivity(intent);
        }else if(id==R.id.tree){
            Intent intent=new Intent(HomeActivity.this,TreeViewActivity.class);
            startActivity(intent);
        }else if(id==R.id.patient){
            Intent intent=new Intent(HomeActivity.this,PatientActivity.class);
            startActivity(intent);
        }else if(id==R.id.myprofile){
            Intent intent=new Intent(HomeActivity.this,MyProfileActivity.class);
            startActivity(intent);
        }else if(id==R.id.account){
            Intent intent=new Intent(HomeActivity.this,MyAccountActivity.class);
            startActivity(intent);
        }else if(id==R.id.kyc){
            Intent intent=new Intent(HomeActivity.this,KycActivity.class);
            startActivity(intent);
        }else if(id==R.id.businesskyc){
            Intent intent=new Intent(HomeActivity.this,BusinessKycActivity.class);
            startActivity(intent);
        }else if(id==R.id.contact){
            final int REQUEST_PHONE_CALL=123;
            new AlertDialog.Builder(HomeActivity.this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Support Call ")
                    .setMessage("Are you sure you want to Call ?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "9222989097"));

                            startActivity(intent);

                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        }else if(id==R.id.change){
            Intent intent=new Intent(HomeActivity.this,ChangePassword.class);
            startActivity(intent);
        }


        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
    public void logoutUser(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().logout(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setUserLoggedIn(false);
                        Intent intent=new Intent(HomeActivity.this,LoginActivity.class);
                        startActivity(intent);
                        finish();

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                realm.deleteAll();
                            }
                        });

                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }

                }else if(response.code()==401){
                    AppUtils.logout(HomeActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }
}
