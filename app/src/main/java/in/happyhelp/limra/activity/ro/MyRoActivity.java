package in.happyhelp.limra.activity.ro;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.adapter.MyRoAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.myroresponse.MyRo;
import in.happyhelp.limra.activity.response.myroresponse.MyRoResponse;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyRoActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    MyRoAdapter adapter;

    @BindView(R.id.data)
    TextView data;
    LinearLayoutManager linearLayoutManager;

    Realm realm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ro);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        realm=RealmHelper.getRealmInstance();
        swipeRefreshLayout.setOnRefreshListener(MyRoActivity.this);
        swipeRefreshLayout.setRefreshing(true);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        adapter=new MyRoAdapter(getApplicationContext());


        getMyRoList();


    }

    @Override
    public void onRefresh() {
        getMyRoList();
    }

    public void getMyRoList(){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
         Call<MyRoResponse> call=RestClient.get().myRoList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
         call.enqueue(new Callback<MyRoResponse>() {
             @Override
             public void onResponse(@NonNull Call<MyRoResponse> call, @NonNull Response<MyRoResponse> response) {
                 swipeRefreshLayout.setRefreshing(false);
                 final MyRoResponse myRoResponse=response.body();
                 if(response.code()==200){
                     if(myRoResponse.getStatus()){

                         if(myRoResponse.getData().size()==0){
                             data.setVisibility(View.VISIBLE);
                         }else{
                             data.setVisibility(View.GONE);
                         }
                         adapter.setData(myRoResponse.getData());
                         recyclerView.setAdapter(adapter);

                         realm.executeTransaction(new Realm.Transaction() {
                             @Override
                             public void execute(Realm realm) {
                                 realm.delete(MyRo.class);
                                 realm.copyToRealmOrUpdate(myRoResponse.getData());
                             }
                         });


                     }else{
                         View parentLayout = findViewById(android.R.id.content);
                         Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                 .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                 .show();

                     }
                 }else if(response.code()==401){
                     AppUtils.logout(MyRoActivity.this);
                 }else{
                     View parentLayout = findViewById(android.R.id.content);
                     Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                             .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                             .show();

                 }
             }

             @Override
             public void onFailure(@NonNull Call<MyRoResponse> call, @NonNull Throwable t) {
                 t.printStackTrace();
                 swipeRefreshLayout.setRefreshing(false);
                 View parentLayout = findViewById(android.R.id.content);
                 Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                         .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                         .show();

             }
         });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MyRoActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MyRoActivity.this,Id);

            }
        });



    }

}
