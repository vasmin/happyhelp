package in.happyhelp.limra.activity.donation;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.ro.RoDetailActivity;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.Response;
import retrofit2.Call;
import retrofit2.Callback;

public class DonateEnquiryActivity extends AppCompatActivity {

    String dId;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.submit)
    TextView submit;

    @BindView(R.id.message)
    EditText message;

    ProgressDialog progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donate_enquiry);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        AppUtils.isNetworkConnectionAvailable(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            dId = extras.getString(Constants.DID);
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                donateEnquiry();
            }
        });

        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

    }

    public void donateEnquiry(){

        if (TextUtils.isEmpty(name.getText().toString())) {
            name.setError("Enter Name ");
            return;
        }

        if(!AppUtils.isValidMail(email.getText().toString(),email)){
            return;
        }

        if(AppUtils.isValidMobile(mobile.getText().toString())){

        }else{
            mobile.setError("Enter valid mobile");
            return;
        }

        if (TextUtils.isEmpty(message.getText().toString())) {
            message.setError("Enter Message ");
            return;
        }

        progressBar.show();

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("donateid",dId);
        hashMap.put("name",name.getText().toString());
        hashMap.put("email",email.getText().toString());
        hashMap.put("mobile",mobile.getText().toString());
        hashMap.put("message",message.getText().toString());

        Call<Response> call=RestClient.get().enquiryDonate(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
               progressBar.dismiss();

               Response response1=response.body();
               if(response.code()==200) {

                   assert response1 != null;
                   if (response1.getStatus()) {
                       View parentLayout = findViewById(android.R.id.content);
                       Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                               .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                               .show();
                       // Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_LONG).show();
                       Intent intent = new Intent(DonateEnquiryActivity.this, DonationActivity.class);
                       startActivity(intent);
                       finish();
                   } else {

                       //  Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_LONG).show();
                   }
               }else if(response.code()==401){
                   AppUtils.logout(DonateEnquiryActivity.this);
               }else{
                   View parentLayout =findViewById(android.R.id.content);
                   Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                           .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                           .show();
               }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                progressBar.dismiss();
                t.printStackTrace();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(DonateEnquiryActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(DonateEnquiryActivity.this,Id);
            }
        });



    }

}
