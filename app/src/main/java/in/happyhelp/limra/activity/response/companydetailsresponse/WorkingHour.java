
package in.happyhelp.limra.activity.response.companydetailsresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkingHour {

    @SerializedName("days")
    @Expose
    private String days;
    @SerializedName("time")
    @Expose
    private String time;

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
