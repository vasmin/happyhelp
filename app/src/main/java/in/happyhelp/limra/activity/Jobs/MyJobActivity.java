package in.happyhelp.limra.activity.Jobs;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.R;
import in.happyhelp.limra.adapter.ViewPagerAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.fragment.CompanyHrFragment;
import in.happyhelp.limra.fragment.MyCompanyFragment;
import in.happyhelp.limra.fragment.MyPostJobFragment;
import in.happyhelp.limra.model.NotificationEvent;

public class MyJobActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener  {

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_job);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        AppUtils.isNetworkConnectionAvailable(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ButterKnife.bind(this);
        initTab();

    }




    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    void initTab() {

        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
        tabLayout.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setupTabIcons() {
        String[] name = {"My Post Job","Company/HR","My Company"};
        Objects.requireNonNull(tabLayout.getTabAt(0)).setText(name[0]);
        Objects.requireNonNull(tabLayout.getTabAt(1)).setText(name[1]);
        Objects.requireNonNull(tabLayout.getTabAt(2)).setText(name[2]);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new MyPostJobFragment(), "My Post Job");
        adapter.addFrag(new CompanyHrFragment(), "Company/HR");
        adapter.addFrag(new MyCompanyFragment(), "My Company");

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onRefresh() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MyJobActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MyJobActivity.this,Id);

            }
        });



    }


}
