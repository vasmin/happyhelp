
package in.happyhelp.limra.activity.response.donateenquiry;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DonateEnquiryResponse {

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("donateenqiry")
    @Expose
    private List<Donateenqiry> donateenqiry = null;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<Donateenqiry> getDonateenqiry() {
        return donateenqiry;
    }

    public void setDonateenqiry(List<Donateenqiry> donateenqiry) {
        this.donateenqiry = donateenqiry;
    }

}
