
package in.happyhelp.limra.activity.response.myservicesresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyServicesResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<MyServices> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<MyServices> getData() {
        return data;
    }

    public void setData(List<MyServices> data) {
        this.data = data;
    }

}
