package in.happyhelp.limra.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.service.OrderDetailsActivity;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.myservicesresponse.MyServices;
import in.happyhelp.limra.activity.response.myservicesresponse.MyServicesResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceRequestActivity extends AppCompatActivity {

    @BindView(R.id.servicename)
    TextView serviceName;

    @BindView(R.id.serviceaddress)
    TextView serviceAddress;

    @BindView(R.id.serviceamt)
    TextView serviceAmt;

    @BindView(R.id.duration)
    TextView duration;

    @BindView(R.id.vendoramt)
    TextView vendorAmt;

    @BindView(R.id.penalty)
    TextView penalty;

    @BindView(R.id.decline)
    TextView decline;

    @BindView(R.id.accept)
    TextView accept;

    @BindView(R.id.date)
    TextView date;

    @BindView(R.id.quantity)
    TextView quantity;


    @BindView(R.id.penaltylinear)
    LinearLayout penaltyLinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_request);
        ButterKnife.bind(this);

        Intent intent=getIntent();

        final String serviceId=intent.getStringExtra("serviceid");
        Log.e("Service ","IS"+serviceId);
        getMyServiceEnquiryDetail(serviceId);

        accept.setOnClickListener(v -> acceptService(serviceId));

        decline.setOnClickListener(v -> finish());
    }


    public  void getMyServiceEnquiryDetail( String id){
        HashMap<String ,String> hashMap=new HashMap<>();
        hashMap.put("id",id);
        Call<MyServicesResponse> call= RestClient.get().myServicesEnquiryDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<MyServicesResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<MyServicesResponse> call, @NonNull Response<MyServicesResponse> response) {
                MyServicesResponse myServicesResponse = response.body();

                if (response.code() == 200) {
                    if (myServicesResponse.getStatus()) {
                        if (myServicesResponse.getData().size() > 0) {
                            MyServices myServices = myServicesResponse.getData().get(0);

                            String service="";
                            for(int j=0;j<myServices.getServices().size();j++) {
                                if(j==0){
                                    service =  myServices.getServices().get(j).getName();
                                }else {
                                    service = service + "," + myServices.getServices().get(j).getName();
                                }
                            }

                            serviceName.setText(service);
                            serviceAddress.setText(myServices.getLocation());

                            if(myServices.getHour().equals("48")){
                                duration.setText(" Schedule Service");
                            }else{
                                duration.setText(myServices.getHour());
                            }

                            quantity.setText(myServices.getQuantity());
                            serviceAmt.setText("Rs "+ myServices.getAmount());
                            vendorAmt.setText("Rs "+ myServices.getVendorAmount());
                            if(myServices.getPenalty().equals("") || myServices.getPenalty()==null){
                                penaltyLinear.setVisibility(View.GONE);
                            }else{
                                penaltyLinear.setVisibility(View.VISIBLE);
                                penalty.setText(myServices.getPenalty()+" "+myServices.getWaiting());
                            }

                            date.setText(myServices.getDate()+" "+myServices.getTime());

                        } else {
                            Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<MyServicesResponse> call, @NonNull Throwable t) {
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });
    }


    private void acceptService( String Id){
        @SuppressLint("SimpleDateFormat") java.text.DateFormat time = new SimpleDateFormat("hh:mm a");
        String currentTime = time.format(Calendar.getInstance().getTime());
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",Id);
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("accept_time",currentTime);
        hashMap.put("serviceid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.COMPANYCODE));
        hashMap.put("status","1");
        Call<in.happyhelp.limra.activity.response.Response> call= RestClient.get().acceptService(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                if(response.code()==200){
                    in.happyhelp.limra.activity.response.Response response1 =response.body();
                    if(response1.getStatus()) {
                        Toast.makeText(ServiceRequestActivity.this, response1.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ServiceRequestActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(ServiceRequestActivity.this, response1.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }else if(response.code()==401){
                    AppUtils.logout(ServiceRequestActivity.this);
                }
            }

            @Override
            public void onFailure(Call<in.happyhelp.limra.activity.response.Response> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(ServiceRequestActivity.this, Constants.SERVERTIMEOUT, Toast.LENGTH_SHORT).show();

            }
        });
    }


}
