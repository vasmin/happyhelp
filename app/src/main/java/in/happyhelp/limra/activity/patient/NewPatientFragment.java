package in.happyhelp.limra.activity.patient;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.property.PostPropertyActivity;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.Response;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import static android.app.Activity.RESULT_OK;


public class NewPatientFragment extends Fragment {

    private static final int GALLERY_DONATEIMAGE = 123;
    private static final int REQUEST_IMAGE_CAPTURE_DONATEIMAGE = 321;
    private static final int REQUEST_IMAGE_CAPTURE_PRECREPTION = 954;
    private static final int GALLERY_PRECREPTION=541;

    @BindView(R.id.patientname)
    EditText patientName;

    @BindView(R.id.paddress)
    EditText patientAddress;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.hospitalname)
    EditText hospitalName;

    @BindView(R.id.doctorname)
    EditText drName;

    @BindView(R.id.amount)
    EditText amount;

    @BindView(R.id.message)
    EditText message;

    @BindView(R.id.image)
    ImageView profileImage;

    @BindView(R.id.precription)
    ImageView precreption;

    String profilePath="";
    String precriptionPath="";


    @BindView(R.id.submit)
    TextView submit;

    ProgressDialog progressBar;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_newpatient, container, false);
        ButterKnife.bind(this,view);

        progressBar=new ProgressDialog(getContext());
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);


        precreption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File filepath=new File(Environment.getExternalStorageDirectory()+Constants.imagePath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }

                precriptionPath=filepath.getAbsolutePath()+System.currentTimeMillis()+"_Precreption.jpg";
                AppUtils.startPickImageDialog(GALLERY_PRECREPTION, REQUEST_IMAGE_CAPTURE_PRECREPTION, precriptionPath,getActivity());
            }
        });

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File filepath=new File(Environment.getExternalStorageDirectory()+Constants.imagePath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }
                profilePath=filepath.getAbsolutePath()+System.currentTimeMillis()+"_Patient_profile.jpg";
                AppUtils.startPickImageDialog(GALLERY_DONATEIMAGE, REQUEST_IMAGE_CAPTURE_DONATEIMAGE, profilePath,getActivity());
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                postPatient();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( resultCode==RESULT_OK)
            if(requestCode==REQUEST_IMAGE_CAPTURE_DONATEIMAGE)
            {
                File newFile=new File(profilePath);

                setImageFromPath(profileImage,newFile.getPath());
            }
            else if(requestCode==GALLERY_DONATEIMAGE){
                Uri imageUri = data.getData();
                profilePath = getPath(getActivity(), imageUri);
                setImageFromPath(profileImage,profilePath);
            } else if(requestCode==REQUEST_IMAGE_CAPTURE_PRECREPTION)
            {
                File newFile=new File(precriptionPath);

                setImageFromPath(precreption,newFile.getPath());
            }
            else if(requestCode==GALLERY_PRECREPTION){
                Uri imageUri = data.getData();
                precriptionPath = getPath(getActivity(), imageUri);
                setImageFromPath(precreption,precriptionPath);
            }

    }

    private String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "";
            Toast.makeText(getContext(),"Sorry your records is not created",Toast.LENGTH_LONG).show();
        }
        return result;
    }

    private void setImageFromPath(ImageView image, String path){
        if(!TextUtils.isEmpty(path)&&!path.equals("")) {

            File imgFile = new File(path);
            if (imgFile.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize=8;      // 1/8 of original image
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(),options);
                image.setImageBitmap(myBitmap);
                image.setVisibility(View.VISIBLE);

            }else {
                image.setVisibility(View.GONE);
            }
        }else {
            image.setVisibility(View.GONE);
        }
    }

    public void postPatient(){

        if (TextUtils.isEmpty(patientName.getText().toString())) {
            patientName.setError("Enter patient Name ");
            patientName.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(patientAddress.getText().toString())) {
            patientAddress.setError("Enter patient Address ");
            patientAddress.requestFocus();
            return;
        }

        if(!AppUtils.isValidMail(email.getText().toString(),email)){
            email.requestFocus();
        }

        if(AppUtils.isValidMobile(mobile.getText().toString())){

        }else{
            mobile.setError("Enter valid mobile");
            mobile.requestFocus();
            return;
        }

        if(profilePath.equals("")){
            Toast.makeText(getContext(),"Upload Patient Image",Toast.LENGTH_LONG).show();
            return;
        }

        if(precriptionPath.equals("")){
            Toast.makeText(getContext(),"Upload Precription  Image ",Toast.LENGTH_LONG).show();
            return;
        }

        progressBar.show();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String userID=SharedPreferenceHelper.getInstance(getContext()).getString(Constants.USERID);
        builder.addFormDataPart("userid", userID);
        builder.addFormDataPart("patient_name", patientName.getText().toString());
        builder.addFormDataPart("hospital_name", hospitalName.getText().toString());
        builder.addFormDataPart("doctor_name", drName.getText().toString());
        builder.addFormDataPart("patient_address", patientAddress.getText().toString());
        builder.addFormDataPart("mobile", mobile.getText().toString());
        builder.addFormDataPart("email", email.getText().toString());
        builder.addFormDataPart("message", mobile.getText().toString());
        builder.addFormDataPart("blood_group", mobile.getText().toString());
        builder.addFormDataPart("amount", amount.getText().toString());
        builder.addFormDataPart("message", message.getText().toString());

        if(profilePath!=null){
            File profilepath = new File(profilePath);
            builder.addFormDataPart("image", profilepath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), profilepath));
        }

        if(precriptionPath!=null){
            File precreptionpath = new File(precriptionPath);
            builder.addFormDataPart("prescription", precreptionpath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), precreptionpath));
        }

        MultipartBody requestBody = builder.build();
        Call<Response> call= RestClient.get().postPatient(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                Response response1=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(response1.getStatus()){
                        View parentLayout =getActivity().findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();

                        new AlertDialog.Builder(getActivity())
                                .setIcon(R.drawable.thank)
                                .setTitle(response1.getMessage())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //   Log.e("VENDORID", response1.getData().get(0).getId());
                                        //    SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.VENDORID, response1.getData().get(0).getId());
                                       getActivity().onBackPressed();
                                    }

                                })
                                .setNegativeButton("", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .show();

                    }else{
                        View parentLayout =getActivity().findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }

                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{
                    View parentLayout =getActivity().findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout =getActivity().findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });

    }
}
