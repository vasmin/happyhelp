package in.happyhelp.limra.activity.patient;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.property.PostPropertyActivity;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.mydetailresponse.MyPatientDetailResponse;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class MyPatientDetailActivity extends AppCompatActivity {
    private static final int GALLERY_DONATEIMAGE = 123;
    private static final int REQUEST_IMAGE_CAPTURE_DONATEIMAGE = 321;
    private static final int REQUEST_IMAGE_CAPTURE_PRECREPTION = 954;
    private static final int GALLERY_PRECREPTION=541;

    @BindView(R.id.patientname)
    EditText patientName;

    @BindView(R.id.paddress)
    EditText patientAddress;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.hospitalname)
    EditText hospitalName;

    @BindView(R.id.doctorname)
    EditText drName;

    @BindView(R.id.amount)
    EditText amount;

    @BindView(R.id.message)
    EditText message;

    @BindView(R.id.image)
    ImageView profileImage;

    @BindView(R.id.precription)
    ImageView precreption;

    String profilePath="";
    String precriptionPath="";

    ProgressDialog progressBar;

    @BindView(R.id.update)
    TextView update;
    String pId="";
    String pricriptionName="";
    String profileName="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_patient_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            pId = extras.getString(Constants.DID);
            getPatientDetails(pId);
        }


        precreption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File filepath=new File(Environment.getExternalStorageDirectory()+ Constants.imagePath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }

                precriptionPath=filepath.getAbsolutePath()+System.currentTimeMillis()+"_Precreption.jpg";
                AppUtils.startPickImageDialog(GALLERY_PRECREPTION, REQUEST_IMAGE_CAPTURE_PRECREPTION, precriptionPath,MyPatientDetailActivity.this);
            }
        });

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File filepath=new File(Environment.getExternalStorageDirectory()+Constants.imagePath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }
                profilePath=filepath.getAbsolutePath()+System.currentTimeMillis()+"_Patient_profile.jpg";
                AppUtils.startPickImageDialog(GALLERY_DONATEIMAGE, REQUEST_IMAGE_CAPTURE_DONATEIMAGE, profilePath,MyPatientDetailActivity.this);
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updatePatient();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( resultCode==RESULT_OK)
            if(requestCode==REQUEST_IMAGE_CAPTURE_DONATEIMAGE)
            {
                File newFile=new File(profilePath);

                setImageFromPath(profileImage,newFile.getPath());
            }
            else if(requestCode==GALLERY_DONATEIMAGE){
                Uri imageUri = data.getData();
                profilePath = getPath(getApplicationContext(), imageUri);
                setImageFromPath(profileImage,profilePath);
            } else if(requestCode==REQUEST_IMAGE_CAPTURE_PRECREPTION)
            {
                File newFile=new File(precriptionPath);

                setImageFromPath(precreption,newFile.getPath());
            }
            else if(requestCode==GALLERY_PRECREPTION){
                Uri imageUri = data.getData();
                precriptionPath = getPath(getApplicationContext(), imageUri);
                setImageFromPath(precreption,precriptionPath);
            }

    }

    private String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "";
            Toast.makeText(getApplicationContext(),"Sorry your records is not created",Toast.LENGTH_LONG).show();
        }
        return result;
    }

    private void setImageFromPath(ImageView image, String path){
        if(!TextUtils.isEmpty(path)&&!path.equals("")) {

            File imgFile = new File(path);
            if (imgFile.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize=8;      // 1/8 of original image
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(),options);
                image.setImageBitmap(myBitmap);
                image.setVisibility(View.VISIBLE);

            }else {
                image.setVisibility(View.GONE);
            }
        }else {
            image.setVisibility(View.GONE);
        }
    }

    public void getPatientDetails(String pId){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("patientid",pId);
        Call<MyPatientDetailResponse> call=RestClient.get().getMyPatientDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<MyPatientDetailResponse>() {
            @Override
            public void onResponse(@NonNull Call<MyPatientDetailResponse> call, @NonNull retrofit2.Response<MyPatientDetailResponse> response) {
                MyPatientDetailResponse myPatientDetailResponse=response.body();
                if(response.code()==200){
                    patientName.setText(myPatientDetailResponse.getData().getPatientName());
                    patientAddress.setText(myPatientDetailResponse.getData().getPatientAddress());
                    email.setText(myPatientDetailResponse.getData().getEmail());
                    mobile.setText(myPatientDetailResponse.getData().getMobile());
                    hospitalName.setText(myPatientDetailResponse.getData().getHospitalName());
                    drName.setText(myPatientDetailResponse.getData().getDoctorName());
                    amount.setText(myPatientDetailResponse.getData().getAmount());
                    message.setText(myPatientDetailResponse.getData().getMessage());
                    pricriptionName=myPatientDetailResponse.getData().getPrescription();
                    profileName=myPatientDetailResponse.getData().getImage();


                    Glide.with(getApplicationContext())
                            .load(RestClient.base_image_url+myPatientDetailResponse.getData().getImage())
                            .into(profileImage);

                    Glide.with(getApplicationContext())
                            .load(RestClient.base_image_url+myPatientDetailResponse.getData().getPrescription())
                            .into(precreption);

                }else if(response.code()==401){
                    AppUtils.logout(MyPatientDetailActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<MyPatientDetailResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void updatePatient(){

        if (TextUtils.isEmpty(patientName.getText().toString())) {
            patientName.setError("Enter patient Name ");
            patientName.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(patientAddress.getText().toString())) {
            patientAddress.setError("Enter patient Address ");
            patientAddress.requestFocus();
            return;
        }

        if(!AppUtils.isValidMail(email.getText().toString(),email)){
            email.setError("Enter valid Email");
            email.requestFocus();
        }

        if(!AppUtils.isValidMobile(mobile.getText().toString())){
            mobile.setError("Enter valid mobile");
            mobile.requestFocus();
            return;
        }



        progressBar.show();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String userID= SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID);
        builder.addFormDataPart("userid", userID);
        builder.addFormDataPart("patient_name", patientName.getText().toString());
        builder.addFormDataPart("hospital_name", hospitalName.getText().toString());
        builder.addFormDataPart("doctor_name", drName.getText().toString());
        builder.addFormDataPart("patient_address", patientAddress.getText().toString());
        builder.addFormDataPart("mobile", mobile.getText().toString());
        builder.addFormDataPart("email", email.getText().toString());
        builder.addFormDataPart("message", mobile.getText().toString());
        builder.addFormDataPart("blood_group", mobile.getText().toString());
        builder.addFormDataPart("amount", amount.getText().toString());
        builder.addFormDataPart("message", message.getText().toString());
        builder.addFormDataPart("patientid",pId );
        builder.addFormDataPart("image1",profileName );
        builder.addFormDataPart("prescription1",pricriptionName );

        if(profilePath!=null) {
            File profilepath = new File(profilePath);
            if (profilepath.exists()) {
                builder.addFormDataPart("image", profilepath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), profilepath));
            }
        }

        if(precriptionPath!=null) {
            File precreptionpath = new File(precriptionPath);
            if (precreptionpath.exists()) {
                builder.addFormDataPart("prescription", precreptionpath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), precreptionpath));
            }
        }

        MultipartBody requestBody = builder.build();
        Call<Response> call= RestClient.get().updatePatient(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                Response response1=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(response1.getStatus()){
                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();

                        new AlertDialog.Builder(MyPatientDetailActivity.this)
                                .setIcon(R.drawable.thank)
                                .setTitle(response1.getMessage())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //   Log.e("VENDORID", response1.getData().get(0).getId());
                                        //    SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.VENDORID, response1.getData().get(0).getId());
                                        onBackPressed();
                                    }

                                })
                                .setNegativeButton("", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .show();

                    }else{
                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }

                }else if(response.code()==401){
                    AppUtils.logout(MyPatientDetailActivity.this);
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
