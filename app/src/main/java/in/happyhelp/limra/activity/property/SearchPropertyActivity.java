package in.happyhelp.limra.activity.property;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.propertiesresponse.PropertyList;
import in.happyhelp.limra.activity.response.propertiesresponse.PropertyListResponse;
import in.happyhelp.limra.activity.video.MyVideoActivity;
import in.happyhelp.limra.adapter.AllPropertyAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchPropertyActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.data)
    TextView data;

    @BindView(R.id.search_view)
    LinearLayout searchView;

    LinearLayoutManager linearLayoutManager;
    AllPropertyAdapter allPropertyAdapter;
    Realm realm;

    String state="";
    String city="";
    String bedroom="";
    String min="";
    String max="";
    String type="";
    String propertyfor="";
    boolean search=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_property);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);



        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            state = extras.getString(Constants.STATE);
            city = extras.getString(Constants.CITY);
            bedroom = extras.getString(Constants.BEDROOM);
            min = extras.getString(Constants.MIN);
            max = extras.getString(Constants.MAX);
            type = extras.getString(Constants.TYPE);
            propertyfor=extras.getString(Constants.PROPERTYFOR);
            Log.e("Search DAta",type+city+bedroom+min+max+propertyfor);
            searchPostProperty();
            search=true;

        }


        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        realm= RealmHelper.getRealmInstance();

        allPropertyAdapter=new AllPropertyAdapter(getApplicationContext(), new AllPropertyAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(final PropertyList d, View view) throws ParseException {
                Intent intent=new Intent(SearchPropertyActivity.this, PropertyDetailsActivity.class);
                intent.putExtra(Constants.PROPERTYID,d.getId());
                startActivity(intent);
            }
        });







        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SearchPropertyActivity.this, FilterActivity.class);
                startActivity(intent);
            }
        });






        if(!search) {
            getAllProperty();
        }
    }

    private void searchPostProperty() {

        swipeRefreshLayout.setRefreshing(true);

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("city[]",city);
        hashMap.put("property_type",type);
        if(!bedroom.equals("Select Bedroom")) {
            hashMap.put("bedroom[]", bedroom);
        }
        hashMap.put("min_price",min);
        hashMap.put("max_price",max);
        hashMap.put("property_for",propertyfor);

        Call<PropertyListResponse> call=RestClient.get(RestClient.PROPERTY_BASE_URL).searchProperties(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<PropertyListResponse>() {
            @Override
            public void onResponse(@NonNull Call<PropertyListResponse> call, @NonNull Response<PropertyListResponse> response) {
                final PropertyListResponse propertyDetailsResponse=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(propertyDetailsResponse.getStatus()){
                        allPropertyAdapter.setData(propertyDetailsResponse.getPropertyList());
                        recyclerView.setAdapter(allPropertyAdapter);

                        if(propertyDetailsResponse.getPropertyList().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }

                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }

                }else if(response.code()==401){
                    AppUtils.logout(SearchPropertyActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<PropertyListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @Override
    public void onRefresh() {
        if(!search) {
            getAllProperty();
        }else{
            searchPostProperty();
        }
    }

    public  void getAllProperty(){
        swipeRefreshLayout.setRefreshing(true);
        Call<PropertyListResponse> call= RestClient.get(RestClient.PROPERTY_BASE_URL).getAllProperty(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<PropertyListResponse>() {
            @Override
            public void onResponse(@NonNull Call<PropertyListResponse> call, @NonNull Response<PropertyListResponse> response) {
                final PropertyListResponse propertyDetailsResponse=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(propertyDetailsResponse.getStatus()){
                        allPropertyAdapter.setData(propertyDetailsResponse.getPropertyList());
                        recyclerView.setAdapter(allPropertyAdapter);

                        if(propertyDetailsResponse.getPropertyList().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }

                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }

                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }

            }

            @Override
            public void onFailure(@NonNull Call<PropertyListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();

            }
        });
    }




    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }



    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }


    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(SearchPropertyActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(SearchPropertyActivity.this,Id);

            }
        });
    }
}
