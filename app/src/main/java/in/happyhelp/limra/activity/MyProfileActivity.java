package in.happyhelp.limra.activity;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.loginresponse.LoginResponse;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import in.happyhelp.limra.activity.response.statecityresponse.City;
import in.happyhelp.limra.activity.response.statecityresponse.State;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyProfileActivity extends AppCompatActivity {
    private static final int GALLERY_PROFILE =321;
    private int REQUEST_IMAGE_CAPTURE=123;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.pincode)
    EditText pincode;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.paytmno)
    EditText paytmNo;

    @BindView(R.id.update)
    TextView update;

    @BindView(R.id.address)
    EditText address;

    @BindView(R.id.profile_image)
    CircleImageView imageView;

    @BindView(R.id.camera)
    RelativeLayout cameraLayout;
    boolean ismycity=true;

    @BindView(R.id.state)
    Spinner stateSpinner;


    @BindView(R.id.cityspinner)
    Spinner citySpinner;


    String path;
    List<String> listCity=new ArrayList<>();
    List<String> listState=new ArrayList<>();
    ProgressDialog progressBar;
    String stateName,cityName,imageName,statename;
    Realm realm;
    RealmResults<State> statesList;
    ArraySpinnerAdapter stateSpinnerAdapter,citySpinnerAdapter;
    AwesomeValidation awesomeValidation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        ButterKnife.bind(this);

        progressBar=new ProgressDialog(MyProfileActivity.this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);
        AppUtils.isNetworkConnectionAvailable(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

      //  awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
       // awesomeValidation.addValidation(this, R.id.email, Patterns.EMAIL_ADDRESS, R.string.emailerror);

        mobile.setFocusable(false);
        mobile.setEnabled(false);
        mobile.setCursorVisible(false);
        mobile.setKeyListener(null);
        mobile.setBackgroundColor(Color.TRANSPARENT);
        realm=RealmHelper.getRealmInstance();


        stateSpinnerAdapter =new ArraySpinnerAdapter(MyProfileActivity.this);
        citySpinnerAdapter =new ArraySpinnerAdapter(MyProfileActivity.this);

        statesList=realm.where(State.class).findAllAsync();
        statesList.addChangeListener(new RealmChangeListener<RealmResults<State>>() {
            @Override
            public void onChange(RealmResults<State> states) {
                listState.clear();
                for(int i=0;i<statesList.size();i++){
                    listState.add(statesList.get(i).getState());
                }
                stateSpinnerAdapter.setData(listState);
                stateSpinner.setAdapter(stateSpinnerAdapter);
            }
        });

        RealmResults<UserData> userData=realm.where(UserData.class).findAllAsync();
        userData.addChangeListener(new RealmChangeListener<RealmResults<UserData>>() {
            @Override
            public void onChange(@NonNull RealmResults<UserData> userData) {
                if(userData.size()>0) {
                    name.setText(userData.get(0).getName());

                    address.setText(userData.get(0).getAddress());
                    email.setText(userData.get(0).getEmail());
                    mobile.setText(userData.get(0).getMobile());
                    pincode.setText(userData.get(0).getPincode());
                    imageName=userData.get(0).getImage();
                    statename=userData.get(0).getState();
                    cityName=userData.get(0).getCity();

                    paytmNo.setText(userData.get(0).getPaytm());

                    if(statename!=null){
                        int pos=0;
                        for(int i=0;i<listState.size();i++){
                            if(statename!=null)
                                if(statename.equals(listState.get(i))){
                                    pos=i;
                                }
                        }

                        Log.e("statename","is "+statename+pos);
                        stateSpinnerAdapter.setData(listState);
                        stateSpinner.setAdapter(stateSpinnerAdapter);
                        stateSpinner.setSelection(pos);
                    }

                    if(cityName!=null){
                        final RealmResults<City> cities=realm.where(City.class).equalTo("state",statename).findAllAsync();
                        cities.addChangeListener(new RealmChangeListener<RealmResults<City>>() {
                            @Override
                            public void onChange(RealmResults<City> cities) {
                                Log.e("CitySize", String.valueOf(cities.size()));
                                listCity.clear();

                                if (cities.size() > 0) {
                                    for (int i = 0; i < cities.size(); i++) {
                                        if(!listCity.contains(cities.get(i).getCity())) {
                                            listCity.add(cities.get(i).getCity());
                                        }
                                    }
                                }

                                int pos=0;
                                for(int j=0;j<listCity.size();j++){
                                    if(cityName!=null)
                                        if(cityName.equals(listCity.get(j))){
                                            pos=j;
                                        }
                                }

                                citySpinnerAdapter.setData(listCity);
                                citySpinner.setAdapter(citySpinnerAdapter);
                                citySpinner.setSelection(pos);
                                ismycity=true;

                            }
                        });
                    }




                    Glide.with(getApplicationContext())
                            .load(RestClient.base_image_url+userData.get(0).getImage())
                            .thumbnail(0.5f)
                            .into(imageView);
                }


            }
        });

        getUserProfile();

        cameraLayout.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {

                File filepath=new File(Environment.getExternalStorageDirectory()+Constants.imagePath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }

                path=filepath.getAbsolutePath()+"/"+System.currentTimeMillis()+"profile_image.jpg";
                AppUtils.startPickImageDialog(GALLERY_PROFILE, REQUEST_IMAGE_CAPTURE, path,MyProfileActivity.this);
            }
        });



        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stateName = String.valueOf(listState.get(i));

                if (!stateName.equals(statename)) {
                    ismycity = false;
                } else {
                    ismycity = true;
                }

                if (!ismycity) {
                    final RealmResults<City> cityRealmResults = realm.where(City.class).equalTo("state", stateName).findAllAsync();
                    cityRealmResults.addChangeListener(new RealmChangeListener<RealmResults<City>>() {
                        @Override
                        public void onChange(RealmResults<City> cities) {

                            listCity.clear();
                            if (cityRealmResults.size() > 0) {
                                for (int i = 0; i < cityRealmResults.size(); i++) {
                                    if (!listCity.contains(cities.get(i).getCity())) {
                                        listCity.add(cities.get(i).getCity());
                                    }
                                }
                                citySpinnerAdapter.setData(listCity);
                                citySpinner.setAdapter(citySpinnerAdapter);
                            }
                        }


                    });

                }else{
                    final RealmResults<City> cities=realm.where(City.class).equalTo("state",statename).findAllAsync();
                    cities.addChangeListener(new RealmChangeListener<RealmResults<City>>() {
                        @Override
                        public void onChange(RealmResults<City> cities) {
                            Log.e("CitySize", String.valueOf(cities.size()));
                            listCity.clear();
                            if (cities.size() > 0) {
                                for (int i = 0; i < cities.size(); i++) {
                                    if(!listCity.contains(cities.get(i).getCity())) {
                                        listCity.add(cities.get(i).getCity());
                                    }
                                }
                            }
                            int pos=0;
                            for(int j=0;j<listCity.size();j++){
                                if(cityName!=null)
                                    if(cityName.equals(listCity.get(j))){
                                        pos=j;
                                    }
                            }
                            citySpinnerAdapter.setData(listCity);
                            citySpinner.setAdapter(citySpinnerAdapter);
                            citySpinner.setSelection(pos);
                            ismycity=true;
                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(listCity.size()>i) {
                    cityName = String.valueOf(listCity.get(i));

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(TextUtils.isEmpty(name.getText().toString())){
                    name.setError("Enter Name ");
                    return;
                }

                if(!AppUtils.isValidMail(email.getText().toString(),email)){
                    email.setError("Enter valid email ");
                    email.requestFocus();
                    return;
                }

                if(TextUtils.isEmpty(address.getText().toString())){
                    address.setError("Enter Address ");
                    address.requestFocus();
                    return;
                }

                if(TextUtils.isEmpty(pincode.getText().toString())){
                    pincode.setError("Enter PinCode ");
                    pincode.requestFocus();
                    return;
                }

                if(pincode.getText().length()!=6){
                    pincode.setError("Enter valud PinCode ");
                    pincode.requestFocus();
                    return;
                }


                if(TextUtils.isEmpty(paytmNo.getText().toString())){
                    paytmNo.setError("Enter Number ");
                    paytmNo.requestFocus();
                    return;
                }

                if(stateName.equals("Select State")){
                    Toast.makeText(getApplicationContext(),"Select State",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(cityName.equals("Select City")){
                    Toast.makeText(getApplicationContext(),"Select City",Toast.LENGTH_SHORT).show();
                    return;
                }


                    updateProfile();


            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( resultCode==RESULT_OK)
            if(requestCode==REQUEST_IMAGE_CAPTURE)
            {
                File newFile=new File(path);
                setImageFromPath(imageView,newFile.getPath());
            }else if(requestCode==GALLERY_PROFILE){
                Uri imageUri = data.getData();
                path = getPath(getApplicationContext(), imageUri);
                setImageFromPath(imageView,path);
            }
    }

    private String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "";
            Toast.makeText(getApplicationContext(),"Sorry your records is not created",Toast.LENGTH_LONG).show();
        }
        return result;
    }

    private void setImageFromPath(ImageView image, String path){
        if(!TextUtils.isEmpty(path)&&!path.equals("")) {
            File imgFile = new File(path);
            if (imgFile.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize=8;      // 1/8 of original image
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(),options);
                image.setImageBitmap(myBitmap);
                image.setVisibility(View.VISIBLE);
            }else {
                image.setVisibility(View.GONE);
            }
        }else {
            image.setVisibility(View.GONE);
        }
    }

    public void updateProfile(){

        progressBar.show();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String userid=SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID);
        builder.addFormDataPart("userid", userid);
        builder.addFormDataPart("name", name.getText().toString());
        builder.addFormDataPart("email", email.getText().toString());
        builder.addFormDataPart("city", cityName);
        builder.addFormDataPart("pincode", pincode.getText().toString());
        builder.addFormDataPart("address", address.getText().toString());
        builder.addFormDataPart("state",stateName);
        builder.addFormDataPart("city",cityName);
        builder.addFormDataPart("profileold",imageName);
        builder.addFormDataPart("paytm",paytmNo.getText().toString());

        if(path!=null) {
            File profilepath = new File(path);
            builder.addFormDataPart("profile", profilepath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), profilepath));
        }

        MultipartBody requestBody = builder.build();

        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().profileUpdates(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
                progressBar.dismiss();
                if(response.code()==200) {
                    in.happyhelp.limra.activity.response.Response response1=response.body();
                    if (response1.getStatus()) {
                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();

                        onBackPressed();
                    } else {
                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();

                    }
                }else if(response.code()==401){
                    AppUtils.logout(MyProfileActivity.this);
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();

                   // Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }


    public void getUserProfile(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<LoginResponse> call=RestClient.get().getUserDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                final LoginResponse loginResponse=response.body();
                if(response.code()==200) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(@NonNull Realm realm) {
                            realm.delete(UserData.class);
                            realm.copyToRealmOrUpdate(loginResponse.getData());
                        }
                    });

                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MyProfileActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MyProfileActivity.this,Id);

            }
        });



    }
}
