
package in.happyhelp.limra.activity.response.savedjobresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SavedJob {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("jobid")
    @Expose
    private String jobid;
    @SerializedName("job_title")
    @Expose
    private String jobTitle;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("experience")
    @Expose
    private String experience;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getJobid() {
        return jobid;
    }

    public void setJobid(String jobid) {
        this.jobid = jobid;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

}
