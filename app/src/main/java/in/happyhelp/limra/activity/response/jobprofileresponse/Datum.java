
package in.happyhelp.limra.activity.response.jobprofileresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("i_am")
    @Expose
    private String iAm;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("web_url")
    @Expose
    private String webUrl;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("plan")
    @Expose
    private String plan;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("planamount")
    @Expose
    private String planamount;
    @SerializedName("planstart")
    @Expose
    private String planstart;
    @SerializedName("planexpire")
    @Expose
    private String planexpire;
    @SerializedName("status")
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getIAm() {
        return iAm;
    }

    public void setIAm(String iAm) {
        this.iAm = iAm;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPlanamount() {
        return planamount;
    }

    public void setPlanamount(String planamount) {
        this.planamount = planamount;
    }

    public String getPlanstart() {
        return planstart;
    }

    public void setPlanstart(String planstart) {
        this.planstart = planstart;
    }

    public String getPlanexpire() {
        return planexpire;
    }

    public void setPlanexpire(String planexpire) {
        this.planexpire = planexpire;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
