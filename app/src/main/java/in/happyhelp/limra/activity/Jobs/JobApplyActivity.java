package in.happyhelp.limra.activity.Jobs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.RegisterActivity;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.Response;
import retrofit2.Call;
import retrofit2.Callback;

public class JobApplyActivity extends AppCompatActivity {
    String jobId="";
    @BindView(R.id.apply)
    TextView apply;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.location)
    EditText location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_apply);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            jobId = extras.getString(Constants.JOBID);
        }

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(name.getText().toString())) {
                    name.setError("Enter Name ");
                    return;
                }

                if (TextUtils.isEmpty(mobile.getText().toString())) {
                    mobile.setError("Enter mobile ");
                    return;
                }

                if(!AppUtils.isValidMobile(mobile.getText().toString())){
                    mobile.setError("Enter Valid mobile");
                    return;
                }

                if (TextUtils.isEmpty(email.getText().toString())) {
                    email.setError("Enter email ");
                    return;
                }

                if(!AppUtils.isValidMail(email.getText().toString(),email)){
                    email.setError("Enter Valid Email");
                    return;
                }

                if (TextUtils.isEmpty(location.getText().toString())) {
                    location.setError("Enter location ");
                    return;
                }

                jobApply();
            }
        });

    }


    public void jobApply(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("jobid",jobId);
     //   hashMap.put("location",location.getText().toString());
     //   hashMap.put("current_salary",)
        Call<Response> call= RestClient.get().jobapply(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<in.happyhelp.limra.activity.response.Response> call, retrofit2.Response<Response> response) {
            if(response.code()==401){
                    AppUtils.logout(JobApplyActivity.this);
                }
            }

            @Override
            public void onFailure(Call<in.happyhelp.limra.activity.response.Response> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(JobApplyActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(JobApplyActivity.this,Id);

            }
        });
    }

}
