package in.happyhelp.limra.activity.property;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.Jobs.JobProfileActivity;
import in.happyhelp.limra.activity.MyProfileActivity;
import in.happyhelp.limra.activity.response.bannerresponse.BannerResponse;
import in.happyhelp.limra.activity.response.jobplanresponse.JobPlan;
import in.happyhelp.limra.activity.response.serrviceresponse.ServiceBannerResponse;
import in.happyhelp.limra.activity.service.ServicePopActivity;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.propertyprofileresponse.Datum;
import in.happyhelp.limra.shoppingresponse.propertyprofileresponse.PropertyProfileResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PropertyHomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.searchproperty)
    CardView searchProperty;

    @BindView(R.id.postproperty)
    CardView postProperty;

    @BindView(R.id.slider)
    SliderLayout slider;

    List<String> listBanner=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        searchProperty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PropertyHomeActivity.this, FilterActivity.class);
                startActivity(intent);
            }
        });

        postProperty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(SharedPreferenceHelper.getInstance(getApplicationContext()).isPropertyCompanyProfile()) {
                    Intent intent = new Intent(PropertyHomeActivity.this, PostPropertyActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(PropertyHomeActivity.this, PropertyProfileActivity.class);
                    startActivity(intent);
                }
            }
        });



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        slider.movePrevPosition(false);
        slider.setPresetTransformer(com.daimajia.slider.library.SliderLayout.Transformer.Accordion);
        slider.setPresetIndicator(com.daimajia.slider.library.SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(3000);
        slider.startAutoCycle();


        getPropertyBanner();
        getPropertyProfile();
    }

    public void getPropertyProfile(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<PropertyProfileResponse> call=RestClient.get(RestClient.PROPERTY_BASE_URL).getPropertyProfile(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<PropertyProfileResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<PropertyProfileResponse> call, @NonNull retrofit2.Response<PropertyProfileResponse> response) {

                PropertyProfileResponse propertyProfileResponse=response.body();
                if(response.code()==200){
                    if(propertyProfileResponse.getStatus()){
                        if(propertyProfileResponse.getData().size()>0) {
                            Datum profileProperty = propertyProfileResponse.getData().get(0);
                            if (profileProperty.isPlanexpired()) {
                                SharedPreferenceHelper.getInstance(getApplicationContext()).setPropertyCompanyProfile(false);

                            } else {
                                SharedPreferenceHelper.getInstance(getApplicationContext()).setPropertyCompanyProfile(true);
                            }
                        }else{

                        }
                    }else{
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setPropertyCompanyProfile(false);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(PropertyHomeActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<PropertyProfileResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void getPropertyBanner(){
        Call<ServiceBannerResponse> call= RestClient.get(RestClient.PROPERTY_BASE_URL).getPropertyBanner(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<ServiceBannerResponse>() {
            @Override
            public void onResponse(@NonNull Call<ServiceBannerResponse> call, @NonNull Response<ServiceBannerResponse> response) {
                final ServiceBannerResponse serviceBannerResponse=response.body();

                if(response.code()==200){
                    if(serviceBannerResponse.getStatus()) {

                        listBanner.clear();
                        for(int i=0;i<serviceBannerResponse.getData().size();i++){
                            listBanner.add(serviceBannerResponse.getData().get(i).getImage());
                        }


                        for (int i = 0; i < listBanner.size(); i++) {
                            DefaultSliderView defaultSliderView = new DefaultSliderView(getApplicationContext());
                            final int finalI = i;
                            defaultSliderView.image(RestClient.base_image_url + listBanner.get(i))
                                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                        @Override
                                        public void onSliderClick(BaseSliderView slider) {
                                            String BannnerName = listBanner.get(finalI);
                            /*JobCategory banners=realm.where(JobCategory.class).equalTo("image",BannnerName).findFirst();
                            String url = banners.getLink();

                            if(!url.equals("")) {
                                Intent i = new Intent(Intent.ACTION_VIEW,
                                        Uri.parse(url));
                                startActivity(i);
                            }*/
                                        }
                                    });
                            slider.addSlider(defaultSliderView);
                        }
                    }


                }else if(response.code()==401){
                    AppUtils.logout(PropertyHomeActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<ServiceBannerResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.property_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.myproperty) {
            Intent intent=new Intent(PropertyHomeActivity.this, MyPropertyActivity.class);
            startActivity(intent);
        }else if (id == R.id.myprofile) {
            Intent intent=new Intent(PropertyHomeActivity.this,PropertyProfileActivity.class);
            startActivity(intent);
        }else if (id == R.id.enquiry) {
            Intent intent=new Intent(PropertyHomeActivity.this,MyPropertyEnquiryActivity.class);
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(PropertyHomeActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(PropertyHomeActivity.this,Id);

            }
        });
    }
}
