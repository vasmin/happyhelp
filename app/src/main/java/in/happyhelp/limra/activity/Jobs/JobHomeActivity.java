package in.happyhelp.limra.activity.Jobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.KycActivity;
import in.happyhelp.limra.activity.MyCompanyActivity;
import in.happyhelp.limra.activity.response.companyprofile.Company;
import in.happyhelp.limra.activity.response.companyprofile.CompanyDetailsResponse;
import in.happyhelp.limra.activity.response.jobcategory.JobCategory;
import in.happyhelp.limra.activity.response.jobcategory.JobCategoryResponse;
import in.happyhelp.limra.adapter.JobTypeAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobHomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.postjob)
    CardView postJob;

    JobTypeAdapter adapter;

    @BindView(R.id.data)
    TextView data;


    @BindView(R.id.search_view)
    SearchView searchView;

    LinearLayoutManager linearLayoutManager;
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);

        realm= RealmHelper.getRealmInstance();

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        adapter=new JobTypeAdapter(getApplicationContext(), new JobTypeAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(JobCategory d, View view) throws ParseException {
                Intent intent=new Intent(JobHomeActivity.this,JobListActivity.class);
                intent.putExtra(Constants.JOBID,d.getId());
                startActivity(intent);
            }
        });


        getJobCategory();
        getJobCompanyProfile();

        postJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(SharedPreferenceHelper.getInstance(JobHomeActivity.this).isCompanyProfile()) {
                    Intent intent = new Intent(JobHomeActivity.this, PostJobsActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(JobHomeActivity.this, MyJobCompanyActivity.class);
                    startActivity(intent);
                }
            }
        });

        searchView.setQueryHint("Search Job");

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if(query!=null && !query.equals("")) {
                    RealmResults<JobCategory> realmResults = realm.where(JobCategory.class).contains("name", query, Case.INSENSITIVE).findAllAsync();
                    realmResults.addChangeListener(new RealmChangeListener<RealmResults<JobCategory>>() {
                        @Override
                        public void onChange(RealmResults<JobCategory> jobCategories) {
                            adapter.setData(jobCategories);
                            recyclerView.setAdapter(adapter);

                            if(jobCategories.size()==0){
                                data.setVisibility(View.VISIBLE);
                            }else{
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }else{
                    RealmResults<JobCategory> realmResults=realm.where(JobCategory.class).findAllAsync();
                    realmResults.addChangeListener(new RealmChangeListener<RealmResults<JobCategory>>() {
                        @Override
                        public void onChange(RealmResults<JobCategory> jobCategories) {
                            adapter.setData(jobCategories);
                            recyclerView.setAdapter(adapter);
                            if(jobCategories.size()==0)
                            {
                                data.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }


                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if(newText!=null && !newText.equals("")) {

                    RealmResults<JobCategory> realmResults = realm.where(JobCategory.class).contains("name", newText, Case.INSENSITIVE).findAllAsync();
                    realmResults.addChangeListener(new RealmChangeListener<RealmResults<JobCategory>>() {
                        @Override
                        public void onChange(RealmResults<JobCategory> jobCategories) {
                            adapter.setData(jobCategories);
                            recyclerView.setAdapter(adapter);

                            if(jobCategories.size()==0)
                            {
                                data.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }else{
                    RealmResults<JobCategory> realmResults=realm.where(JobCategory.class).findAllAsync();
                    realmResults.addChangeListener(new RealmChangeListener<RealmResults<JobCategory>>() {
                        @Override
                        public void onChange(RealmResults<JobCategory> jobCategories) {
                            adapter.setData(jobCategories);
                            recyclerView.setAdapter(adapter);

                            if(jobCategories.size()==0)
                            {
                                data.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                data.setVisibility(View.GONE);
                            }

                        }
                    });
                }

                return false;
            }
        });




        RealmResults<JobCategory> realmResults=realm.where(JobCategory.class).findAllAsync();
        realmResults.addChangeListener(new RealmChangeListener<RealmResults<JobCategory>>() {
            @Override
            public void onChange(RealmResults<JobCategory> jobCategories) {
                adapter.setData(jobCategories);
                recyclerView.setAdapter(adapter);
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void getJobCompanyProfile(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<CompanyDetailsResponse> call=RestClient.get().jobscompany_details(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<CompanyDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<CompanyDetailsResponse> call, @NonNull retrofit2.Response<CompanyDetailsResponse> response) {
                final CompanyDetailsResponse companyDetailsResponse=response.body();
                if(response.code()==200){
                    if(companyDetailsResponse.getStatus()){
                        if(companyDetailsResponse.getData()!=null) {

                            if(companyDetailsResponse.getData().size()>0) {
                                if(!companyDetailsResponse.getData().get(0).isPlanexpired()) {
                                    SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(true);
                                }else{
                                    SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(false);
                                }
                            }
                        } else
                        {
                            SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(false);
                        }
                    }else{
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(false);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(JobHomeActivity.this);
                }else{
                    SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CompanyDetailsResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(false);
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getJobCategory(){
        swipeRefreshLayout.setRefreshing(true);
        Call<JobCategoryResponse> call= RestClient.get().getJobCategory(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<JobCategoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<JobCategoryResponse> call, @NonNull Response<JobCategoryResponse> response) {
                final JobCategoryResponse jobCategoryResponse=response.body();

                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200) {
                    if (jobCategoryResponse.getStatus()) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(JobCategory.class);
                                realm.copyToRealmOrUpdate(jobCategoryResponse.getData());
                            }
                        });
                    } else {
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(JobHomeActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<JobCategoryResponse> call, Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onRefresh() {
        getJobCategory();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.job_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.myjob) {
            Intent intent=new Intent(JobHomeActivity.this,MyJobListActivity.class);
            startActivity(intent);

        } else if (id == R.id.myprofile) {
            Intent intent=new Intent(JobHomeActivity.this,JobProfileActivity.class);
            startActivity(intent);

        } else if (id == R.id.mycompany) {
            Intent intent=new Intent(JobHomeActivity.this,MyJobCompanyActivity.class);
            startActivity(intent);
        }else if (id == R.id.companyhr) {
            Intent intent=new Intent(JobHomeActivity.this,CompanyHRActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(JobHomeActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(JobHomeActivity.this,Id);

            }
        });
    }
}
