
package in.happyhelp.limra.activity.response.loginresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class UserData extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mobile")
    @Expose
    private String mobile;

    @SerializedName("locality")
    @Expose
    private String locality;
    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("state")
    @Expose
    private String state;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("pincode")
    @Expose
    private String pincode;

    @SerializedName("serviceid")
    @Expose
    private String serviceid;


    @SerializedName("use_ref_code")
    @Expose
    private String use_ref_code;

    @SerializedName("wallet")
    @Expose
    private double wallet;

    @SerializedName("pan_no")
    @Expose
    private String pan_no;

    @SerializedName("aadhar_front")
    @Expose
    private String aadhar_front;

    @SerializedName("aadhar_back")
    @Expose
    private String aadhar_back;
    @SerializedName("bank_name")
    @Expose
    private String bank_name;
    @SerializedName("bank_branch_name")
    @Expose
    private String bank_branch_name;
    @SerializedName("bank_acc_no")
    @Expose
    private String bank_acc_no;
    @SerializedName("ifsc_code")
    @Expose
    private String ifsc_code;
    @SerializedName("gst_no")
    @Expose
    private String gst_no;
    @SerializedName("kycupload")
    @Expose
    private String kycupload;

    @SerializedName("cancel_check")
    @Expose
    private String cancel_check;

    @SerializedName("wallet_limit")
    @Expose
    private String wallet_limit;

    @SerializedName("paytm")
    @Expose
    private String paytm;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address1) {
        this.address = address1;
    }



    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getUse_ref_code() {
        return use_ref_code;
    }

    public void setUse_ref_code(String use_ref_code) {
        this.use_ref_code = use_ref_code;
    }

    public double getWallet() {
        return wallet;
    }

    public void setWallet(double wallet) {
        this.wallet = wallet;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getServiceid() {
        return serviceid;
    }

    public void setServiceid(String serviceid) {
        this.serviceid = serviceid;
    }

    public String getPan_no() {
        return pan_no;
    }

    public void setPan_no(String pan_no) {
        this.pan_no = pan_no;
    }

    public String getAadhar_front() {
        return aadhar_front;
    }

    public void setAadhar_front(String aadhar_front) {
        this.aadhar_front = aadhar_front;
    }

    public String getAadhar_back() {
        return aadhar_back;
    }

    public void setAadhar_back(String aadhar_back) {
        this.aadhar_back = aadhar_back;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBank_branch_name() {
        return bank_branch_name;
    }

    public void setBank_branch_name(String bank_branch_name) {
        this.bank_branch_name = bank_branch_name;
    }

    public String getBank_acc_no() {
        return bank_acc_no;
    }

    public void setBank_acc_no(String bank_acc_no) {
        this.bank_acc_no = bank_acc_no;
    }

    public String getIfsc_code() {
        return ifsc_code;
    }

    public void setIfsc_code(String ifsc_code) {
        this.ifsc_code = ifsc_code;
    }

    public String getGst_no() {
        return gst_no;
    }

    public void setGst_no(String gst_no) {
        this.gst_no = gst_no;
    }

    public String getKycupload() {
        return kycupload;
    }

    public void setKycupload(String kycupload) {
        this.kycupload = kycupload;
    }

    public String getCancel_check() {
        return cancel_check;
    }

    public void setCancel_check(String cancel_check) {
        this.cancel_check = cancel_check;
    }

    public String getPaytm() {
        return paytm;
    }

    public void setPaytm(String paytm) {
        this.paytm = paytm;
    }

    public String getWallet_limit() {
        return wallet_limit;
    }

    public void setWallet_limit(String wallet_limit) {
        this.wallet_limit = wallet_limit;
    }
}
