
package in.happyhelp.limra.activity.response.propertydetailsresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PropertyDetails {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("property_name")
    @Expose
    private String propertyName;
    @SerializedName("posted_date")
    @Expose
    private String postedDate;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("profile")
    @Expose
    private String profile;
    @SerializedName("pro_type")
    @Expose
    private String proType;
    @SerializedName("deal_type")
    @Expose
    private String dealType;
    @SerializedName("bedroom")
    @Expose
    private String bedroom;
    @SerializedName("carp_area")
    @Expose
    private String carpArea;
    @SerializedName("built_area")
    @Expose
    private String builtArea;
    @SerializedName("floor_no")
    @Expose
    private String floorNo;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("min_exp_price")
    @Expose
    private String minExpPrice;
    @SerializedName("offer_price")
    @Expose
    private String offerPrice;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("prop_id")
    @Expose
    private String propId;
    @SerializedName("no_parking")
    @Expose
    private String noParking;


    @SerializedName("state")
    @Expose
    private String state;


    @SerializedName("other_amenities")
    @Expose
    private String otherAmenities;
    @SerializedName("no_of_bldngs")
    @Expose
    private String noOfBldngs;
    @SerializedName("buildr_name")
    @Expose
    private String buildrName;
    @SerializedName("owner")
    @Expose
    private String owner;
    @SerializedName("images")
    @Expose
    private List<String> images = null;
    @SerializedName("no_of_months")
    @Expose
    private String noOfMonths;
    @SerializedName("no_of_years")
    @Expose
    private String noOfYears;

    @SerializedName("website_url")
    @Expose
    private String website_url;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getProType() {
        return proType;
    }

    public void setProType(String proType) {
        this.proType = proType;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getBedroom() {
        return bedroom;
    }

    public void setBedroom(String bedroom) {
        this.bedroom = bedroom;
    }

    public String getCarpArea() {
        return carpArea;
    }

    public void setCarpArea(String carpArea) {
        this.carpArea = carpArea;
    }

    public String getBuiltArea() {
        return builtArea;
    }

    public void setBuiltArea(String builtArea) {
        this.builtArea = builtArea;
    }

    public String getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(String floorNo) {
        this.floorNo = floorNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getMinExpPrice() {
        return minExpPrice;
    }

    public void setMinExpPrice(String minExpPrice) {
        this.minExpPrice = minExpPrice;
    }

    public String getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(String offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPropId() {
        return propId;
    }

    public void setPropId(String propId) {
        this.propId = propId;
    }

    public String getNoParking() {
        return noParking;
    }

    public void setNoParking(String noParking) {
        this.noParking = noParking;
    }

    public String getOtherAmenities() {
        return otherAmenities;
    }

    public void setOtherAmenities(String otherAmenities) {
        this.otherAmenities = otherAmenities;
    }

    public String getNoOfBldngs() {
        return noOfBldngs;
    }

    public void setNoOfBldngs(String noOfBldngs) {
        this.noOfBldngs = noOfBldngs;
    }

    public String getBuildrName() {
        return buildrName;
    }

    public void setBuildrName(String buildrName) {
        this.buildrName = buildrName;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getNoOfMonths() {
        return noOfMonths;
    }

    public void setNoOfMonths(String noOfMonths) {
        this.noOfMonths = noOfMonths;
    }

    public String getNoOfYears() {
        return noOfYears;
    }

    public void setNoOfYears(String noOfYears) {
        this.noOfYears = noOfYears;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getWebsite_url() {
        return website_url;
    }

    public void setWebsite_url(String website_url) {
        this.website_url = website_url;
    }
}
