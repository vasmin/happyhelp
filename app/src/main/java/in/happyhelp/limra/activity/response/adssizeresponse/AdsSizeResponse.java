
package in.happyhelp.limra.activity.response.adssizeresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdsSizeResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("gst")
    @Expose
    private String gst;

    @SerializedName("minenddate")
    @Expose
    private String minenddate;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getMinenddate() {
        return minenddate;
    }

    public void setMinenddate(String minenddate) {
        this.minenddate = minenddate;
    }
}
