package in.happyhelp.limra.activity.Jobs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.KycActivity;
import in.happyhelp.limra.activity.response.joblistresponse.JobList;
import in.happyhelp.limra.activity.response.joblistresponse.JobListResponse;
import in.happyhelp.limra.adapter.MyJobAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyJobListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.data)
    TextView data;
    LinearLayoutManager linearLayoutManager;

    MyJobAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_job_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        adapter=new MyJobAdapter(this, new MyJobAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(final JobList d, View view) throws ParseException {

                TextView viewStatus=view.findViewById(R.id.view);
                viewStatus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                });
              /*  Intent intent=new Intent(getActivity(), PostJobsActivity.class);
                intent.putExtra(Constants.JOBID,d.getId());
                intent.putExtra(Constants.JOB,true);
                startActivity(intent);
                getActivity().finish();*/
            }
        });
        getMyJobList();
    }

    public void getMyJobList()
    {
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<JobListResponse> call= RestClient.get().getMyJobList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<JobListResponse>() {
            @Override
            public void onResponse(@NonNull Call<JobListResponse> call, @NonNull Response<JobListResponse> response) {
                JobListResponse jobListResponse=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){

                    if(jobListResponse.getStatus()){
                        adapter.setData(jobListResponse.getData());
                        recyclerView.setAdapter(adapter);
                        if(jobListResponse.getData().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }
                    }else{
                        if(jobListResponse.getData().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }
                    }



                }else if(response.code()==401){
                    AppUtils.logout(MyJobListActivity.this);
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JobListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @Override
    public void onRefresh() {
        getMyJobList();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MyJobListActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MyJobListActivity.this,Id);

            }
        });
    }
}
