package in.happyhelp.limra.activity.property;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.propertydetailsresponse.PropertyDetailResponse;
import in.happyhelp.limra.activity.response.propertydetailsresponse.PropertyDetails;
import in.happyhelp.limra.activity.response.statecityresponse.City;
import in.happyhelp.limra.activity.response.statecityresponse.State;
import in.happyhelp.limra.activity.video.MyVideoActivity;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.adapter.PropertyImagesAdapter;
import in.happyhelp.limra.adapter.WorkImagesAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.model.PropertyImageModel;
import in.happyhelp.limra.model.WorkModel;
import in.happyhelp.limra.network.RestClient;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.support.v4.content.FileProvider.getUriForFile;

public class PostPropertyActivity extends AppCompatActivity {
    private static final int REQUEST_PHONE_CALL = 123;


    @BindView(R.id.uploadimage)
    TextView uploadImage;


    @BindView(R.id.state)
    Spinner stateSpinner;


    @BindView(R.id.bedroom)
    Spinner bedRoomSpinner;


    String stateName,cityName;
    String bedRoom="";

    @BindView(R.id.cityspinner)
    Spinner citySpinner;


    RealmResults<State> statesList;
    ArraySpinnerAdapter stateSpinnerAdapter,citySpinnerAdapter;

    List<String> listCity=new ArrayList<>();
    List<String> listState=new ArrayList<>();



    WorkImagesAdapter workImagesAdapter;
    PropertyImagesAdapter adapter;
    List<WorkModel> lists = new ArrayList<>();
    ArrayList<String> propertytype=new ArrayList<>();
    ArrayList<String> propertyfor=new ArrayList<>();


    ArrayList<String> yearList=new ArrayList<>();
    ArrayList<String> monthList=new ArrayList<>();



    @BindView(R.id.address)
    EditText address;


    @BindView(R.id.floor)
    EditText floor;

    @BindView(R.id.amenities)
    EditText comment;

    @BindView(R.id.pincode)
    EditText pincode;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.propertyname)
    EditText propertyName;

    @BindView(R.id.ownername)
    EditText ownerName;

    @BindView(R.id.contactdetails)
    EditText mobile;

    @BindView(R.id.buildingarea)
    EditText buildingArea;

    @BindView(R.id.carpetarea)
    EditText carpetArea;

    @BindView(R.id.buildingname)
    EditText buildName;

    @BindView(R.id.weburl)
    EditText weburl;

    @BindView(R.id.wing)
    EditText wing;

    @BindView(R.id.flatnumber)
    EditText flatNo;


    @BindView(R.id.price)
    EditText offerPrice;

    @BindView(R.id.expectedprice)
    EditText expectedPrice;

    @BindView(R.id.years)
    Spinner yearsSpinner;

    @BindView(R.id.month)
    Spinner monthSpinner;

    @BindView(R.id.propertytype)
    Spinner propertyType;

    @BindView(R.id.propertyfor)
    Spinner propertyFor;

    @BindView(R.id.submit)
    TextView submit;

    @BindView(R.id.city)
    Spinner city;

    @BindView(R.id.parking)
    EditText parking;

    boolean isUpdate=false;




    List<String> bedList=new ArrayList<>();



    String PropertyType,PropertyFor;
    String month="";
    String year="";



    ProgressDialog progressBar;

    String propertyId="";

    private static final int GALLERY_WORK = 456;
    private static final int REQUEST_IMAGE_CAPTURE_WORK = 789;
    Realm realm;
    ArraySpinnerAdapter propertyTypeAdapter,propertyForAdapter,yearAdapter,monthAdapter,bedAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_property);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);


        realm= RealmHelper.getRealmInstance();

        submit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                ValidatioCheck();
            }
        });

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        stateSpinnerAdapter =new ArraySpinnerAdapter(this);
        citySpinnerAdapter =new ArraySpinnerAdapter(this);

        statesList=realm.where(State.class).findAllAsync();
        statesList.addChangeListener(new RealmChangeListener<RealmResults<State>>() {
            @Override
            public void onChange(RealmResults<State> states) {
                listState.clear();
                for(int i=0;i<statesList.size();i++){
                    listState.add(statesList.get(i).getState());
                }
                stateSpinnerAdapter.setData(listState);
                stateSpinner.setAdapter(stateSpinnerAdapter);
            }
        });

        wing.setSingleLine();
        wing.setImeOptions(EditorInfo.IME_ACTION_NEXT);


        buildName.setSingleLine();
        buildName.setImeOptions(EditorInfo.IME_ACTION_NEXT);


        carpetArea.setSingleLine();
        carpetArea.setImeOptions(EditorInfo.IME_ACTION_NEXT);

        buildingArea.setSingleLine();
        buildingArea.setImeOptions(EditorInfo.IME_ACTION_NEXT);

        flatNo.setSingleLine();
        flatNo.setImeOptions(EditorInfo.IME_ACTION_NEXT);


        workImagesAdapter=new WorkImagesAdapter(getApplicationContext(), new WorkImagesAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(final WorkModel d, View view) throws ParseException {





                ImageView delete=view.findViewById(R.id.cancel);
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new AlertDialog.Builder(PostPropertyActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle("Are you sure you want to delete Property Image ?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if(d.isServer()) {
                                            deletePropertyImage(d);
                                        }else{
                                            lists.remove(d);
                                            workImagesAdapter.notifyDataSetChanged();
                                        }
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                    }
                });
            }
        });


        GridLayoutManager gridLayoutManager=new GridLayoutManager(getApplicationContext(), 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);

        recyclerView.setHasFixedSize(true);

        adapter = new PropertyImagesAdapter(getApplicationContext());

        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  startDialog();
            }
        });

        propertytype.add("Property type");
        propertytype.add("Residential");
        propertytype.add("Commercial");

        propertyTypeAdapter=new ArraySpinnerAdapter(PostPropertyActivity.this);

        propertyTypeAdapter.setData(propertytype);
        propertyType.setAdapter(propertyTypeAdapter);

        propertyfor.add("Property for");
        propertyfor.add("Sell");
        propertyfor.add("Rent");

        yearList.clear();
        monthList.clear();
        yearList.add("Select Year");
        monthList.add("Select Month");

        for(int i=1;i<13;i++){
            yearList.add(i+" Year ");
            monthList.add(i+" month ");
        }

        yearAdapter=new ArraySpinnerAdapter(PostPropertyActivity.this);
        yearAdapter.setData(yearList);
        yearsSpinner.setAdapter(yearAdapter);

        monthAdapter=new ArraySpinnerAdapter(PostPropertyActivity.this);
        monthAdapter.setData(monthList);
        monthSpinner.setAdapter(monthAdapter);

        bedAdapter=new ArraySpinnerAdapter(this);

        bedList.clear();
        bedList.add("Select Bedroom");
        bedList.add("1 RK");
        bedList.add("1 BHK");
        bedList.add("2 BHK");
        bedList.add("3 BHK");
        bedList.add("4 BHK");

        bedAdapter.setData(bedList);
        bedRoomSpinner.setAdapter(bedAdapter);

        propertyForAdapter=new ArraySpinnerAdapter(PostPropertyActivity.this);

        propertyForAdapter.setData(propertyfor);

        propertyFor.setAdapter(propertyForAdapter);

        progressBar=new ProgressDialog(PostPropertyActivity.this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);


        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stateName = String.valueOf(listState.get(i));
                final RealmResults<City> cityRealmResults = realm.where(City.class).equalTo("state", stateName).findAllAsync();
                cityRealmResults.addChangeListener(new RealmChangeListener<RealmResults<City>>() {
                    @Override
                    public void onChange(RealmResults<City> cities) {

                        listCity.clear();
                        listCity.add("Select City");
                        if (cityRealmResults.size() > 0) {
                            for (int i = 0; i < cityRealmResults.size(); i++) {
                                if (!listCity.contains(cities.get(i).getCity())) {
                                    listCity.add(cities.get(i).getCity());
                                }
                            }
                            citySpinnerAdapter.setData(listCity);
                            citySpinner.setAdapter(citySpinnerAdapter);
                        }
                    }


                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(listCity.size()>i) {
                    cityName = String.valueOf(listCity.get(i));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        propertyType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                PropertyType= String.valueOf(propertytype.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        bedRoomSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(bedList.size()>i) {
                    bedRoom = String.valueOf(bedList.get(i));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        propertyFor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                PropertyFor= String.valueOf(propertyfor.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        yearsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                year= String.valueOf(yearList.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                month= String.valueOf(monthList.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            propertyId = extras.getString(Constants.PROPERTYID);
            getPropertyDetails(propertyId);
        }


    }



    public void deletePropertyImage(final WorkModel workModel){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("propertyid",propertyId);
        hashMap.put("propertyimage",workModel.getUrl());
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get(RestClient.PROPERTY_BASE_URL).deleteGallary(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull retrofit2.Response<Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_SHORT).show();
                        lists.remove(workModel);
                        workImagesAdapter.notifyDataSetChanged();
                    }else{
                        Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<in.happyhelp.limra.activity.response.Response> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_SHORT).show();
            }
        });
    }




    public void getPropertyDetails(final String propertyId){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("property_id",propertyId);
        Call<PropertyDetailResponse> call= RestClient.get(RestClient.PROPERTY_BASE_URL).getPropertyDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<PropertyDetailResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<PropertyDetailResponse> call, @NonNull retrofit2.Response<PropertyDetailResponse> response) {
                PropertyDetailResponse propertyDetailResponse=response.body();
                if(response.code()==200){
                    if(propertyDetailResponse.getStatus()){
                        final PropertyDetails propertyDetails=propertyDetailResponse.getPropertyDetails();
                        if(propertyDetails!=null) {
                            propertyName.setText(propertyDetails.getPropertyName());
                            address.setText(propertyDetails.getAddress());
                            buildingArea.setText(propertyDetails.getBuiltArea());
                            carpetArea.setText(propertyDetails.getCarpArea());
                            mobile.setText(propertyDetails.getMobile());



                            offerPrice.setText(propertyDetails.getOfferPrice());
                            parking.setText(propertyDetails.getNoParking());
                            comment.setText(propertyDetails.getOtherAmenities());
                            ownerName.setText(propertyDetails.getName());
                            buildName.setText(propertyDetails.getBuildrName());
                            flatNo.setText(propertyDetails.getNoOfBldngs());
                           // city.setText(propertyDetails.getCity());
                            pincode.setText(propertyDetails.getPincode());
                            floor.setText(propertyDetails.getFloorNo());
                            expectedPrice.setText(propertyDetails.getMinExpPrice());

                          /*  years.setText(propertyDetails.getNoOfYears());
                            month.setText(propertyDetails.getNoOfMonths());*/


                            if(propertyDetails.getBedroom().equals("0")) {
                                bedRoomSpinner.setSelection(1);
                            }else if(propertyDetails.getBedroom().equals("1")) {
                                bedRoomSpinner.setSelection(2);
                            }else if(propertyDetails.getBedroom().equals("2")) {
                                bedRoomSpinner.setSelection(3);
                            }else if(propertyDetails.getBedroom().equals("3")) {
                                bedRoomSpinner.setSelection(4);
                            }else if(propertyDetails.getBedroom().equals("4")) {
                                bedRoomSpinner.setSelection(5);
                            }else{
                                bedRoomSpinner.setSelection(0);
                            }


                            weburl.setText(propertyDetails.getWebsite_url());



                          for(int i=0;i<yearList.size();i++){
                              if(propertyDetails.getNoOfYears().equals(yearList.get(i))){
                                  yearsSpinner.setSelection(i);
                              }
                          }

                            for(int i=0;i<monthList.size();i++){
                                if(propertyDetails.getNoOfMonths().equals(monthList.get(i))){
                                    monthSpinner.setSelection(i);
                                }
                            }




                            State stateResult = realm.where(State.class).equalTo("id", propertyDetails.getState()).findFirst();

                            for(int i=0;i<listState.size();i++){
                                if(stateResult!=null)
                                if(stateResult.getState().equals(listState.get(i))){
                                    stateSpinner.setSelection(i);
                                }
                            }


                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                   // City cityResult = realm.where(City.class).equalTo("city", propertyDetails.getCity()).findFirst();
                                    for(int i=0;i<listCity.size();i++){

                                        if( propertyDetails.getCity().equals(listCity.get(i))){
                                            citySpinner.setSelection(i);
                                        }
                                    }
                                }
                            }, 100);



                            if(propertyDetails.getDealType().equals("Sell")){
                                propertyFor.setSelection(1);
                            }else if(propertyDetails.getDealType().equals("Rent")){
                                propertyFor.setSelection(2);
                            }else {
                                propertyFor.setSelection(0);
                            }

                            if(propertyDetails.getProType().equals("Commercial")){
                                propertyType.setSelection(2);
                            }else if(propertyDetails.getProType().equals("Residential")){
                                propertyType.setSelection(1);
                            }else {
                                propertyType.setSelection(0);
                            }

                            submit.setText("Update Property");
                            isUpdate=true;

                            lists.clear();
                            for(int i=0;i<propertyDetails.getImages().size();i++) {
                                WorkModel workModel=new WorkModel();
                                workModel.setServer(true);
                                workModel.setUrl(propertyDetails.getImages().get(i));
                                lists.add(workModel);
                            }
                            workImagesAdapter.setData(lists);
                            recyclerView.setAdapter(workImagesAdapter);

                        }else{

                        }
                    }else{

                    }

                }else if(response.code()==401){
                    AppUtils.logout(PostPropertyActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<PropertyDetailResponse> call, @NonNull Throwable t) {
                t.printStackTrace(); View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }


    private void startDialog() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(
                PostPropertyActivity.this);
        myAlertDialog.setTitle("Upload Pictures Option");
        myAlertDialog.setMessage("How do you want to set your picture?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent pictureActionIntent = null;

                        pictureActionIntent = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        pictureActionIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        startActivityForResult(
                                pictureActionIntent,
                                GALLERY_WORK);

                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (ContextCompat.checkSelfPermission(PostPropertyActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(PostPropertyActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, REQUEST_PHONE_CALL);

                            Intent intent = new Intent(
                                    MediaStore.ACTION_IMAGE_CAPTURE);
                            File imagePath = new File(Environment.getExternalStorageDirectory(), Constants.imagePath);
                            if(!imagePath.exists()){
                                imagePath.mkdirs();
                            }

                            File mypath = new File(imagePath.getAbsolutePath(), System.currentTimeMillis() + "Work.jpg");
                            // File newFile = new File(imagePath, "default_image.jpg");


                            WorkModel model = new WorkModel();
                            model.setUrl(mypath.getAbsolutePath());
                            lists.add(model);

                            Uri contentUri = getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", mypath);
                            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, contentUri);
                            startActivityForResult(intent,
                                    REQUEST_IMAGE_CAPTURE_WORK);

                        } else {

                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File imagePath = new File(Environment.getExternalStorageDirectory(), Constants.imagePath);
                            if(!imagePath.exists()){
                                imagePath.mkdirs();
                            }
                            File mypath = new File(imagePath.getAbsolutePath(), System.currentTimeMillis() + "Work.jpg");
                            WorkModel model = new WorkModel();
                            model.setUrl(mypath.getAbsolutePath());
                            lists.add(model);
                            Uri contentUri = getUriForFile(PostPropertyActivity.this, getApplicationContext().getPackageName() + ".provider", mypath);
                            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, contentUri);
                            startActivityForResult(intent,
                                    REQUEST_IMAGE_CAPTURE_WORK);
                        }
                    }
                });
        myAlertDialog.show();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void ValidatioCheck(){

        if(TextUtils.isEmpty(propertyName.getText().toString())) {
            propertyName.setError("Enter Property Name");
            propertyName.requestFocus();
            return;
        }

        if(PropertyType.equals("Property type")){
            Toast.makeText(getApplicationContext(),"Select property type", Toast.LENGTH_LONG).show();
            return;
        }

        if(PropertyFor.equals("Property for")){
            Toast.makeText(getApplicationContext(),"Select property for", Toast.LENGTH_LONG).show();
            return;
        }


        if(TextUtils.isEmpty(ownerName.getText().toString())) {
            ownerName.setError("Enter Name");
            ownerName.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(mobile.getText().toString())) {
            mobile.setError("Enter mobile number");
            mobile.requestFocus();
            return;
        }

        if(!AppUtils.isValidMobile(mobile.getText().toString())){
            mobile.setError("Enter valid mobile");
            mobile.requestFocus();
            Toast.makeText(this, "Enter valid mobile", Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(address.getText().toString())) {
            address.setError("Enter address");
            address.requestFocus();
            return;
        }

        if(stateName.equals("Select State")){
            Toast.makeText(getApplicationContext(),"Select State",Toast.LENGTH_SHORT).show();
            return;
        }

        if(cityName.equals("Select City")){
            Toast.makeText(getApplicationContext(),"Select City",Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(pincode.getText().toString())) {
            pincode.setError("Enter pincode");
            pincode.requestFocus();
            return;
        }


        if(pincode.getText().length()!=6) {
            pincode.setError("Enter valid pincode");
            pincode.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(floor.getText().toString())) {
            floor.setError("Enter No of floor");
            floor.requestFocus();
            return;
        }


        if(TextUtils.isEmpty(buildingArea.getText().toString())) {
            buildingArea.setError("Enter Builtup Area");
            buildingArea.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(carpetArea.getText().toString())) {
            carpetArea.setError("Enter Carpet Area");
            carpetArea.requestFocus();
            return;
        }

        int buildArea= Integer.parseInt(buildingArea.getText().toString());
        int carpetarea=Integer.parseInt(carpetArea.getText().toString());

        if(buildArea<carpetarea){
            Toast.makeText(this, "Please Fill valid Build and Carpet Area ", Toast.LENGTH_SHORT).show();
            return;
        }
      /*  if(TextUtils.isEmpty(buildName.getText().toString())) {
            buildName.setError("Enter BuildingName");
            return;
        }*/


        if(TextUtils.isEmpty(flatNo.getText().toString())) {
            flatNo.setError("Enter No of building");
            flatNo.requestFocus();
            return;
        }

        if(bedRoom.equals("Select Bedroom")){
            Toast.makeText(getApplicationContext(),"Select bedRoom ", Toast.LENGTH_LONG).show();
            return;
        }




        if(TextUtils.isEmpty(offerPrice.getText().toString())) {
            offerPrice.setError("Enter offer Price");
            offerPrice.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(expectedPrice.getText().toString())) {
            expectedPrice.setError("Enter Minimum expected price");
            expectedPrice.requestFocus();
            return;
        }




        if(year.equals("Select Year")){

            Toast.makeText(getApplicationContext(),"Select Year ", Toast.LENGTH_LONG).show();
            return;
        }

        if(month.equals("Select Month")){
            Toast.makeText(getApplicationContext(),"Select Month ", Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(weburl.getText().toString())) {
            weburl.setError("Enter web url");
            weburl.requestFocus();
            return;
        }


       /* if( !AppUtils.isValidURL(weburl.getText().toString())){
            weburl.setError("Enter valid web Url ");
            weburl.requestFocus();
            return;
        }*/

        if(TextUtils.isEmpty(parking.getText().toString())) {
            parking.setError("Enter No of parking");
            parking.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(comment.getText().toString())) {
            comment.setError("Enter other amenities");
            comment.requestFocus();
            return;
        }

        if(lists.size()==0){
            Toast.makeText(this, "Please Upload Property Images ", Toast.LENGTH_SHORT).show();
            return;
        }


        if(isUpdate){
            updateProperty();
        }else {
            uploadMultiFile();
        }
    }

    private void uploadMultiFile() {
        progressBar.show();
        ArrayList<String> filePaths = new ArrayList<>();
        for (int i = 0; i < lists.size(); i++) {
            filePaths.add(lists.get(i).getUrl());
        }
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("property_name", propertyName.getText().toString());
        builder.addFormDataPart("name", ownerName.getText().toString());
        builder.addFormDataPart("mobile", mobile.getText().toString());
        builder.addFormDataPart("state",stateName);
        builder.addFormDataPart("city",cityName);

        builder.addFormDataPart("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));


        builder.addFormDataPart("built_area", buildingArea.getText().toString());
        builder.addFormDataPart("carp_area", carpetArea.getText().toString());
        builder.addFormDataPart("buildr_name", buildName.getText().toString());


        builder.addFormDataPart("expected_price", expectedPrice.getText().toString());
        builder.addFormDataPart("offer_price", offerPrice.getText().toString());

        State stateResult = realm.where(State.class).equalTo("state", stateName).findFirst();

        if(stateResult!=null) {
            builder.addFormDataPart("state", stateResult.getId());
        }

        City cityResult = realm.where(City.class).equalTo("city", cityName).findFirst();
        if(cityResult!=null) {
            builder.addFormDataPart("city", cityResult.getId());
        }

        builder.addFormDataPart("pincode", pincode.getText().toString());
        builder.addFormDataPart("address", address.getText().toString());
        builder.addFormDataPart("other_amenities", comment.getText().toString());
        builder.addFormDataPart("no_parking", parking.getText().toString());

        if(bedRoom.equals("1 RK")) {
            builder.addFormDataPart("bedroom", "0");
        }else if(bedRoom.equals("1 BHK")) {
            builder.addFormDataPart("bedroom", "1");
        }else if(bedRoom.equals("2 BHK")) {
            builder.addFormDataPart("bedroom", "2");
        }else if(bedRoom.equals("3 BHK")) {
            builder.addFormDataPart("bedroom", "3");
        }else if(bedRoom.equals("4 BHK")) {
            builder.addFormDataPart("bedroom", "4");
        }else{
            builder.addFormDataPart("bedroom", "0");
        }

        builder.addFormDataPart("no_bldng", flatNo.getText().toString());
        builder.addFormDataPart("flr_no", floor.getText().toString());

        if(PropertyFor.equals("Sell")) {
            builder.addFormDataPart("deal_type", "1");
        }else{
            builder.addFormDataPart("deal_type", "0");
        }


        if(PropertyType.equals("Residential")) {
            builder.addFormDataPart("property_type", "1");
        }else{
            builder.addFormDataPart("property_type", "2");
        }


        builder.addFormDataPart("no_of_month", month);
        builder.addFormDataPart("no_of_yr", year);
        builder.addFormDataPart("website_url", weburl.getText().toString());


        for (int i = 0; i < filePaths.size(); i++) {
            File file = new File(filePaths.get(i));
            builder.addFormDataPart("uploadFile[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
        }

        final MultipartBody requestBody = builder.build();
        Call<Response> call = RestClient.get(RestClient.PROPERTY_BASE_URL).postProperty(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken()
                , requestBody);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                progressBar.dismiss();
                Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()) {
                        new AlertDialog.Builder(PostPropertyActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(response1.getMessage())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        onBackPressed();
                                    }
                                })
                                .show();

                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(PostPropertyActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });


    }


    private void updateProperty() {
        progressBar.show();
        ArrayList<String> filePaths = new ArrayList<>();
        for (int i = 0; i < lists.size(); i++) {
            filePaths.add(lists.get(i).getUrl());
        }
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("property_name", propertyName.getText().toString());
        builder.addFormDataPart("name", ownerName.getText().toString());
        builder.addFormDataPart("mobile", mobile.getText().toString());

        builder.addFormDataPart("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));


        builder.addFormDataPart("property_id", propertyId);
        builder.addFormDataPart("built_area", buildingArea.getText().toString());
        builder.addFormDataPart("carp_area", carpetArea.getText().toString());
     //   builder.addFormDataPart("buildr_name", buildName.getText().toString());


        builder.addFormDataPart("expected_price", expectedPrice.getText().toString());
        builder.addFormDataPart("offer_price", offerPrice.getText().toString());


        State stateResult = realm.where(State.class).equalTo("state", stateName).findFirst();

        if(stateResult!=null) {
            builder.addFormDataPart("state", stateResult.getId());
        }

        City cityResult = realm.where(City.class).equalTo("city", cityName).findFirst();
        if(cityResult!=null) {
            builder.addFormDataPart("city", cityResult.getId());
        }

        builder.addFormDataPart("pincode", pincode.getText().toString());
        builder.addFormDataPart("address", address.getText().toString());
        builder.addFormDataPart("other_amenities", comment.getText().toString());
        builder.addFormDataPart("no_parking", parking.getText().toString());

        if(bedRoom.equals("1 RK")) {
            builder.addFormDataPart("bedroom", "0");
        }else if(bedRoom.equals("1 BHK")) {
            builder.addFormDataPart("bedroom", "1");
        }else if(bedRoom.equals("2 BHK")) {
            builder.addFormDataPart("bedroom", "2");
        }else if(bedRoom.equals("3 BHK")) {
            builder.addFormDataPart("bedroom", "3");
        }else if(bedRoom.equals("4 BHK")) {
            builder.addFormDataPart("bedroom", "4");
        }else{
            builder.addFormDataPart("bedroom", "0");
        }

        builder.addFormDataPart("no_bldng", flatNo.getText().toString());
        builder.addFormDataPart("flr_no", floor.getText().toString());

        if(PropertyFor.equals("Sell")) {
            builder.addFormDataPart("deal_type", "1");
        }else{
            builder.addFormDataPart("deal_type", "0");
        }


        if(PropertyType.equals("Residential")) {
            builder.addFormDataPart("property_type", "1");
        }else{
            builder.addFormDataPart("property_type", "2");
        }


        builder.addFormDataPart("no_of_month", month);
        builder.addFormDataPart("no_of_yr", year);


        for (int i = 0; i < filePaths.size(); i++) {
            File file = new File(filePaths.get(i));
            if (file.exists()) {
                builder.addFormDataPart("uploadFile[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
            }
        }

        final MultipartBody requestBody = builder.build();
        Call<Response> call = RestClient.get(RestClient.PROPERTY_BASE_URL).updateProperty(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken()
                , requestBody);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                progressBar.dismiss();
                Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()) {
                        new AlertDialog.Builder(PostPropertyActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(response1.getMessage())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        onBackPressed();
                                    }
                                })
                                .show();

                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(PostPropertyActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });


    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if( resultCode==RESULT_OK)
             if(requestCode==REQUEST_IMAGE_CAPTURE_WORK)
            {
                workImagesAdapter.setData(lists);
                recyclerView.setAdapter(workImagesAdapter);

            }else if(requestCode==GALLERY_WORK){
                if (data != null) {


                    if (data.getClipData() != null) {
                        int count = data.getClipData().getItemCount(); //evaluate the count before the for loop --- otherwise, the count is evaluated every loop.
                        for (int i = 0; i < count; i++) {
                            Uri imageUri = data.getClipData().getItemAt(i).getUri();
                            try {
                                InputStream is = getContentResolver().openInputStream(imageUri);
                                Bitmap bitmap = BitmapFactory.decodeStream(is);
                                savebitmap(bitmap);
                                is.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    } else if (data.getData() != null) {
                        String imagePath = data.getData().getPath();
                        try {
                            InputStream is = getContentResolver().openInputStream(Uri.parse(imagePath));
                            Bitmap bitmap = BitmapFactory.decodeStream(is);
                            savebitmap(bitmap);
                            is.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    workImagesAdapter.setData(lists);
                    recyclerView.setAdapter(workImagesAdapter);

                } else {
                    Toast.makeText(getApplicationContext(), "Cancelled",
                            Toast.LENGTH_SHORT).show();
                }
            }
    }

    public  File savebitmap(Bitmap bmp) throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        File f = new File(Environment.getExternalStorageDirectory()+File.separator
                + Constants.imagePath + System.currentTimeMillis()+"Work.jpg");
        f.createNewFile();
        WorkModel model = new WorkModel();
        model.setUrl(f.getAbsolutePath());
        lists.add(model);
        FileOutputStream fo = new FileOutputStream(f);
        fo.write(bytes.toByteArray());
        fo.close();
        return f;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(PostPropertyActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(PostPropertyActivity.this,Id);

            }
        });
    }

}
