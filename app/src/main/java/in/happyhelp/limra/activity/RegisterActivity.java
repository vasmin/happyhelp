package in.happyhelp.limra.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.chaos.view.PinView;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;

import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.loginresponse.LoginResponse;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.refreral)
    EditText referalCode;

    @BindView(R.id.pass)
    EditText password;

    @BindView(R.id.submit)
    TextView submit;
    CountDownTimer countDownTimer;

    ProgressDialog progressBar;

    @BindView(R.id.signup)
    CardView signupCardview;

    @BindView(R.id.otp)
    CardView otpCard;

    @BindView(R.id.resend)
    TextView resend;

    @BindView(R.id.verify)
    TextView verify;
    PinView pinView;
    String imei;

    Realm realm;

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);
        AppUtils.checkAndRequestPermissions(this);
        Objects.requireNonNull(getSupportActionBar()).hide();
        getWindow().setBackgroundDrawableResource(R.drawable.loginbg);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        realm = RealmHelper.getRealmInstance();
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        @SuppressLint("HardwareIds") String imeiNos1 = telephonyManager.getDeviceId(1);
        @SuppressLint("HardwareIds") String imeiNos2=telephonyManager.getDeviceId(2);
         imei=imeiNos1+","+imeiNos2;
        String androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        UUID deviceUuid = new UUID(androidId.hashCode(), ((long)telephonyManager.hashCode() << 32) | telephonyManager.hashCode());
        String deviceId = deviceUuid.toString();
        Log.e("IMEI",imei+"deviceId "+deviceId);

        progressBar = new ProgressDialog(RegisterActivity.this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        pinView = findViewById(R.id.firstPinView);


        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendOtp();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(name.getText().toString())) {
                    name.setError("Enter Name ");
                    name.requestFocus();
                    return;
                }


                if(!AppUtils.isValidMobile(mobile.getText().toString())){
                    mobile.setError("Enter valid mobile");
                    mobile.requestFocus();
                    return;
                }

                if(!AppUtils.isValidMail(email.getText().toString(),email)){
                    email.setError("Enter valid email");
                    email.requestFocus();
                    return;
                }

                register();

            }
        });

        referalCode.setText(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.PLAYREFERALCODE));

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyOtp();
            }
        });
    }



    public void resendOtp(){
        HashMap<String,String>hashMap=new HashMap<>();
        hashMap.put("mobile",mobile.getText().toString());
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().resendOtp(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
                if(response.code()==200) {
                    countDownTimer=new CountDownTimer(120000, 1000) {

                        @SuppressLint("SetTextI18n")
                        public void onTick(long millisUntilFinished) {
                            resend.setText("Remaining: " + millisUntilFinished / 1000+"s");
                            resend.setClickable(false);
                            //here you can have your logic to set text to edittext
                        }

                        @SuppressLint("SetTextI18n")
                        public void onFinish() {
                            resend.setText("Resend Otp");
                            resend.setClickable(true);
                        }

                    }.start();
                }else if(response.code()==401){
                    AppUtils.logout(RegisterActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private String handleOTPDetection(String messageText) {
        Log.d("messagetext",messageText);
        Pattern pattern = Pattern.compile("(\\d{4})");
        Matcher matcher = pattern.matcher(messageText);
        String code="";
        if (matcher.find()) {
            code = matcher.group(1);
            Log.e("code",code);
        }
        return code;

    }

    public void register() {

       /* if (TextUtils.isEmpty(referalCode.getText().toString())) {
            referalCode.setError("Enter referalcode ");
            return;
        }*/

        if (TextUtils.isEmpty(password.getText().toString())) {
            password.setError("Enter Password ");
            password.requestFocus();
            return;
        }

        progressBar.show();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("name", name.getText().toString());
        hashMap.put("mobile", mobile.getText().toString());
        hashMap.put("email", email.getText().toString());
        hashMap.put("referal_code", referalCode.getText().toString());
        hashMap.put("password", password.getText().toString());
        hashMap.put("device_no",imei);
        Call<in.happyhelp.limra.activity.response.Response> call = RestClient.get().registerUser(hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
                progressBar.dismiss();
                if (response.code() == 200) {
                    in.happyhelp.limra.activity.response.Response response1 = response.body();

                    if (response1.getStatus()) {
                        signupCardview.setVisibility(View.GONE);
                        otpCard.setVisibility(View.VISIBLE);
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();

                        countDownTimer=new CountDownTimer(120000, 1000) {

                            @SuppressLint("SetTextI18n")
                            public void onTick(long millisUntilFinished) {
                                resend.setText("Remaining: " + millisUntilFinished / 1000+"s");
                                resend.setClickable(false);
                                //here you can have your logic to set text to edittext
                            }

                            @SuppressLint("SetTextI18n")
                            public void onFinish() {
                                resend.setText("Resend Otp");
                                resend.setClickable(true);
                            }

                        }.start();

                    } else {
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();


                    }
                } else {
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();

                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();

                progressBar.dismiss();
            }
        });
    }

    public void verifyOtp() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("mobile", mobile.getText().toString());
        hashMap.put("otp", pinView.getText().toString());
        hashMap.put("referal_code", referalCode.getText().toString());

        Call<LoginResponse> call = RestClient.get().verifyOtp(hashMap);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                if(response.code()==200) {
                    final LoginResponse loginResponse = response.body();

                    if (loginResponse.getStatus()) {
                        //updateFcm();
                       /* SharedPreferenceHelper.getInstance(getApplicationContext()).saveAuthToken(loginResponse.getToken());
                        SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.USERID, loginResponse.getData().get(0).getId());
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setUserLoggedIn(true);

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                realm.copyToRealmOrUpdate(loginResponse.getData());
                            }
                        });
                        if(loginResponse.getData().size()>0) {
                            SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.REFERALCODE, loginResponse.getData().get(0).getUse_ref_code());
                        }*/

                        new AlertDialog.Builder(RegisterActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle("Happy Help Registeration successfully")
                                .setMessage(loginResponse.getMessage())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                })
                                .show();

                    }
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
                t.printStackTrace();
            }
        });

    }
}
