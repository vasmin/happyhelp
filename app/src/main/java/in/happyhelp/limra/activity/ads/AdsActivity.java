package in.happyhelp.limra.activity.ads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.dialogplus.DialogPlus;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.ConfirmationActivity;
import in.happyhelp.limra.activity.donation.DonationDetailsActivity;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import in.happyhelp.limra.adapter.AdsSizeAdapter;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.gstresponse.GstRateResponse;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.adssizeresponse.AdsSizeResponse;
import in.happyhelp.limra.activity.response.adssizeresponse.Datum;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdsActivity extends AppCompatActivity {
    private static final int GALLERY_BANNERIMAGE = 123;
    private static final int REQUEST_IMAGE_CAPTURE_BANNERIMAGE = 321;
    ArraySpinnerAdapter adapter;

    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.ads)
    EditText adsTitle;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.buisness)
    EditText business;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.category)
    EditText category;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.weburl)
            EditText weburl;

    String type;
    String daysSelect="";
    List<String> list=new ArrayList<>();

    List<String> listdays=new ArrayList<>();

    @BindView(R.id.adssize)
    Spinner adsSize;

    @BindView(R.id.image)
    ImageView imageView;

    @BindView(R.id.start)
    LinearLayout startLinear;

    @BindView(R.id.end)
    LinearLayout endLinear;

    @BindView(R.id.startdate)
    TextView startDate;

    @BindView(R.id.enddate)
    TextView endDate;

    @BindView(R.id.pay)
    TextView payNow;

    @BindView(R.id.subtotal)
    TextView subTotal;

    @BindView(R.id.total)
    TextView TotalAmt;


    @BindView(R.id.gst)
    TextView gstAmt;

    @BindView(R.id.gstlabel)
    TextView gstLabel;

    int gst=0;
    int subtotal=0;
    int total=0;
    int gstamt=0;

    ProgressDialog progressBar;
    AdsSizeAdapter adsSizeAdapter;
    List<Datum> adsSizeList=new ArrayList<>();
    Datum datum;
    Calendar dateSelected = Calendar.getInstance();
    String bannerImagePath="";

    @BindView(R.id.days)
    TextView days;
    int adsdays=0;

    Realm realm;
    @BindView(R.id.daysspinner)
    Spinner daysSpinner;

    ArraySpinnerAdapter spinnerAdapter;

    String minDate;

    DialogPlus dialog;
    TextView decline,accept;
    TextView serviceName,serviceaddress,serviceamt,duration;
    String Id="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        AppUtils.isNetworkConnectionAvailable(this);
        realm= RealmHelper.getRealmInstance();
        progressBar=new ProgressDialog(AdsActivity.this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        list.clear();
        list.add("Select Ads type");
        list.add("Online");
        list.add("Offline");

        adapter=new ArraySpinnerAdapter(this);
        adapter.setData(list);

        spinnerAdapter=new ArraySpinnerAdapter(this);
        listdays.clear();
        listdays.add("Select days");
        listdays.add("15 days");
        listdays.add("30 days");
        listdays.add("45 days");
        listdays.add("60 days");

        spinnerAdapter.setData(listdays);
        daysSpinner.setAdapter(spinnerAdapter);

        spinner.setAdapter(adapter);

        getGstRate();

        adsSizeAdapter=new AdsSizeAdapter(this);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("Type","is"+type);
                type=list.get(i);
                if(!type.equals("Select Ads type")) {
                    getAdsSize(type);
                }else{
                    adsSizeList.clear();
                    Datum datum=new Datum();
                    datum.setSize("Select Ads Size");
                    adsSizeList.add(datum);
                    adsSizeAdapter.setData(adsSizeList);
                    adsSize.setAdapter(adsSizeAdapter);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        RealmResults<UserData> userData=realm.where(UserData.class).findAllAsync();
        userData.addChangeListener(new RealmChangeListener<RealmResults<UserData>>() {
            @Override
            public void onChange(@NonNull RealmResults<UserData> userData) {
                if (userData.size() > 0) {
                    name.setText(userData.get(0).getName());
                    email.setText(userData.get(0).getEmail());
                    mobile.setText(userData.get(0).getMobile());
                }
            }
        });


        daysSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                daysSelect=listdays.get(i);


                if(daysSelect.equals("Select days")){
                    adsdays=0;
                }else if(daysSelect.equals("15 days")){
                    adsdays=15;
                }else if(daysSelect.equals("30 days")){
                    adsdays=30;
                }else if(daysSelect.equals("45 days")){
                    adsdays=45;
                }else if(daysSelect.equals("60 days")){
                    adsdays=60;
                }
                if(datum!=null) {
                    subtotal = adsdays * Integer.parseInt(datum.getAmount());
                    subTotal.setText("\u20B9 " + String.valueOf(subtotal));
                    setTotalData();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.equals("Select Ads type")) {
                    Toast.makeText(getApplicationContext(),"Please Select Ads type ",Toast.LENGTH_LONG).show();
                    return;
                }

                if (TextUtils.isEmpty(adsTitle.getText().toString())) {
                    adsTitle.setError("Enter Title ");
                    adsTitle.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(business.getText().toString())) {
                    business.setError("Enter Buisness Name ");
                    business.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(name.getText().toString())) {
                    name.setError("Enter Name ");
                    name.requestFocus();
                    return;
                }

                if(!AppUtils.isValidMail(email.getText().toString(),email)){
                    email.setError("Enter Valid Email");
                    email.requestFocus();
                    return;
                }

                if(!AppUtils.isValidMobile(mobile.getText().toString())){
                    mobile.requestFocus();
                    mobile.setError("Enter valid mobile");
                    return;
                }


                if (TextUtils.isEmpty(category.getText().toString())) {
                    category.setError("Enter Category ");
                    category.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(weburl.getText().toString())) {
                    weburl.setError("Enter web url ");
                    weburl.requestFocus();
                    return;
                }

              /*  if(!AppUtils.isValidURL(weburl.getText().toString().trim())){
                    weburl.setError("Enter web url ");
                    weburl.requestFocus();
                    return;
                }*/

                if(datum.getSize().equals("Select Ads Size")) {
                    Toast.makeText(getApplicationContext(),"Please Select Select Ads Size ",Toast.LENGTH_LONG).show();
                    return;
                }

                if(adsdays==0){
                    Toast.makeText(getApplicationContext(),"Please Select Number of Days ",Toast.LENGTH_LONG).show();
                    return;
                }
/*
                if (TextUtils.isEmpty(startDate.getText().toString())) {
                    Toast.makeText(getApplicationContext(),"Please Select StartDate ",Toast.LENGTH_LONG).show();
                    return;
                }

                if (TextUtils.isEmpty(endDate.getText().toString())) {
                    Toast.makeText(getApplicationContext(),"Enter Select EndDate ",Toast.LENGTH_LONG).show();
                    return;
                }*/

                if(bannerImagePath.equals("")) {
                    Toast.makeText(getApplicationContext(), "Please upload Banner", Toast.LENGTH_LONG).show();
                    return;
                }

                File file=new File(bannerImagePath);
                if(!file.exists()){
                    Toast.makeText(getApplicationContext(), "Please upload Banner", Toast.LENGTH_LONG).show();
                    return;
                }

                callInstamojoPay(email.getText().toString(),mobile.getText().toString(), String.valueOf(total),"Ads Booking",name.getText().toString());
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File filepath=new File(Environment.getExternalStorageDirectory()+Constants.imagePath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }


                bannerImagePath=filepath.getAbsolutePath()+"/BannerImage.jpg";

                AppUtils.startPickImageDialog(GALLERY_BANNERIMAGE, REQUEST_IMAGE_CAPTURE_BANNERIMAGE, filepath.getAbsolutePath()+"/BannerImage.jpg",AdsActivity.this);

            }
        });


        startLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(datum!=null&&!datum.getSize().equals("Select Ads Size")) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(AdsActivity.this, new DatePickerDialog.OnDateSetListener() {

                        @SuppressLint("SetTextI18n")
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


                            final String receivedDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                            startDate.setText(receivedDate);
                            if (!startDate.getText().toString().equals("Start Date") && !endDate.getText().toString().equals("End Date")) {
                                adsdays = getCountOfDays(startDate.getText().toString(), endDate.getText().toString());

                                if (adsdays < 0) {
                                    new AlertDialog.Builder(AdsActivity.this)
                                            .setIcon(R.drawable.logo)
                                            .setTitle("Date Not Vaid")
                                            .setMessage("Please Select Proper date")
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    startDate.setText("");
                                                    endDate.setText("");
                                                }
                                            })
                                            .show();
                                } else {
                                    days.setText(adsdays + " Days");
                                    days.setVisibility(View.VISIBLE);
                                    subtotal = adsdays * Integer.parseInt(datum.getAmount());
                                    subTotal.setText("\u20B9 " + String.valueOf(subtotal));
                                    setTotalData();
                                }


                            } else {
                                days.setVisibility(View.GONE);
                            }
                        }

                    }, dateSelected.get(Calendar.YEAR), dateSelected.get(Calendar.MONTH), dateSelected.get(Calendar.DAY_OF_MONTH));
                    datePickerDialog.getDatePicker().setMinDate(new Date().getTime());
                    datePickerDialog.show();
                }else{
                    Toast.makeText(AdsActivity.this, "Please Select Ads Size", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });

        endLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(datum!=null&&!datum.getSize().equals("Select Ads Size")) {

                    DatePickerDialog datePickerDialog = new DatePickerDialog(AdsActivity.this, new DatePickerDialog.OnDateSetListener() {

                        @SuppressLint("SetTextI18n")
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            final String receivedDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                            endDate.setText(receivedDate);
                            if (!startDate.getText().toString().equals("Start Date") && !endDate.getText().toString().equals("End Date")) {
                                adsdays = getCountOfDays(startDate.getText().toString(), endDate.getText().toString());

                                if (adsdays < 0) {
                                    new AlertDialog.Builder(AdsActivity.this)
                                            .setIcon(R.drawable.logo)
                                            .setTitle("Date Not Valid")
                                            .setMessage("Please Select Proper date")
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    startDate.setText("");
                                                    endDate.setText("");
                                                }
                                            })
                                            .show();
                                } else {
                                    days.setText(adsdays + " Days");
                                    days.setVisibility(View.VISIBLE);
                                    subtotal = adsdays * Integer.parseInt(datum.getAmount());
                                    subTotal.setText("\u20B9 " + String.valueOf(subtotal));
                                    setTotalData();
                                }


                            } else {
                                days.setVisibility(View.GONE);
                            }
                        }
                    }, dateSelected.get(Calendar.YEAR), dateSelected.get(Calendar.MONTH), dateSelected.get(Calendar.DAY_OF_MONTH));


                    @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                    Date date = null;
                    try {
                        date = formatter.parse(startDate.getText().toString());
                        datePickerDialog.getDatePicker().setMinDate(date.getTime());
                        //  datePickerDialog.getDatePicker().setMaxDate(date.getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                        datePickerDialog.getDatePicker().setMinDate(new Date().getTime());
                    }

                    datePickerDialog.show();
                }else{
                    Toast.makeText(AdsActivity.this, "Please Select Ads Size", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });


        adsSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                datum=adsSizeList.get(i);
                if(datum!=null) {
                    if(!datum.getSize().equals("Select Ads Size")) {
                        subtotal = adsdays * Integer.parseInt(datum.getAmount());
                        subTotal.setText("\u20B9 " + String.valueOf(subtotal));
                        setTotalData();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


    }


    public void getGstRate(){
        Call<GstRateResponse> call=RestClient.get().getGstRate(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<GstRateResponse>() {
            @Override
            public void onResponse(Call<GstRateResponse> call, retrofit2.Response<GstRateResponse> response) {

                if(response.code()==200){
                    GstRateResponse gstRateResponse=response.body();

                    if(gstRateResponse.getStatus()) {
                        if (gstRateResponse.getGst().size() >= 1) {
                            gst = Integer.parseInt(gstRateResponse.getGst().get(0).getName());
                        }
                    }else{
                        gst=0;
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getApplicationContext());
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<GstRateResponse> call, Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    public void setTotalData(){
        try {
            int result = Math.round ((subtotal * gst) / 100);
            total=result+subtotal;
            gstamt=result;

            gstAmt.setText("\u20B9 "+String.valueOf(result));
            gstLabel.setText("Exclusive Gst ("+gst+"% )");
            TotalAmt.setText("\u20B9 "+String.valueOf(total));

        }
        catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public int getCountOfDays(String createdDateString, String expireDateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

        Date createdConvertedDate = null, expireCovertedDate = null, todayWithZeroTime = null;
        try {
            createdConvertedDate = dateFormat.parse(createdDateString);
            expireCovertedDate = dateFormat.parse(expireDateString);

            Date today = new Date();

            todayWithZeroTime = dateFormat.parse(dateFormat.format(today));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int cYear = 0, cMonth = 0, cDay = 0;

        if (createdConvertedDate.after(todayWithZeroTime)) {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(createdConvertedDate);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);

        } else {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(todayWithZeroTime);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);
        }

        Calendar eCal = Calendar.getInstance();
        eCal.setTime(expireCovertedDate);

        int eYear = eCal.get(Calendar.YEAR);
        int eMonth = eCal.get(Calendar.MONTH);
        int eDay = eCal.get(Calendar.DAY_OF_MONTH);

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(cYear, cMonth, cDay);
        date2.clear();
        date2.set(eYear, eMonth, eDay);

        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);

        return (int) dayCount+1;
    }



    private String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "";
            Toast.makeText(getApplicationContext(),"Sorry your records is not created",Toast.LENGTH_LONG).show();
        }
        return result;
    }

    private void setImageFromPath(ImageView image, String path){
        if(!TextUtils.isEmpty(path)&&!path.equals("")) {

            File imgFile = new File(path);
            if (imgFile.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize=8;      // 1/8 of original image
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(),options);
                image.setImageBitmap(myBitmap);
                image.setVisibility(View.VISIBLE);

            }else {
                image.setVisibility(View.GONE);
            }
        }else {
            image.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if( resultCode==RESULT_OK)

            if(requestCode==REQUEST_IMAGE_CAPTURE_BANNERIMAGE)
            {
                File newFile=new File(bannerImagePath);

                setImageFromPath(imageView,newFile.getPath());
            }
            else if(requestCode==GALLERY_BANNERIMAGE){
                Uri imageUri = data.getData();
                bannerImagePath = getPath(getApplicationContext(), imageUri);
                setImageFromPath(imageView,bannerImagePath);
            }
    }

    public void getAdsSize(String type){
        HashMap<String,String> hashMap=new HashMap<>();
        String typesend;
        if(type.equals("Offline")){
            typesend="0";
        }else{
            typesend="1";
        }
        hashMap.put("type",typesend);
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<AdsSizeResponse> call=RestClient.get().getAdsSize(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<AdsSizeResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<AdsSizeResponse> call, @NonNull Response<AdsSizeResponse> response) {

                if(response.code()==200) {
                    AdsSizeResponse adsSizeResponse = response.body();
                    if (adsSizeResponse.getStatus()) {
                        adsSizeList.clear();
                        Datum datum=new Datum();
                        datum.setSize("Select Ads Size");
                        adsSizeList.add(datum);
                        adsSizeList.addAll(adsSizeResponse.getData());
                        adsSizeAdapter.setData(adsSizeList);
                        adsSize.setAdapter(adsSizeAdapter);
                        minDate=adsSizeResponse.getMinenddate();
                       // gstLabel.setText("GST "+"("+adsSizeResponse.getGst()+"%)");
                       // gst= Integer.parseInt(adsSizeResponse.getGst());
                    } else {
                        adsSizeList.clear();
                        Datum datum=new Datum();
                        datum.setSize("Select Ads Size");
                        adsSizeList.add(datum);
                        adsSizeList.addAll(adsSizeResponse.getData());
                        adsSizeAdapter.setData(adsSizeList);
                        adsSize.setAdapter(adsSizeAdapter);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(AdsActivity.this);
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                    //Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<AdsSizeResponse> call, Throwable t) {
                t.printStackTrace();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
                //Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
            }
        });
    }

    public void postAds(String paymentId){

        progressBar.show();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String userID=SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID);
        builder.addFormDataPart("userid", userID);
        builder.addFormDataPart("name", name.getText().toString());
        builder.addFormDataPart("mobile", mobile.getText().toString());
        builder.addFormDataPart("email", email.getText().toString());
        builder.addFormDataPart("title", adsTitle.getText().toString());
        builder.addFormDataPart("business_name",business.getText().toString());
        builder.addFormDataPart("category",category.getText().toString());
        builder.addFormDataPart("category", category.getText().toString());
        builder.addFormDataPart("size",datum.getSize());
       // builder.addFormDataPart("startdate", startDate.getText().toString());
        builder.addFormDataPart("payment_id",paymentId);
       // builder.addFormDataPart("enddate",endDate.getText().toString());
        builder.addFormDataPart("gst", String.valueOf(gstamt));
        builder.addFormDataPart("total", String.valueOf(total));
        builder.addFormDataPart("subtotal", String.valueOf(subtotal));
        builder.addFormDataPart("website_url", weburl.getText().toString());


        if(adsdays!=0) {
            builder.addFormDataPart("days", String.valueOf(adsdays));
        }

        String typesend;
        if(type.equals("Offline")){
            typesend="0";
        }else{
            typesend="1";
        }


        builder.addFormDataPart("type",typesend);

        File bannerpath = new File(bannerImagePath);
        if(bannerpath.exists()){

            builder.addFormDataPart("image", "BannerImage.jpeg", RequestBody.create(MediaType.parse("multipart/form-data"), bannerpath));
        }

        MultipartBody requestBody = builder.build();
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().postAds(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                progressBar.dismiss();

                if(response.code()==200) {
                    in.happyhelp.limra.activity.response.Response response1 = response.body();
                    if (response1.getStatus()) {
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();
                        // Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(AdsActivity.this, ConfirmationActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();
                        // Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(AdsActivity.this);
                }
            }

            @Override
            public void onFailure(Call<in.happyhelp.limra.activity.response.Response> call, Throwable t) {
                progressBar.dismiss();
                t.printStackTrace();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();

            }
        });

    }


    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {


        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);
                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);
                postAds(payment);
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(AdsActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(AdsActivity.this,Id);

            }
        });



    }

}
