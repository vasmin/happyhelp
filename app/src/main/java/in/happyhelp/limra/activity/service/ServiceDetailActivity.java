package in.happyhelp.limra.activity.service;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.HomeActivity;
import in.happyhelp.limra.adapter.WorkingAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.companydetailsresponse.CompanyDetailsResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceDetailActivity extends AppCompatActivity {

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.detail)
    TextView details;

    @BindView(R.id.slider)
    SliderLayout slider;

    @BindView(R.id.location)
    TextView location;

    @BindView(R.id.address)
    TextView address;

    @BindView(R.id.email)
    TextView email;

    @BindView(R.id.mobile)
    TextView mobile;

    @BindView(R.id.amount)
    TextView amount;

    @BindView(R.id.enquiry)
    TextView enquiry;

    @BindView(R.id.service)
    TextView servicesName;

    @BindView(R.id.working)
    RecyclerView workingHours;

    @BindView(R.id.emailicon)
    ImageView emailIcon;

    @BindView(R.id.mobileicon)
    ImageView mobileIcon;

    WorkingAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    String serviceId;
    ProgressDialog progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_detail);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        workingHours.setLayoutManager(linearLayoutManager);
        workingHours.setHasFixedSize(true);
        workingHours.setNestedScrollingEnabled(true);

        progressBar=new ProgressDialog(ServiceDetailActivity.this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            serviceId = extras.getString(Constants.SERVICEID);

            getServiceDetails(serviceId);
        }
        emailIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:"+email.getText().toString()));
                startActivity(Intent.createChooser(emailIntent, "Send feedback"));
            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:"+email.getText().toString()));
                startActivity(Intent.createChooser(emailIntent, "Send feedback"));
            }
        });



        mobileIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mobile.getText().toString()));
                startActivity(intent);
            }
        });

        mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mobile.getText().toString()));
                startActivity(intent);
            }
        });

        enquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ServiceDetailActivity.this,ServiceEnquiryActivity.class);
                intent.putExtra(Constants.SERVICEID,serviceId);
                startActivity(intent);
            }
        });

        adapter=new WorkingAdapter(getApplicationContext());
    }

    public void getServiceDetails(String serviceId){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",serviceId);
        Call<CompanyDetailsResponse> call=RestClient.get().getServiceDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<CompanyDetailsResponse>() {
            @SuppressLint({"CheckResult", "SetTextI18n"})
            @Override
            public void onResponse(Call<CompanyDetailsResponse> call, Response<CompanyDetailsResponse> response) {
                progressBar.dismiss();
                if(response.code()==200){
                    CompanyDetailsResponse servicesListResponse=response.body();

                    if(servicesListResponse.isStatus())
                    if(servicesListResponse.getData().size()>0){
                        final in.happyhelp.limra.activity.response.companydetailsresponse.Datum datum=servicesListResponse.getData().get(0);

                        name.setText(datum.getCompanyName());
                        details.setText(datum.getDescription());
                        email.setText(datum.getEmail());
                        mobile.setText(datum.getMobile());

                        address.setText(datum.getAddress());
                        location.setText(datum.getLocation());
                        amount.setText("\u20B9 "+datum.getPlanamount());
                        amount.setVisibility(View.GONE);

                        if(!datum.getShow_button()){
                            enquiry.setVisibility(View.INVISIBLE);
                        }else{
                            enquiry.setVisibility(View.VISIBLE);
                        }

                        String serviceName="";
                        for(int i=0;i<datum.getServices().size();i++){
                            if(i==datum.getServices().size()-1){
                                serviceName=serviceName+datum.getServices().get(i);
                            }
                            else{
                                serviceName=serviceName+datum.getServices().get(i)+",";
                            }
                        }

                        servicesName.setText( serviceName);

                        if(datum.getWorkingHours().size()>0) {
                            adapter.setData(datum.getWorkingHours());
                            workingHours.setAdapter(adapter);
                        }

                        slider.setPresetTransformer(com.daimajia.slider.library.SliderLayout.Transformer.Accordion);
                        slider.setPresetIndicator(com.daimajia.slider.library.SliderLayout.PresetIndicators.Center_Bottom);
                        slider.setCustomAnimation(new DescriptionAnimation());
                        slider.setDuration(5000);
                        slider.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                            }

                            @Override
                            public void onPageSelected(int position) {

                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });

                        for(int i = 0; i<datum.getGalleryImages().size();i ++) {
                            DefaultSliderView defaultSliderView = new DefaultSliderView(getApplicationContext());
                            final int finalI = i;
                            defaultSliderView.image(RestClient.base_image_url+datum.getImage())
                                    .setScaleType(BaseSliderView.ScaleType.CenterInside)
                                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                        @Override
                                        public void onSliderClick(BaseSliderView slider) {
                                           /* String BannnerName=datum.getGalleryImages().get(finalI);
                                            in.happyhelp.limra.activity.response.couponresponse.JobCategory banners=realm.where(in.happyhelp.limra.activity.response.couponresponse.JobCategory.class).equalTo("image",BannnerName).findFirst();
                                            String url = banners.getLink();

                                            if(!url.equals("")) {
                                                Intent i = new Intent(Intent.ACTION_VIEW,
                                                        Uri.parse(url));
                                                startActivity(i);
                                            }*/
                                        }
                                    });
                            slider.addSlider(defaultSliderView);
                        }
                    }

                }else if(response.code()==401){
                    AppUtils.logout(ServiceDetailActivity.this);
                }else{
                   // Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_LONG).show();
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<CompanyDetailsResponse> call, Throwable t) {
                progressBar.dismiss(); View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
                t.printStackTrace();
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(ServiceDetailActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(ServiceDetailActivity.this,Id);

            }
        });



    }
}
