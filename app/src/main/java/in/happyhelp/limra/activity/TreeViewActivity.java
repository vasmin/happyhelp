package in.happyhelp.limra.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.EndlessRecyclerViewScrollListener;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.treeresponse.Result;
import in.happyhelp.limra.activity.response.treeresponse.TreeResponse;
import in.happyhelp.limra.adapter.TreeAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tellh.com.recyclertreeview_lib.TreeNode;
import tellh.com.recyclertreeview_lib.TreeViewAdapter;

public class TreeViewActivity extends AppCompatActivity {


    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    LinearLayoutManager linearLayoutManager;

    TreeAdapter treeAdapter;
    int totalPage = 0;
    List<TreeNode> nodes;

    List<Result> list=new ArrayList<>();
    boolean isPagination=false;
    boolean isMain=false;
    boolean isChild=false;
    String tid="";

    @BindView(R.id.data)
    TextView data;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.count)
    TextView count;

    @BindView(R.id.linear)
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tree_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            tid = extras.getString(Constants.TID);
            isChild = true;
            isPagination=false;
            getTreeView("1");
        }

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        AppUtils.isNetworkConnectionAvailable(this);


        nodes = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Log.e("page", "is" + String.valueOf(page));
                if (page < totalPage) {
                    isPagination=true;
                    getTreeView(String.valueOf(page));
                }
            }
        };

        treeAdapter = new TreeAdapter(this, new TreeAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(Result d, View view) throws ParseException {

                if(d.getCount()>0){
                    Intent intent=new Intent(getApplicationContext(),TreeViewActivity.class);
                    intent.putExtra(Constants.TID,d.getId());
                    startActivity(intent);
                }else{
                    Toast.makeText(TreeViewActivity.this, "Sorry No Referal Available", Toast.LENGTH_SHORT).show();
                }

            }
        });

        // loadHeroList();
        // getTree("1");
        getTreeView("1");
        recyclerView.addOnScrollListener(scrollListener);
        //initData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void getTreeView(String page) {
        HashMap<String, String> hashMap = new HashMap<>();
        //  hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));

        if(isChild){
            hashMap.put("userid", tid);
        }else{
            hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        }
        hashMap.put("page", page);

        Call<TreeResponse> call = RestClient.get().getTree(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(), hashMap);
        call.enqueue(new Callback<TreeResponse>() {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(@NonNull Call<TreeResponse> call, @NonNull Response<TreeResponse> response) {

                if (response.code() == 200) {
                    TreeResponse treeResponse = response.body();
                    if(isPagination) {
                        list.addAll(treeResponse.getResult());
                        treeAdapter.setData(list);
                    }else{
                        list.clear();
                        list.addAll(treeResponse.getResult());
                        treeAdapter.setData(list);
                    }


                    name.setText(treeResponse.getName());
                    count.setText(String.valueOf(treeResponse.getTotalcount()));

                    if(treeResponse.getResult().size()==0){
                        data.setVisibility(View.VISIBLE);
                        linearLayout.setVisibility(View.GONE);
                    }else{
                        data.setVisibility(View.GONE);
                        linearLayout.setVisibility(View.VISIBLE);
                    }


                    if(!isPagination) {
                        recyclerView.setAdapter(treeAdapter);
                    }
                    totalPage=treeResponse.getPagecount();

                } else if (response.code() == 401) {
                    AppUtils.logout(TreeViewActivity.this);
                } else {
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<TreeResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }







}
