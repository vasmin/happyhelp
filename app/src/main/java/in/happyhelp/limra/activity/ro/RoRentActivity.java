package in.happyhelp.limra.activity.ro;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.property.PropertyEnquiryActivity;
import in.happyhelp.limra.activity.response.serrviceresponse.ServiceBannerResponse;
import in.happyhelp.limra.adapter.RoAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.roresponse.Ro;
import in.happyhelp.limra.activity.response.roresponse.RoResponse;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoRentActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.data)
    TextView data;

    RoAdapter adapter;

    @BindView(R.id.slider)
    SliderLayout slider;

    Realm realm;
    List<String> listBanner=new ArrayList<>();
    RealmResults<Ro> roList;

    LinearLayoutManager linearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ro_rent);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);
        realm=RealmHelper.getRealmInstance();
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getRoList();

        swipeRefreshLayout.setOnRefreshListener(RoRentActivity.this);
        swipeRefreshLayout.setRefreshing(true);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);


        roList=realm.where(Ro.class).findAllAsync();
        roList.addChangeListener(new RealmChangeListener<RealmResults<Ro>>() {
            @Override
            public void onChange(@NonNull RealmResults<Ro> ros) {
                adapter=new RoAdapter(getApplicationContext(), new RoAdapter.OnItemClickListner() {
                    @Override
                    public void onItemClick(final Ro d, View view) {

                                Intent intent=new Intent(RoRentActivity.this,RoDetailActivity.class);
                                intent.putExtra(Constants.ROID,d.getId());
                                startActivity(intent);


                    }
                });
                adapter.setData(roList);
                recyclerView.setAdapter(adapter);
            }
        });

        slider.movePrevPosition(false);
        slider.setPresetTransformer(com.daimajia.slider.library.SliderLayout.Transformer.Accordion);
        slider.setPresetIndicator(com.daimajia.slider.library.SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(3000);
        slider.startAutoCycle();

        roBanner();

    }

    public void roBanner(){
        Call<ServiceBannerResponse> call=RestClient.get().getRoBanner(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<ServiceBannerResponse>() {
            @Override
            public void onResponse(@NonNull Call<ServiceBannerResponse> call, @NonNull Response<ServiceBannerResponse> response) {
                final ServiceBannerResponse serviceBannerResponse=response.body();

                if(response.code()==200){
                    if(serviceBannerResponse.getStatus()) {

                        listBanner.clear();
                        for(int i=0;i<serviceBannerResponse.getData().size();i++){
                            listBanner.add(serviceBannerResponse.getData().get(i).getImage());
                        }


                        for (int i = 0; i < listBanner.size(); i++) {
                            DefaultSliderView defaultSliderView = new DefaultSliderView(getApplicationContext());
                            final int finalI = i;
                            defaultSliderView.image(RestClient.base_image_url + listBanner.get(i))
                                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                        @Override
                                        public void onSliderClick(BaseSliderView slider) {
                                            String BannnerName = listBanner.get(finalI);
                            /*JobCategory banners=realm.where(JobCategory.class).equalTo("image",BannnerName).findFirst();
                            String url = banners.getLink();

                            if(!url.equals("")) {
                                Intent i = new Intent(Intent.ACTION_VIEW,
                                        Uri.parse(url));
                                startActivity(i);
                            }*/
                                        }
                                    });
                            slider.addSlider(defaultSliderView);
                        }
                    }


                }else if(response.code()==401){
                    AppUtils.logout(RoRentActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<ServiceBannerResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getRoList(){
        swipeRefreshLayout.setRefreshing(true);
        Call<RoResponse> call=RestClient.get().getRoList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<RoResponse>() {
            @Override
            public void onResponse(@NonNull Call<RoResponse> call, @NonNull Response<RoResponse> response) {
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    final RoResponse roResponse=response.body();
                    if(roResponse.getStatus()){
                        data.setVisibility(View.GONE);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                realm.delete(Ro.class);
                                realm.copyToRealmOrUpdate(roResponse.getData());
                            }
                        });
                    }else{
                        data.setVisibility(View.VISIBLE);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(RoRentActivity.this);
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                   // Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RoResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
              //  Toast.makeText(getApplicationContext(), Constants.SERVERTIMEOUT, Toast.LENGTH_LONG).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onRefresh() {
        getRoList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(RoRentActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(RoRentActivity.this,Id);

            }
        });



    }
}
