package in.happyhelp.limra.activity.Jobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.RegisterActivity;
import in.happyhelp.limra.activity.response.savedjobresponse.SavedJob;
import in.happyhelp.limra.activity.response.savedjobresponse.SavedListResponse;
import in.happyhelp.limra.adapter.JobAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shopping.ShoppyAddressActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyHRActivity extends AppCompatActivity {

    @BindView(R.id.appliedjobrecycler)
    RecyclerView appliedJobRecycler;

    @BindView(R.id.savedjob)
    RecyclerView savedJobRecycler;

    @BindView(R.id.saveddata)
    TextView saveData;

    @BindView(R.id.applieddata)
    TextView appliedData;

    JobAdapter savedAdapter,appliedAdapter;
    LinearLayoutManager linearLayoutManager,linearLayoutManager2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_hr);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        appliedJobRecycler.setLayoutManager(linearLayoutManager);
        appliedJobRecycler.setHasFixedSize(true);

        linearLayoutManager2 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        savedJobRecycler.setLayoutManager(linearLayoutManager2);
        savedJobRecycler.setHasFixedSize(true);

        savedAdapter=new JobAdapter(getApplicationContext(), new JobAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(SavedJob d, View view) throws ParseException {
                Intent intent=new Intent(CompanyHRActivity.this,JobDescriptionActivity.class);
                intent.putExtra(Constants.JOBID,d.getJobid());
                startActivity(intent);
            }
        });


        appliedAdapter=new JobAdapter(getApplicationContext(), new JobAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(SavedJob d, View view) throws ParseException {
                Intent intent=new Intent(CompanyHRActivity.this,JobDescriptionActivity.class);
                intent.putExtra(Constants.JOBID,d.getJobid());
                startActivity(intent);
            }
        });


        getSavedJobList();
        getAppliedJobList();

    }

    public void getSavedJobList(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<SavedListResponse> call= RestClient.get().getSavedJobList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<SavedListResponse>() {
            @Override
            public void onResponse(@NonNull Call<SavedListResponse> call, @NonNull Response<SavedListResponse> response) {
                SavedListResponse savedListResponse=response.body();
                if(response.code()==200){
                    if(savedListResponse.getStatus()){
                        savedAdapter.setData(savedListResponse.getData());
                        savedJobRecycler.setAdapter(savedAdapter);

                        if(savedListResponse.getData().size()==0){
                            saveData.setVisibility(View.VISIBLE);
                        }else{
                            saveData.setVisibility(View.GONE);
                        }
                    }else{
                        if(savedListResponse.getData().size()==0){
                            saveData.setVisibility(View.VISIBLE);
                        }else{
                            saveData.setVisibility(View.GONE);
                        }
                    }
                }else if(response.code()==401){
                    AppUtils.logout(CompanyHRActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<SavedListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }


    public void getAppliedJobList(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<SavedListResponse> call= RestClient.get().getAppliedJobList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<SavedListResponse>() {
            @Override
            public void onResponse(@NonNull Call<SavedListResponse> call, @NonNull Response<SavedListResponse> response) {
                SavedListResponse appliedresponse=response.body();
                if(response.code()==200){

                    appliedAdapter.setData(appliedresponse.getData());
                    appliedJobRecycler.setAdapter(appliedAdapter);

                    if(appliedresponse.getData().size()==0){
                        appliedData.setVisibility(View.VISIBLE);
                    }else{
                        appliedData.setVisibility(View.GONE);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(CompanyHRActivity.this);
                }else{
                    if(appliedresponse.getData().size()==0){
                        appliedData.setVisibility(View.VISIBLE);
                    }else{
                        appliedData.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<SavedListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(CompanyHRActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(CompanyHRActivity.this,Id);

            }
        });
    }


}
