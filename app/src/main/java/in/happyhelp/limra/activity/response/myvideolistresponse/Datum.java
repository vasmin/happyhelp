
package in.happyhelp.limra.activity.response.myvideolistresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("youtube_code")
    @Expose
    private String youtubeCode;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("views")
    @Expose
    private String views;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("plan")
    @Expose
    private String plan;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("planamount")
    @Expose
    private String planamount;
    @SerializedName("planstart")
    @Expose
    private String planstart;
    @SerializedName("planexpire")
    @Expose
    private String planexpire;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("date")
    @Expose
    private String date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYoutubeCode() {
        return youtubeCode;
    }

    public void setYoutubeCode(String youtubeCode) {
        this.youtubeCode = youtubeCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPlanamount() {
        return planamount;
    }

    public void setPlanamount(String planamount) {
        this.planamount = planamount;
    }

    public String getPlanstart() {
        return planstart;
    }

    public void setPlanstart(String planstart) {
        this.planstart = planstart;
    }

    public String getPlanexpire() {
        return planexpire;
    }

    public void setPlanexpire(String planexpire) {
        this.planexpire = planexpire;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
