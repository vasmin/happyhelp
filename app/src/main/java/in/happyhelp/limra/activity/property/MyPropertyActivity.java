package in.happyhelp.limra.activity.property;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.Jobs.JobProfileActivity;
import in.happyhelp.limra.activity.response.propertiesresponse.PropertyList;
import in.happyhelp.limra.activity.response.propertiesresponse.PropertyListResponse;
import in.happyhelp.limra.activity.video.MyVideoActivity;
import in.happyhelp.limra.adapter.AllPropertyAdapter;
import in.happyhelp.limra.adapter.JobTypeAdapter;
import in.happyhelp.limra.adapter.MyPropertyAdapter;
import in.happyhelp.limra.adapter.PropertyImagesAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPropertyActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.data)
    TextView data;

    LinearLayoutManager linearLayoutManager;
    MyPropertyAdapter myPropertyAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_property);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ButterKnife.bind(this);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);

        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        myPropertyAdapter=new MyPropertyAdapter(this, new MyPropertyAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(final PropertyList d, View view) throws ParseException {
                TextView delete=view.findViewById(R.id.delete);
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new AlertDialog.Builder(MyPropertyActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle("Are you sure you want to delete Property ?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        deleteProperties(d.getId());
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();

                    }
                });

                ImageView imageView=view.findViewById(R.id.imageView);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(MyPropertyActivity.this,PostPropertyActivity.class);
                        intent.putExtra(Constants.PROPERTYID,d.getId());
                        startActivity(intent);
                    }
                });

                LinearLayout linearLayout=view.findViewById(R.id.linear);
                linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(MyPropertyActivity.this,PostPropertyActivity.class);
                        intent.putExtra(Constants.PROPERTYID,d.getId());
                        startActivity(intent);
                    }
                });

            }
        });
        getMyProperties();
    }

    public void deleteProperties(String propertyId){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("property_id",propertyId);
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get(RestClient.PROPERTY_BASE_URL).deleteProperty(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(response1.getStatus()){
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                        getMyProperties();
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }

                }else if(response.code()==401){
                    AppUtils.logout(MyPropertyActivity.this);

                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMyProperties();
        EventBus.getDefault().unregister(this);
    }

    public void getMyProperties(){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<PropertyListResponse> call= RestClient.get(RestClient.PROPERTY_BASE_URL).getMyProperties(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<PropertyListResponse>() {
            @Override
            public void onResponse(@NonNull Call<PropertyListResponse> call, @NonNull Response<PropertyListResponse> response) {
                swipeRefreshLayout.setRefreshing(false);
                PropertyListResponse propertyListResponse=response.body();
                if(response.code()==200){
                    if(propertyListResponse.getStatus()){
                        myPropertyAdapter.setData(propertyListResponse.getPropertyList());
                        recyclerView.setAdapter(myPropertyAdapter);

                        if(propertyListResponse.getPropertyList().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }
                    }else{
                        data.setVisibility(View.VISIBLE);
                    }

                }else if(response.code()==401){
                    AppUtils.logout(MyPropertyActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<PropertyListResponse> call, @NonNull Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }
    @Override
    public void onRefresh() {
        getMyProperties();
    }



    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MyPropertyActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MyPropertyActivity.this,Id);

            }
        });
    }
}
