package in.happyhelp.limra.activity.service;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.ConfirmationActivity;
import in.happyhelp.limra.activity.HomeActivity;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.gstresponse.GstRateResponse;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.myservicesresponse.MyServices;
import in.happyhelp.limra.activity.response.myservicesresponse.MyServicesResponse;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsActivity extends AppCompatActivity {

    @BindView(R.id.orderid)
    TextView orderId;

    @BindView(R.id.amount)
    TextView amount;

    @BindView(R.id.address)
    TextView address;

    @BindView(R.id.status)
    TextView status;

    @BindView(R.id.paymentlinear)
    LinearLayout paymentLinear;

    @BindView(R.id.cancel)
    TextView cancel;

    @BindView(R.id.cash)
    RadioButton cash;

    @BindView(R.id.online)
    RadioButton online;

    @BindView(R.id.payment)
    TextView payment;

    @BindView(R.id.paymentstatus)
    TextView paymentstatus;

    @BindView(R.id.paymentmode)
    LinearLayout paymentMode;

    @BindView(R.id.wallet)
    LinearLayout wallet;

    @BindView(R.id.process)
    ImageView process;

    String email;
    String mobile;

    @BindView(R.id.date)
    TextView date;

    @BindView(R.id.time)
    TextView time;

    String orderID="";
    String paymentStatus="0";
   // ProgressDialog progressBar;


    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.mobile)
    TextView mobileNo;

    @BindView(R.id.otplinear)
    LinearLayout otpLinear;

    @BindView(R.id.otp)
    TextView otp;

    @BindView(R.id.vendorname)
    TextView vendorname;

    @BindView(R.id.vendorlinear)
    LinearLayout vendorLinear;

    @BindView(R.id.service)
    TextView service;

    @BindView(R.id.servicetype)
    TextView serviceType;

    @BindView(R.id.visiting)
    TextView visitingCharge;

    @BindView(R.id.visitinglinear)
    LinearLayout visitingLinear;

    @BindView(R.id.cashwallet)
    CheckBox cashwallet;

    @BindView(R.id.cashbackwallet)
    CheckBox cashbackwallet;

    @BindView(R.id.walletamt)
    TextView walletAmt;

    @BindView(R.id.cashbackwalletamt)
    TextView cashbackwalletAmt;


    @BindView(R.id.gstamt)
    TextView gstAmount;

    @BindView(R.id.gst)
    TextView gst;

    @BindView(R.id.cashbackwallet_amt)
    TextView cashbackwalletamt;

    @BindView(R.id.cashwalletamt)
    TextView cashwalletamt;

    @BindView(R.id.cashlinear)
    LinearLayout cashLinear;

    @BindView(R.id.cashbacklinear)
    LinearLayout cashbacklinear;



    double result = 0;

    @BindView(R.id.gstlinear)
    LinearLayout gstLinear;

    double payamount=0;
    double cashwalletAmt=0;
    double cashbackWalletAmt=0;
    String totalamount="0";
    private  Handler handler = new Handler();

    boolean isClick=false;
    boolean isSettled=false;

    @BindView(R.id.walletlinear)
    LinearLayout walletLinear;

    @BindView(R.id.summary)
            LinearLayout summary;

    int gstRate=0;
    boolean isSelected=false;

    @BindView(R.id.area)
    TextView area;

    @BindView(R.id.quantity)
    TextView quantity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Glide.with(getApplicationContext())
                //.load("https://media.giphy.com/media/sSgvbe1m3n93G/giphy.gif")
                .load(R.drawable.giphy)
                .into(process);



        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            orderID = extras.getString(Constants.ORDERID);
            getMyOrder();
        }


        getGstRate();
        getCashbackUsage();
        doTheAutoRefresh();


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelOrder();
            }
        });

        cashwallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    if(payamount>0) {
                        useWalletApi("0");


                    }else{
                        cashwallet.setChecked(false);
                    }
                }else{
                   /* payamount=payamount+cashwalletAmt;

                    Log.e("Cash","is "+payamount+" "+cashwalletAmt+" "+cashbackWalletAmt);

                    cashwalletAmt=0;

                    if(payamount<0){
                        payment.setText("Pay ");
                        wallet.setVisibility(View.GONE);
                    }else {
                        payment.setText("Pay " + payamount);
                    }*/
                   cashwalletAmt=0;
                   process.setVisibility(View.VISIBLE);
                   cashLinear.setVisibility(View.GONE);
                   getMyOrder();
                }
            }
        });

        cashbackwallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    if(payamount>0) {
                        useWalletApi("1");
                        cashLinear.setVisibility(View.VISIBLE);
                        cashwalletamt.setText("\u20B9 "+ String.valueOf(payamount));
                    }else{
                        cashbackwallet.setChecked(false);
                    }
                }else{
                   /* payamount=payamount+cashbackWalletAmt;
                    Log.e("Cashback","is "+payamount+" "+cashwalletAmt+" "+cashbackWalletAmt);
                    cashbackWalletAmt=0;
                    if(payamount<0){
                        payment.setText("Pay ");
                        wallet.setVisibility(View.GONE);
                    }else {
                        payment.setText("Pay " + payamount);
                    }*/
                    cashbackWalletAmt=0;
                   process.setVisibility(View.VISIBLE);
                    cashbacklinear.setVisibility(View.GONE);
                   getMyOrder();
                }

            }
        });



        cash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(cash.isChecked()){
                    paymentStatus="0";
                    gstLinear.setVisibility(View.GONE);
                }else{
                    paymentStatus="1";
                }
                process.setVisibility(View.VISIBLE);
                getMyOrder();
            }
        });

        online.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(online.isChecked()){
                    paymentStatus="1";
                }else{
                    paymentStatus="0";
                    gstLinear.setVisibility(View.GONE);
                }
                process.setVisibility(View.VISIBLE);
                getMyOrder();
            }
        });

        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isClick) {
                    if (paymentStatus.equals("1")) {
                        callInstamojoPay(email, mobile, totalamount, "Service Booking");
                    } else if(paymentStatus.equals("2")) {
                        updatePayment("1");
                    }else{
                        updatePayment("0");
                    }
                    isClick=true;
                }
            }
        });
    }


    @Override
    protected void onResumeFragments() {
        getMyOrder();
        getGstRate();
        super.onResumeFragments();
    }

    @SuppressLint("SetTextI18n")
    public void setDataVisible(){

        if(isSelected){
            gstLinear.setVisibility(View.VISIBLE);
            result = (double) (payamount * gstRate) / 100;
            totalamount= String.valueOf(payamount+result);
            gstAmount.setText("\u20B9 "+String.valueOf(result));
            gst.setText("Exclusive Gst ("+gstRate+"% )");
            payment.setText("Pay "+"\u20B9 "+totalamount);
        }else{
            gstLinear.setVisibility(View.GONE);
            totalamount= String.valueOf(Double.parseDouble(totalamount)-result);
            payment.setText("Pay "+"\u20B9 "+payamount);
        }
    }

    public void getCashbackUsage(){
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().get_cashback_charges(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){

                    }else{

                    }

                }else if(response.code()==401){
                    AppUtils.logout(OrderDetailsActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<in.happyhelp.limra.activity.response.Response> call, Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void getGstRate(){
        Call<GstRateResponse> call=RestClient.get().getGstRate(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<GstRateResponse>() {
            @Override
            public void onResponse(Call<GstRateResponse> call, Response<GstRateResponse> response) {
                if(response.code()==200){
                    GstRateResponse gstRateResponse=response.body();
                    if(gstRateResponse.getStatus()) {
                        if (gstRateResponse.getGst().size() >= 7) {
                            gstRate = Integer.parseInt(gstRateResponse.getGst().get(7).getName());
                        }
                    }else{
                        gstRate=0;
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getApplicationContext());
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<GstRateResponse> call, Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void useWalletApi(String walletType){
        process.setVisibility(View.VISIBLE);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("amount", String.valueOf(payamount));
        hashMap.put("wallet_type",walletType);
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().useWallet(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                process.setVisibility(View.GONE);
                if(response.code()==200){
                    if(response1.getStatus()) {
                        if (walletType.equals("0")) {
                            cashwalletAmt= Double.parseDouble(response1.getUsevalue());
                            payamount=payamount-cashwalletAmt;
                            payment.setText("Pay "+"\u20B9 "+payamount);

                            if(payamount==0.0){
                                isSettled=true;
                                //wallet used
                                paymentStatus="2";
                                paymentMode.setVisibility(View.GONE);
                                gstLinear.setVisibility(View.GONE);
                            }else{
                                isSettled=false;
                                paymentStatus="2";
                                paymentMode.setVisibility(View.VISIBLE);
                                gstLinear.setVisibility(View.VISIBLE);
                            }
                        }else{
                            cashbackWalletAmt= Double.parseDouble(response1.getUsevalue());
                            payamount=payamount-cashbackWalletAmt;
                            payment.setText("Pay "+"\u20B9 "+payamount);

                            if(payamount==0.0){
                                isSettled=true;
                                //wallet used
                                paymentStatus="2";
                                paymentMode.setVisibility(View.GONE);
                                gstLinear.setVisibility(View.GONE);
                            }else{
                                isSettled=false;
                                paymentMode.setVisibility(View.VISIBLE);
                                gstLinear.setVisibility(View.VISIBLE);
                            }
                        }


                        if(cashwallet.isChecked()) {
                            cashLinear.setVisibility(View.VISIBLE);
                            cashwalletamt.setText("\u20B9 "+ String.valueOf(cashwalletAmt));
                        }else{
                            cashLinear.setVisibility(View.GONE);
                        }

                        if(cashbackwallet.isChecked()) {
                            cashbacklinear.setVisibility(View.VISIBLE);
                            cashbackwalletamt.setText("\u20B9 "+ String.valueOf(cashbackWalletAmt));
                        }else{
                            cashbacklinear.setVisibility(View.GONE);
                        }

                    }else{
                        Toast.makeText(OrderDetailsActivity.this,response1.getMessage(), Toast.LENGTH_SHORT).show();
                        if(walletType.equals("0")){
                            cashwallet.setChecked(false);
                        }else{
                            cashbackwallet.setChecked(false);
                        }
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getApplicationContext());
                }else{
                    Toast.makeText(OrderDetailsActivity.this, Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<in.happyhelp.limra.activity.response.Response> call, Throwable t) {
                Toast.makeText(OrderDetailsActivity.this, Constants.SERVERTIMEOUT, Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }


    private void doTheAutoRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getMyOrder();
                    }
                });
            }
        }, 5000);
    }



    private void callInstamojoPay(String email, String phone, String amount, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", "Service Booking");
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);

                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();

                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);

                updatePayment(payment);
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }


    public void updatePayment(String payment){
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("lastid",orderID);
        if(payment.equals("0") ){
            hashMap.put("paid","0");
        }else if(payamount==0.0){
            hashMap.put("paid","1");
        }else{
            hashMap.put("paid","1");
        }
        hashMap.put("payment_id",payment);

        hashMap.put("payment_mode",paymentStatus);

        hashMap.put("status","5");
        hashMap.put("amount", String.valueOf(payamount));
        hashMap.put("wallet_amount", String.valueOf(cashwalletAmt));
        hashMap.put("cashback_amount", String.valueOf(cashbackWalletAmt));
        hashMap.put("gst", String.valueOf(gstRate));
        hashMap.put("gstamt", String.valueOf(result));


        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().servicePaymentUpdate(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        Intent intent=new Intent(OrderDetailsActivity.this,ConfirmationActivity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(getApplicationContext(),response1.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(OrderDetailsActivity.this);
                }else{
                    Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_SHORT).show();
            }
        });


    }


    public void  cancelOrder(){
        //progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",orderID);
        hashMap.put("status","6");
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().serviceCancel(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
               // progressBar.dismiss();
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus())
                    {
                        Toast.makeText(OrderDetailsActivity.this, response1.getMessage(), Toast.LENGTH_SHORT).show();
                        getMyOrder();
                    }else{
                        Toast.makeText(OrderDetailsActivity.this, response1.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(OrderDetailsActivity.this, Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
               // progressBar.dismiss();
                Toast.makeText(OrderDetailsActivity.this, Constants.SERVERTIMEOUT, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(OrderDetailsActivity.this, HomeActivity.class));
        finishAffinity();
    }

    public void getMyOrder(){

        /*progressBar=new ProgressDialog(OrderDetailsActivity.this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);
        progressBar.show();*/


        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("id",orderID);
        Call<MyServicesResponse> call= RestClient.get().MyOrder(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<MyServicesResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<MyServicesResponse> call, @NonNull Response<MyServicesResponse> response) {
               // progressBar.dismiss();
                doTheAutoRefresh();
                process.setVisibility(View.GONE);
                MyServicesResponse myServicesResponse=response.body();
                if(response.code()==200){
                    if(myServicesResponse.getStatus()){
                        MyServices myServices=myServicesResponse.getData().get(0);
                        amount.setText("Rs "+myServices.getAmount());
                        payamount= Double.parseDouble(myServices.getAmount());
                        address.setText(myServices.getAddress());
                        area.setText(myServices.getLocation());
                        mobileNo.setText(myServices.  getMobile());
                        name.setText(myServices.getName());
                        email=myServices.getEmail();
                        mobile=myServices.getMobile();

                        quantity.setText(myServices.getQuantity());

                        date.setText(myServices.getDate());
                        if(myServices.getVendorname()==null || myServices.getVendorname().equals("")) {
                            vendorLinear.setVisibility(View.GONE);
                        }else{
                            vendorname.setText(myServices.getVendorname());
                            vendorLinear.setVisibility(View.VISIBLE);
                        }

                        walletAmt.setText(myServices.getWallet());
                        cashbackwalletAmt.setText(myServices.getCashback_wallet());

                        String services="";
                        for(int j=0;j<myServices.getServices().size();j++) {
                            if(j==0){
                                services =  myServices.getServices().get(j).getName();
                            }else {
                                services = services + "\n" + myServices.getServices().get(j).getName();
                            }
                        }
                        service.setText(services);

                        if(myServices.getHour().equals("48")){
                            serviceType.setText(" Schedule Service");
                        }else{
                            serviceType.setText(myServices.getHour()+" Hours");
                        }
                        if(myServices.getVisitingCharge().equals("")||myServices.getVisitingCharge()==null){
                            visitingLinear.setVisibility(View.GONE);
                        }else{
                            visitingLinear.setVisibility(View.GONE);
                            visitingCharge.setText("Rs "+myServices.getVisitingCharge());
                        }

                        time.setText(myServices.getTime());
                        orderId.setText(myServices.getId());

                        if(myServices.getPaymentMode().equals("0")) {
                            paymentstatus.setText("Cash On Delivery");
                        }else if(myServices.getPaymentMode().equals("2")) {
                            paymentstatus.setText("Wallet Used");
                        }else{
                            paymentstatus.setText("Online ");
                        }


                        if(myServices.getPaid().equals("1")){
                            paymentLinear.setVisibility(View.VISIBLE);
                            wallet.setVisibility(View.GONE);
                            walletLinear.setVisibility(View.VISIBLE);
                        }else{
                            paymentLinear.setVisibility(View.VISIBLE);
                            wallet.setVisibility(View.VISIBLE);
                            walletLinear.setVisibility(View.GONE);
                        }

                        switch (myServices.getStatus()) {
                            case "0":
                                status.setText("Processing");
                                paymentLinear.setVisibility(View.GONE);
                                isSettled = true;
                                wallet.setVisibility(View.GONE);
                                status.setTextColor(Color.RED);
                                cancel.setVisibility(View.VISIBLE);
                                break;
                            case "1":
                                isSettled = false;
                                status.setText("Vendor Accepted ");
                                status.setTextColor(getResources().getColor(R.color.green));
                                paymentMode.setVisibility(View.VISIBLE);
                                wallet.setVisibility(View.VISIBLE);
                                payment.setVisibility(View.VISIBLE);
                                process.setVisibility(View.GONE);
                                otpLinear.setVisibility(View.GONE);
                                break;
                            case "2":
                                status.setText("Completed");
                                isSettled = true;
                                paymentMode.setVisibility(View.GONE);
                                wallet.setVisibility(View.GONE);
                                status.setTextColor(getResources().getColor(R.color.green));
                                cancel.setVisibility(View.GONE);
                                payment.setVisibility(View.GONE);
                                process.setVisibility(View.GONE);
                                otpLinear.setVisibility(View.GONE);

                                break;
                            case "3":
                                status.setText("Order Confirm by Vendor");
                                isSettled = true;
                                paymentMode.setVisibility(View.GONE);
                                wallet.setVisibility(View.GONE);
                                status.setTextColor(getResources().getColor(R.color.green));
                                cancel.setVisibility(View.GONE);
                                payment.setVisibility(View.GONE);
                                process.setVisibility(View.GONE);
                                otpLinear.setVisibility(View.GONE);
                                wallet.setVisibility(View.GONE);

                                break;
                            case "4":
                                status.setText("Order Rejected by Vendor");
                                isSettled = true;
                                visitingLinear.setVisibility(View.VISIBLE);
                                paymentMode.setVisibility(View.GONE);
                                wallet.setVisibility(View.GONE);
                                status.setTextColor(Color.RED);
                                cancel.setVisibility(View.GONE);
                                payment.setVisibility(View.GONE);
                                process.setVisibility(View.GONE);
                                otpLinear.setVisibility(View.GONE);
                                wallet.setVisibility(View.GONE);
                                isSettled = true;

                                break;
                            case "5":
                                status.setText("Order Confirm ");
                                isSettled = true;
                                gstLinear.setVisibility(View.GONE);
                                cancel.setVisibility(View.GONE);
                                payment.setVisibility(View.GONE);
                                paymentMode.setVisibility(View.GONE);
                                wallet.setVisibility(View.GONE);
                                status.setTextColor(getResources().getColor(R.color.green));
                                process.setVisibility(View.GONE);
                                otpLinear.setVisibility(View.GONE);

                                break;
                            case "6":
                                status.setText("Order Cancel by User");
                                status.setTextColor(Color.RED);
                                cancel.setVisibility(View.GONE);
                                process.setVisibility(View.GONE);
                                payment.setVisibility(View.GONE);
                                otpLinear.setVisibility(View.GONE);
                                wallet.setVisibility(View.GONE);
                                isSettled = true;

                                break;
                            case "7":
                                isSettled = true;
                                status.setText("Vendor Reach Your location");
                                status.setTextColor(getResources().getColor(R.color.green));
                                cancel.setVisibility(View.GONE);
                                process.setVisibility(View.GONE);
                                payment.setVisibility(View.GONE);
                                otpLinear.setVisibility(View.GONE);
                                walletLinear.setVisibility(View.GONE);
                                wallet.setVisibility(View.GONE);

                                break;
                            case "8":
                                isSettled = true;
                                status.setText("Vendor Reach Your Location.");
                                status.setTextColor(getResources().getColor(R.color.green));
                                cancel.setVisibility(View.GONE);
                                process.setVisibility(View.GONE);
                                payment.setVisibility(View.GONE);
                                otpLinear.setVisibility(View.VISIBLE);
                                wallet.setVisibility(View.GONE);
                                otp.setText(myServices.getOtp());
                                break;

                            case "9":
                                isSettled = true;
                                status.setText("Order Cancel due to Vendor not Reach..! Please wait we will contact you soon..");
                                status.setTextColor(Color.RED);
                                cancel.setVisibility(View.GONE);
                                process.setVisibility(View.GONE);
                                payment.setVisibility(View.GONE);
                                otpLinear.setVisibility(View.GONE);
                                break;

                            case "10":
                                isSettled = true;
                                status.setText("Service request cancel due to service provider not available..! Please Select other or Please Try again..");
                                status.setTextColor(Color.RED);
                                cancel.setVisibility(View.GONE);
                                process.setVisibility(View.GONE);
                                payment.setVisibility(View.GONE);
                                otpLinear.setVisibility(View.GONE);
                                break;

                        }

                        if(cashwallet.isChecked()){
                            payamount=payamount-cashwalletAmt;
                        }


                        if(cashbackwallet.isChecked()){
                            payamount=payamount-cashbackWalletAmt;
                        }

                        if(payamount==0.0){
                            isSettled=true;
                        }

                        payment.setText("Pay "+"\u20B9 "+payamount);


                        if(online.isChecked()){
                            if(payamount>0) {
                                gstLinear.setVisibility(View.VISIBLE);
                            }
                            result = (double) (payamount * gstRate) / 100;
                            totalamount= String.valueOf(payamount+result);
                            gstAmount.setText("\u20B9 "+String.valueOf(result));
                            gst.setText("Gst ("+gstRate+"% )");
                            payment.setText("Pay "+"\u20B9 "+totalamount);
                            paymentStatus="1";

                        }else if(cash.isChecked()){

                            gstLinear.setVisibility(View.GONE);
                            totalamount= String.valueOf(Double.parseDouble(totalamount)-result);
                            payment.setText("Pay "+"\u20B9 "+payamount);
                            Log.e("Payment","Gst is  "+totalamount );
                            paymentStatus="0";
                        }else{
                            payment.setText("Pay "+"\u20B9 "+payamount);
                            paymentStatus="2";
                        }


                        Log.e("Payment","is "+payamount );

                       // setDataVisible();

                        if(isSettled){
                            paymentMode.setVisibility(View.GONE);
                        }else{
                            paymentMode.setVisibility(View.VISIBLE);
                        }


                        if(walletLinear.getVisibility() == View.VISIBLE || cashbacklinear.getVisibility()==View.VISIBLE){
                            summary.setVisibility(View.VISIBLE);
                        }else if(walletLinear.getVisibility() == View.GONE && cashbacklinear.getVisibility()==View.GONE){
                            summary.setVisibility(View.GONE);
                        }


                    }else{

                    }

                }else if(response.code()==401){
                    AppUtils.logout(OrderDetailsActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<MyServicesResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
               // progressBar.dismiss();
                doTheAutoRefresh();
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isClick=false;
        EventBus.getDefault().register(this);
        doTheAutoRefresh();
    }



    @Override
    protected void onPause() {
        handler.removeCallbacksAndMessages(null);
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);

                AppUtils.setDialog(OrderDetailsActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(OrderDetailsActivity.this,Id);

            }
        });





    }



}
