package in.happyhelp.limra.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.service.MyServiceDetailsActivity;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.loginresponse.LoginResponse;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KycActivity extends AppCompatActivity {

    private static final int GALLERY_PANCARD = 456;
    private static final int REQUEST_IMAGE_CAPTURE_PANCARD = 789;
    private static final int GALLERY_ADHARDCARDFRONT = 753;
    private static final int REQUEST_IMAGE_CAPTURE_ADHARCARDFRONT = 951;
    private static final int GALLERY_ADHARDCARDBACK = 357;
    private static final int REQUEST_IMAGE_CAPTURE_ADHARCARDBACK = 865;
    private static final int GALLERY_CANCELCHECK = 836;
    private static final int REQUEST_IMAGE_CAPTURE_CANCELCHECk = 922;
    @BindView(R.id.pancard)
    EditText panCard;

    @BindView(R.id.adharcard)
    EditText adharCard;

    @BindView(R.id.bankname)
    EditText bankName;

    @BindView(R.id.branch)
    EditText branch;

    @BindView(R.id.bankaccount)
    EditText accountNo;

    @BindView(R.id.ifsc)
    EditText ifsc;

    @BindView(R.id.gst)
    EditText gstNo;


    @BindView(R.id.pancardlinear)
    LinearLayout pancardLinear;

    @BindView(R.id.adharcardlinear)
    LinearLayout adharcardfrontLinear;

    @BindView(R.id.adharcardbacklinear)
    LinearLayout adharcardBackLinear;

    @BindView(R.id.cancelcheck)
    LinearLayout cancelCheck;

    @BindView(R.id.submit)
    TextView submit;
    ProgressDialog progressBar;

    Realm realm;

    String path,adharcardfrontpath,adharcardbackpath,cancelCheckpath;

    @BindView(R.id.pancardimage)
    ImageView pancardImage;

    @BindView(R.id.adharcardfront)
    ImageView adharcardFrontImage;

    @BindView(R.id.adharcardback)
    ImageView adharcardBackImage;

    @BindView(R.id.checkimage)
            ImageView checkImage;


    String panName =" ",adharfrontName=" ",AdharBackName=" ";
    String cancelCheque="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kyc);

        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressBar=new ProgressDialog(KycActivity.this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kycUpload();
            }
        });



        pancardLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File filepath=new File(Environment.getExternalStorageDirectory()+Constants.imagePath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }

                path=filepath.getAbsolutePath()+"/"+System.currentTimeMillis()+"pancard.jpg";


                AppUtils.startPickImageDialog(GALLERY_PANCARD, REQUEST_IMAGE_CAPTURE_PANCARD, path,KycActivity.this);
            }
        });

        adharcardfrontLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File filepath=new File(Environment.getExternalStorageDirectory()+Constants.imagePath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }

                adharcardfrontpath=filepath.getAbsolutePath()+"/"+System.currentTimeMillis()+"adharcardfront.jpg";

                AppUtils.startPickImageDialog(GALLERY_ADHARDCARDFRONT, REQUEST_IMAGE_CAPTURE_ADHARCARDFRONT, adharcardfrontpath,KycActivity.this);
            }
        });

        adharcardBackLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File filepath=new File(Environment.getExternalStorageDirectory()+Constants.imagePath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }

                adharcardbackpath=filepath.getAbsolutePath()+"/"+System.currentTimeMillis()+"adharcardback.jpg";


                AppUtils.startPickImageDialog(GALLERY_ADHARDCARDBACK, REQUEST_IMAGE_CAPTURE_ADHARCARDBACK, adharcardbackpath,KycActivity.this);
            }
        });

        cancelCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File filepath=new File(Environment.getExternalStorageDirectory()+Constants.imagePath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }

                cancelCheckpath=filepath.getAbsolutePath()+"/"+System.currentTimeMillis()+"cancelCheck.jpg";


                AppUtils.startPickImageDialog(GALLERY_CANCELCHECK, REQUEST_IMAGE_CAPTURE_CANCELCHECk, cancelCheckpath,KycActivity.this);
            }
        });

        realm= RealmHelper.getRealmInstance();


        progressBar=new ProgressDialog(KycActivity.this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

           getUserProfile();

        RealmResults<UserData> userData=realm.where(UserData.class).findAllAsync();
        userData.addChangeListener(new RealmChangeListener<RealmResults<UserData>>() {
            @Override
            public void onChange(@NonNull RealmResults<UserData> userData) {
                if(userData.size()>0) {
                    bankName.setText(userData.get(0).getBank_name());

                    branch.setText(userData.get(0).getBank_branch_name());
                    accountNo.setText(userData.get(0).getBank_acc_no());
                    ifsc.setText(userData.get(0).getIfsc_code());
                    gstNo.setText(userData.get(0).getGst_no());



                    panName=userData.get(0).getPan_no();
                    adharfrontName=userData.get(0).getAadhar_front();
                    AdharBackName=userData.get(0).getAadhar_back();
                    cancelCheque=userData.get(0).getCancel_check();


                    Glide.with(getApplicationContext())
                            .load(RestClient.base_image_url+userData.get(0).getPan_no())
                            .thumbnail(0.5f)
                            .into(pancardImage);

                    Glide.with(getApplicationContext())
                            .load(RestClient.base_image_url+userData.get(0).getAadhar_front())
                            .thumbnail(0.5f)
                            .into(adharcardFrontImage);

                    Glide.with(getApplicationContext())
                            .load(RestClient.base_image_url+userData.get(0).getAadhar_back())
                            .thumbnail(0.5f)
                            .into(adharcardBackImage);

                    Glide.with(getApplicationContext())
                            .load(RestClient.base_image_url+userData.get(0).getCancel_check())
                            .thumbnail(0.5f)
                            .into(checkImage);

                }


            }
        });

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( resultCode==RESULT_OK)

            if(requestCode==REQUEST_IMAGE_CAPTURE_PANCARD)
            {   if(path!=null) {
                File newFile = new File(path);
                setImageFromPath(pancardImage, newFile.getPath());
            }
            }
            else if(requestCode==GALLERY_PANCARD){
                Uri imageUri = data.getData();
                path = getPath(getApplicationContext(), imageUri);
                setImageFromPath(pancardImage,path);
            }
            else if (requestCode == REQUEST_IMAGE_CAPTURE_ADHARCARDFRONT) {
                if(adharcardfrontpath!=null) {
                    File newFile = new File(adharcardfrontpath);

                    setImageFromPath(adharcardFrontImage, newFile.getPath());
                }
            }
            else if(requestCode==GALLERY_ADHARDCARDFRONT){
                Uri imageUri = data.getData();
                adharcardfrontpath = getPath(getApplicationContext(), imageUri);
                setImageFromPath(adharcardFrontImage,adharcardfrontpath);
            }
            else if (requestCode == REQUEST_IMAGE_CAPTURE_ADHARCARDBACK) {
                if(adharcardbackpath!=null) {
                    File newFile = new File(adharcardbackpath);

                    setImageFromPath(adharcardBackImage, newFile.getPath());
                }
            }
            else if(requestCode==GALLERY_ADHARDCARDBACK){
                Uri imageUri = data.getData();
                adharcardbackpath = getPath(getApplicationContext(), imageUri);
                setImageFromPath(adharcardBackImage,adharcardbackpath);
            }
            else if (requestCode == REQUEST_IMAGE_CAPTURE_CANCELCHECk) {
                if(cancelCheckpath!=null) {
                    File newFile = new File(cancelCheckpath);

                    setImageFromPath(checkImage, newFile.getPath());
                }
            }
            else if(requestCode==GALLERY_CANCELCHECK){
                Uri imageUri = data.getData();
                cancelCheckpath = getPath(getApplicationContext(), imageUri);
                setImageFromPath(checkImage,cancelCheckpath);
            }
    }

    private String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "";
            Toast.makeText(getApplicationContext(),"Sorry your records is not created",Toast.LENGTH_LONG).show();
        }
        return result;
    }


    private void setImageFromPath(ImageView image, String path){
        if(!TextUtils.isEmpty(path)&&!path.equals("")) {
            File imgFile = new File(path);
            if (imgFile.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize=8;      // 1/8 of original image
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(),options);
                image.setImageBitmap(myBitmap);
                image.setVisibility(View.VISIBLE);
            }else {
                image.setVisibility(View.GONE);
            }
        }else {
            image.setVisibility(View.GONE);
        }
    }

   /* public void getUserProfile(){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("vendorid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.VENDORID));
        Call<ProfileResponse> call=RestClient.get().profileDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProfileResponse> call, @NonNull Response<ProfileResponse> response) {
                final ProfileResponse profileResponse=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(profileResponse.getStatus()){
                        Company data=profileResponse.getData();
                        panCard.setText(data.getPanNo());
                        // adharCard.setText(dataList.get(0).getAadharNo());
                        bankName.setText(data.getBankName());
                        branch.setText(data.getBankBranchName());
                        accountNo.setText(data.getBankAccNo());
                        gstNo.setText(data.getGstNo());
                        ifsc.setText(data.getIfscCode());
                        panName=data.getPanNo();
                        adharfrontName=data.getAadharFront();
                        AdharBackName=data.getAadharBack();

                        Glide.with(getApplicationContext())
                                .load(RestClient.base_image_url+panName)
                                .thumbnail(0.5f)
                                .into(pancardImage);
                        Glide.with(getApplicationContext())
                                .load(RestClient.base_image_url+adharfrontName)
                                .thumbnail(0.5f)
                                .into(adharcardFrontImage);

                        Glide.with(getApplicationContext())
                                .load(RestClient.base_image_url+AdharBackName)
                                .thumbnail(0.5f)
                                .into(adharcardBackImage);

                    }else{
                        Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ProfileResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_SHORT).show();
            }
        });
    }*/

    public void kycUpload(){
       /* if(TextUtils.isEmpty(panCard.getText().toString())){
            panCard.setError("Enter Pancard number");
            return;
        }

        if(TextUtils.isEmpty(adharCard.getText().toString())){
            adharCard.setError("Enter adharCard number");
            return;
        }*/


        if(TextUtils.isEmpty(bankName.getText().toString())){
            bankName.setError("Enter bankName ");
            return;
        }
        if(TextUtils.isEmpty(branch.getText().toString())){
            branch.setError("Enter branch");
            return;
        }

        if(TextUtils.isEmpty(accountNo.getText().toString())){
            accountNo.setError("Enter account number");
            return;
        }

        if(!AppUtils.isBankAccount(accountNo.getText().toString(),accountNo)){
            accountNo.setError("Enter account number");
            return;
        }


        if(TextUtils.isEmpty(ifsc.getText().toString())){
            ifsc.setError("Enter ifsc number");
            return;
        }


        progressBar.show();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String vendorId=SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID);
        builder.addFormDataPart("userid", vendorId);
        builder.addFormDataPart("ifsc", ifsc.getText().toString());
        builder.addFormDataPart("gst", gstNo.getText().toString());
        builder.addFormDataPart("accountno", accountNo.getText().toString());
        builder.addFormDataPart("branchname", branch.getText().toString());
        builder.addFormDataPart("bankname", bankName.getText().toString());
        builder.addFormDataPart("bankname", bankName.getText().toString());

        if(panName!=null) {
            builder.addFormDataPart("pan_noold", panName);
        }

        if(adharfrontName!=null) {
            builder.addFormDataPart("aadhar_frontold", adharfrontName);
        }

        if(AdharBackName!=null) {
            builder.addFormDataPart("aadhar_backold", AdharBackName);
        }

        if(cancelCheque!=null) {
            builder.addFormDataPart("cancel_check_old", cancelCheque);
        }


        if(adharcardfrontpath!=null) {
            File adharfrontpath = new File(adharcardfrontpath);
            builder.addFormDataPart("aadhar_front", adharfrontpath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), adharfrontpath));
        }

        if(adharcardbackpath!=null) {
            File adharbackpath = new File(adharcardbackpath);
            builder.addFormDataPart("aadhar_back", adharbackpath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), adharbackpath));
        }

        if(path!=null) {
            File panpath = new File(path);
            builder.addFormDataPart("pan_no", panpath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), panpath));
        }

        if(cancelCheckpath!=null) {
            File cancel_check = new File(cancelCheckpath);
            builder.addFormDataPart("cancel_check", cancel_check.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), cancel_check));
        }

        final MultipartBody requestBody = builder.build();

        Call<in.happyhelp.limra.activity.response.Response> call= RestClient.get().kycSubmit(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull final Response<in.happyhelp.limra.activity.response.Response> response) {
                final in.happyhelp.limra.activity.response.Response response1=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(response1.getStatus()) {
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setUserLoggedIn(true);
                        new AlertDialog.Builder(KycActivity.this)
                                .setIcon(R.drawable.thank)
                                .setTitle("Kyc Submit")
                                .setMessage("Thank you for Kyc Submited")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                     //   Log.e("VENDORID", response1.getData().get(0).getId());
                                    //    SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.VENDORID, response1.getData().get(0).getId());
                                        Intent intent = new Intent(KycActivity.this, HomeActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }

                                })
                                .setNegativeButton("", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }

                                })
                                .show();

                    }else{
                        Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                    }

                }else if(response.code()==401){
                    AppUtils.logout(KycActivity.this);
                }else {
                    Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void getUserProfile(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<LoginResponse> call=RestClient.get().getUserDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                final LoginResponse loginResponse=response.body();
                if(response.code()==200) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(@NonNull Realm realm) {

                            realm.delete(UserData.class);
                            realm.copyToRealmOrUpdate(loginResponse.getData());
                        }
                    });
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
