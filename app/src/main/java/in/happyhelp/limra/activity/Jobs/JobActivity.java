package in.happyhelp.limra.activity.Jobs;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.KycActivity;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.adapter.JobTypeAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.jobcategory.JobCategory;
import in.happyhelp.limra.activity.response.jobcategory.JobCategoryResponse;
import in.happyhelp.limra.shopping.ShoppyAddressActivity;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.postjob)
    CardView postJob;

    JobTypeAdapter adapter;

    @BindView(R.id.data)
    TextView data;

    Boolean iscompany;

    @BindView(R.id.search_view)
    SearchView searchView;

    LinearLayoutManager linearLayoutManager;
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);

        realm= RealmHelper.getRealmInstance();

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        adapter=new JobTypeAdapter(getApplicationContext(), new JobTypeAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(JobCategory d, View view) throws ParseException {
                Intent intent=new Intent(JobActivity.this,JobListActivity.class);
                intent.putExtra(Constants.JOBID,d.getId());
                startActivity(intent);
            }
        });


        getJobCategory();

        iscompany =SharedPreferenceHelper.getInstance(JobActivity.this).isCompanyProfile();
        postJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(iscompany) {
                    Intent intent = new Intent(JobActivity.this, PostJobsActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(JobActivity.this, CompanyMembershipActivity.class);
                    startActivity(intent);
                }
            }
        });




        searchView.setQueryHint("Search Job");

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if(query!=null && !query.equals("")) {
                    RealmResults<JobCategory> realmResults = realm.where(JobCategory.class).contains("name", query, Case.INSENSITIVE).findAllAsync();
                    realmResults.addChangeListener(new RealmChangeListener<RealmResults<JobCategory>>() {
                        @Override
                        public void onChange(RealmResults<JobCategory> jobCategories) {
                            adapter.setData(jobCategories);
                            recyclerView.setAdapter(adapter);

                            if(jobCategories.size()==0){
                                data.setVisibility(View.VISIBLE);
                            }else{
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }else{
                    RealmResults<JobCategory> realmResults=realm.where(JobCategory.class).findAllAsync();
                    realmResults.addChangeListener(new RealmChangeListener<RealmResults<JobCategory>>() {
                        @Override
                        public void onChange(RealmResults<JobCategory> jobCategories) {
                            adapter.setData(jobCategories);
                            recyclerView.setAdapter(adapter);
                            if(jobCategories.size()==0)
                            {
                                data.setVisibility(View.VISIBLE);
                            }
                            else
                                {
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }


                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if(newText!=null && !newText.equals("")) {

                    RealmResults<JobCategory> realmResults = realm.where(JobCategory.class).contains("name", newText, Case.INSENSITIVE).findAllAsync();
                    realmResults.addChangeListener(new RealmChangeListener<RealmResults<JobCategory>>() {
                        @Override
                        public void onChange(RealmResults<JobCategory> jobCategories) {
                            adapter.setData(jobCategories);
                            recyclerView.setAdapter(adapter);

                            if(jobCategories.size()==0)
                            {
                                data.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                data.setVisibility(View.GONE);
                            }
                        }
                    });
                }else{
                    RealmResults<JobCategory> realmResults=realm.where(JobCategory.class).findAllAsync();
                    realmResults.addChangeListener(new RealmChangeListener<RealmResults<JobCategory>>() {
                        @Override
                        public void onChange(RealmResults<JobCategory> jobCategories) {
                            adapter.setData(jobCategories);
                            recyclerView.setAdapter(adapter);

                            if(jobCategories.size()==0)
                            {
                                data.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                data.setVisibility(View.GONE);
                            }

                        }
                    });
                }

                return false;
            }
        });




        RealmResults<JobCategory> realmResults=realm.where(JobCategory.class).findAllAsync();
        realmResults.addChangeListener(new RealmChangeListener<RealmResults<JobCategory>>() {
            @Override
            public void onChange(RealmResults<JobCategory> jobCategories) {
                adapter.setData(jobCategories);
                recyclerView.setAdapter(adapter);
            }
        });
    }

    public void getJobCategory(){
        swipeRefreshLayout.setRefreshing(true);
        Call<JobCategoryResponse> call= RestClient.get().getJobCategory(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<JobCategoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<JobCategoryResponse> call, @NonNull Response<JobCategoryResponse> response) {
                final JobCategoryResponse jobCategoryResponse=response.body();

                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200) {
                    if (jobCategoryResponse.getStatus()) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(JobCategory.class);
                                realm.copyToRealmOrUpdate(jobCategoryResponse.getData());
                            }
                        });
                    } else {
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(JobActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<JobCategoryResponse> call, Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(JobActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(JobActivity.this,Id);

            }
        });
    }

    @Override
    public void onRefresh() {
        getJobCategory();
    }
}
