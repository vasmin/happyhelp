
package in.happyhelp.limra.activity.response.rodetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detail {

    @SerializedName("tenure")
    @Expose
    private String tenure;

    @SerializedName("rent")
    @Expose
    private String rent;

    @SerializedName("refundable_deposit")
    @Expose
    private String refundableDeposit;



    public String getTenure() {
        return tenure;
    }

    public void setTenure(String tenure) {
        this.tenure = tenure;
    }

    public String getRent() {
        return rent;
    }

    public void setRent(String rent) {
        this.rent = rent;
    }

    public String getRefundableDeposit() {
        return refundableDeposit;
    }

    public void setRefundableDeposit(String refundableDeposit) {
        this.refundableDeposit = refundableDeposit;
    }





}
