package in.happyhelp.limra.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.service.MyServiceDetailsActivity;
import in.happyhelp.limra.activity.service.OrderDetailsActivity;
import in.happyhelp.limra.adapter.MyOrderAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.myservicesresponse.MyServices;
import in.happyhelp.limra.activity.response.myservicesresponse.MyServicesResponse;
import in.happyhelp.limra.shopping.MyOrdersActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrderActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    MyOrderAdapter myOrderAdapter;

    @BindView(R.id.data)
    TextView data;

    LinearLayoutManager linearLayoutManager;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order);
        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        swipeRefreshLayout.setOnRefreshListener(MyOrderActivity.this);
        swipeRefreshLayout.setRefreshing(true);


        myOrderAdapter=new MyOrderAdapter(getApplicationContext(), new MyOrderAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(MyServices d, View view) throws ParseException {
                Intent intent=new Intent(getApplicationContext(), OrderDetailsActivity.class);
                intent.putExtra(Constants.ORDERID,d.getId());
                startActivity(intent);
            }
        });
        getMyOrder();
    }


    public void getMyOrder(){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("id","0");
        Call<MyServicesResponse> call= RestClient.get().MyOrder(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<MyServicesResponse>() {
            @Override
            public void onResponse(Call<MyServicesResponse> call, Response<MyServicesResponse> response) {

                swipeRefreshLayout.setRefreshing(false);
                MyServicesResponse myServicesResponse=response.body();
                if(response.code()==200){

                    if(myServicesResponse.getStatus()){
                        myOrderAdapter.setData(myServicesResponse.getData());
                        recyclerView.setAdapter(myOrderAdapter);

                        if(myServicesResponse.getData().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }
                    }else{
                        data.setVisibility(View.VISIBLE);
                    }

                }else if(response.code()==401){
                    AppUtils.logout(MyOrderActivity.this);
                }else{
                    Toast.makeText(MyOrderActivity.this, Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<MyServicesResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(MyOrderActivity.this, Constants.SERVERTIMEOUT, Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onRefresh() {
        getMyOrder();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MyOrderActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MyOrderActivity.this,Id);

            }
        });



    }
}
