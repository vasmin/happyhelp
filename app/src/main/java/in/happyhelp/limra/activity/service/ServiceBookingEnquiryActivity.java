package in.happyhelp.limra.activity.service;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;
import java.util.HashMap;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.HomeActivity;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceBookingEnquiryActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.message)
    EditText message;

    @BindView(R.id.enquiry)
    TextView enquiry;

    @BindView(R.id.city)
    EditText city;

    @BindView(R.id.date)
    TextView date;

    @BindView(R.id.time)
    TextView time;

    int mHour,mMinute;

    @BindView(R.id.address)
    EditText address;

    String total="";
    String coupon="";
    String lastid="";
    String latitude="";
    String longitude="";
    String hours="";
    String vAmt="";
    String visitingAmt="";
    String qty="";

    ProgressDialog progressBar;
    String serviceId="";
    Realm realm;

    @BindView(R.id.building)
    EditText building;

    @BindView(R.id.linear)
    LinearLayout linearLayout;
    String City="";

    @BindView(R.id.visiting)
    CheckBox visiting;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_booking_enquiry);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        realm= RealmHelper.getRealmInstance();

        AppUtils.isNetworkConnectionAvailable(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            total = extras.getString("total");
            coupon = extras.getString("coupon");
            hours = extras.getString("hours");
            latitude=extras.getString(Constants.LATITUDE);
            longitude=extras.getString(Constants.LONGITUDE);
            serviceId=extras.getString(Constants.SERVICEID);
            vAmt=extras.getString(Constants.VENDORAMT);
            visitingAmt=extras.getString(Constants.VISITING);
            qty=extras.getString(Constants.QTY);
             City=AppUtils.getCompleteAddressString(getApplicationContext(),Double.parseDouble(latitude),Double.parseDouble(longitude));

            Log.e("Company",total+coupon+latitude+longitude);
            RealmResults<UserData> userData=realm.where(UserData.class).findAllAsync();
            userData.addChangeListener(new RealmChangeListener<RealmResults<UserData>>() {
                @Override
                public void onChange(@NonNull RealmResults<UserData> userData) {
                    if(userData!=null) {
                        if (userData.size() > 0) {
                            name.setText(userData.get(0).getName());
                            email.setText(userData.get(0).getEmail());
                            city.setText(userData.get(0).getCity());
                            city.setText(City);
                            city.setEnabled(false);
                            //address.setText(userData.get(0).getAddress());
                            mobile.setText(userData.get(0).getMobile());
                        }
                    }
                }
            });

            visiting.setChecked(true);
            visiting.setText("If Wrong service booking You need to pay visiting charges" +" Rs "+visitingAmt);

            visiting.setEnabled(false);

            if(hours.equals("48")){
               linearLayout.setVisibility(View.VISIBLE);
            }else{
                linearLayout.setVisibility(View.GONE);
            }

            date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar now = Calendar.getInstance();

                    now.add(Calendar.DATE, +3);
                    DatePickerDialog dpd = DatePickerDialog.newInstance(
                            ServiceBookingEnquiryActivity.this,
                            now.get(Calendar.YEAR), // Initial year selection
                            now.get(Calendar.MONTH), // Initial month selection
                            now.get(Calendar.DAY_OF_MONTH) // Initial day selection
                    );
                    dpd.setMinDate(now);
                    dpd.show(getFragmentManager(), "Datepickerdialog");
                }
            });

            time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar c = Calendar.getInstance();
                    mHour = c.get(Calendar.HOUR_OF_DAY);
                    mMinute = c.get(Calendar.MINUTE);
                    TimePickerDialog timePickerDialog= com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(ServiceBookingEnquiryActivity.this, mHour, mMinute, true);
                    timePickerDialog.show(getFragmentManager(),"timePicker");
                }
            });


            enquiry.setText("Confirm Order");

            enquiry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (TextUtils.isEmpty(name.getText().toString())) {
                        name.setError("Enter Name ");
                        return;
                    }

                    if (AppUtils.isValidMobile(mobile.getText().toString())) {

                    } else {
                        mobile.setError("Enter valid mobile");
                        return;
                    }


                    if (!AppUtils.isValidMail(email.getText().toString(), email)) {
                        email.setError("Enter Valid Email");
                        return;
                    }

                    if (TextUtils.isEmpty(city.getText().toString())) {
                        city.setError("Enter Locality ");
                        return;
                    }

                    if (TextUtils.isEmpty(address.getText().toString())) {
                        address.setError("Enter Flat Number ");
                        address.requestFocus();
                        return;
                    }

                    if (TextUtils.isEmpty(building.getText().toString())) {
                        building.setError("Enter building Name ");
                        building.requestFocus();
                        return;
                    }


                    if(hours.equals("48")) {
                        if (TextUtils.isEmpty(date.getText().toString())) {
                            date.setError("Enter date ");
                            return;
                        }

                        if (TextUtils.isEmpty(time.getText().toString())) {
                            time.setError("Enter time ");
                            return;
                        }
                    }

                    if (TextUtils.isEmpty(message.getText().toString())) {
                        message.setError("Enter Message ");
                        return;
                    }

                    postServiceEnquiry();
                }
            });
        }

        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);


    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void postServiceEnquiry () {
        progressBar.show();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("services", serviceId);
        hashMap.put("name", name.getText().toString());
        hashMap.put("email", email.getText().toString());
        hashMap.put("mobile", mobile.getText().toString());
        hashMap.put("message", message.getText().toString());
        hashMap.put("location", city.getText().toString());
        hashMap.put("amount", total);
        hashMap.put("latitude", latitude);
        hashMap.put("longitude", longitude);
        hashMap.put("address",address.getText().toString()+building.getText().toString());
        hashMap.put("coupon", coupon);
        hashMap.put("hour", hours);
        hashMap.put("vendor_amount",vAmt);
        hashMap.put("schedule_date",date.getText().toString());
        hashMap.put("schedule_time",time.getText().toString());
        hashMap.put("visiting_amount",visitingAmt);
        hashMap.put("gst","0");
        hashMap.put("qty",qty);

        Call<in.happyhelp.limra.activity.response.Response> call = RestClient.get().serviceEnquiryPost(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(), hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                progressBar.dismiss();
                if (response.code() == 200) {
                    if(response1.getStatus()){
                            Intent intent=new Intent(ServiceBookingEnquiryActivity.this,OrderDetailsActivity.class);
                            intent.putExtra(Constants.ORDERID,String.valueOf(response1.getLastid()));
                            startActivity(intent);
                            finish();

                    }else{
                        Toast.makeText(ServiceBookingEnquiryActivity.this, response1.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else if(response.code()==401){
                    AppUtils.logout(ServiceBookingEnquiryActivity.this);
                }else {
                    Toast.makeText(ServiceBookingEnquiryActivity.this, Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                Toast.makeText(ServiceBookingEnquiryActivity.this, Constants.SERVERTIMEOUT, Toast.LENGTH_SHORT).show();

            }
        });
    }







    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        MenuItem skip = menu.findItem(R.id.skip);
        skip.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.skip) {
            Intent intent=new Intent(ServiceBookingEnquiryActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(ServiceBookingEnquiryActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(ServiceBookingEnquiryActivity.this,Id);

            }
        });



    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String selectdate = dayOfMonth+"-"+(monthOfYear+1)+"-"+year;
        date.setText(selectdate);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

        String am_pm = "";

        Calendar datetime = Calendar.getInstance();
        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        datetime.set(Calendar.MINUTE, minute);

        if (datetime.get(Calendar.AM_PM) == Calendar.AM)
            am_pm = "AM";
        else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
            am_pm = "PM";

        String strHrsToShow = (datetime.get(Calendar.HOUR) == 0) ?"12":datetime.get(Calendar.HOUR)+"";
        time.setText(strHrsToShow+":"+datetime.get(Calendar.MINUTE)+" "+am_pm);






    }
}
