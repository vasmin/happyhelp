package in.happyhelp.limra.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.businesskyc.BusinessKycResponse;
import in.happyhelp.limra.activity.response.businesskyc.Datum;
import in.happyhelp.limra.activity.response.loginresponse.LoginResponse;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import io.realm.Realm;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

@SuppressLint("Registered")
public class BusinessKycActivity extends AppCompatActivity {


    private static final int GALLERY_PANCARD = 456;
    private static final int REQUEST_IMAGE_CAPTURE_PANCARD = 789;
    private static final int GALLERY_UDYOGCARD = 753;
    private static final int REQUEST_IMAGE_CAPTURE_UDYOGCARD = 951;
    private static final int GALLERY_GSTCERTIFICATE = 357;
    private static final int REQUEST_IMAGE_CAPTURE_GSTCERTIFICATE = 865;



    @BindView(R.id.pancardimage)
    ImageView panCardImage;

    @BindView(R.id.udyogadharimage)
    ImageView udyogImage;

    @BindView(R.id.gstcertificate)
    ImageView GStImage;


    @BindView(R.id.bankname)
    EditText bankName;

    @BindView(R.id.paytmno)
    EditText paytmNo;

    @BindView(R.id.phonepe)
    EditText phonePe;

    @BindView(R.id.branch)
    EditText branch;

    @BindView(R.id.bankaccount)
    EditText accountNo;

    @BindView(R.id.ifsc)
    EditText ifsc;

    @BindView(R.id.gst)
    EditText gstNo;


    @BindView(R.id.pancardlinear)
    LinearLayout pancardLinear;

    @BindView(R.id.udyogadharlinear)
    LinearLayout udyogadharlinear;

    @BindView(R.id.gstlinear)
    LinearLayout gstlinear;


    @BindView(R.id.submit)
    TextView submit;

    ProgressDialog progressBar;


    String pancardPath="",udyogadharPath="",GstPath="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_kyc);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kycUpload();
            }
        });
        getBusinessKyc();

        pancardLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File filepath=new File(Environment.getExternalStorageDirectory()+Constants.imagePath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }

                pancardPath=filepath.getAbsolutePath()+"/"+System.currentTimeMillis()+"business_pancard.jpg";


                AppUtils.startPickImageDialog(GALLERY_PANCARD, REQUEST_IMAGE_CAPTURE_PANCARD, pancardPath,BusinessKycActivity.this);
            }
        });

        udyogadharlinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File filepath=new File(Environment.getExternalStorageDirectory()+Constants.imagePath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }

                udyogadharPath=filepath.getAbsolutePath()+"/"+System.currentTimeMillis()+"business_udyogCard.jpg";

                AppUtils.startPickImageDialog(GALLERY_UDYOGCARD, REQUEST_IMAGE_CAPTURE_UDYOGCARD, udyogadharPath,BusinessKycActivity.this);
            }
        });

        gstlinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File filepath=new File(Environment.getExternalStorageDirectory()+Constants.imagePath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }

                GstPath=filepath.getAbsolutePath()+"/"+System.currentTimeMillis()+"business_gst.jpg";


                AppUtils.startPickImageDialog(GALLERY_GSTCERTIFICATE, REQUEST_IMAGE_CAPTURE_GSTCERTIFICATE, GstPath,BusinessKycActivity.this);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( resultCode==RESULT_OK)

            if(requestCode==REQUEST_IMAGE_CAPTURE_PANCARD)
            {   if(pancardPath!=null) {
                File newFile = new File(pancardPath);
                setImageFromPath(panCardImage, newFile.getPath());
            }
            }
            else if(requestCode==GALLERY_PANCARD){
                Uri imageUri = data.getData();
                pancardPath = getPath(getApplicationContext(), imageUri);
                setImageFromPath(panCardImage,pancardPath);
            }
            else if (requestCode == REQUEST_IMAGE_CAPTURE_UDYOGCARD) {
                if(udyogadharPath!=null) {
                    File newFile = new File(udyogadharPath);

                    setImageFromPath(udyogImage, newFile.getPath());
                }
            }
            else if(requestCode==GALLERY_UDYOGCARD){
                Uri imageUri = data.getData();
                udyogadharPath = getPath(getApplicationContext(), imageUri);
                setImageFromPath(udyogImage,udyogadharPath);
            }
            else if (requestCode == REQUEST_IMAGE_CAPTURE_GSTCERTIFICATE) {
                if(GstPath!=null) {
                    File newFile = new File(GstPath);

                    setImageFromPath(GStImage, newFile.getPath());
                }
            }
            else if(requestCode==GALLERY_GSTCERTIFICATE){
                Uri imageUri = data.getData();
                GstPath = getPath(getApplicationContext(), imageUri);
                setImageFromPath(GStImage,GstPath);
            }

    }

    private String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "";
            Toast.makeText(getApplicationContext(),"Sorry your records is not created",Toast.LENGTH_LONG).show();
        }
        return result;
    }


    private void setImageFromPath(ImageView image, String path){
        if(!TextUtils.isEmpty(path)&&!path.equals("")) {
            File imgFile = new File(path);
            if (imgFile.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize=8;      // 1/8 of original image
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(),options);
                image.setImageBitmap(myBitmap);
                image.setVisibility(View.VISIBLE);
            }else {
                image.setVisibility(View.GONE);
            }
        }else {
            image.setVisibility(View.GONE);
        }
    }

    public void kycUpload(){
        if(pancardPath.equals("")){
            Toast.makeText(this, "Please upload business pancard", Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(bankName.getText().toString())){
            bankName.setError("Enter bankName ");
            bankName.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(branch.getText().toString())){
            branch.setError("Enter branch");
            branch.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(accountNo.getText().toString())){
            accountNo.setError("Enter account number");
            accountNo.requestFocus();
            return;
        }

        if(!AppUtils.isBankAccount(accountNo.getText().toString(),accountNo)){
            accountNo.setError("Enter account number");
            accountNo.requestFocus();
            return;
        }


        if(TextUtils.isEmpty(ifsc.getText().toString())){
            ifsc.setError("Enter ifsc number");
            ifsc.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(paytmNo.getText().toString())){
            paytmNo.setError("Enter paytm number");
            paytmNo.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(phonePe.getText().toString())){
            phonePe.setError("Enter phonePe number");
            phonePe.requestFocus();
            return;
        }


        progressBar.show();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String vendorId= SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID);
        builder.addFormDataPart("userid", vendorId);
        builder.addFormDataPart("ifsc", ifsc.getText().toString());
        builder.addFormDataPart("gst", gstNo.getText().toString());
        builder.addFormDataPart("accountno", accountNo.getText().toString());
        builder.addFormDataPart("branchname", branch.getText().toString());
        builder.addFormDataPart("bankname", bankName.getText().toString());
        builder.addFormDataPart("paytm", paytmNo.getText().toString());
        builder.addFormDataPart("phonepe", phonePe.getText().toString());

        if(pancardPath!=null) {
            builder.addFormDataPart("pan_noold", pancardPath);
        }

        if(udyogadharPath!=null) {
            builder.addFormDataPart("aadhar_frontold", udyogadharPath);
        }

        if(GstPath!=null) {
            builder.addFormDataPart("gst_old", GstPath);
        }




        if(pancardPath!=null) {
            File panpath = new File(pancardPath);
            builder.addFormDataPart("pan_no", panpath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), panpath));
        }

        if(udyogadharPath!=null) {
            File udyogpath = new File(udyogadharPath);
            builder.addFormDataPart("aadhar_card", udyogpath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), udyogpath));
        }

        if(GstPath!=null) {
            File gstpath = new File(GstPath);
            builder.addFormDataPart("gst", gstpath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), gstpath));
        }


        final MultipartBody requestBody = builder.build();

        Call<Response> call= RestClient.get().businessKycSubmit(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull final retrofit2.Response<Response> response) {
                final in.happyhelp.limra.activity.response.Response response1=response.body();
                progressBar.dismiss();
                if(response.code()==200){
                    if(response1.getStatus()) {
                        SharedPreferenceHelper.getInstance(getApplicationContext()).setUserLoggedIn(true);
                        new AlertDialog.Builder(BusinessKycActivity.this)
                                .setIcon(R.drawable.thank)
                                .setTitle("Kyc Submit")
                                .setMessage("Thank you for Kyc Submited")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }

                                })
                                .setNegativeButton("", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }

                                })
                                .show();

                    }else{
                        Toast.makeText(getApplicationContext(), Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                    }

                }else if(response.code()==401){
                    AppUtils.logout(BusinessKycActivity.this);
                }else {
                    Toast.makeText(getApplicationContext(),Constants.SOMTHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void getBusinessKyc(){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<BusinessKycResponse> call=RestClient.get().getBusinessKyc(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<BusinessKycResponse>() {
            @Override
            public void onResponse(@NonNull Call<BusinessKycResponse> call, @NonNull retrofit2.Response<BusinessKycResponse> response) {
                BusinessKycResponse businessKycResponse=response.body();
                progressBar.dismiss();
                if(response.code()==200) {
                    if(businessKycResponse.getStatus()){

                        if(businessKycResponse.getData().size()>0) {
                            Datum datum=businessKycResponse.getData().get(0);
                            Glide.with(getApplicationContext())
                                    .load(RestClient.base_image_url+datum.getBusinessPan())
                                    .into(panCardImage);

                            Glide.with(getApplicationContext())
                                    .load(RestClient.base_image_url+datum.getAadharCard())
                                    .into(udyogImage);

                            Glide.with(getApplicationContext())
                                    .load(RestClient.base_image_url+datum.getGst())
                                    .into(GStImage);

                            bankName.setText(datum.getBankName());
                            branch.setText(datum.getBankBranchName());
                            accountNo.setText(datum.getBankAccNo());
                            ifsc.setText(datum.getIfscCode());
                            gstNo.setText(datum.getGstNo());
                            paytmNo.setText(datum.getPaytm());
                            phonePe.setText(datum.getPhonepe());


                        }
                    }else{
                    }
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<BusinessKycResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
