package in.happyhelp.limra.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.property.MyPropertyEnquiryActivity;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import in.happyhelp.limra.activity.ro.RoBookingActivity;
import in.happyhelp.limra.adapter.MemberAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.gstresponse.GstRateResponse;
import in.happyhelp.limra.membershipresponse.Membership;
import in.happyhelp.limra.membershipresponse.MembershipResponse;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.model.ServiceModel;
import in.happyhelp.limra.network.RestClient;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemberShipActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.data)
    TextView data;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.pay)
    TextView pay;

    @BindView(R.id.gst)
    TextView gst;

    @BindView(R.id.gstAmount)
    TextView gstAmount;

    String totalamount="";

    Realm realm;
    String mobile,email,username;

    MemberAdapter adapter;
    GridLayoutManager gridLayoutManager;
    boolean isSelected=false;

    List<Membership> list=new ArrayList<>();
    String membershipAmt;
    Membership membership;
    int gstRate=0;

    @BindView(R.id.linear)
    LinearLayout linearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_ship);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        realm= RealmHelper.getRealmInstance();
        ButterKnife.bind(this);


        gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2, GridLayoutManager.VERTICAL, false);
       // int mNoOfColumns = AppUtils.calculateNoOfColumns(getApplicationContext(),120);
        gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);

        adapter=new MemberAdapter(getApplicationContext(), new MemberAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(Membership d, View view) throws ParseException {
                isSelected=true;
                membership=d;
                membershipAmt=d.getPrice();
                for(int i=0;i<list.size();i++){
                    Membership membership=list.get(i);
                    if(d.getName().equals(membership.getName())){
                        d.setSelected(true);
                    }else{
                        membership.setSelected(false);
                    }

                }
                adapter.setData(list);
                setDataVisible();
            }
        });
        getMembershipPlan();
        getGstRate();
        RealmResults<UserData> userData=realm.where(UserData.class).findAllAsync();
        userData.addChangeListener(new RealmChangeListener<RealmResults<UserData>>() {
            @Override
            public void onChange(@NonNull RealmResults<UserData> userData) {
                if(userData!=null) {
                    if (userData.size() > 0) {
                        username=userData.get(0).getName();
                        email=userData.get(0).getEmail();
                        mobile=userData.get(0).getMobile();
                    }
                }
            }
        });


        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callInstamojoPay(email,mobile, totalamount, "User Cashback Membership",username );
            }
        });

    }

    @SuppressLint("SetTextI18n")
    public void setDataVisible(){
        if(isSelected){
            pay.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.VISIBLE);
            double result = (double) ((Integer.parseInt(membershipAmt) * gstRate) / 100);
            totalamount= String.valueOf(Double.parseDouble(membershipAmt)+result);
            gstAmount.setText(String.valueOf(result));
            gst.setText("Exclusive Gst ("+gstRate+"% )");
            pay.setText("Pay for Membership  "+"\u20B9 "+totalamount);
        }
    }
    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }
    InstapayListener listener;
    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);
                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);
                postMembershipPlan(payment);

            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }

    public void postMembershipPlan(String paymentId){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("amount",totalamount);
        hashMap.put("payment_id",paymentId);
        hashMap.put("packageid",membership.getId());
        hashMap.put("gst", String.valueOf(gstRate));
        hashMap.put("usevalue",membership.getUsevalue());
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().postMembershipPlan(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    new AlertDialog.Builder(MemberShipActivity.this)
                            .setIcon(R.drawable.logo)
                            .setTitle("Thank You")
                            .setMessage(response1.getMessage())
                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent=new Intent(MemberShipActivity.this,HomeActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            })
                            .show();

                }else if(response.code()==401){
                    AppUtils.logout(getApplicationContext());
                }
                else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }

            }

            @Override
            public void onFailure(Call<in.happyhelp.limra.activity.response.Response> call, Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void getGstRate(){
        Call<GstRateResponse> call=RestClient.get().getGstRate(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<GstRateResponse>() {
            @Override
            public void onResponse(Call<GstRateResponse> call, Response<GstRateResponse> response) {

                if(response.code()==200){
                    GstRateResponse gstRateResponse=response.body();

                    if(gstRateResponse.getStatus()) {
                        if (gstRateResponse.getGst().size() >= 6) {
                            gstRate = Integer.parseInt(gstRateResponse.getGst().get(6).getName());

                        }
                    }else{
                        gstRate=0;
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getApplicationContext());
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<GstRateResponse> call, Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }


    public void getMembershipPlan(){
        Call<MembershipResponse> call= RestClient.get().getMembership(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<MembershipResponse>() {
            @Override
            public void onResponse(Call<MembershipResponse> call, Response<MembershipResponse> response) {
                MembershipResponse membershipResponse=response.body();
                if(response.code()==200){
                    list.clear();
                    list.addAll(membershipResponse.getMembership());
                    adapter.setData(list);
                    recyclerView.setAdapter(adapter);

                }else if(response.code()==401){
                    AppUtils.logout(getApplicationContext());
                }else{

                }
            }

            @Override
            public void onFailure(Call<MembershipResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onRefresh() {
        getMembershipPlan();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MemberShipActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MemberShipActivity.this,Id);

            }
        });
    }
}
