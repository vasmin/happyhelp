
package in.happyhelp.limra.activity.response.myservicesresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyServices {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("serviceid")
    @Expose
    private String serviceid;
    @SerializedName("serviceuser")
    @Expose
    private String serviceuser;
    @SerializedName("services")
    @Expose
    private List<Service> services = null;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("hour")
    @Expose
    private String hour;
    @SerializedName("paid")
    @Expose
    private String paid;
    @SerializedName("coupon")
    @Expose
    private String coupon;
    @SerializedName("vendor_amount")
    @Expose
    private String vendorAmount;

    @SerializedName("visiting_amount")
    @Expose
    private String visitingCharge;

    @SerializedName("wallet")
    @Expose
    private String wallet;

    @SerializedName("cashback_wallet")
    @Expose
    private String cashback_wallet;


    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("payment_mode")
    @Expose
    private String paymentMode;
    @SerializedName("accept_time")
    @Expose
    private String acceptTime;
    @SerializedName("reach_time")
    @Expose
    private String reachTime;
    @SerializedName("waiting_time")
    @Expose
    private String waitingTime;
    @SerializedName("waiting_charges")
    @Expose
    private String waitingCharges;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("penalty_charges")
    @Expose
    private String penalty;


    @SerializedName("waiting")
    @Expose
    private String waiting;

    @SerializedName("vendor_name")
    @Expose
    private String vendorname;

    @SerializedName("service_name")
    @Expose
    private String service_name;

    @SerializedName("showdetails")
    @Expose
    private Boolean showdetails;

    @SerializedName("qty")
    @Expose
    private String quantity;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getServiceid() {
        return serviceid;
    }

    public void setServiceid(String serviceid) {
        this.serviceid = serviceid;
    }

    public String getServiceuser() {
        return serviceuser;
    }

    public void setServiceuser(String serviceuser) {
        this.serviceuser = serviceuser;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getVendorAmount() {
        return vendorAmount;
    }

    public void setVendorAmount(String vendorAmount) {
        this.vendorAmount = vendorAmount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getAcceptTime() {
        return acceptTime;
    }

    public void setAcceptTime(String acceptTime) {
        this.acceptTime = acceptTime;
    }

    public String getReachTime() {
        return reachTime;
    }

    public void setReachTime(String reachTime) {
        this.reachTime = reachTime;
    }

    public String getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(String waitingTime) {
        this.waitingTime = waitingTime;
    }

    public String getWaitingCharges() {
        return waitingCharges;
    }

    public void setWaitingCharges(String waitingCharges) {
        this.waitingCharges = waitingCharges;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPenalty() {
        return penalty;
    }

    public void setPenalty(String penalty) {
        this.penalty = penalty;
    }

    public String getVendorname() {
        return vendorname;
    }

    public void setVendorname(String vendorname) {
        this.vendorname = vendorname;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getVisitingCharge() {
        return visitingCharge;
    }

    public void setVisitingCharge(String visitingCharge) {
        this.visitingCharge = visitingCharge;
    }

    public Boolean getShowdetails() {
        return showdetails;
    }

    public void setShowdetails(Boolean showdetails) {
        this.showdetails = showdetails;
    }

    public String getWaiting() {
        return waiting;
    }

    public void setWaiting(String waiting) {
        this.waiting = waiting;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getCashback_wallet() {
        return cashback_wallet;
    }

    public void setCashback_wallet(String cashback_wallet) {
        this.cashback_wallet = cashback_wallet;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
