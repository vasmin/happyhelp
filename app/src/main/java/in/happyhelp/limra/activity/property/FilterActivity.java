package in.happyhelp.limra.activity.property;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.Jobs.JobProfileActivity;
import in.happyhelp.limra.activity.MyProfileActivity;
import in.happyhelp.limra.activity.response.citiresponse.CitiesResponse;
import in.happyhelp.limra.activity.response.citiresponse.Datum;
import in.happyhelp.limra.activity.response.propertiesresponse.PropertyList;
import in.happyhelp.limra.activity.response.statecityresponse.City;
import in.happyhelp.limra.activity.response.statecityresponse.State;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.adapter.CitiesSpinnerAdapter;
import in.happyhelp.limra.adapter.CitySpinnerAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;

public class FilterActivity extends AppCompatActivity {


    @BindView(R.id.bedroom)
    Spinner bedRoom;

    @BindView(R.id.min)
    Spinner min;

    @BindView(R.id.max)
    Spinner max;

    @BindView(R.id.filter)
    TextView filter;

    @BindView(R.id.propertytype)
    Spinner propertyType;

    @BindView(R.id.propertycard)
    CardView propertyCard;

    @BindView(R.id.statecard)
    CardView stateCard;

    @BindView(R.id.citycard)
    CardView cityCard;

    @BindView(R.id.state)
    Spinner stateSpinner;

    @BindView(R.id.propertys)
    CardView propertycard;

    @BindView(R.id.cityspinner)
    Spinner citySpinner;



    String cityName;
    ArrayList<String> propertytype=new ArrayList<>();
    Realm realm;


    RealmResults<State> statesList;
    ArraySpinnerAdapter stateSpinnerAdapter,citySpinnerAdapter;


    ArraySpinnerAdapter propertyTypeAdapter,propertyForAdapter,bedAdapter,minAdapter,maxAdapter;
    List<String> bedList=new ArrayList<>();
    List<String> minList=new ArrayList<>();
    List<String> maxList=new ArrayList<>();

    String bedroom="",Propertytype="",minbudget,maxbudget;
    String PropertyFor="";

    String stateName;
    List<String> listCity=new ArrayList<>();
    List<String> listState=new ArrayList<>();

    @BindView(R.id.propertytypehint)
    TextView propertyTypehint;

    @BindView(R.id.statehint)
    TextView stateHint;

    @BindView(R.id.cityhint)
    TextView cityHint;

    @BindView(R.id.bedroomhint)
    TextView bedroomHint;


    @BindView(R.id.propertyfor)
            Spinner propertyFor;

    @BindView(R.id.property)
            TextView propertyforTxt;


    ArrayList<String> propertyfor=new ArrayList<>();
     RealmResults<City> cityRealmResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        realm= RealmHelper.getRealmInstance();

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        bedAdapter=new ArraySpinnerAdapter(this);
        minAdapter =new ArraySpinnerAdapter(this);
        maxAdapter =new ArraySpinnerAdapter(this);


        stateHint.setVisibility(View.VISIBLE);
        cityHint.setVisibility(View.VISIBLE);

        stateSpinnerAdapter =new ArraySpinnerAdapter(this);
        citySpinnerAdapter =new ArraySpinnerAdapter(this);


        propertycard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                propertyFor.performClick();
            }
        });

        statesList=realm.where(State.class).findAllAsync();
        statesList.addChangeListener(new RealmChangeListener<RealmResults<State>>() {
            @Override
            public void onChange(RealmResults<State> states) {
                listState.clear();
                for(int i=0;i<statesList.size();i++){
                    listState.add(statesList.get(i).getState());
                }
                stateSpinnerAdapter.setData(listState);
                stateSpinner.setAdapter(stateSpinnerAdapter);
            }
        });


        propertyCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                propertyType.performClick();
            }
        });


        stateCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateSpinner.performClick();
            }
        });

        cityCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                citySpinner.performClick();
            }
        });


        bedList.clear();
        bedList.add("Select Bedroom");
        bedList.add("1 RK");
        bedList.add("1 BHK");
        bedList.add("2 BHK");
        bedList.add("3 BHK");
        bedList.add("4 BHK");

        bedAdapter.setData(bedList);
        bedRoom.setAdapter(bedAdapter);

        propertytype.clear();
        propertytype.add("Property type");
        propertytype.add("Residential");
        propertytype.add("Commercial");

        propertyTypeAdapter=new ArraySpinnerAdapter(FilterActivity.this);

        propertyTypeAdapter.setData(propertytype);
        propertyType.setAdapter(propertyTypeAdapter);

        minList.clear();
        minList.add("0");
        minList.add("50000");
        minList.add("100000");
        minList.add("150000");
        minList.add("200000");
        minList.add("250000");
        minList.add("300000");
        minList.add("350000");
        minList.add("400000");
        minList.add("450000");
        minList.add("500000");

        minAdapter.setData(minList);
        min.setAdapter(minAdapter);

        maxList.clear();
        maxList.add("0");
        maxList.add("50000");
        maxList.add("100000");
        maxList.add("150000");
        maxList.add("200000");
        maxList.add("250000");
        maxList.add("300000");
        maxList.add("350000");
        maxList.add("400000");
        maxList.add("450000");
        maxList.add("500000");


        maxAdapter.setData(maxList);
        max.setAdapter(maxAdapter);


        propertyfor.add("Property for");
        propertyfor.add("Sell");
        propertyfor.add("Rent");


        propertyForAdapter=new ArraySpinnerAdapter(this);

        propertyForAdapter.setData(propertyfor);

        propertyFor.setAdapter(propertyForAdapter);

      //  getCities();


        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(listState.size()>i){
                stateHint.setVisibility(View.GONE);
                stateName = String.valueOf(listState.get(i));
                cityRealmResults = realm.where(City.class).equalTo("state", stateName).findAllAsync();
                cityRealmResults.addChangeListener(new RealmChangeListener<RealmResults<City>>() {
                    @Override
                    public void onChange(RealmResults<City> cities) {

                        listCity.clear();
                        listCity.add("Select City");
                        if (cityRealmResults.size() > 0) {
                            for (int i = 0; i < cityRealmResults.size(); i++) {
                                if (!listCity.contains(cities.get(i).getCity())) {
                                    listCity.add(cities.get(i).getCity());
                                }
                            }
                            citySpinnerAdapter.setData(listCity);
                            citySpinner.setAdapter(citySpinnerAdapter);
                        }
                    }


                });
                }else{
                    stateHint.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(listCity.size()>i) {
                    cityName = String.valueOf(listCity.get(i));
                    cityHint.setVisibility(View.GONE);

                }else{
                    cityHint.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        propertyType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if( propertytype.get(i).equals("Residential")){
                    Propertytype="1";
                    propertyTypehint.setVisibility(View.GONE);
                }else if( propertytype.get(i).equals("Commercial")){
                    Propertytype="0";
                    propertyTypehint.setVisibility(View.GONE);
                }else{
                    Propertytype="";
                    propertyTypehint.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        propertyFor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                PropertyFor= String.valueOf(propertyfor.get(i));
                if(!PropertyFor.equals("")) {
                    propertyforTxt.setVisibility(View.GONE);
                }else{
                    propertyforTxt.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int min= Integer.parseInt(minbudget);
                int max= Integer.parseInt(maxbudget);
                if(min>max){
                    Toast.makeText(FilterActivity.this, "Please Select Proper Budget Range", Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent=new Intent(FilterActivity.this, SearchPropertyActivity.class);
                String cityid="";
                for(int i=0;i<cityRealmResults.size();i++){
                    if(cityName.equals(cityRealmResults.get(i).getCity())){
                        cityid=cityRealmResults.get(i).getId();

                    }
                }
                intent.putExtra(Constants.CITY,cityid);
                intent.putExtra(Constants.BEDROOM,bedroom);
                intent.putExtra(Constants.TYPE,Propertytype);
                intent.putExtra(Constants.MIN,minbudget);
                intent.putExtra(Constants.MAX,maxbudget);
                if(PropertyFor.equals("Sell")) {
                    intent.putExtra(Constants.PROPERTYFOR, "1");
                }else if(PropertyFor.equals("Rent")){
                    intent.putExtra(Constants.PROPERTYFOR, "0");
                }else{

                }
                startActivity(intent);
                finish();
            }
        });


        bedRoom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(bedList.size()>i) {
                    bedroom = String.valueOf(bedList.get(i));
                    bedroomHint.setVisibility(View.GONE);
                }else{
                    bedroomHint.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        min.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(minList.size()>i) {
                    minbudget = String.valueOf(minList.get(i));

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        max.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(maxList.size()>i) {
                    maxbudget = String.valueOf(maxList.get(i));

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

  /*  public void getCities(){
        Call<CitiesResponse> call= RestClient.get(RestClient.PROPERTY_BASE_URL).getCities(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<CitiesResponse>() {
            @Override
            public void onResponse(@NonNull Call<CitiesResponse> call, @NonNull retrofit2.Response<CitiesResponse> response) {
                CitiesResponse citiesResponse=response.body();
                if(response.code()==200){
                    if(citiesResponse.getStatus()){

                        cities.clear();
                        cities.addAll(citiesResponse.getData());
                        citySpinnerAdapter.setData(citiesResponse.getData());
                        citySpinner.setAdapter(citySpinnerAdapter);
                    }else{

                    }

                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<CitiesResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();

            }
        });
    }*/

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(FilterActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(FilterActivity.this,Id);


            }
        });
    }
}
