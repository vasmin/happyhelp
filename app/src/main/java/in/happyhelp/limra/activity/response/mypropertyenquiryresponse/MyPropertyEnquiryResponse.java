
package in.happyhelp.limra.activity.response.mypropertyenquiryresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyPropertyEnquiryResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("enquiries")
    @Expose
    private List<Enquiry> enquiries = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Enquiry> getEnquiries() {
        return enquiries;
    }

    public void setEnquiries(List<Enquiry> enquiries) {
        this.enquiries = enquiries;
    }

}
