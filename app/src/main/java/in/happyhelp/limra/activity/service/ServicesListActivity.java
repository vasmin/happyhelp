package in.happyhelp.limra.activity.service;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.GPSTracker;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.HomeActivity;
import in.happyhelp.limra.activity.MapsActivity;
import in.happyhelp.limra.adapter.ServiceListAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.servicecategoryresponse.ServiceCategoryResponse;
import in.happyhelp.limra.activity.response.servicelistresponse.Datum;
import in.happyhelp.limra.activity.response.servicelistresponse.ServicesListResponse;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServicesListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.data)
    TextView data;
    LinearLayoutManager linearLayoutManager;

    ServiceListAdapter adapter;

    GPSTracker gps;
    Location location;
    String latitude="";
    String longitude="";
    boolean isSet=false;


    @BindView(R.id.searchs)
    LinearLayout searchSLinear;

    @BindView(R.id.searchservice)
            TextView searchService;
    String serviceId="";
    Realm realm;
    String isPopUp="";
    MultiSelectDialog multiSelectDialog;
    ArrayList<Integer>preSelected= new ArrayList<>();
    ArrayList<MultiSelectModel> selectModels = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services_list);
        ButterKnife.bind(this);

        realm= RealmHelper.getRealmInstance();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            latitude = extras.getString(Constants.LATITUDE);
            longitude=extras.getString(Constants.LONGITUDE);
            isPopUp=extras.getString(Constants.ISPOPUP);
            Log.e("Lat",latitude+longitude);
            if(!isPopUp.equals("")) {
                isSet = true;
            }else
            {
                isSet = false;
            }

        }
        AppUtils.isNetworkConnectionAvailable(this);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        swipeRefreshLayout.setOnRefreshListener(ServicesListActivity.this);
        swipeRefreshLayout.setRefreshing(true);

        adapter=new ServiceListAdapter(getApplicationContext(), new ServiceListAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(Datum d, View view) throws ParseException {

            }
        });
        searchSLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                multiSelectDialog.show(getSupportFragmentManager(),"Service Dialoge");
            }
        });




        gps = new GPSTracker(this);

        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            gps.showSettingsAlert();
        }

        // Check if GPS enabled
        if(gps.canGetLocation()) {
            location=gps.getLocation();
            if(location!=null){
                if(!isSet) {
                    latitude = String.valueOf(location.getLatitude());
                    longitude = String.valueOf(location.getLongitude());
                    SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.LATITUDE, latitude);
                    SharedPreferenceHelper.getInstance(getApplicationContext()).putString(Constants.LONGITUDE, longitude);
                }
            }
        } else {
            gps.showSettingsAlert();
        }



        getService();
        getServices();


    }


    public void getService() {
        HashMap<String, String> hashMap = new HashMap<>();
        Call<ServiceCategoryResponse> call = RestClient.get().getMainServices(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(), hashMap);
        call.enqueue(new Callback<ServiceCategoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<ServiceCategoryResponse> call, @NonNull Response<ServiceCategoryResponse> response) {
                if (response.code() == 200) {
                    final ServiceCategoryResponse serviceCategoryResponse = response.body();

                    for (int i = 0; i < serviceCategoryResponse.getServiceCategoryList().size(); i++) {
                        selectModels.add(new MultiSelectModel(Integer.parseInt(serviceCategoryResponse.getServiceCategoryList().get(i).getId()), serviceCategoryResponse.getServiceCategoryList().get(i).getName()));
                    }

                    multiSelectDialog = new MultiSelectDialog()
                            .title("Please Select") //setting title for dialog
                            .titleSize(25)
                            .positiveText("Done")
                            .negativeText("Cancel")
                            .setMinSelectionLimit(0)
                            .preSelectIDsList(preSelected)
                            .setMaxSelectionLimit(1)
                            .multiSelectList(selectModels) // the multi select model list with ids and name
                            .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                                @Override
                                public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                                    //will return list of selected IDS
                                    StringBuilder builder = new StringBuilder();

                                    for (int i = 0; i < selectedIds.size(); i++) {
                                        for (int j = 0 ; j < selectModels.size(); j++ )

                                            if(selectModels.get(j).getId()==selectedIds.get(i)){
                                                builder.append(selectModels.get(j).getName()+",");

                                                serviceId= String.valueOf(selectModels.get(j).getId());
                                            }
                                    }
                                      searchService.setText(builder);
                                    System.out.println(builder);
                                    getServices();
                                }

                                @Override
                                public void onCancel() {
                                    multiSelectDialog.dismiss();
                                }
                            });
                }else if(response.code()==401){
                    AppUtils.logout(ServicesListActivity.this);
                } else {
                }
            }

            @Override
            public void onFailure(@NonNull Call<ServiceCategoryResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getServices(){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String ,String> hashMap=new HashMap<>();

        if(latitude.equals("") && longitude.equals("")){
            hashMap.put("latitude",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.LATITUDE));
            hashMap.put("longitude",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.LONGITUDE));
        }else{
            hashMap.put("latitude",latitude);
            hashMap.put("longitude",longitude);
        }
        hashMap.put("serviceid",serviceId);

        Call<ServicesListResponse> call=RestClient.get().getServicesList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<ServicesListResponse>() {
            @Override
            public void onResponse(@NonNull Call<ServicesListResponse> call, @NonNull Response<ServicesListResponse> response) {

                swipeRefreshLayout.setRefreshing(false);
                Log.e("RESPONSE","response"+response.body().toString());
                if(response.code()==200){
                    final ServicesListResponse servicesListResponse=response.body();

                    if(servicesListResponse.getStatus()){


                        adapter.setData(servicesListResponse.getData());
                        recyclerView.setAdapter(adapter);

                        if(servicesListResponse.getData().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }
                    }else{
                        data.setVisibility(View.VISIBLE);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(ServicesListActivity.this);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ServicesListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
                swipeRefreshLayout.setRefreshing(false);

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        MenuItem map = menu.findItem(R.id.map);
        map.setVisible(true);

        MenuItem enquiryservice = menu.findItem(R.id.enquiryservice);
        enquiryservice.setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.map) {
            Intent intent=new Intent(this, MapsActivity.class);
            intent.putExtra(Constants.LATITUDE,latitude);
            intent.putExtra(Constants.LONGITUDE,longitude);
            intent.putExtra(Constants.ISPOPUP,"1");
            startActivity(intent);

        }

        if (id == R.id.enquiryservice) {
            Intent intent=new Intent(this, ServiceEnquiryActivity.class);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        getServices();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(ServicesListActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(ServicesListActivity.this,Id);

            }
        });



    }
}
