
package in.happyhelp.limra.activity.response.myrodetailresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bill {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("roenquiryid")
    @Expose
    private String roenquiryid;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("status")
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRoenquiryid() {
        return roenquiryid;
    }

    public void setRoenquiryid(String roenquiryid) {
        this.roenquiryid = roenquiryid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
