package in.happyhelp.limra.activity.service;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.GPSTracker;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.HomeActivity;
import in.happyhelp.limra.activity.response.categorylastresponse.CategoryLastResponse;
import in.happyhelp.limra.adapter.HourAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.model.ServiceModel;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.CouponResponse;
import in.happyhelp.limra.activity.response.categoryresponse.CategoryResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RatesActivity extends AppCompatActivity {

    @BindView(R.id.coupon)
    EditText coupon;
    int baseamount;

    @BindView(R.id.service)
    TextView service;

    @BindView(R.id.apply)
    TextView applyCoupons;

    String discountAmount="0";
    String discountPer="0";

    @BindView(R.id.percent)
    TextView percent;

    @BindView(R.id.percentamt)
    TextView percentageAmt;

    @BindView(R.id.flatamt)
    TextView flatAmt;

    @BindView(R.id.totalamt)
    TextView totalAmt;

    @BindView(R.id.per)
    LinearLayout perLinear;

    @BindView(R.id.amt)
    LinearLayout amtLinear;

    @BindView(R.id.payment)
    TextView payment;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.count)
    TextView count;

    @BindView(R.id.increment)
    ImageView increment;

    @BindView(R.id.decrement)
    ImageView decrement;

    @BindView(R.id.call)
    TextView callUs;

    int counter = 0;
    int amount=0;

    String visitingCharge="0";
    LinearLayoutManager linearLayoutManager;
    HourAdapter adapter;
    Boolean isFlat=false;

    @BindView(R.id.nofound)
            TextView noFound;

    List<ServiceModel> serviceList=new ArrayList<>();

    GPSTracker gps;
    Location location;
    String latitude="";
    String servicesId="";
    String longitude="";

    ServiceModel selectedService;
    String catid="";
    Menu mMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rates);
        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            servicesId = extras.getString(Constants.SERVICEID);
            catid=extras.getString(Constants.CATEGORYID);
            latitude=extras.getString(Constants.LATITUDE);
            longitude=extras.getString(Constants.LONGITUDE);
            getcategory(catid);

            Log.e("service List","Is "+servicesId);
        }
       /* final ArrayList<String> treeList = Objects.requireNonNull(getIntent().getExtras()).getStringArrayList(Constants.SERVICELIST);
        Log.e("Size", String.valueOf(treeList.size()));


        for(int i=0;i<treeList.size();i++){
            if(i==treeList.size()-1){
                servicesId=servicesId+treeList.get(i)+",";
            }
            else{
                servicesId=servicesId+treeList.get(i)+",";
            }
        }


        final String catid = treeList.get(treeList.size()-1);*/


        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        adapter=new HourAdapter(getApplicationContext(), new HourAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(ServiceModel d, View view) throws ParseException {

                totalAmt.setText(d.getRps());
                baseamount = Integer.parseInt(d.getRps());
                amount=baseamount;
                selectedService=d;
                counter=1;
                count.setText(String.valueOf(counter));
                visitingCharge=d.getVisitingAmt();
                setData();

                for(int i=0;i<serviceList.size();i++){
                    ServiceModel serviceModel=serviceList.get(i);
                    if(d.getHours().equals(serviceModel.getHours())){

                            d.setSelected(true);

                    }else{
                        serviceModel.setSelected(false);
                    }
                }
                adapter.setData(serviceList);
                if(serviceList.size()==0){
                    noFound.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }else{
                    noFound.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }

            }
        });

        increment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(baseamount>0){
                counter++;
                count.setText(String.valueOf(counter));
                amount = counter*baseamount;
                totalAmt.setText(String.valueOf(amount));
                }
            }
        });

        decrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(baseamount>0 && counter > 0){
                    counter--;
                    count.setText(String.valueOf(counter));
                    amount = amount-baseamount;
                    totalAmt.setText(String.valueOf(amount));}
            }
        });

        callUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int REQUEST_PHONE_CALL=123;
                new AlertDialog.Builder(RatesActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Support Call ")
                        .setMessage("Are you sure you want to Call ?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "9222989097"));

                                startActivity(intent);

                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });






        gps = new GPSTracker(this);

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            gps.showSettingsAlert();
        }




        applyCoupons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(coupon.getText().toString())){
                    coupon.setError("Enter Coupons ");
                    return;
                }
                applyCoupon();
            }
        });

        coupon.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                perLinear.setVisibility(View.GONE);
                amtLinear.setVisibility(View.GONE);
                discountPer="0";
                discountAmount="0";
                if(selectedService!=null){
                    totalAmt.setText(selectedService.getRps());
                }else{
                    totalAmt.setText("0");
                }
                setData();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });



        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!servicesId.equals("") && selectedService!=null) {
                    Intent intent = new Intent(RatesActivity.this, ServiceBookingEnquiryActivity.class);
                    intent.putExtra("total", totalAmt.getText().toString());
                    intent.putExtra("coupon", coupon.getText().toString());
                    intent.putExtra(Constants.SERVICEID, servicesId);
                    intent.putExtra(Constants.LATITUDE, latitude);
                    intent.putExtra(Constants.LONGITUDE, longitude);
                    intent.putExtra(Constants.VENDORAMT,selectedService.getVendor_amount());
                    intent.putExtra("hours", selectedService.getHours());
                    intent.putExtra(Constants.VISITING,visitingCharge);
                    intent.putExtra(Constants.QTY,String.valueOf(counter));
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(),"please Select Hourly service ",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void getcategory(String serviceModel) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("catid", serviceModel);
        hashMap.put("latitude",latitude);
        hashMap.put("longitude",longitude);
        Call<CategoryLastResponse> call = RestClient.get().getSubsubCategory(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(), hashMap);
        call.enqueue(new Callback<CategoryLastResponse>() {
            @Override
            public void onResponse(@NonNull Call<CategoryLastResponse> call, @NonNull Response<CategoryLastResponse> response) {
                if (response.code() == 200) {
                    CategoryLastResponse categoryResponse = response.body();
                    if (categoryResponse.getStatus()) {
                        serviceList.clear();
                        if(categoryResponse.getData().size()>0) {
                            if (categoryResponse.getData().get(0).getServices().equals("")) {
                                service.setVisibility(View.GONE);
                            } else {
                                service.setVisibility(View.VISIBLE);
                                service.setText(categoryResponse.getData().get(0).getServices());
                            }
                        }
                            for(int i=0;i<categoryResponse.getHours().size();i++){
                                ServiceModel serviceModel = new ServiceModel();
                                serviceModel.setHours(categoryResponse.getHours().get(i).getHour());
                                serviceModel.setRps(categoryResponse.getHours().get(i).getAmount());
                                serviceModel.setSelected(false);
                                serviceModel.setService(categoryResponse.getHours().get(i).getServices());
                                serviceModel.setServiceName(categoryResponse.getHours().get(i).getServiceName());
                                serviceModel.setVendor_amount(categoryResponse.getHours().get(i).getVamount());
                                serviceModel.setVisitingAmt(categoryResponse.getHours().get(i).getVisitingamount());
                              //  serviceModel.setVisitingAmt("299");

                                serviceList.add(serviceModel);
                            }

                        adapter.setData(serviceList);
                        recyclerView.setAdapter(adapter);
                        if(serviceList.size()==0){
                            noFound.setVisibility(View.VISIBLE);
                            callUs.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        }else{
                            noFound.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            callUs.setVisibility(View.GONE);
                        }
                    }
                }else if(response.code()==401){
                    AppUtils.logout(RatesActivity.this);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CategoryLastResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public  void setData(){
        try {
            int total = Integer.parseInt(totalAmt.getText().toString());
            int flatamt = Integer.parseInt(discountAmount);
            int perAmt = Integer.parseInt(discountPer);
            if (isFlat) {
                int finalPrice = total - flatamt;
                totalAmt.setText(String.valueOf(finalPrice));
            } else {
                try {
                    int result = Math.round((total * perAmt) / 100);
                    percentageAmt.setText(String.valueOf(result));
                    int finaltotal = total - result;

                    totalAmt.setText(String.valueOf(finaltotal));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    public void applyCoupon(){
        HashMap<String,String>hashMap=new HashMap<>();
        hashMap.put("code",coupon.getText().toString());
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<CouponResponse> call=RestClient.get().applyCoupon(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<CouponResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<CouponResponse> call, @NonNull Response<CouponResponse> response) {
                CouponResponse couponResponse=response.body();
                if(response.code()==200) {
                    if (couponResponse.isStatus()) {
                        if(couponResponse!=null)
                        if(couponResponse.getType().equals("0")){
                            isFlat=true;
                            discountAmount=couponResponse.getDiscount();
                            flatAmt.setText(couponResponse.getDiscount());
                            perLinear.setVisibility(View.GONE);
                            amtLinear.setVisibility(View.VISIBLE);
                            setData();
                        }else{
                            isFlat=false;
                            discountPer=couponResponse.getDiscount();
                            percent.setText("Percentage ("+couponResponse.getDiscount()+"% )");
                            perLinear.setVisibility(View.VISIBLE);
                            amtLinear.setVisibility(View.GONE);
                            setData();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(),"Coupon Already Used ",Toast.LENGTH_SHORT).show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(RatesActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<CouponResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(),Constants.SERVERTIMEOUT,Toast.LENGTH_SHORT).show();

            }
        });
    }


  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        MenuItem skip = menu.findItem(R.id.skip);
        skip.setVisible(true);



        this.mMenu=menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement

        if (id == R.id.skip) {
            Intent intent=new Intent(RatesActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(RatesActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(RatesActivity.this,Id);

            }
        });



    }
}
