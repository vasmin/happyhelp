
package in.happyhelp.limra.activity.response.jobplanresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobPlanResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<JobPlan> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<JobPlan> getData() {
        return data;
    }

    public void setData(List<JobPlan> data) {
        this.data = data;
    }

}
