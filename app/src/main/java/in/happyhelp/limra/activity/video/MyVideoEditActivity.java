package in.happyhelp.limra.activity.video;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.ConfirmationActivity;
import in.happyhelp.limra.activity.property.PostPropertyActivity;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import in.happyhelp.limra.activity.response.videodetailresponse.VideoDetailResponse;
import in.happyhelp.limra.activity.response.videoplan.Datum;
import in.happyhelp.limra.activity.response.videoplan.VideoPlanResponse;
import in.happyhelp.limra.adapter.VideoPlanAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;

public class MyVideoEditActivity extends AppCompatActivity {


    @BindView(R.id.monthrentspinner)
    Spinner monthSpinner;
    VideoPlanAdapter adapter;
    List<Datum> list=new ArrayList<>();

    @BindView(R.id.amount)
    TextView amountTxt;

    @BindView(R.id.submit)
    TextView submit;

    @BindView(R.id.title)
    EditText title;

    @BindView(R.id.description)
    EditText description;

    @BindView(R.id.link)
    EditText link;


    @BindView(R.id.imagespinner)
    ImageView imageSpinner;

    String amount="00";
    String planId="0";

    Realm realm;
    String email="";
    String mobile="";
    String buyerName="";
    String vId="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_video_edit);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        ButterKnife.bind(this);
        adapter=new VideoPlanAdapter(this);
        realm= RealmHelper.getRealmInstance();


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            vId = extras.getString(Constants.VID);
            getVideoDetail(vId);
        }

        imageSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                monthSpinner.performClick();
            }
        });
        RealmResults<UserData> userData=realm.where(UserData.class).findAllAsync();
        userData.addChangeListener(new RealmChangeListener<RealmResults<UserData>>() {
            @Override
            public void onChange(RealmResults<UserData> userData) {
                if(userData!=null){
                    email=userData.get(0).getEmail();
                    mobile=userData.get(0).getMobile();
                    buyerName=userData.get(0).getName();
                }
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(title.getText().toString())) {
                    title.setError("Enter title ");
                    return;
                }


                if (TextUtils.isEmpty(link.getText().toString())) {
                    link.setError("Enter link ");
                    return;
                }


                if (TextUtils.isEmpty(description.getText().toString())) {
                    description.setError("Enter description ");
                    return;
                }

                if(amount.equals("00")){
                    Toast.makeText(getApplicationContext(), "Please Select Month", Toast.LENGTH_SHORT).show();
                    return;
                }

                callInstamojoPay(email,mobile,amount,"Video Promotion",buyerName);
            }
        });


        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(list.size()>i) {
                    amount = list.get(i).getAmount();

                    if (amount != null) {
                        amountTxt.setText("\u20B9 " + amount);
                        planId = list.get(i).getId();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        getVideoPlan();
    }

    public void getVideoDetail(final String videoId){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",videoId);
        Call<VideoDetailResponse> call=RestClient.get().getVideoDetail(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<VideoDetailResponse>() {
            @Override
            public void onResponse(@NonNull Call<VideoDetailResponse> call, @NonNull retrofit2.Response<VideoDetailResponse> response) {
                VideoDetailResponse videoDetailResponse=response.body();
                if(response.code()==200){
                    if(videoDetailResponse.getStatus()){
                        title.setText(videoDetailResponse.getData().getName());
                    }else{

                    }

                }else if(response.code()==401){
                    AppUtils.logout(MyVideoEditActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<VideoDetailResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }


    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);

                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();

                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);
                postVideo(payment);
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }

    public void postVideo(String paymentId){

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("title",title.getText().toString());
        hashMap.put("link", link.getText().toString());
        hashMap.put("description", description.getText().toString());
        hashMap.put("plan", planId);
        hashMap.put("payment_id", paymentId);
        hashMap.put("userid", SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));


        Call<Response> call= RestClient.get().postVideo(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull retrofit2.Response<Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){

                    if(response1.getStatus()) {
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();

                        Intent intent = new Intent(MyVideoEditActivity.this, ConfirmationActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(MyVideoEditActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void getVideoPlan(){
        Call<VideoPlanResponse> call= RestClient.get().getVideoPlan(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<VideoPlanResponse>() {
            @Override
            public void onResponse(@NonNull Call<VideoPlanResponse> call, @NonNull retrofit2.Response<VideoPlanResponse> response) {
                VideoPlanResponse videoPlanResponse=response.body();
                if(response.code()==200){
                    if(videoPlanResponse.getStatus()){
                        list.clear();

                        Datum datum=new Datum();
                        datum.setMonths("Select ");
                        datum.setAmount("00");
                        datum.setId("");
                        list.add(datum);
                        list.addAll(videoPlanResponse.getData());
                        adapter.setData(list);
                        monthSpinner.setAdapter(adapter);

                    }else{

                    }

                }else if(response.code()==401){
                    AppUtils.logout(MyVideoEditActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<VideoPlanResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });


    }

}
