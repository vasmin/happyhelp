package in.happyhelp.limra.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.ro.RoDetailActivity;
import in.happyhelp.limra.activity.wallet.SendWalletActivity;
import in.happyhelp.limra.adapter.MyWalletAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.mywallet.MyWalletResponse;
import in.happyhelp.limra.activity.response.mywallet.Wallet;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.total)
    TextView total;


    @BindView(R.id.data)
    TextView data;


    LinearLayoutManager linearLayoutManager;
    MyWalletAdapter adapter;
    RealmResults<MyWalletResponse> myWalletResponses;

    Menu mMenu;
    MenuItem menuItem;
    Realm realm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        AppUtils.isNetworkConnectionAvailable(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        realm=RealmHelper.getRealmInstance();

        adapter=new MyWalletAdapter(getApplicationContext(), new MyWalletAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(Wallet d, View view) throws ParseException {

            }
        });

        myWalletResponses=realm.where(MyWalletResponse.class).findAllAsync();
        myWalletResponses.addChangeListener(new RealmChangeListener<RealmResults<MyWalletResponse>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onChange(@NonNull RealmResults<MyWalletResponse> myWalletResponses) {
                if(myWalletResponses.size()>0) {


                    data.setVisibility(View.GONE);
                    adapter.setData(myWalletResponses.get(0).getData());
                    recyclerView.setAdapter(adapter);
                    total.setVisibility(View.VISIBLE);


                    if(myWalletResponses.get(0).getWalletTotal()!=0) {
                        total.setText(" Total " + "\u20B9 " + myWalletResponses.get(0).getWalletTotal());
                        if (myWalletResponses.get(0).getWalletTotal() > 30) {
                            if (mMenu != null) {
                                menuItem = mMenu.findItem(R.id.redeem);
                                menuItem.setVisible(true);


                                MenuItem trasfer=mMenu.findItem(R.id.wallettransfer);
                                trasfer.setVisible(true);

                            }
                        } else {
                            if (mMenu != null) {
                                menuItem = mMenu.findItem(R.id.redeem);
                                menuItem.setVisible(false);
                            }
                        }
                    }else{
                        total.setText(" Total " + "\u20B9 0");
                    }
                }else{
                    data.setVisibility(View.VISIBLE);
                    total.setVisibility(View.GONE);

                }
            }
        });

        getWallet();


        swipeRefreshLayout.setOnRefreshListener(WalletActivity.this);
        swipeRefreshLayout.setRefreshing(true);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
    }

    public void getWallet(){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<MyWalletResponse> call=RestClient.get().getMyWallet(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<MyWalletResponse>() {
            @Override
            public void onResponse(@NonNull Call<MyWalletResponse> call, @NonNull Response<MyWalletResponse> response) {
                swipeRefreshLayout.setRefreshing(false);
                final MyWalletResponse myWalletResponse=response.body();
                myWalletResponse.setId(1);
                if(response.code()==200){
                    if(myWalletResponse.getStatus()){


                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(MyWalletResponse.class);
                                realm.copyToRealmOrUpdate(myWalletResponse);
                            }
                        });
                    }else{

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(MyWalletResponse.class);
                                realm.copyToRealmOrUpdate(myWalletResponse);
                            }
                        });
                    }
                }else if(response.code()==401){
                    AppUtils.logout(WalletActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<MyWalletResponse> call, @NonNull Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
                t.printStackTrace();

            }
        });
    }



    @Override
    public void onRefresh() {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        MenuItem redeem=menu.findItem(R.id.redeem);

        MenuItem addwallet=menu.findItem(R.id.addwallet);
        addwallet.setVisible(true);



        this.mMenu=menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.redeem) {
           Intent intent=new Intent(WalletActivity.this,RedeemActivity.class);
           startActivity(intent);
        }


        if (id == R.id.wallettransfer) {
            Intent intent=new Intent(WalletActivity.this, SendWalletActivity.class);
            startActivity(intent);
        }

        if(id==R.id.addwallet){
            Intent intent=new Intent(WalletActivity.this,AddWalletActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWallet();
        try {
            EventBus.getDefault().register(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onRestart() {
        super.onRestart();
        //When BACK BUTTON is pressed, the activity on the stack is restarted
        //Do what you want on the refresh procedure here
        getWallet();
    }


    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(WalletActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(WalletActivity.this,Id);

            }
        });
    }
}
