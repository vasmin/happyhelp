
package in.happyhelp.limra.activity.response.businesskyc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("business_pan")
    @Expose
    private String businessPan;
    @SerializedName("aadhar_card")
    @Expose
    private String aadharCard;
    @SerializedName("gst")
    @Expose
    private String gst;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("bank_branch_name")
    @Expose
    private String bankBranchName;
    @SerializedName("bank_acc_no")
    @Expose
    private String bankAccNo;
    @SerializedName("ifsc_code")
    @Expose
    private String ifscCode;
    @SerializedName("gst_no")
    @Expose
    private String gstNo;
    @SerializedName("paytm")
    @Expose
    private String paytm;
    @SerializedName("phonepe")
    @Expose
    private String phonepe;
    @SerializedName("kycupload")
    @Expose
    private String kycupload;
    @SerializedName("kycapproved")
    @Expose
    private String kycapproved;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBusinessPan() {
        return businessPan;
    }

    public void setBusinessPan(String businessPan) {
        this.businessPan = businessPan;
    }

    public String getAadharCard() {
        return aadharCard;
    }

    public void setAadharCard(String aadharCard) {
        this.aadharCard = aadharCard;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankBranchName() {
        return bankBranchName;
    }

    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName;
    }

    public String getBankAccNo() {
        return bankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        this.bankAccNo = bankAccNo;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getPaytm() {
        return paytm;
    }

    public void setPaytm(String paytm) {
        this.paytm = paytm;
    }

    public String getPhonepe() {
        return phonepe;
    }

    public void setPhonepe(String phonepe) {
        this.phonepe = phonepe;
    }

    public String getKycupload() {
        return kycupload;
    }

    public void setKycupload(String kycupload) {
        this.kycupload = kycupload;
    }

    public String getKycapproved() {
        return kycapproved;
    }

    public void setKycapproved(String kycapproved) {
        this.kycapproved = kycapproved;
    }

}
