package in.happyhelp.limra.activity.wallet;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.DashboardResponse;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.AddWalletActivity;
import in.happyhelp.limra.activity.RedeemActivity;
import in.happyhelp.limra.adapter.ViewPagerAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.fragment.AllFragment;
import in.happyhelp.limra.fragment.DonateListFragment;
import in.happyhelp.limra.fragment.NewDonateFragment;
import in.happyhelp.limra.fragment.TransactionFragment;
import in.happyhelp.limra.network.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrollingWalletActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    TextView userName,balance,totalbalance,redeemable,cashback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling_wallet);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        initTab();

        NestedScrollView scrollView = (NestedScrollView) findViewById (R.id.nestedtscroll);
        scrollView.setFillViewport (true);

        AppBarLayout mAppBarLayout = findViewById(R.id.app_bar);
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle("Wallet");
                    isShow = true;
                } else if(isShow) {
                    collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }

        });


        totalbalance=collapsingToolbarLayout.findViewById(R.id.totalbalance);
        redeemable=collapsingToolbarLayout.findViewById(R.id.redeemable);
        cashback=collapsingToolbarLayout.findViewById(R.id.cashback);
        balance=collapsingToolbarLayout.findViewById(R.id.balance);
        userName=collapsingToolbarLayout.findViewById(R.id.profile_name);


        LinearLayout addmoney=collapsingToolbarLayout.findViewById(R.id.addmoney);
        addmoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ScrollingWalletActivity.this, AddWalletActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout sendmoney=collapsingToolbarLayout.findViewById(R.id.sendmoney);
        sendmoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ScrollingWalletActivity.this, SendWalletActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout redeem=collapsingToolbarLayout.findViewById(R.id.redeem);
        redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ScrollingWalletActivity.this, RedeemActivity.class);
                startActivity(intent);
            }
        });

        getDashboardWallet();

    }

    @Override
    protected void onResume() {
        getDashboardWallet();
        super.onResume();
    }

    public void getDashboardWallet(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<DashboardResponse> call= RestClient.get().getDashboardWallet(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<DashboardResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<DashboardResponse> call, Response<DashboardResponse> response) {
                DashboardResponse dashboardResponse=response.body();
                if(response.code()==200){
                    if(dashboardResponse.getStatus()){
                        totalbalance.setText("\u20B9 "+String.valueOf(dashboardResponse.getTotalBalance()));
                        redeemable.setText("\u20B9 "+String.valueOf(dashboardResponse.getWalletTotal()));
                        cashback.setText("\u20B9 "+String.valueOf(dashboardResponse.getCashbackWalletTotal()));
                        userName.setText(dashboardResponse.getUserName());
                        balance.setText("\u20B9 "+String.valueOf(dashboardResponse.getTotalBalance()));
                    }else{
                        totalbalance.setText("\u20B9 "+"0");
                        redeemable.setText("\u20B9 "+"0");
                        cashback.setText("\u20B9 "+"0");
                    }

                }else if(response.code()==401){
                    AppUtils.logout(getApplicationContext());
                }else{
                    totalbalance.setText("\u20B9 "+"0");
                    redeemable.setText("\u20B9 "+"0");
                    cashback.setText("\u20B9 "+"0");
                }

            }

            @Override
            public void onFailure(Call<DashboardResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    void initTab() {

        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
        tabLayout.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setupTabIcons() {
        String[] name = {"Wallet Trasaction","Cashback Trasaction"};
        Objects.requireNonNull(tabLayout.getTabAt(0)).setText(name[0]);
        Objects.requireNonNull(tabLayout.getTabAt(1)).setText(name[1]);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Bundle bundle = new Bundle();
        bundle.putString("edttext", "From Activity");

        adapter.addFrag(new AllFragment(), "Wallet Trasaction");
        adapter.addFrag(new TransactionFragment(), "Cashback Trasaction");

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onRefresh() {

    }
}
