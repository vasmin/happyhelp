package in.happyhelp.limra.activity.Jobs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.MyCompanyActivity;
import in.happyhelp.limra.activity.RegisterActivity;
import in.happyhelp.limra.activity.property.MembershipPropertyActivity;
import in.happyhelp.limra.activity.property.PostPropertyActivity;
import in.happyhelp.limra.activity.property.PropertyProfileActivity;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.companyprofile.Company;
import in.happyhelp.limra.activity.response.companyprofile.CompanyDetailsResponse;
import in.happyhelp.limra.activity.response.jobplanresponse.JobPlan;
import in.happyhelp.limra.activity.response.jobplanresponse.JobPlanResponse;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.adapter.MembershipAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;

public class MyJobCompanyActivity extends AppCompatActivity {
    @BindView(R.id.name)
    EditText companyName;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.description)
    EditText description;

    @BindView(R.id.location)
    EditText location;

    @BindView(R.id.address)
    EditText address;

    @BindView(R.id.weburl)
    EditText webUrl;

    @BindView(R.id.company)
    Spinner companySpinner;

    @BindView(R.id.createprofile)
    TextView createProfile;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.memberlinear)
    LinearLayout memberLinear;

    ArraySpinnerAdapter adapter;
    List<String> list=new ArrayList<>();
    List<JobPlan> jobList=new ArrayList<>();
    ProgressDialog progressBar;
    String company="";
    MembershipAdapter membershipAdapter;
    Realm realm;
    JobPlan selectedPlan;
    List<Company> companyProfileDetails;
    boolean isMembershipUpdate=false;

    @BindView(R.id.membership)
    TextView membership;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_job_company);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        realm= RealmHelper.getRealmInstance();
        getJobCompanyProfile();
        getMembershipPlan();
        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);



        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);

        companyProfileDetails=realm.where(Company.class).findAll();
        ((RealmResults<Company>) companyProfileDetails).addChangeListener(new RealmChangeListener<RealmResults<Company>>() {
            @Override
            public void onChange(@NonNull RealmResults<Company> data) {
                if(data.size()>0){
                    Company company=data.get(0);

                }
            }
        });

        membershipAdapter=new MembershipAdapter(this, new MembershipAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(JobPlan d, View view) throws ParseException {
                for(int i=0;i<jobList.size();i++){
                    JobPlan jobPlan=jobList.get(i);
                    if(d.getAmount().equals(jobPlan.getAmount())){
                        d.setSelected(true);
                        selectedPlan=d;
                    }else{
                        jobPlan.setSelected(false);
                    }
                }
                membershipAdapter.setData(jobList);
            }
        });

        createProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(companyName.getText().toString())) {
                    companyName.setError("Enter companyName ");
                    return;
                }

                if (TextUtils.isEmpty(email.getText().toString())) {
                    email.setError("Enter email ");
                    return;
                }

                if (TextUtils.isEmpty(mobile.getText().toString())) {
                    mobile.setError("Enter mobile ");
                    return;
                }

                if (TextUtils.isEmpty(location.getText().toString())) {
                    location.setError("Enter location ");
                    return;
                }

                if (TextUtils.isEmpty(address.getText().toString())) {
                    address.setError("Enter address ");
                    return;
                }

                if (TextUtils.isEmpty(description.getText().toString())) {
                    description.setError("Enter description ");
                    return;
                }

                if (TextUtils.isEmpty(webUrl.getText().toString())) {
                    webUrl.setError("Enter web Url ");
                    return;
                }


               /* if( !AppUtils.isValidURL(webUrl.getText().toString())){
                    webUrl.setError("Enter valid web Url ");
                    return;
                }*/

                /* if(selectedPlan==null){
                    Toast.makeText(getApplicationContext(), "Please Select Member Ship Plan", Toast.LENGTH_SHORT).show();
                    return;
                }*/

                updateJobCompany();
                //callInstamojoPay(email.getText().toString(),mobile.getText().toString(),selectedPlan.getAmount(),"Company MemberShip Plan",companyName.getText().toString());

            }
        });

        list.add("I am Company");
        list.add("Individual");
        list.add("Consultancy");
        adapter=new ArraySpinnerAdapter(this);
        adapter.setData(list);
        companySpinner.setAdapter(adapter);
        companySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                company=list.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        membership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isMembershipUpdate) {
                    Intent intent = new Intent(MyJobCompanyActivity.this, MembershipJobActivity.class);
                    startActivity(intent);
                }
            }
        });

    }

    public void updateJobCompany(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",  SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("company_name",companyName.getText().toString());
        hashMap.put("mobile",mobile.getText().toString());
        hashMap.put("email",email.getText().toString());
        hashMap.put("description",description.getText().toString());
        hashMap.put("location",location.getText().toString());
        hashMap.put("i_am",company);
        hashMap.put("address",address.getText().toString());
        hashMap.put("web_url",webUrl.getText().toString());


        Call<Response> call=RestClient.get().updateJobCompnay(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                progressBar.dismiss();
                Response response1=response.body();
                if(response.code()==200){

                    if(response1.getStatus()) {

                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();

                        new AlertDialog.Builder(MyJobCompanyActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(response1.getMessage())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .show();
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(MyJobCompanyActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void getJobCompanyProfile(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid", SharedPreferenceHelper.getInstance(MyJobCompanyActivity.this).getString(Constants.USERID));
        Call<CompanyDetailsResponse> call= RestClient.get().jobscompany_details(SharedPreferenceHelper.getInstance(MyJobCompanyActivity.this).getAuthToken(),hashMap);
        call.enqueue(new Callback<CompanyDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<CompanyDetailsResponse> call, @NonNull retrofit2.Response<CompanyDetailsResponse> response) {
                final CompanyDetailsResponse companyDetailsResponse=response.body();
                if(response.code()==200){
                    if(companyDetailsResponse.getStatus()){
                        if(companyDetailsResponse.getData()!=null) {

                            if(companyDetailsResponse.getData().size()>0) {
                                if(companyDetailsResponse.getData().size()>0) {
                                    if(!companyDetailsResponse.getData().get(0).isPlanexpired()) {
                                        SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(true);
                                    }else{
                                        SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(false);
                                    }
                                }

                                Company company = companyDetailsResponse.getData().get(0);
                                companyName.setText(company.getCompanyName());
                                email.setText(company.getEmail());
                                mobile.setText(company.getMobile());
                                location.setText(company.getLocation());
                                address.setText(company.getAddress());
                                description.setText(company.getDescription());
                                webUrl.setText(company.getWebUrl());

                                for(int i=0;i<list.size();i++){
                                    if(company.getIAm().equals(list.get(i)))
                                    {
                                        companySpinner.setSelection(i);
                                    }
                                }

                                if(company.isPlanexpired()){
                                    membership.setText("Membership plan Expired Please Renew plan");
                                    isMembershipUpdate=true;
                                }else{
                                    isMembershipUpdate=false;
                                    membership.setText("Membership plan Expire Date "+company.getPlanexpire()+"\n Your plan is "+company.getPlan());
                                }
                                createProfile.setText("Update Profile");
                                memberLinear.setVisibility(View.GONE);

                            }

                        } else
                        {
                            SharedPreferenceHelper.getInstance(MyJobCompanyActivity.this).setCompanyProfile(false);
                        }
                    }else{
                        SharedPreferenceHelper.getInstance(MyJobCompanyActivity.this).setCompanyProfile(false);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(MyJobCompanyActivity.this);
                }else{
                    SharedPreferenceHelper.getInstance(MyJobCompanyActivity.this).setCompanyProfile(false);

                }
            }

            @Override
            public void onFailure(@NonNull Call<CompanyDetailsResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                SharedPreferenceHelper.getInstance(MyJobCompanyActivity.this).setCompanyProfile(false);
                Toast.makeText(MyJobCompanyActivity.this,Constants.SERVERTIMEOUT,Toast.LENGTH_LONG).show();
            }
        });
    }

    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = MyJobCompanyActivity.this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);
                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();

                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);
                companyMembership(payment);
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(MyJobCompanyActivity.this, "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }


    public void getMembershipPlan(){
        Call<JobPlanResponse> call= RestClient.get().getJobPlan(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<JobPlanResponse>() {
            @Override
            public void onResponse(Call<JobPlanResponse> call, retrofit2.Response<JobPlanResponse> response) {

                final JobPlanResponse jobPlanResponse=response.body();

                if(response.code()==200){
                    if(jobPlanResponse.getStatus()){
                        jobList.clear();
                        jobList.addAll(jobPlanResponse.getData());
                        membershipAdapter.setData(jobList);
                        recyclerView.setAdapter(membershipAdapter);

                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(MyJobCompanyActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<JobPlanResponse> call, Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void companyMembership(String payment){
        progressBar.show();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String userID= SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID);
        builder.addFormDataPart("userid", userID);
        builder.addFormDataPart("company_name", companyName.getText().toString());
        builder.addFormDataPart("mobile", mobile.getText().toString());
        builder.addFormDataPart("email", email.getText().toString());
        //  builder.addFormDataPart("location", );
        builder.addFormDataPart("description", description.getText().toString());
        builder.addFormDataPart("location",location.getText().toString());
        builder.addFormDataPart("i_am", company);
        builder.addFormDataPart("address", address.getText().toString());
        builder.addFormDataPart("web_url", webUrl.getText().toString());
        builder.addFormDataPart("payment_id", payment);
        builder.addFormDataPart("plan", selectedPlan.getId());


       /* if(path!=null){
            File filepath = new File(path);
            builder.addFormDataPart("cv", filepath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), filepath));
        }*/

        MultipartBody requestBody = builder.build();





        Call<Response> call= RestClient.get().companyMemberShip(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {

                progressBar.dismiss();
                Response response1=response.body();
                if(response.code()==200){

                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                    SharedPreferenceHelper.getInstance(getApplicationContext()).setCompanyProfile(true);
                    Intent intent=new Intent(getApplicationContext(), PostJobsActivity.class);
                    startActivity(intent);
                    finish();

                }else if(response.code()==401){
                    AppUtils.logout(MyJobCompanyActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(MyJobCompanyActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(MyJobCompanyActivity.this,Id);

            }
        });
    }

}
