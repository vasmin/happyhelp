
package in.happyhelp.limra.tdsresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TdsResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("tds")
    @Expose
    private List<Td> tds = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Td> getTds() {
        return tds;
    }

    public void setTds(List<Td> tds) {
        this.tds = tds;
    }

}
