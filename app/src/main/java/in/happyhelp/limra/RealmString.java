package in.happyhelp.limra;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by bhushan on 12/12/2017.
 */

@RealmClass
public class RealmString extends RealmObject {
    public String value;

    public RealmString(){

    }

    public RealmString(String deserialize) {
        this.value = deserialize;
    }

    public String getString() {
        return value;
    }

    public void setString(String string) {
        this.value = string;
    }
}
