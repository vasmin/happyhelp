
package in.happyhelp.limra.shoppingresponse.productresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("vendorid")
    @Expose
    private String vendorid;
    @SerializedName("catid")
    @Expose
    private String catid;
    @SerializedName("subcat")
    @Expose
    private String subcat;
    @SerializedName("subsubcat")
    @Expose
    private String subsubcat;
    @SerializedName("vcatid")
    @Expose
    private String vcatid;
    @SerializedName("vsubcat")
    @Expose
    private String vsubcat;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("discountprice")
    @Expose
    private Integer discountprice;
    @SerializedName("stock")
    @Expose
    private String stock;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("tax_type")
    @Expose
    private String taxType;
    @SerializedName("shipping_charges")
    @Expose
    private String shippingCharges;
    @SerializedName("featured")
    @Expose
    private String featured;
    @SerializedName("sale")
    @Expose
    private String sale;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("seo_title")
    @Expose
    private String seoTitle;
    @SerializedName("seo_description")
    @Expose
    private String seoDescription;
    @SerializedName("seo_keywords")
    @Expose
    private String seoKeywords;
    @SerializedName("features")
    @Expose
    private String features;
    @SerializedName("return_policy")
    @Expose
    private String returnPolicy;
    @SerializedName("return_date_limit")
    @Expose
    private String returnDateLimit;
    @SerializedName("attributelist")
    @Expose
    private String attributelist;
    @SerializedName("marketplace")
    @Expose
    private String marketplace;
    @SerializedName("reviews")
    @Expose
    private String reviews;
    @SerializedName("variant")
    @Expose
    private List<Variant> variant = null;

    @SerializedName("wishlist")
    @Expose
    String wishlist;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVendorid() {
        return vendorid;
    }

    public void setVendorid(String vendorid) {
        this.vendorid = vendorid;
    }

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getSubcat() {
        return subcat;
    }

    public void setSubcat(String subcat) {
        this.subcat = subcat;
    }

    public String getSubsubcat() {
        return subsubcat;
    }

    public void setSubsubcat(String subsubcat) {
        this.subsubcat = subsubcat;
    }

    public String getVcatid() {
        return vcatid;
    }

    public void setVcatid(String vcatid) {
        this.vcatid = vcatid;
    }

    public String getVsubcat() {
        return vsubcat;
    }

    public void setVsubcat(String vsubcat) {
        this.vsubcat = vsubcat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public Integer getDiscountprice() {
        return discountprice;
    }

    public void setDiscountprice(Integer discountprice) {
        this.discountprice = discountprice;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(String shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public String getFeatured() {
        return featured;
    }

    public void setFeatured(String featured) {
        this.featured = featured;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSeoTitle() {
        return seoTitle;
    }

    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }

    public String getSeoDescription() {
        return seoDescription;
    }

    public void setSeoDescription(String seoDescription) {
        this.seoDescription = seoDescription;
    }

    public String getSeoKeywords() {
        return seoKeywords;
    }

    public void setSeoKeywords(String seoKeywords) {
        this.seoKeywords = seoKeywords;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getReturnPolicy() {
        return returnPolicy;
    }

    public void setReturnPolicy(String returnPolicy) {
        this.returnPolicy = returnPolicy;
    }

    public String getReturnDateLimit() {
        return returnDateLimit;
    }

    public void setReturnDateLimit(String returnDateLimit) {
        this.returnDateLimit = returnDateLimit;
    }

    public String getAttributelist() {
        return attributelist;
    }

    public void setAttributelist(String attributelist) {
        this.attributelist = attributelist;
    }

    public String getMarketplace() {
        return marketplace;
    }

    public void setMarketplace(String marketplace) {
        this.marketplace = marketplace;
    }

    public String getReviews() {
        return reviews;
    }

    public void setReviews(String reviews) {
        this.reviews = reviews;
    }

    public List<Variant> getVariant() {
        return variant;
    }

    public void setVariant(List<Variant> variant) {
        this.variant = variant;
    }

    public String getWishlist() {
        return wishlist;
    }

    public void setWishlist(String wishlist) {
        this.wishlist = wishlist;
    }
}
