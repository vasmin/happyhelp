
package in.happyhelp.limra.shoppingresponse.orderdetailresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Vendor {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("contactname")
    @Expose
    private String contactname;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("area")
    @Expose
    private Object area;
    @SerializedName("zipcode")
    @Expose
    private String zipcode;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("categories")
    @Expose
    private String categories;
    @SerializedName("mtoken")
    @Expose
    private String mtoken;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("verify")
    @Expose
    private String verify;
    @SerializedName("forgottoken")
    @Expose
    private String forgottoken;
    @SerializedName("documents")
    @Expose
    private String documents;
    @SerializedName("documentsubmit")
    @Expose
    private String documentsubmit;
    @SerializedName("documentverify")
    @Expose
    private String documentverify;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getArea() {
        return area;
    }

    public void setArea(Object area) {
        this.area = area;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getMtoken() {
        return mtoken;
    }

    public void setMtoken(String mtoken) {
        this.mtoken = mtoken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getVerify() {
        return verify;
    }

    public void setVerify(String verify) {
        this.verify = verify;
    }

    public String getForgottoken() {
        return forgottoken;
    }

    public void setForgottoken(String forgottoken) {
        this.forgottoken = forgottoken;
    }

    public String getDocuments() {
        return documents;
    }

    public void setDocuments(String documents) {
        this.documents = documents;
    }

    public String getDocumentsubmit() {
        return documentsubmit;
    }

    public void setDocumentsubmit(String documentsubmit) {
        this.documentsubmit = documentsubmit;
    }

    public String getDocumentverify() {
        return documentverify;
    }

    public void setDocumentverify(String documentverify) {
        this.documentverify = documentverify;
    }

}
