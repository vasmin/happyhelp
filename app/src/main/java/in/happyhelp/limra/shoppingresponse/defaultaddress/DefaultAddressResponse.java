
package in.happyhelp.limra.shoppingresponse.defaultaddress;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DefaultAddressResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("address")
    @Expose
    private Address address;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
