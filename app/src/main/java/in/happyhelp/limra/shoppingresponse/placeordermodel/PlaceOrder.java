
package in.happyhelp.limra.shoppingresponse.placeordermodel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlaceOrder {

    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("amount")
    @Expose
    private Amount amount;
    @SerializedName("products")
    @Expose
    private List<Product> products = null;
    @SerializedName("promo")
    @Expose
    private Promo promo;
    @SerializedName("wallet")
    @Expose
    private Wallet wallet;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Promo getPromo() {
        return promo;
    }

    public void setPromo(Promo promo) {
        this.promo = promo;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

}
