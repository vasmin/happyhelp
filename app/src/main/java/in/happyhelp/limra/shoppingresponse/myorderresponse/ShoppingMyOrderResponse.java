
package in.happyhelp.limra.shoppingresponse.myorderresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShoppingMyOrderResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("myorders")
    @Expose
    private List<Myorder> myorders = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Myorder> getMyorders() {
        return myorders;
    }

    public void setMyorders(List<Myorder> myorders) {
        this.myorders = myorders;
    }

}
