
package in.happyhelp.limra.shoppingresponse.subcategoryresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShoppingSubCategoryResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("subcategories")
    @Expose
    private List<Subcategory> subcategories = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Subcategory> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<Subcategory> subcategories) {
        this.subcategories = subcategories;
    }

}
