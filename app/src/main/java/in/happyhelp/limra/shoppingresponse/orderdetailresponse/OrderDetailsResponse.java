
package in.happyhelp.limra.shoppingresponse.orderdetailresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetailsResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("orderdetails")
    @Expose
    private Orderdetails orderdetails;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Orderdetails getOrderdetails() {
        return orderdetails;
    }

    public void setOrderdetails(Orderdetails orderdetails) {
        this.orderdetails = orderdetails;
    }

}
