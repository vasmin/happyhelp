
package in.happyhelp.limra.shoppingresponse.homeresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeScreenResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("banner_products")
    @Expose
    private List<BannerProduct> bannerProducts = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<BannerProduct> getBannerProducts() {
        return bannerProducts;
    }

    public void setBannerProducts(List<BannerProduct> bannerProducts) {
        this.bannerProducts = bannerProducts;
    }

}
