
package in.happyhelp.limra.shoppingresponse.wishlistresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WishListResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("wishlists")
    @Expose
    private List<Wishlist> wishlists = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Wishlist> getWishlists() {
        return wishlists;
    }

    public void setWishlists(List<Wishlist> wishlists) {
        this.wishlists = wishlists;
    }

}
