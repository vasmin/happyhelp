
package in.happyhelp.limra.shoppingresponse.categoryresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShoppingCategoryResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

}
