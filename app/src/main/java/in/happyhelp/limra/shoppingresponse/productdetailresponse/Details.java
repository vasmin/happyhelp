
package in.happyhelp.limra.shoppingresponse.productdetailresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details {

    @SerializedName("sdds")
    @Expose
    private String sdds;
    @SerializedName("dfffffff")
    @Expose
    private String dfffffff;

    public String getSdds() {
        return sdds;
    }

    public void setSdds(String sdds) {
        this.sdds = sdds;
    }

    public String getDfffffff() {
        return dfffffff;
    }

    public void setDfffffff(String dfffffff) {
        this.dfffffff = dfffffff;
    }

}
