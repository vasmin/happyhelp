
package in.happyhelp.limra.shoppingresponse.addressresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddressListResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("addresses")
    @Expose
    private List<Address> addresses = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

}
