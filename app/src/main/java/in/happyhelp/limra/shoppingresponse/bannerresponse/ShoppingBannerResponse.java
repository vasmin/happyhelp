
package in.happyhelp.limra.shoppingresponse.bannerresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShoppingBannerResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("banners")
    @Expose
    private List<Banner> banners = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Banner> getBanners() {
        return banners;
    }

    public void setBanners(List<Banner> banners) {
        this.banners = banners;
    }

}
