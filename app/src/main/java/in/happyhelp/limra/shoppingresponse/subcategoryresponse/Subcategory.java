
package in.happyhelp.limra.shoppingresponse.subcategoryresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subcategory {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("catid")
    @Expose
    private String catid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("subsubcategories")
    @Expose
    private List<Subsubcategory> subsubcategories = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Subsubcategory> getSubsubcategories() {
        return subsubcategories;
    }

    public void setSubsubcategories(List<Subsubcategory> subsubcategories) {
        this.subsubcategories = subsubcategories;
    }

}
