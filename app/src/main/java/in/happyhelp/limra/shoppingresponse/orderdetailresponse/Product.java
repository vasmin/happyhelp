
package in.happyhelp.limra.shoppingresponse.orderdetailresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.happyhelp.limra.shoppingresponse.homeresponse.Variant;

public class Product {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("vendor_id")
    @Expose
    private String vendorId;
    @SerializedName("odid")
    @Expose
    private String odid;
    @SerializedName("pid")
    @Expose
    private String pid;
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("variants")
    @Expose
    private String variants;
    @SerializedName("shipping_charges")
    @Expose
    private String shippingCharges;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("commission")
    @Expose
    private String commission;
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("image")
    @Expose
    private String image;


    @SerializedName("variant_details")
    @Expose
    private Variant variantDetails;
    @SerializedName("total_price")
    @Expose
    private Integer totalPrice;

    @SerializedName("return")
    @Expose
    private Integer return_status;

    @SerializedName("return_status")
    @Expose
    private String returnData;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getOdid() {
        return odid;
    }

    public void setOdid(String odid) {
        this.odid = odid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getVariants() {
        return variants;
    }

    public void setVariants(String variants) {
        this.variants = variants;
    }

    public String getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(String shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public Object getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Variant getVariantDetails() {
        return variantDetails;
    }

    public void setVariantDetails(Variant variantDetails) {
        this.variantDetails = variantDetails;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getReturn_status() {
        return return_status;
    }

    public void setReturn_status(Integer return_status) {
        this.return_status = return_status;
    }

    public String getReturnData() {
        return returnData;
    }

    public void setReturnData(String returnData) {
        this.returnData = returnData;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
