
package in.happyhelp.limra.shoppingresponse.subcategoryresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subsubcategory {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("subcat")
    @Expose
    private String subcat;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubcat() {
        return subcat;
    }

    public void setSubcat(String subcat) {
        this.subcat = subcat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
