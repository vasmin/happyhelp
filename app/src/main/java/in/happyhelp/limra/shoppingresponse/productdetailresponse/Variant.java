
package in.happyhelp.limra.shoppingresponse.productdetailresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Variant {

    @SerializedName("variant")
    @Expose
    private List<String> variant = null;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("discprice")
    @Expose
    private Integer discprice;
    @SerializedName("sku")
    @Expose
    private String sku;

    public List<String> getVariant() {
        return variant;
    }

    public void setVariant(List<String> variant) {
        this.variant = variant;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getDiscprice() {
        return discprice;
    }

    public void setDiscprice(Integer discprice) {
        this.discprice = discprice;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

}
