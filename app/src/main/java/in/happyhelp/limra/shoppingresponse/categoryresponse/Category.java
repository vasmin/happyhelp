
package in.happyhelp.limra.shoppingresponse.categoryresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("commission")
    @Expose
    private String commission;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("attributes")
    @Expose
    private String attributes;
    @SerializedName("featured")
    @Expose
    private String featured;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("sub_category_available")
    @Expose
    private String subCategoryAvailable;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getFeatured() {
        return featured;
    }

    public void setFeatured(String featured) {
        this.featured = featured;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubCategoryAvailable() {
        return subCategoryAvailable;
    }

    public void setSubCategoryAvailable(String subCategoryAvailable) {
        this.subCategoryAvailable = subCategoryAvailable;
    }

}
