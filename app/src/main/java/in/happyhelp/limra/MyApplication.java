package in.happyhelp.limra;

import android.app.Application;
import android.support.multidex.MultiDex;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import io.realm.Realm;


public class MyApplication extends Application {

    public static final String TAG = MyApplication.class
            .getSimpleName();



    private static MyApplication mInstance;
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Realm.init(this);

        MultiDex.install(this);
        mInstance = this;
    }

}