package in.happyhelp.limra.network;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by Pranit on 08-09-2016.
 */

public class NetworkSingleton {
    private static NetworkSingleton instance;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;
    private static Context context;

    public static final String TAG = NetworkSingleton.class.getSimpleName();

    public NetworkSingleton(Context context) {
        this.context = context;
        requestQueue = getRequestQueue();

        imageLoader = new ImageLoader(requestQueue, new LruBitmapCache());
    }

    /**
     * Return singleton object of this class, so that it can be used anywhere in the application
     *
     * @param context Any subclass of {@link Context} class
     * @return instance of {@link NetworkSingleton}
     */
    public static synchronized NetworkSingleton getInstance(Context context) {
        if (instance == null) {
            instance = new NetworkSingleton(context);
        }

        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return requestQueue;
    }

    /**
     * Add request to queue without tag
     *
     * @param req request to add in queue
     * @param <T> Any subclass of {@link Request} class
     */
    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    /**
     * Add request to queue with tag
     *
     * @param req request to add in queue
     * @param tag Tag for request
     * @param <T> Any subclass of {@link Request} class
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    /**
     * @return {@link ImageLoader}
     */
    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    /**
     * Cancel pending requests with tag specified
     *
     * @param tag to cancel request specified by tag
     */
    public void cancelPendingRequests(Object tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }
}
