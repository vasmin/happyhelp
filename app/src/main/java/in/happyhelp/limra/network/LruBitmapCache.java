package in.happyhelp.limra.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.util.DisplayMetrics;

import com.android.volley.toolbox.ImageLoader;

/**
 * Created by Pranit on 08-09-2016.
 */

public class LruBitmapCache extends LruCache<String, Bitmap> implements ImageLoader.ImageCache {
    /**
     * Get default cache size for LruBitmapCache
     *
     * @return
     */
    public static int getDefaultLruCacheSize() {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        return (maxMemory / 8);
    }

    /**
     * Returns a cache size equal to approximately three screens worth of images.
     *
     * @param ctx context of the application
     * @return
     */
    public static int getCacheSize(Context ctx) {
        final DisplayMetrics displayMetrics = ctx.getResources().getDisplayMetrics();
        final int screenWidth = displayMetrics.widthPixels;
        final int screenHeight = displayMetrics.heightPixels;
        // 4 bytes per pixel
        final int screenBytes = screenWidth * screenHeight * 4;

        return screenBytes * 3;
    }

    public LruBitmapCache() {
        this(getDefaultLruCacheSize());
    }

    /**
     * @param sizeInKiloBytes for caches that do not override {@link #sizeOf}, this is
     *                        the maximum number of entries in the cache. For all other caches,
     *                        this is the maximum sum of the sizes of the entries in this cache.
     */
    public LruBitmapCache(int sizeInKiloBytes) {
        super(sizeInKiloBytes);
    }

    /**
     * Returns size of the image cache
     *
     * @param key
     * @param value
     * @return
     */
    @Override
    protected int sizeOf(String key, Bitmap value) {
        return value.getRowBytes() * value.getHeight() / 1024;
    }

    /**
     * Returns {@link Bitmap} for the specified url
     *
     * @param url to get bitmap image
     * @return
     */
    @Override
    public Bitmap getBitmap(String url) {
        return get(url);
    }

    /**
     * Puts {@link Bitmap} in the specified {@code url}
     *
     * @param url    key to put {@link Bitmap}
     * @param bitmap actual {@link Bitmap}
     */
    @Override
    public void putBitmap(String url, Bitmap bitmap) {
        put(url, bitmap);
    }
}
