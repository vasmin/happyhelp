package in.happyhelp.limra.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import in.happyhelp.limra.RealmString;
import io.realm.RealmList;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by tushar on 12/2/17.
 */

public class RestClient {


    private static APIService REST_CLIENT = null;
    public static String BASE_URL = "https://www.happyhelpp.com/vendorapi/";
    public static String base_image_url = "https://www.happyhelpp.com/";
    public static  String SHOPPING_BASE_URL="https://www.happyhelpp.com/shoppingapi/";
    public static  String PROPERTY_BASE_URL="https://www.happyhelpp.com/propertyapi/";
    public static final String RECHARGE_BASE_URL = "https://www.happyhelpp.com/Rechargeapi/";

    static {
        setupRestClient();
    }

    private RestClient() {
    }

    public static APIService get() {
        return REST_CLIENT;
    }

    public static APIService get(String baseUrl) {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client.interceptors().add(interceptor);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client.build())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(APIService.class);
    }

    private static void setupRestClient() {

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client.interceptors().add(interceptor);

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(new TypeToken<RealmList<RealmString>>() {}.getType(),
                        new TagRealmListConverter())
                .setLenient()
                .create();

        if (REST_CLIENT == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client.build())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            REST_CLIENT = retrofit.create(APIService.class);
        }
    }
}
