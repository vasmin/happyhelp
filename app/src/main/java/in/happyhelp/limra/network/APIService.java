package in.happyhelp.limra.network;
import java.util.HashMap;
import java.util.Objects;

import in.happyhelp.limra.DashboardResponse;
import in.happyhelp.limra.activity.response.CouponResponse;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.adslistresponse.AdsListResponse;
import in.happyhelp.limra.activity.response.adssizeresponse.AdsSizeResponse;
import in.happyhelp.limra.activity.response.bannerresponse.BannerResponse;
import in.happyhelp.limra.activity.response.businesskyc.BusinessKycResponse;
import in.happyhelp.limra.activity.response.categorylastresponse.CategoryLastResponse;
import in.happyhelp.limra.activity.response.categoryresponse.CategoryResponse;
import in.happyhelp.limra.activity.response.citiresponse.CitiesResponse;
import in.happyhelp.limra.activity.response.companydetailsresponse.CompanyDetailsResponse;
import in.happyhelp.limra.activity.response.couponresponse.CouponsResponse;
import in.happyhelp.limra.activity.response.donateenquiry.DonateEnquiryResponse;
import in.happyhelp.limra.activity.response.donatelist.DonateListResponse;
import in.happyhelp.limra.activity.response.jobapplicationresponse.JobApplicationResponse;
import in.happyhelp.limra.activity.response.jobcategory.JobCategoryResponse;
import in.happyhelp.limra.activity.response.jobdetail.JobDetailResponse;
import in.happyhelp.limra.activity.response.jobdetailresponse.JobDetailsResponse;
import in.happyhelp.limra.activity.response.joblistresponse.JobListResponse;
import in.happyhelp.limra.activity.response.jobplanresponse.JobPlanResponse;
import in.happyhelp.limra.activity.response.jobsubresponse.JobSubcategoryResponse;
import in.happyhelp.limra.activity.response.loginresponse.LoginResponse;
import in.happyhelp.limra.activity.response.mydetailresponse.MyPatientDetailResponse;
import in.happyhelp.limra.activity.response.mydonationdetails.MyDonationDetailResponse;
import in.happyhelp.limra.activity.response.mydonationresponse.MyDonationResponse;
import in.happyhelp.limra.activity.response.mypatientresponse.MyPatientResponse;
import in.happyhelp.limra.activity.response.mypropertyenquiryresponse.MyPropertyEnquiryResponse;
import in.happyhelp.limra.activity.response.myrodetailresponse.MyRoDetailsResponse;
import in.happyhelp.limra.activity.response.myroresponse.MyRoResponse;
import in.happyhelp.limra.activity.response.myservicesresponse.MyServicesResponse;
import in.happyhelp.limra.activity.response.myvideolistresponse.MyVideoListResponse;
import in.happyhelp.limra.activity.response.mywallet.MyWalletResponse;
import in.happyhelp.limra.activity.response.notificationresponse.NotificationResponse;
import in.happyhelp.limra.activity.response.patientresponse.PatientResponse;
import in.happyhelp.limra.activity.response.propertiesresponse.PropertyListResponse;
import in.happyhelp.limra.activity.response.propertydetailsresponse.PropertyDetailResponse;
import in.happyhelp.limra.activity.response.rodetails.RoDetailResponse;
import in.happyhelp.limra.activity.response.roresponse.RoResponse;
import in.happyhelp.limra.activity.response.savedjobresponse.SavedListResponse;
import in.happyhelp.limra.activity.response.serrviceresponse.ServiceBannerResponse;
import in.happyhelp.limra.activity.response.servicecategoryresponse.ServiceCategoryResponse;
import in.happyhelp.limra.activity.response.servicechargesresponse.ServiceChargeResponse;
import in.happyhelp.limra.activity.response.servicelistresponse.ServicesListResponse;
import in.happyhelp.limra.activity.response.statecityresponse.StateCityResponse;
import in.happyhelp.limra.activity.response.treeresponse.TreeResponse;
import in.happyhelp.limra.activity.response.videodetailresponse.VideoDetailResponse;
import in.happyhelp.limra.activity.response.videoplan.VideoPlanResponse;
import in.happyhelp.limra.activity.response.videoresponse.VideoListResponse;
import in.happyhelp.limra.cashbackwalletresponse.CashbackWalletResponse;
import in.happyhelp.limra.gstresponse.GstRateResponse;
import in.happyhelp.limra.membershipresponse.MembershipResponse;
import in.happyhelp.limra.myadsresponse.MyAdsResponse;
import in.happyhelp.limra.receiverresponse.ReceiverInfoResponse;
import in.happyhelp.limra.rechargeresponse.RechargeResponse;
import in.happyhelp.limra.rechargeresponse.SecurreResponse;
import in.happyhelp.limra.rechargeresponse.operator.OperatorResponse;
import in.happyhelp.limra.rechargeresponse.rechargelist.RechargeListResponse;
import in.happyhelp.limra.shoppingresponse.addressdetails.AddressResponse;
import in.happyhelp.limra.shoppingresponse.addressresponse.AddressListResponse;
import in.happyhelp.limra.shoppingresponse.bannerresponse.ShoppingBannerResponse;
import in.happyhelp.limra.shoppingresponse.categoryresponse.ShoppingCategoryResponse;
import in.happyhelp.limra.shoppingresponse.defaultaddress.DefaultAddressResponse;
import in.happyhelp.limra.shoppingresponse.myorderresponse.ShoppingMyOrderResponse;
import in.happyhelp.limra.shoppingresponse.orderdetailresponse.OrderDetailsResponse;
import in.happyhelp.limra.shoppingresponse.placeordermodel.PlaceOrder;
import in.happyhelp.limra.shoppingresponse.productdetailresponse.ProductDetailResponse;
import in.happyhelp.limra.shoppingresponse.productresponse.ProductResponse;
import in.happyhelp.limra.shoppingresponse.promodata.PromoData;
import in.happyhelp.limra.shoppingresponse.propertyprofileresponse.PropertyProfileResponse;
import in.happyhelp.limra.shoppingresponse.subcategoryresponse.SubCategoryResponse;
import in.happyhelp.limra.shoppingresponse.wishlistresponse.WishListResponse;
import in.happyhelp.limra.tdsresponse.TdsResponse;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface APIService {

    @POST("login")
    Call<LoginResponse> signInUser(@Body HashMap<String, String> hashMap);

    @POST("signup")
    Call<Response> registerUser(@Body HashMap<String, String> hashMap);

    @POST("otpverify")
    Call<LoginResponse> verifyOtp(@Body HashMap<String, String> hashMap);

    @POST("roenquiry")
    Call<Response> roBooking(@Header("Authorization") String auth,@Body HashMap<String, String> hashMap);

    @POST("forgotpassword")
    Call<Response> forgetPassword( @Body HashMap<String, String> hashMap);

    @POST("rodetails")
    Call<RoDetailResponse> roDetail(@Header("Authorization") String auth,@Body HashMap<String, String> hashMap);

    @POST("myrodetails")
    Call<MyRoDetailsResponse> myRoDetail(@Header("Authorization") String auth, @Body HashMap<String, String> hashMap);

    @POST("myrolist")
    Call<MyRoResponse> myRoList(@Header("Authorization") String auth, @Body HashMap<String, String> hashMap);

    @POST("myadslist")
    Call<MyAdsResponse> myAdsList(@Header("Authorization") String auth, @Body HashMap<String, String> hashMap);

    @POST("robillstransaction")
    Call<Object> myRoBill(@Header("Authorization") String auth, @Body HashMap<String, String> hashMap);

    @POST("wallet_transaction")
    Call<MyWalletResponse> getMyWallet(@Header("Authorization") String auth, @Body HashMap<String, String> hashMap);

    @POST("cashback_wallet_transaction")
    Call<CashbackWalletResponse> getCashbackMyWallet(@Header("Authorization") String auth, @Body HashMap<String, String> hashMap);

    @POST("redeem_request")
    Call<in.happyhelp.limra.activity.response.Response> redeemCode(@Header("Authorization") String auth, @Body HashMap<String, String> hashMap);

    @POST("donateenquiry")
    Call<Response> postDonate(@Header("Authorization") String auth,@Body RequestBody file);

    @POST("donatecontact")
    Call<Response> enquiryDonate(@Header("Authorization") String auth,@Body HashMap<String,String> hashMap);

    @POST("enquiry")
    Call<Response> propertyEnquiry(@Header("Authorization") String auth,@Body HashMap<String,String> hashMap);

    @GET("rolist")
    Call<RoResponse> getRoList(@Header("Authorization") String auth);

    @GET("state")
    Call<StateCityResponse> getStateCity(@Header("Authorization") String auth);

    @GET("banners")
    Call<BannerResponse> getBanner(@Header("Authorization") String auth);

    @GET("servicebanners")
    Call<ServiceBannerResponse> getServiceBanner(@Header("Authorization") String auth);

    @GET("propertybanners")
    Call<ServiceBannerResponse> getPropertyBanner(@Header("Authorization") String auth);

    @GET("donatelist")
    Call<DonateListResponse> getDonateList(@Header("Authorization") String auth);

    @GET("couponbanners")
    Call<CouponsResponse> getCoupons(@Header("Authorization") String auth);

    @GET("footerads")
    Call<CouponsResponse> getFooter(@Header("Authorization") String auth);

    @GET("adsbanner")
    Call<AdsListResponse> getAdsList(@Header("Authorization") String auth);

    @POST("mydonatelist")
    Call<MyDonationResponse> getMyDonateList(@Header("Authorization") String auth,@Body HashMap<String,String> hashMap);

    @POST("donatedetails")
    Call<MyDonationDetailResponse> getMyDonationDetail(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("adssize")
    Call<AdsSizeResponse> getAdsSize(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("adsclick")
    Call<Response> getPoints(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("adsenquiry")
    Call<Response> postAds(@Header("Authorization") String auth,@Body RequestBody file);

    @POST("userdetails")
    Call<LoginResponse> getUserDetails(@Header("Authorization") String auth,@Body  HashMap<String,String> hashMap);

    @POST("profileupdate")
    Call<Response> profileUpdates(@Header("Authorization") String auth, @Body RequestBody file);

    @POST("servicelist")
    Call<ServicesListResponse> getServicesList(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("servicedetails")
    Call<CompanyDetailsResponse> getServiceDetails(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("servicedetails")
    Call<CompanyDetailsResponse> getCompanyDetails(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("serviceenquiry")
    Call<Response> serviceEnquiry(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("myserviceenquiry")
    Call<MyServicesResponse> myServicesEnquiry(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("serviceenquirydetails")
    Call<MyServicesResponse> myServicesEnquiryDetails(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("serviceenquirydetails")
    Call<Object> postServicePromotion(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @GET("servicecategory")
    Call<ServiceCategoryResponse> getServicesCategory(@Header("Authorization") String auth);

    @POST("mainservicecategory")
    Call<ServiceCategoryResponse> getMainServices(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @GET("servicecharges")
    Call<ServiceChargeResponse> getServiceCharges(@Header("Authorization") String auth);

    @POST("servicelisting")
    Call<Response> postCompanyServices(@Header("Authorization") String auth, @Body RequestBody file);

    @POST("updateservicelisiting")
    Call<Response> updateCompanyServices(@Header("Authorization") String auth, @Body RequestBody file);

    @POST("promoteservice")
    Call<Response> payforPrmotion(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("deleteimage")
    Call<Response> deleteGallary(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("getcategory")
    Call<CategoryResponse> getSubCategory(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("getcategory")
    Call<CategoryLastResponse> getSubsubCategory(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("apply_coupon")
    Call<CouponResponse> applyCoupon(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("popupenquiry")
    Call<Response> serviceEnquiryPost(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("popuppayment")
    Call<Response> servicePaymentUpdate(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("fcmtokenupdate")
    Call<Object> updatefcm(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("update_service_enquiry")
    Call<Response> acceptService(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("complete_service_enquiry")
    Call<Response> completeService(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("reach_service_enquiry")
    Call<Response> setReachLocation(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("confirmbyvendor_service_enquiry")
    Call<Response> setConfirmOrder(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("reject_service_enquiry")
    Call<Response> serviceReject(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("confirm_reach_service_enquiry")
    Call<Response> verifyReachOtp(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("myorders")
    Call<MyServicesResponse> MyOrder(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("cancel_service_enquiry")
    Call<Response> serviceCancel(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("kycupdate")
    Call<Response> kycSubmit(@Header("Authorization") String auth, @Body RequestBody file);

    @POST("business_kycupdate")
    Call<Response> businessKycSubmit(@Header("Authorization") String auth, @Body RequestBody file);


    @POST("getDonationEnquiry")
    Call<DonateEnquiryResponse> getDonateEnquiry(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @POST("add_wallet_amount")
    Call<Response> addWalleyMoney(@Header("Authorization") String auth, @Body  HashMap<String,String> hashMap);

    @GET("jobscategory")
    Call<JobCategoryResponse> getJobCategory(@Header("Authorization") String auth);

    @POST("jobslist")
    Call<JobListResponse> getJobList(@Header("Authorization") String auth,@Body HashMap<String,String> hashMap);

    @POST("postjobslist")
    Call<JobListResponse> getMyJobList(@Header("Authorization") String auth,@Body HashMap<String,String> hashMap);


    @POST("get_enquiry_list")
    Call<MyPropertyEnquiryResponse> getMyPropertyEnquiry(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);



    @POST("changepassword")
    Call<Response> changePassword(@Header("Authorization") String auth, @Body HashMap<String, String> hashMap);

    @POST("resendotp")
    Call<Response> resendOtp(@Header("Authorization") String auth, @Body HashMap<String, String> hashMap);

    @POST("postjob")
    Call<Response> postJob(@Header("Authorization") String auth,@Body RequestBody file);

    @POST("update_job")
    Call<Response> updateJob(@Header("Authorization") String auth,@Body RequestBody file);



    @POST("update_jobscompany")
    Call<Response> updateJobCompnay(@Header("Authorization") String auth,@Body HashMap<String,String> hashMap);

    @POST("jobdetails")
    Call<JobDetailResponse> jobdetails(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("jobapply")
    Call<Response> jobapply(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("savedjoblist")
    Call<SavedListResponse> getSavedJobList(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("appliedjoblist")
    Call<SavedListResponse> getAppliedJobList(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("jobapplicants")
    Call<JobApplicationResponse> getJobApplications(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("jobprofile")
    Call<Response> jobProfile(@Header("Authorization") String auth,@Body RequestBody file);

    @POST("jobprofile_details")
    Call<JobDetailsResponse> jobProfileDetail(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("jobscompany_details")
    Call<in.happyhelp.limra.activity.response.companyprofile.CompanyDetailsResponse> jobscompany_details(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("propertycompany_details")
    Call<in.happyhelp.limra.activity.response.companyprofile.CompanyDetailsResponse> propertycompany_details(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("job_membership")
    Call<Response> companyMemberShip(@Header("Authorization") String auth,@Body RequestBody file);

    @POST("property_membership")
    Call<Response> companyPropertyMemberShip(@Header("Authorization") String auth,@Body RequestBody file);

    @POST("update_propertycompany")
    Call<Response> updatePropertyCompany(@Header("Authorization") String auth,@Body RequestBody file);

    @POST("upgrade_propertymembership")
    Call<Response> updatePropertyMembership(@Header("Authorization") String auth,@Body RequestBody file);

    @POST("upgrade_jobmembership")
    Call<Response> updateJobMembership(@Header("Authorization") String auth,@Body RequestBody file);

    @POST("add_deal")
    Call<Response> postProperty(@Header("Authorization") String auth,@Body RequestBody file);

    @GET("getcities")
    Call<CitiesResponse> getCities(@Header("Authorization") String auth);

    @POST("submit_deal_editdata")
    Call<Response> updateProperty(@Header("Authorization") String auth,@Body RequestBody file);

    @POST("delete_deal")
    Call<Response> deleteProperty(@Header("Authorization") String auth,@Body HashMap<String,String> hashMap);

    @GET("jobplans")
    Call<JobPlanResponse> getJobPlan(@Header("Authorization") String auth);

    @GET("propertyplans")
    Call<JobPlanResponse> getPropertyPlan(@Header("Authorization") String auth);



    @POST("savedjobs")
    Call<Response> savedJob(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @GET("patientlist")
    Call<PatientResponse> getPatientLst(@Header("Authorization") String auth);

    @POST("postpatient")
    Call<Response> postPatient(@Header("Authorization") String auth,@Body RequestBody file);

    @POST("editpatient")
    Call<Response> updatePatient(@Header("Authorization") String auth,@Body RequestBody file);

    @POST("mypatientlist")
    Call<MyPatientResponse> getMyPatient(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("patientdetails")
    Call<MyPatientDetailResponse> getMyPatientDetails(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @GET("youtubevideoslist")
    Call<VideoListResponse> getVideoList(@Header("Authorization") String auth);

    @GET("videoplans")
    Call<VideoPlanResponse> getVideoPlan(@Header("Authorization") String auth);

    @POST("postyoutubevideo")
    Call<Response> postVideo(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("myvideolist")
    Call<MyVideoListResponse> getMyVideoList(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("youtubevideodetails")
    Call<VideoDetailResponse> getVideoDetail(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("reach_resendotp")
    Call<Response> resendOtpService(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("jobsubcategory")
    Call<JobSubcategoryResponse> jobSubcategory(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @GET("get_property_list")
    Call<PropertyListResponse> getAllProperty(@Header("Authorization")String auth);

    @POST("search")
    Call<PropertyListResponse> searchProperties(@Header("Authorization")String auth, @Body HashMap<String,String> hashMap);

    @POST("get_my_property_list")
    Call<PropertyListResponse> getMyProperties(@Header("Authorization")String auth, @Body HashMap<String,String> hashMap);

    @POST("get_property")
    Call<PropertyDetailResponse> getPropertyDetails(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("propertycompany_details")
    Call<PropertyProfileResponse> getPropertyProfile(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("get_unread_notification_count")
    Call<Response> getNotificationCount(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("update_notification_read_by")
    Call<Response> update_notification_read_by(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("update_jobsstatus")
    Call<Response> updateJobStatus(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("donate_active_inactive")
    Call<Response> updateDonateStatus(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("patient_active_inactive")
    Call<Response> updatePatientStatus(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("property_active_inactive")
    Call<Response> updatePropertyStatus(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("delete_jobs")
    Call<Response> deleteJob(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("get_notification")
    Call<NotificationResponse> getNotification(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @GET("videobanners")
    Call<ServiceBannerResponse> getVideoBanner(@Header("Authorization") String auth);

    @GET("robanners ")
    Call<ServiceBannerResponse> getRoBanner(@Header("Authorization") String auth);

    @GET("donatebanners")
    Call<ServiceBannerResponse> getDonateBanner(@Header("Authorization") String auth);

    @GET("patientbanners")
    Call<ServiceBannerResponse> getPatientBanner(@Header("Authorization") String auth);

    @GET("get_categories")
    Call<ShoppingCategoryResponse> getShoppingCategory(@Header("Authorization") String auth);

    @POST("get_ecommerce_banner")
    Call<ShoppingBannerResponse> getEcommerceBanner(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("get_all_categories_details")
    Call<SubCategoryResponse> getShoppingSubCategory(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("get_addresses")
    Call<AddressListResponse> getAddressList(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("get_address_details")
    Call<AddressResponse> getAddressDetails(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("post_address")
    Call<Response> addAddress(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("get_banner_products")
    Call<ProductResponse> getHomeScreenBanner(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("get_products")
    Call<ProductResponse> getProducts(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("get_banner_products")
    Call<ProductResponse> getHomeBanner(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("add_to_wishlist")
    Call<Response> addWishList(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("get_product_details")
    Call<ProductDetailResponse> getProductDetails(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("check_delivery_pincode")
    Call<Response> checkPincode(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("get_wishlists")
    Call<WishListResponse> getWishList(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("remove_from_wishlist")
    Call<Response> removeWishList(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("set_default_address")
    Call<Response> setDefaultAddress(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("delete_address")
    Call<Response> deleteAddress(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("invoice")
    Call<ResponseBody> downloadInvoice(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("get_default_address_details")
    Call<DefaultAddressResponse> getDefaultAddress(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("get_myorders")
    Call<ShoppingMyOrderResponse> getMyOrders(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("cancel_order")
    Call<Response> cancelOrder(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("return_product")
    Call<Response> returnOrder(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("get_order_details")
    Call<OrderDetailsResponse> getOrderDetail(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("place_order")
    Call<Response> placeOrder(@Header("Authorization") String auth, @Body PlaceOrder placeOrder);

    @POST("apply_promo_code")
    Call<Response> applyPrmoCode(@Header("Authorization") String auth, @Body PromoData promoData);

    @GET("get_operators")
    Call<OperatorResponse> getOperator(@Header("Authorization") String auth);

    @POST("securelogin/")
    Call<SecurreResponse> getSecureCode(@Body HashMap<String,String> hashMap);

    @POST("Instantrecharge ")
    Call<RechargeResponse> rechargeApi(@Body HashMap<String,String> hashMap);

    @POST("logout")
    Call<Response> logout(@Header("Authorization") String auth,@Body HashMap<String,String> hashMap);

    @POST("recharge_transaction")
    Call<Response> rechargePostApi(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("recharge_history")
    Call<RechargeListResponse> getRechargeList(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("tree_view")
    Call<TreeResponse> getTree(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("tree_view")
    Call<Object> getTreeView(@Header("Authorization") String auth, @Body HashMap<String,String> hashMap);

    @POST("business_kycdetails")
    Call<BusinessKycResponse> getBusinessKyc(@Header("Authorization")String authToken, @Body HashMap<String, String> hashMap);

    @POST("wallet_to_wallet_transfer")
    Call<Response> sendMoney(@Header("Authorization")String authToken, @Body HashMap<String, String> hashMap);

    @POST("dashboard_wallet")
    Call<DashboardResponse> getDashboardWallet(@Header("Authorization")String authToken, @Body HashMap<String, String> hashMap);

    @GET("tds")
    Call<TdsResponse> getTdsCharge(@Header("Authorization")String authToken);

    @GET("membership")
    Call<MembershipResponse> getMembership(@Header("Authorization")String authToken);

    @POST("cashback_purchase")
    Call<Response> postMembershipPlan(@Header("Authorization")String authToken, @Body HashMap<String, String> hashMap);

    @POST("get_user_details")
    Call<ReceiverInfoResponse> getMobileNumber(@Header("Authorization")String authToken, @Body HashMap<String, String> hashMap);

    @GET("gst")
    Call<GstRateResponse> getGstRate(@Header("Authorization")String authToken);

    @POST("usewallet")
    Call<Response> useWallet(@Header("Authorization")String authToken, @Body HashMap<String, String> hashMap);

    @POST("youtubevideocount")
    Call<Response> youtubevideocount(@Header("Authorization")String authToken, @Body HashMap<String, String> hashMap);

    @GET("cashback_charges")
    Call<Response> get_cashback_charges(@Header("Authorization")String authToken);
}
