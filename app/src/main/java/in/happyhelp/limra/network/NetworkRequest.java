package in.happyhelp.limra.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pranit on 04-04-2017.
 */

public class NetworkRequest {
    public interface Method {
        int DEPRECATED_GET_OR_POST = Request.Method.DEPRECATED_GET_OR_POST;
        int GET = Request.Method.GET;
        int POST = Request.Method.POST;
        int PUT = Request.Method.PUT;
        int DELETE = Request.Method.DELETE;
        int HEAD = Request.Method.HEAD;
        int OPTIONS = Request.Method.OPTIONS;
        int TRACE = Request.Method.TRACE;
        int PATCH = Request.Method.PATCH;
    }

    private static NetworkRequest instance;
    private static Context context;

    private OnResponseListener onResponseListener;

    private int method;
    private String url;
    private Map<String, String> requestParams, headers;
    private boolean shouldCache;

    public NetworkRequest(Context context) {
        this.context = context;
    }

    /**
     * Creates a static instance variable if null and returns
     *
     * @param context can be any context
     * @return
     */
    public static synchronized NetworkRequest getInstance(Context context) {
        if (instance == null) {
            instance = new NetworkRequest(context);
        }

        return instance;
    }

    /**
     * Creates a static instance variable if null and returns
     *
     * @param context can be any context
     * @return
     */
    public static NetworkRequest getAsyncInstance(Context context) {
        instance = new NetworkRequest(context);

        return instance;
    }

    /**
     * Sets method for volley request
     *
     * @param method request {@link Method} to be used
     * @return
     */
    public NetworkRequest setMethod(int method) {
        this.method = method;
        return instance;
    }

    /**
     * Sets url for volley request
     *
     * @param url URL to fetch the string at
     * @return
     */
    public NetworkRequest setUrl(String url) {
        this.url = url;
        return instance;
    }

    /**
     * Sets request params for volley request
     *
     * @param requestParams should be in key, value pair format
     * @return
     */
    public NetworkRequest setRequestParams(Map<String, String> requestParams) {
        this.requestParams = requestParams;
        return instance;
    }

    /**
     * Sets headers for volley request
     *
     * @param headers should be in key, value pair format
     * @return
     */
    public NetworkRequest setHeaders(Map<String, String> headers) {
        this.headers = headers;
        return instance;
    }

    /**
     * Enable or disable caching for particular request
     *
     * @param shouldCache should be in key, value pair format
     * @return
     */
    public NetworkRequest setShouldCache(boolean shouldCache) {
        this.shouldCache = shouldCache;
        return instance;
    }

    /**
     * Creates a new volley requests, processes it and sends success or error response via {@link OnResponseListener}
     */
    public void processRequest() {
        StringRequest request = new StringRequest(this.method, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("data", response);
                            if (onResponseListener != null)
                                onResponseListener.onSuccessResponse(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("response", error.toString());

                String message = error.getMessage();
                NetworkResponse networkResponse = error.networkResponse;

                JSONObject responseError = new JSONObject();
                try {
                    responseError.put("statusCode", 0);
                    responseError.put("data", message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (networkResponse != null && networkResponse.data != null) {
                    switch (networkResponse.statusCode) {
                        case 400:
                        case 401:
                        case 403:
                        case 404:
                        case 405:
                        case 409:
                        case 410:
                        case 429:
                        case 500:
                            String json = new String(networkResponse.data);
                            Log.e("networkResponse.data", json);
                            try {
                                responseError.remove("statusCode");
                                responseError.remove("data");

                                responseError.put("statusCode", networkResponse.statusCode);
                                responseError.put("data", new JSONObject(json));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                    }
                }
                if (onResponseListener != null)
                    onResponseListener.onErrorResponse(responseError);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                if (NetworkRequest.this.requestParams == null) {
                    return new HashMap<>();
                } else {
                    return NetworkRequest.this.requestParams;
                }
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                if (NetworkRequest.this.headers == null) {
                    return new HashMap<>();
                } else {
                    return NetworkRequest.this.headers;
                }
            }
        };
        // request.setShouldCache(false);//uncomment this to disable cache

        //set socketTimeout to avoid timeout error
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        if (!this.shouldCache) {
            request.setShouldCache(false);
        }
        NetworkSingleton.getInstance(this.context).addToRequestQueue(request);
    }

    public interface OnResponseListener {
        void onSuccessResponse(JSONObject response) throws JSONException;

        void onErrorResponse(JSONObject error);
    }

    public NetworkRequest setOnResponseListener(OnResponseListener onResponseListener) {
        this.onResponseListener = onResponseListener;
        return instance;
    }
}
