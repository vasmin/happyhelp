
package in.happyhelp.limra.rechargeresponse.rechargelist;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RechargeListResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("history")
    @Expose
    private List<History> history = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<History> getHistory() {
        return history;
    }

    public void setHistory(List<History> history) {
        this.history = history;
    }

}
