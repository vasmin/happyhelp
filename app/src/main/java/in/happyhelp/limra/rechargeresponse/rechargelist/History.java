
package in.happyhelp.limra.rechargeresponse.rechargelist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class History {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("is_postpaid")
    @Expose
    private String isPostpaid;
    @SerializedName("provider")
    @Expose
    private String provider;
    @SerializedName("service_type")
    @Expose
    private String serviceType;
    @SerializedName("system_ref")
    @Expose
    private String systemRef;
    @SerializedName("transaction_ref")
    @Expose
    private String transactionRef;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getIsPostpaid() {
        return isPostpaid;
    }

    public void setIsPostpaid(String isPostpaid) {
        this.isPostpaid = isPostpaid;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getSystemRef() {
        return systemRef;
    }

    public void setSystemRef(String systemRef) {
        this.systemRef = systemRef;
    }

    public String getTransactionRef() {
        return transactionRef;
    }

    public void setTransactionRef(String transactionRef) {
        this.transactionRef = transactionRef;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
