
package in.happyhelp.limra.rechargeresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RechargeResponse {

    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("IsPostpaid")
    @Expose
    private String isPostpaid;
    @SerializedName("Location")
    @Expose
    private String location;
    @SerializedName("MobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("Provider")
    @Expose
    private String provider;
    @SerializedName("ServiceType")
    @Expose
    private String serviceType;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("SystemReference")
    @Expose
    private String systemReference;
    @SerializedName("TransactionReference")
    @Expose
    private String transactionReference;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getIsPostpaid() {
        return isPostpaid;
    }

    public void setIsPostpaid(String isPostpaid) {
        this.isPostpaid = isPostpaid;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSystemReference() {
        return systemReference;
    }

    public void setSystemReference(String systemReference) {
        this.systemReference = systemReference;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

}
