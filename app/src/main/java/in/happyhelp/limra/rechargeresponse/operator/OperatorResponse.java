
package in.happyhelp.limra.rechargeresponse.operator;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OperatorResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("operators")
    @Expose
    private List<Operator> operators = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Operator> getOperators() {
        return operators;
    }

    public void setOperators(List<Operator> operators) {
        this.operators = operators;
    }

}
