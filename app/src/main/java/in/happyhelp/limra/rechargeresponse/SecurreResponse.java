
package in.happyhelp.limra.rechargeresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SecurreResponse {

    @SerializedName("AuthenticationKey")
    @Expose
    private String authenticationKey;
    @SerializedName("CorporateId")
    @Expose
    private String corporateId;
    @SerializedName("Status")
    @Expose
    private String status;

    public String getAuthenticationKey() {
        return authenticationKey;
    }

    public void setAuthenticationKey(String authenticationKey) {
        this.authenticationKey = authenticationKey;
    }

    public String getCorporateId() {
        return corporateId;
    }

    public void setCorporateId(String corporateId) {
        this.corporateId = corporateId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
