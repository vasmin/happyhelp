package in.happyhelp.limra;

import in.juspay.godel.analytics.Event;

/**
 * Created by bhushan on 11/26/2017.
 */

public class Constants {

    public static final String CATEGORYID = "categoryid";
    public static final String SKU = "sku";
    public static final String CARTID = "cartID";
    public static final String VISIBLE = "1";
    public static final String ORDERID = "orderid";
    public static final String LASTDATE = "lastdate";
    public static final String CURRENTINDEX = "currentIndex";
    public static final String CATEGORYID1 = "1";
    public static final String CATEGORYID2 = "2";
    public static final String CATEGORYID3 = "3";
    public static final String SOMTHING_WENT_WRONG = "Something went wrong from Server Side ! Please Contact Administrator";
    public static final String NOTIFICATION = "notification";
    public static final String ACCOUNT_TYPE = "accounttype";
    public static final String SCHOOLID = "schoolID";
    public static final String QUESTIONID = "questionID";
    public static final String STUDENTNAME = "studentName";
    public static final String VENDORID = "vendorId";
    public static final String LEADID = "leadId";
    public static final String SERVICEID = "serviceId";
    public static final String CLOSEDLEAD = "closelead";
    public static final String TOTALLEAD = "totallead";
    public static final String USERID = "userid";
    public static final String FCMTOKEN = "fcmtoken";
    public static final String INVOICEID = "invoiceId";
    public static final String WORKINGLEAD = "workinglead";
    public static final String EARN = "earn";
    public static final String SERVERTIMEOUT ="Server Timeout! Please Try Again.." ;
    public static final String ROID = "roId";
    public static final String DEPOSIT = "deposit";
    public static final String RENT = "rent";
    public static final String MONTH = "month";
    public static final String MYRO = "myRo";
    public static final String DID = "did";
    public static final String REFERALCODE = "referalCode";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String COMPANYCODE = "companycode";
    public static final String SERVICELIST = "serviceList";
    public static final String PAYMENTSTATUS = "paymentStatus";
    public static final String VENDORAMT = "vamt";
    public static final String ENQUIRY = "enquiry";
    public static final String ISPOPUP = "isPop";
    public static final String BUY = "buy";
    public static final String BOOKINGTYPE = "bookingtype";
    public static final String JOBID = "jobId";
    public static final String VID = "vId";
    public static final String JOB = "job";
    public static final String VISITING = "visitingCharge";
    public static final String SHOPID = "shopId";
    public static final String SUBCAT = "subCat";
    public static final String PID = "Pid";
    public static final String PROPERTYID = "propertyId";
    public static final String BEDROOM = "bedroom";
    public static final String STATE = "state";
    public static final String CITY = "city";
    public static final String MIN = "min";
    public static final String MAX = "max";
    public static final String TYPE = "property_Type";
    public static final String ADDRESSSID = "addressId";
    public static final String CompanyPropertyProfile = "propertyCompany";
    public static final String IMAGE = "image";
    public static final String TID = "treeId";
    public static final String GST = "gst";
    public static final String QTY = "qty";
    public static final String VURL = "vurl";
    public static final String GSTAMT = "gstAmt";
    public static final String BUYAMT = "buyAmt";
    public static final String PROPERTYFOR = "propertyfor";
    public static String username = "username";
    public static String password = "password";
    public static String confirmpassword = "password_confirmation";
    public static String id = "id";
    public static String token = "token";
    public static String isLoggedIn = "isloggedin";
    public static String CompanyProfile = "companyProfile";
    public static String AuthToken = "AuthToken";
    public static final String PLAYREFERALCODE = "referalCodePlay";



    public static String TESTID="testId";
    public static String TeacherName="teacherName";
    public static String imagePath="/HappyHelp/Images/";
    public static String pdfPath="/HappyHelp/";
}
