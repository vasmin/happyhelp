
package in.happyhelp.limra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashboardResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("wallet_total")
    @Expose
    private double walletTotal;
    @SerializedName("cashback_wallet_total")
    @Expose
    private double cashbackWalletTotal;
    @SerializedName("total_balance")
    @Expose
    private double totalBalance;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public double getWalletTotal() {
        return walletTotal;
    }

    public void setWalletTotal(double walletTotal) {
        this.walletTotal = walletTotal;
    }

    public double getCashbackWalletTotal() {
        return cashbackWalletTotal;
    }

    public void setCashbackWalletTotal(double cashbackWalletTotal) {
        this.cashbackWalletTotal = cashbackWalletTotal;
    }

    public double getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(double totalBalance) {
        this.totalBalance = totalBalance;
    }
}
