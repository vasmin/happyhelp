package in.happyhelp.limra.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.ConfirmationActivity;
import in.happyhelp.limra.activity.ro.RoDetailActivity;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.statecityresponse.City;
import in.happyhelp.limra.activity.response.statecityresponse.State;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;

public class NewDonateFragment extends Fragment {

    private static final int GALLERY_DONATEIMAGE = 123;
    private static final int REQUEST_IMAGE_CAPTURE_DONATEIMAGE = 321;
    ImageView imageView;
    EditText name,address,email,mobile,category,postalcode,details;
    LinearLayout selectDate;
    String donateImagePath="";
    TextView date,submit;
    RadioButton male,female;
    String gender="Male";
    Calendar dateSelected = Calendar.getInstance();
    CheckBox checkIAgree;
    Realm realm;
    List<String> stateList=new ArrayList<>();
    List<String> cityList=new ArrayList<>();
    RealmResults<State> statesList;
    ArraySpinnerAdapter stateAdapter,cityAdapter;
    Spinner stateSpinner;
    Spinner citySpinner;
    String state,city;
    ProgressDialog progressBar;

    public static NewDonateFragment newInstance() {
        NewDonateFragment fragment = new NewDonateFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.isNetworkConnectionAvailable(getActivity());
        realm=RealmHelper.getRealmInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_newdonation, container, false);
        name=view.findViewById(R.id.name);
        address=view.findViewById(R.id.address);
        email=view.findViewById(R.id.email);
        mobile=view.findViewById(R.id.mobile);
        category=view.findViewById(R.id.category);
        postalcode=view.findViewById(R.id.postalcode);
        imageView=view.findViewById(R.id.image);
        date=view.findViewById(R.id.date);
        male=view.findViewById(R.id.male);
        female=view.findViewById(R.id.female);
        selectDate= view.findViewById(R.id.selectdate);
        submit=view.findViewById(R.id.submit);
        checkIAgree=view.findViewById(R.id.agree);
        stateSpinner=view.findViewById(R.id.statespinner);
        citySpinner=view.findViewById(R.id.cityspinner);
        details=view.findViewById(R.id.details);

        stateAdapter=new ArraySpinnerAdapter(getActivity());
        cityAdapter=new ArraySpinnerAdapter(getActivity());

        statesList=realm.where(State.class).findAllAsync();
        statesList.addChangeListener(new RealmChangeListener<RealmResults<State>>() {
            @Override
            public void onChange(RealmResults<State> states) {
                stateList.clear();
                for(int i=0;i<statesList.size();i++){
                    stateList.add(statesList.get(i).getState());
                }
                stateAdapter.setData(stateList);
                stateSpinner.setAdapter(stateAdapter);

            }
        });


        progressBar=new ProgressDialog(getContext());
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);

        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                state= stateList.get(i);

                if(state!=null) {
                    RealmResults<City> cities=realm.where(City.class).equalTo("state",state).findAllAsync();
                    cities.addChangeListener(new RealmChangeListener<RealmResults<City>>() {
                        @Override
                        public void onChange(RealmResults<City> cities) {
                            cityList.clear();
                            cityList.add("Select City");
                            for(int i=0;i<cities.size();i++){
                                cityList.add(cities.get(i).getCity());
                            }
                            cityAdapter.setData(cityList);
                            citySpinner.setAdapter(cityAdapter);
                        }
                    });
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                city=cityList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        male.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    gender="Male";
                }
            }
        });

        female.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    gender="Female";
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                postDonate();
            }
        });


        selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar newCalendar = dateSelected;
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        final String receivedDate = dayOfMonth+"-"+(monthOfYear+1)+"-"+year;
                        date.setText(receivedDate);
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File filepath=new File(Environment.getExternalStorageDirectory()+Constants.imagePath);
                if(!filepath.exists()){
                    filepath.mkdirs();
                }

                donateImagePath=filepath.getAbsolutePath()+"/donateImage.jpg";


                AppUtils.startPickImageDialog(GALLERY_DONATEIMAGE, REQUEST_IMAGE_CAPTURE_DONATEIMAGE, donateImagePath,getActivity());
            }
        });


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( resultCode==RESULT_OK)
            if(requestCode==REQUEST_IMAGE_CAPTURE_DONATEIMAGE)
            {
                File newFile=new File(donateImagePath);

                setImageFromPath(imageView,newFile.getPath());
            }
            else if(requestCode==GALLERY_DONATEIMAGE){
                Uri imageUri = data.getData();
                donateImagePath = getPath(getActivity(), imageUri);
                setImageFromPath(imageView,donateImagePath);
            }

    }

    private String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "";
            Toast.makeText(getContext(),"Sorry your records is not created",Toast.LENGTH_LONG).show();
        }
        return result;
    }

    private void setImageFromPath(ImageView image, String path){
        if(!TextUtils.isEmpty(path)&&!path.equals("")) {

            File imgFile = new File(path);
            if (imgFile.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize=8;      // 1/8 of original image
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(),options);
                image.setImageBitmap(myBitmap);
                image.setVisibility(View.VISIBLE);

            }else {
                image.setVisibility(View.GONE);
            }
        }else {
            image.setVisibility(View.GONE);
        }
    }

    public void postDonate(){


        if (TextUtils.isEmpty(name.getText().toString())) {
            name.setError("Enter Fullname ");
            name.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(address.getText().toString())) {
            address.setError("Enter Address ");
            address.requestFocus();
            return;
        }

        if(!AppUtils.isValidMail(email.getText().toString(),email)){
            email.setError("Enter valid Email");
            email.requestFocus();
            return;
        }

        if(!AppUtils.isValidMobile(mobile.getText().toString())){
            mobile.setError("Enter valid mobile");
            mobile.requestFocus();
            return;

        }


        if (TextUtils.isEmpty(category.getText().toString())) {
            category.setError("Enter Category ");
            category.requestFocus();
            return;
        }


        if (TextUtils.isEmpty(state)) {
            Toast.makeText(getContext(),"Please Select State ",Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(city)) {
            Toast.makeText(getContext(),"Enter Select City ",Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(gender)) {
            Toast.makeText(getContext(),"Please Select Gender ",Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(postalcode.getText().toString())) {
            postalcode.setError("Enter Postal Code ");
            postalcode.requestFocus();
            return;
        }

        if (postalcode.getText().length()!=6) {
            postalcode.setError("Enter valid Postal Code ");
            postalcode.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(date.getText().toString())) {
            Toast.makeText(getContext(),"Enter D O B ",Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(details.getText().toString())) {
            details.setError("Enter Details Here ");
            details.requestFocus();
            return;
        }

        if(!checkIAgree.isChecked()){
            Toast.makeText(getContext(),"Please select terms and conditions ",Toast.LENGTH_LONG).show();
            return;
        }

        if(donateImagePath.equals("")){
            Toast.makeText(getContext(),"Upload picture for donation",Toast.LENGTH_LONG).show();
            return;
        }

        progressBar.show();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        String userID=SharedPreferenceHelper.getInstance(getContext()).getString(Constants.USERID);
        builder.addFormDataPart("userid", userID);
        builder.addFormDataPart("name", name.getText().toString());
        builder.addFormDataPart("address", address.getText().toString());
        builder.addFormDataPart("mobile", mobile.getText().toString());
        builder.addFormDataPart("email", email.getText().toString());
        builder.addFormDataPart("gender", gender);
        builder.addFormDataPart("state", state);
        builder.addFormDataPart("city", city);
        builder.addFormDataPart("category", category.getText().toString());
        builder.addFormDataPart("dob", date.getText().toString());
        builder.addFormDataPart("detail", details.getText().toString());

        builder.addFormDataPart("pincode",postalcode.getText().toString());


        if(!donateImagePath.equals("")) {
            File adharfrontpath = new File(donateImagePath);
            if (adharfrontpath.exists()) {
                builder.addFormDataPart("image", "donateImage.jpeg", RequestBody.create(MediaType.parse("multipart/form-data"), adharfrontpath));
            }
        }

        MultipartBody requestBody = builder.build();
        Call<Response> call=RestClient.get().postDonate(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),requestBody);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                progressBar.dismiss();
                Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        View parentLayout =getActivity().findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                       // Toast.makeText(getContext(),response1.getMessage(),Toast.LENGTH_LONG).show();
                        Intent intent=new Intent(getContext(),ConfirmationActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }else{
                       // Toast.makeText(getContext(),response1.getMessage(),Toast.LENGTH_LONG).show();
                        View parentLayout = getActivity().findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout =getActivity().findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();


            }
        });
    }
}
