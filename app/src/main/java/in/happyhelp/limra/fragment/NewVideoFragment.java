package in.happyhelp.limra.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.ConfirmationActivity;
import in.happyhelp.limra.activity.property.PostPropertyActivity;
import in.happyhelp.limra.activity.response.jobplanresponse.JobPlan;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import in.happyhelp.limra.activity.response.videoplan.Datum;
import in.happyhelp.limra.activity.response.videoplan.VideoPlanResponse;
import in.happyhelp.limra.activity.ro.RoBookingActivity;
import in.happyhelp.limra.adapter.VideoPlanAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.gstresponse.GstRateResponse;
import in.happyhelp.limra.network.RestClient;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewVideoFragment extends Fragment {

    @BindView(R.id.monthrentspinner)
    Spinner monthSpinner;
    VideoPlanAdapter adapter;
    List<Datum> list=new ArrayList<>();

    @BindView(R.id.amount)
    TextView amountTxt;

    @BindView(R.id.submit)
    TextView submit;

    @BindView(R.id.title)
    EditText title;

    @BindView(R.id.description)
    EditText description;

    @BindView(R.id.link)
    EditText link;

    @BindView(R.id.total)
    TextView totalAmt;


    @BindView(R.id.imagespinner)
    ImageView imageSpinner;
    String amount="00";
    String planId="0";

    Realm realm;
    String email="";
    String mobile="";
    String buyerName="";

    @BindView(R.id.gst)
    TextView gst;

    @BindView(R.id.gstamt)
    TextView gstAmount;

    @BindView(R.id.gstlinear)
    LinearLayout gstLinear;


    boolean isSelected=false;
    int gstRate=0;
    double result=0;
    String totalamount="0";

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_video, container, false);
        ButterKnife.bind(this,view);
        adapter=new VideoPlanAdapter(getActivity());
        realm= RealmHelper.getRealmInstance();

        imageSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                monthSpinner.performClick();
            }
        });

        RealmResults<UserData> userData=realm.where(UserData.class).findAllAsync();
        userData.addChangeListener(new RealmChangeListener<RealmResults<UserData>>() {
            @Override
            public void onChange(RealmResults<UserData> userData) {
                if(userData!=null){
                    email=userData.get(0).getEmail();
                    mobile=userData.get(0).getMobile();
                    buyerName=userData.get(0).getName();
                }
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(title.getText().toString())) {
                    title.setError("Enter title ");
                    title.requestFocus();
                    return;
                }


                if (TextUtils.isEmpty(link.getText().toString())) {
                    link.setError("Enter link ");
                    link.requestFocus();
                    return;
                }


                if (TextUtils.isEmpty(description.getText().toString())) {
                    description.setError("Enter description ");
                    description.requestFocus();
                    return;
                }

                if(amount.equals("00")){
                    Toast.makeText(getActivity(), "Please Select Month", Toast.LENGTH_SHORT).show();
                    return;
                }

                callInstamojoPay(email,mobile,totalamount,"Video Promotion",buyerName);
            }
        });


        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(list.size()>i) {
                    amount = list.get(i).getAmount();

                    if (amount != null) {
                        amountTxt.setText("\u20B9 " + amount);
                        planId = list.get(i).getId();
                        isSelected=true;
                        setDataVisible();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        getGstRate();
        getVideoPlan();

        return view;
    }

    @SuppressLint("SetTextI18n")
    public void setDataVisible(){
        if(isSelected){
            gstLinear.setVisibility(View.VISIBLE);
            result =  ((Double.parseDouble(amount) * gstRate) / 100);
            totalamount= String.valueOf(Double.parseDouble(amount)+result);
            gstAmount.setText("\u20B9 "+String.valueOf(result));
            gst.setText("Exclusive Gst ("+gstRate+"% )");
            totalAmt.setText("\u20B9 "+ totalamount);
        }else{
            gstLinear.setVisibility(View.GONE);
            totalAmt.setText("\u20B9 "+amount);
        }
    }


    public void getGstRate(){
        Call<GstRateResponse> call=RestClient.get().getGstRate(SharedPreferenceHelper.getInstance(getActivity()).getAuthToken());
        call.enqueue(new Callback<GstRateResponse>() {
            @Override
            public void onResponse(Call<GstRateResponse> call, retrofit2.Response<GstRateResponse> response) {

                if(response.code()==200){
                    GstRateResponse gstRateResponse=response.body();

                    if(gstRateResponse.getStatus()) {
                        if (gstRateResponse.getGst().size() >= 4) {
                            gstRate = Integer.parseInt(gstRateResponse.getGst().get(4).getName());
                        }
                    }else{
                        gstRate=0;
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getContext());
                }else{
                    View parentLayout = getActivity().findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(Call<GstRateResponse> call, Throwable t) {
                t.printStackTrace();
                View parentLayout = getActivity().findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = getActivity();
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        getActivity().registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);

                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();

                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);
                postVideo(payment);
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }

    public void postVideo(String paymentId){

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("title",title.getText().toString());
        hashMap.put("link", link.getText().toString());
        hashMap.put("description", description.getText().toString());
        hashMap.put("plan", planId);
        hashMap.put("payment_id", paymentId);
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getContext()).getString(Constants.USERID));
        hashMap.put("gst", String.valueOf(gstRate));
        hashMap.put("gstamt",String.valueOf(result));


        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get().postVideo(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){

                    if(response1.getStatus()) {
                        View parentLayout = getActivity().findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();

                        Intent intent = new Intent(getActivity(), ConfirmationActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }else{
                        View parentLayout = getActivity().findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{
                    View parentLayout = getActivity().findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = getActivity().findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void getVideoPlan(){
        Call<VideoPlanResponse> call= RestClient.get().getVideoPlan(SharedPreferenceHelper.getInstance(getContext()).getAuthToken());
        call.enqueue(new Callback<VideoPlanResponse>() {
            @Override
            public void onResponse(@NonNull Call<VideoPlanResponse> call, @NonNull Response<VideoPlanResponse> response) {
                VideoPlanResponse videoPlanResponse=response.body();
                if(response.code()==200){
                    if(videoPlanResponse.getStatus()){
                        list.clear();

                        Datum datum=new Datum();
                        datum.setMonths("Select ");
                        datum.setAmount("00");
                        datum.setId("");
                        list.add(datum);
                        list.addAll(videoPlanResponse.getData());
                        adapter.setData(list);
                        monthSpinner.setAdapter(adapter);

                    }else{

                    }

                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<VideoPlanResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });


    }
}
