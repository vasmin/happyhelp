package in.happyhelp.limra.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.property.PropertyDetailsActivity;
import in.happyhelp.limra.activity.property.PropertyHomeActivity;
import in.happyhelp.limra.activity.response.serrviceresponse.ServiceBannerResponse;
import in.happyhelp.limra.adapter.DonationAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.activity.response.donatelist.Donate;
import in.happyhelp.limra.activity.response.donatelist.DonateListResponse;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DonateListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayoutManager linearLayoutManager;
    DonationAdapter adapter;
    Realm realm;


    @BindView(R.id.slider)
    SliderLayout slider;

    List<String> listBanner=new ArrayList<>();

    RealmResults<Donate> donates;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm=RealmHelper.getRealmInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_donatelist, container, false);
        recyclerView=view.findViewById(R.id.recycler);
        ButterKnife.bind(this,view);

        swipeRefreshLayout=view.findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener((SwipeRefreshLayout.OnRefreshListener) getActivity());

        swipeRefreshLayout.setRefreshing(true);
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        adapter=new DonationAdapter(getContext());
        donates=realm.where(Donate.class).findAllAsync();
        donates.addChangeListener(new RealmChangeListener<RealmResults<Donate>>() {
            @Override
            public void onChange(RealmResults<Donate> donates) {
                adapter.setData(donates);
                recyclerView.setAdapter(adapter);
            }
        });

        slider.movePrevPosition(false);
        slider.setPresetTransformer(com.daimajia.slider.library.SliderLayout.Transformer.Accordion);
        slider.setPresetIndicator(com.daimajia.slider.library.SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(3000);
        slider.startAutoCycle();

        getDonateList();
        getDonationBanner();

        return view;
    }
    public void getDonationBanner(){
        Call<ServiceBannerResponse> call=RestClient.get().getDonateBanner(SharedPreferenceHelper.getInstance(getContext()).getAuthToken());
        call.enqueue(new Callback<ServiceBannerResponse>() {
            @Override
            public void onResponse(@NonNull Call<ServiceBannerResponse> call, @NonNull Response<ServiceBannerResponse> response) {
                final ServiceBannerResponse serviceBannerResponse=response.body();

                if(response.code()==200){
                    if(serviceBannerResponse.getStatus()) {

                        listBanner.clear();
                        for(int i=0;i<serviceBannerResponse.getData().size();i++){
                            listBanner.add(serviceBannerResponse.getData().get(i).getImage());
                        }


                        for (int i = 0; i < listBanner.size(); i++) {
                            DefaultSliderView defaultSliderView = new DefaultSliderView(getContext());
                            final int finalI = i;
                            defaultSliderView.image(RestClient.base_image_url + listBanner.get(i))
                                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                        @Override
                                        public void onSliderClick(BaseSliderView slider) {
                                            String BannnerName = listBanner.get(finalI);
                            /*JobCategory banners=realm.where(JobCategory.class).equalTo("image",BannnerName).findFirst();
                            String url = banners.getLink();

                            if(!url.equals("")) {
                                Intent i = new Intent(Intent.ACTION_VIEW,
                                        Uri.parse(url));
                                startActivity(i);
                            }*/
                                        }
                                    });
                            slider.addSlider(defaultSliderView);
                        }
                    }


                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<ServiceBannerResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getDonateList(){
        swipeRefreshLayout.setRefreshing(true);
        Call<DonateListResponse> call=RestClient.get().getDonateList(SharedPreferenceHelper.getInstance(getContext()).getAuthToken());
        call.enqueue(new Callback<DonateListResponse>() {
            @Override
            public void onResponse(@NonNull Call<DonateListResponse> call, @NonNull Response<DonateListResponse> response) {
                final DonateListResponse donateListResponse=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(donateListResponse.getStatus()){
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(Donate.class);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.copyToRealmOrUpdate(donateListResponse.getData());
                                    }
                                });
                            }
                        });

                    }else{
                        View parentLayout =getActivity().findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{
                    View parentLayout =getActivity().findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }

            }

            @Override
            public void onFailure(@NonNull Call<DonateListResponse> call, @NonNull Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                View parentLayout =getActivity().findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onRefresh() {
      getDonateList();
    }
}
