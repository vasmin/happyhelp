package in.happyhelp.limra.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.R;
import io.realm.Realm;

public class PostJobFragment extends Fragment {
    Realm realm;

    @BindView(R.id.postjob)
    LinearLayout postJob;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public static PostJobFragment newInstance() {
        PostJobFragment fragment = new PostJobFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_post_job, container, false);
        ButterKnife.bind(this,view);

        postJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postJob();
            }
        });
        return view;
    }

    public void postJob(){
        /*HashMap<String,String> hashMap=new HashMap<>();
        Call<Response> call= RestClient.get().postJob(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){

                    }else{

                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                t.printStackTrace();
            }
        });*/
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
