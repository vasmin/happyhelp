package in.happyhelp.limra.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.Jobs.JobDescriptionActivity;
import in.happyhelp.limra.activity.Jobs.PostJobsActivity;
import in.happyhelp.limra.activity.KycActivity;
import in.happyhelp.limra.activity.response.joblistresponse.JobList;
import in.happyhelp.limra.activity.response.joblistresponse.JobListResponse;
import in.happyhelp.limra.adapter.JobDetailAdapter;
import in.happyhelp.limra.adapter.MyJobAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPostJobFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.data)
    TextView data;
    LinearLayoutManager linearLayoutManager;

    MyJobAdapter adapter;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_user, container, false);
        ButterKnife.bind(this,view);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        adapter=new MyJobAdapter(getActivity(), new MyJobAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(final JobList d, View view) throws ParseException {

                TextView viewStatus=view.findViewById(R.id.view);
                viewStatus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                    }
                });
              /*  Intent intent=new Intent(getActivity(), PostJobsActivity.class);
                intent.putExtra(Constants.JOBID,d.getId());
                intent.putExtra(Constants.JOB,true);
                startActivity(intent);
                getActivity().finish();*/
            }
        });
        getMyJobList();
        return view;
    }



    public void getMyJobList()
    {
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getContext()).getString(Constants.USERID));
        Call<JobListResponse> call= RestClient.get().getMyJobList(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<JobListResponse>() {
            @Override
            public void onResponse(@NonNull Call<JobListResponse> call, @NonNull Response<JobListResponse> response) {
                JobListResponse jobListResponse=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){

                    if(jobListResponse.getStatus()){
                        adapter.setData(jobListResponse.getData());
                        recyclerView.setAdapter(adapter);
                        if(jobListResponse.getData().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }
                    }else{
                        if(jobListResponse.getData().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }
                    }



                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{
                    View parentLayout =getActivity().findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<JobListResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
                View parentLayout =getActivity().findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onRefresh() {
        getMyJobList();
    }
}
