package in.happyhelp.limra.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import customfont.MyEditText;
import customfont.MyTextView;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.HomeActivity;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.adapter.PropertyImagesAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.PropertyImageModel;
import in.happyhelp.limra.network.RestClient;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.support.v4.content.FileProvider.getUriForFile;


public class FragmentNewProperties extends Fragment {

    private static final int REQUEST_PHONE_CALL = 123;
    MyEditText PropertyName, PropertyOwnername, PropertyContact, buildArea, carpetArea, BuildingName, BuildingWing, FlatNumber, Location,ParkingArea,Price;

    RecyclerView recyclerView;
    MyTextView uploadImage;
    private int GALLERY_PICTURE = 123;
    private int CAMERA_REQUEST = 321;

    PropertyImagesAdapter adapter;
    List<PropertyImageModel> list = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;

    ArrayList<String> propertytype=new ArrayList<>();

    ArrayList<String> propertyfor=new ArrayList<>();
    ArrayList<String> propertystatus=new ArrayList<>();
    ArrayList<String> vastucomplaint=new ArrayList<>();
    ArrayList<String> parking=new ArrayList<>();
    ArrayList<String> parkingNo=new ArrayList<>();

    String PropertyType,PropertyFor,PropertyStatus,VastuComplaint,Parking,ParkingNo;

    Button Submit;

    ProgressDialog progressBar;

    Spinner PropertyTypeSpinner,propertyForSpinner,propertyStatusSpinner,propertyvastucomplaint,parkingSpinner,parkingNoSpinner;

    ArraySpinnerAdapter propertyTypeAdapter,propertyForAdapter,propertyStatusAdapter,vastucomplaintAdapter,parkingAdapter,parkingNoAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_addproperty, container, false);
        recyclerView = view.findViewById(R.id.recycler);
        uploadImage = view.findViewById(R.id.uploadimage);
        PropertyTypeSpinner=view.findViewById(R.id.propertytype);
        propertyForSpinner=view.findViewById(R.id.propertyfor);
        propertyStatusSpinner=view.findViewById(R.id.propertystatus);
        propertyvastucomplaint=view.findViewById(R.id.vastucomplaint);
        parkingSpinner=view.findViewById(R.id.parking);
        parkingNoSpinner=view.findViewById(R.id.parkingno);

        PropertyName=view.findViewById(R.id.propertyname);
        PropertyOwnername=view.findViewById(R.id.ownername);
        PropertyContact=view.findViewById(R.id.contactdetails);
        buildArea=view.findViewById(R.id.buildingarea);
        carpetArea=view.findViewById(R.id.carpetarea);
        BuildingName=view.findViewById(R.id.buildingname);
        BuildingWing=view.findViewById(R.id.wing);
        FlatNumber=view.findViewById(R.id.flatname);
        Location=view.findViewById(R.id.clientlocation);
        Price=view.findViewById(R.id.price);
        ParkingArea=view.findViewById(R.id.parkingarea);

        Submit=view.findViewById(R.id.submit);

        Submit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                ValidatioCheck();
            }
        });


        BuildingWing.setSingleLine();
        BuildingWing.setImeOptions(EditorInfo.IME_ACTION_NEXT);


        BuildingName.setSingleLine();
        BuildingName.setImeOptions(EditorInfo.IME_ACTION_NEXT);


        carpetArea.setSingleLine();
        carpetArea.setImeOptions(EditorInfo.IME_ACTION_NEXT);

        ParkingArea.setSingleLine();
        ParkingArea.setImeOptions(EditorInfo.IME_ACTION_NEXT);

        buildArea.setSingleLine();
        buildArea.setImeOptions(EditorInfo.IME_ACTION_NEXT);


        GridLayoutManager gridLayoutManager=new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);

        recyclerView.setHasFixedSize(true);

        adapter = new PropertyImagesAdapter(getContext());

        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


              //  startDialog();
            }
        });

        propertytype.add("Property type");
        propertytype.add("Residencial");
        propertytype.add("Commercial");

        propertyTypeAdapter=new ArraySpinnerAdapter(getActivity());

        propertyTypeAdapter.setData(propertytype);
        PropertyTypeSpinner.setAdapter(propertyTypeAdapter);


        propertyfor.add("Property for");
        propertyfor.add("Sale");
        propertyfor.add("Rent");

        propertyForAdapter=new ArraySpinnerAdapter(getActivity());
        propertyForAdapter.setData(propertyfor);

        propertyForSpinner.setAdapter(propertyForAdapter);



        propertystatus.add("Property status");
        propertystatus.add("Ready possesion");
        propertystatus.add("Under Construction");

        propertyStatusAdapter=new ArraySpinnerAdapter(getActivity());
        propertyStatusAdapter.setData(propertystatus);
        propertyStatusSpinner.setAdapter(propertyStatusAdapter);


        vastucomplaint.add("Vastu complaint");
        vastucomplaint.add("North");
        vastucomplaint.add("South");
        vastucomplaint.add("East");
        vastucomplaint.add("West");
        vastucomplaint.add("North/West");
        vastucomplaint.add("North/East");
        vastucomplaint.add("South/West");
        vastucomplaint.add("South/East");

        vastucomplaintAdapter=new ArraySpinnerAdapter(getActivity());
        vastucomplaintAdapter.setData(vastucomplaint);
        propertyvastucomplaint.setAdapter(vastucomplaintAdapter);

        parking.add("Parking");
        parking.add("Open");
        parking.add("Stilt");
        parking.add("Podium");
        parking.add("Basement");
        parking.add("Stack");
        parking.add("Puzzle");
        parkingAdapter=new ArraySpinnerAdapter(getActivity());
        parkingAdapter.setData(parking);
        parkingSpinner.setAdapter(parkingAdapter);


        parkingNo.add("0");
        parkingNo.add("1");
        parkingNo.add("2");

        parkingNoAdapter=new ArraySpinnerAdapter(getActivity());
        parkingNoAdapter.setData(parkingNo);
        parkingNoSpinner.setAdapter(parkingNoAdapter);

        progressBar=new ProgressDialog(getContext());
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);


        PropertyTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                PropertyType= String.valueOf(propertytype.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        propertyForSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                PropertyFor= String.valueOf(propertyfor.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        propertyStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                PropertyStatus= String.valueOf(propertystatus.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        propertyvastucomplaint.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                VastuComplaint= String.valueOf(vastucomplaint.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        parkingSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Parking= String.valueOf(parking.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        parkingNoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ParkingNo= String.valueOf(parkingNo.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        return view;
    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void ValidatioCheck(){
        if(TextUtils.isEmpty(PropertyName.getText().toString())) {
            PropertyName.setError("Enter Property Name");
            return;
        }

        if(PropertyType.equals("Property type")){
            Toast.makeText(getContext(),"Select property type", Toast.LENGTH_LONG).show();
            return;
        }

        if(PropertyFor.equals("Property for")){
            Toast.makeText(getContext(),"Select property for", Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(buildArea.getText().toString())) {
            buildArea.setError("Enter Building Area");
            return;
        }

        if(TextUtils.isEmpty(carpetArea.getText().toString())) {
            carpetArea.setError("Enter CarpetArea");
            return;
        }

        if(PropertyStatus.equals("Property status")){
            Toast.makeText(getContext(),"Select property status", Toast.LENGTH_LONG).show();
            return;
        }

        if(VastuComplaint.equals("Vastu complaint")){
            Toast.makeText(getContext(),"Select Vastu complaint", Toast.LENGTH_LONG).show();
            return;
        }


        if(TextUtils.isEmpty(BuildingName.getText().toString())) {
            BuildingName.setError("Enter BuildingName");
            return;
        }
        if(TextUtils.isEmpty(BuildingWing.getText().toString())) {
            BuildingWing.setError("Enter Building Wing");
            return;
        }

        if(TextUtils.isEmpty(FlatNumber.getText().toString())) {
            FlatNumber.setError("Enter Flat Number");
            return;
        }

        if(TextUtils.isEmpty(Location.getText().toString())) {
            Location.setError("Enter Location");
            return;
        }

        if(Parking.equals("Parking")){
            Toast.makeText(getContext(),"Select parking", Toast.LENGTH_LONG).show();
            return;
        }





        if(TextUtils.isEmpty(ParkingArea.getText().toString())) {
            ParkingArea.setError("Enter Parking Area");
            return;
        }

        if(TextUtils.isEmpty(Price.getText().toString())) {
            Price.setError("Enter Price");
            return;
        }
        uploadMultiFile();
    }








    private void uploadMultiFile() {
        progressBar.show();
        ArrayList<String> filePaths = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            filePaths.add(list.get(i).getUrl());
        }
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("property_name", PropertyName.getText().toString());
        builder.addFormDataPart("property_owner_name", PropertyOwnername.getText().toString());
        builder.addFormDataPart("property_owner_contact", PropertyContact.getText().toString());
        builder.addFormDataPart("property_builtup_area", buildArea.getText().toString());
        builder.addFormDataPart("property_carpet_area", carpetArea.getText().toString());
        builder.addFormDataPart("property_building", BuildingName.getText().toString());
        builder.addFormDataPart("property_wing", BuildingWing.getText().toString());
        builder.addFormDataPart("property_flat", FlatNumber.getText().toString());
        builder.addFormDataPart("property_location", Location.getText().toString());
        builder.addFormDataPart("property_parking_space", ParkingArea.getText().toString());
        builder.addFormDataPart("property_price", Price.getText().toString());
        builder.addFormDataPart("property_type", PropertyType);
        builder.addFormDataPart("property_for", PropertyFor);
        builder.addFormDataPart("property_status", PropertyStatus);
        builder.addFormDataPart("property_vastu_compliance", VastuComplaint);
        builder.addFormDataPart("property_parking", Parking);
        builder.addFormDataPart("property_parking_need", ParkingNo);



        for (int i = 0; i < filePaths.size(); i++) {
            File file = new File(filePaths.get(i));
            builder.addFormDataPart("property_photos[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
        }

      /*  File file = new File("");
        MultipartBody requestBody = builder.build();
        Call<PropertyResponse> call = RestClient.get().submitProperty(SharedPreferenceHelper.getInstance(getContext()).getAuthToken()
                , requestBody);
        call.enqueue(new Callback<PropertyResponse>() {
            @Override
            public void onResponse(Call<PropertyResponse> call, Response<PropertyResponse> response) {
                if (response.code() == 200) {
                    PropertyResponse propertyResponse = response.body();
                    progressBar.dismiss();
                    if (propertyResponse.getStatus()) {
                        Toast.makeText(getContext(), "Property submitted successfully", Toast.LENGTH_LONG).show();
                        Intent intent=new Intent(getActivity(), HomeActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }


                } else if (response.code() == 401) {
                    progressBar.dismiss();
                    SharedPreferenceHelper.getInstance(getContext()).logout();
                    AppUtils.logout(getActivity());


                } else {
                    progressBar.dismiss();
                    Toast.makeText(getContext(), Constants.SOMTHING_WENT_WRONG, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<PropertyResponse> call, Throwable t) {
                progressBar.dismiss();
                t.printStackTrace();
            }
        });*/

    }










    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }



    @TargetApi(Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        String selectedImagePath = null;

        if (resultCode == RESULT_OK && requestCode == CAMERA_REQUEST) {

            adapter.setData(list);
            recyclerView.setAdapter(adapter);

/*
            File f = new File(Environment.getExternalStorageDirectory()+Constants.folderImagePath);
            for (File temp : f.listFiles()) {
                if (temp.getName().equals("temp.jpg")) {
                    f = temp;
                    break;
                }
            }

            if (!f.exists()) {

                Toast.makeText(getContext(),

                        "Error while capturing image", Toast.LENGTH_LONG)

                        .show();

                return;

            }

            try {

                Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath());

                bitmap = Bitmap.createScaledBitmap(bitmap, 400, 400, true);

                int rotate = 0;
                try {
                    ExifInterface exif = new ExifInterface(f.getAbsolutePath());
                    int orientation = exif.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_NORMAL);

                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotate = 270;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotate = 180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotate = 90;
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Matrix matrix = new Matrix();
                matrix.postRotate(rotate);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                        bitmap.getHeight(), matrix, true);


                // img_logo.setImageBitmap(bitmap);
                //storeImageTosdCard(bitmap);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }*/

        } else if (resultCode == RESULT_OK && requestCode == GALLERY_PICTURE) {
            if (data != null) {






            } else {
                Toast.makeText(getContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

}

