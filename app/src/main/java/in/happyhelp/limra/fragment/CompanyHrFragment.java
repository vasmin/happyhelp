package in.happyhelp.limra.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.RegisterActivity;
import in.happyhelp.limra.activity.response.savedjobresponse.SavedJob;
import in.happyhelp.limra.activity.response.savedjobresponse.SavedListResponse;
import in.happyhelp.limra.adapter.JobAdapter;
import in.happyhelp.limra.adapter.VideoAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyHrFragment extends Fragment {

    @BindView(R.id.appliedjobrecycler)
    RecyclerView appliedJobRecycler;

    @BindView(R.id.savedjob)
    RecyclerView savedJobRecycler;

    @BindView(R.id.saveddata)
    TextView saveData;

    @BindView(R.id.applieddata)
            TextView appliedData;

    JobAdapter savedAdapter,appliedAdapter;
    LinearLayoutManager linearLayoutManager,linearLayoutManager2;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_company_user, container, false);
        ButterKnife.bind(this,view);




        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        appliedJobRecycler.setLayoutManager(linearLayoutManager);
        appliedJobRecycler.setHasFixedSize(true);

        linearLayoutManager2 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        savedJobRecycler.setLayoutManager(linearLayoutManager2);
        savedJobRecycler.setHasFixedSize(true);

        savedAdapter=new JobAdapter(getContext(), new JobAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(SavedJob d, View view) throws ParseException {

            }
        });


        appliedAdapter=new JobAdapter(getContext(), new JobAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(SavedJob d, View view) throws ParseException {

            }
        });


        getSavedJobList();
        getAppliedJobList();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    public void getSavedJobList(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getContext()).getString(Constants.USERID));
        Call<SavedListResponse> call= RestClient.get().getSavedJobList(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<SavedListResponse>() {
            @Override
            public void onResponse(@NonNull Call<SavedListResponse> call, @NonNull Response<SavedListResponse> response) {
                SavedListResponse savedListResponse=response.body();
                if(response.code()==200){
                    if(savedListResponse.getStatus()){
                        savedAdapter.setData(savedListResponse.getData());
                        savedJobRecycler.setAdapter(savedAdapter);

                        if(savedListResponse.getData().size()==0){
                            saveData.setVisibility(View.VISIBLE);
                        }else{
                            saveData.setVisibility(View.GONE);
                        }
                    }else{
                        if(savedListResponse.getData().size()==0){
                            saveData.setVisibility(View.VISIBLE);
                        }else{
                            saveData.setVisibility(View.GONE);
                        }
                    }

                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<SavedListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }


    public void getAppliedJobList(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getContext()).getString(Constants.USERID));
        Call<SavedListResponse> call= RestClient.get().getAppliedJobList(SharedPreferenceHelper.getInstance(getContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<SavedListResponse>() {
            @Override
            public void onResponse(@NonNull Call<SavedListResponse> call, @NonNull Response<SavedListResponse> response) {
                SavedListResponse appliedresponse=response.body();
                if(response.code()==200){

                    appliedAdapter.setData(appliedresponse.getData());
                    appliedJobRecycler.setAdapter(appliedAdapter);

                    if(appliedresponse.getData().size()==0){
                        appliedData.setVisibility(View.VISIBLE);
                    }else{
                        appliedData.setVisibility(View.GONE);
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{
                    if(appliedresponse.getData().size()==0){
                        appliedData.setVisibility(View.VISIBLE);
                    }else{
                        appliedData.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<SavedListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

}
