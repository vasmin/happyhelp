package in.happyhelp.limra.fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import java.text.ParseException;

import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.property.FilterActivity;
import in.happyhelp.limra.activity.property.PropertyActivity;
import in.happyhelp.limra.activity.property.PropertyDetailsActivity;
import in.happyhelp.limra.activity.response.propertiesresponse.PropertyList;
import in.happyhelp.limra.activity.response.propertiesresponse.PropertyListResponse;
import in.happyhelp.limra.activity.video.MyVideoActivity;
import in.happyhelp.limra.adapter.AllPropertyAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_All_Properties extends Fragment {


    LinearLayoutManager linearLayoutManager;
    AllPropertyAdapter allPropertyAdapter;
    RecyclerView recyclerView;
    SearchView searchView;
    TextView data;
    Realm realm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragmentleadlist, container, false);
        recyclerView=view.findViewById(R.id.recycler);
        searchView=view.findViewById(R.id.search_view);
        data=view.findViewById(R.id.data);

        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        realm= RealmHelper.getRealmInstance();

        allPropertyAdapter=new AllPropertyAdapter(getContext(), new AllPropertyAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(final PropertyList d, View view) throws ParseException {
                Intent intent=new Intent(getActivity(), PropertyDetailsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.PROPERTYID,d.getId());
                startActivity(intent);
            }
        });





        searchView.setQueryHint("Search Property");

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent=new Intent(getActivity(), FilterActivity.class);
               startActivity(intent);
            }
        });


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });





        getAllProperty();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }



    public  void getAllProperty(){
        Call<PropertyListResponse> call= RestClient.get(RestClient.PROPERTY_BASE_URL).getAllProperty(SharedPreferenceHelper.getInstance(getContext()).getAuthToken());
        call.enqueue(new Callback<PropertyListResponse>() {
            @Override
            public void onResponse(@NonNull Call<PropertyListResponse> call, @NonNull Response<PropertyListResponse> response) {
                final PropertyListResponse propertyDetailsResponse=response.body();
                if(response.code()==200){
                    if(propertyDetailsResponse.getStatus()){
                        allPropertyAdapter.setData(propertyDetailsResponse.getPropertyList());
                        recyclerView.setAdapter(allPropertyAdapter);

                        if(propertyDetailsResponse.getPropertyList().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }

                    }

                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }

            }

            @Override
            public void onFailure(@NonNull Call<PropertyListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();

            }
        });
    }
}
