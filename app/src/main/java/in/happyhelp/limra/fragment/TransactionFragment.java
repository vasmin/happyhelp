package in.happyhelp.limra.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import java.text.ParseException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.response.mywallet.MyWalletResponse;
import in.happyhelp.limra.adapter.CashbackAdapter;
import in.happyhelp.limra.cashbackwalletresponse.CashbackWallet;
import in.happyhelp.limra.cashbackwalletresponse.CashbackWalletResponse;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.network.RestClient;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransactionFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{


    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.data)
    TextView data;

    Realm realm;
    DialogPlus dialogPlus;
    TextView senderName,receiveName,debitAmt,creditAmt;
    LinearLayoutManager linearLayoutManager;
    CashbackAdapter adapter;
    RealmResults<CashbackWalletResponse> myWalletResponses;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_transaction, container, false);
        ButterKnife.bind(this,view);

        realm= RealmHelper.getRealmInstance();
        adapter=new CashbackAdapter(getActivity(), new CashbackAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(CashbackWallet d, View view) throws ParseException {
                senderName.setText(d.getFromid());
                receiveName.setText(d.getUid());
                creditAmt.setText(d.getCredit());
                debitAmt.setText(d.getDebit());
                dialogPlus.show();
            }
        });

        myWalletResponses=realm.where(CashbackWalletResponse.class).findAllAsync();
        myWalletResponses.addChangeListener(new RealmChangeListener<RealmResults<CashbackWalletResponse>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onChange(@NonNull RealmResults<CashbackWalletResponse> cashbackWalletResponses) {
                if(myWalletResponses.size()>0) {

                    data.setVisibility(View.GONE);
                    adapter.setData(cashbackWalletResponses.get(0).getData());
                    recyclerView.setAdapter(adapter);
                    // total.setVisibility(View.VISIBLE);
                    //  total.setText(" Total " + "\u20B9 0");
                }else{
                    data.setVisibility(View.VISIBLE);
                    // total.setVisibility(View.GONE);
                }
            }
        });
        setDialog();
        getCashbackWallet();


        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        return view;
    }


    @SuppressLint("PrivateResource")
    public void setDialog() {
        LayoutInflater inflater = getLayoutInflater();
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.item_wallet_detail, null);
        senderName=view.findViewById(R.id.name);
        receiveName=view.findViewById(R.id.receviername);
        debitAmt=view.findViewById(R.id.debitamount);
        creditAmt=view.findViewById(R.id.creditamount);
        dialogPlus = DialogPlus.newDialog(getActivity())
                .setContentHolder(new ViewHolder(view))
                .setGravity(Gravity.CENTER)
                .setInAnimation(R.anim.abc_fade_in)
                .setMargin(20, 20, 20, 20)
                .setPadding(20,20,20,20)
                .setOutAnimation(R.anim.abc_fade_out)
                //.setContentBackgroundResource(R.drawable.popup_border)
                .setOnItemClickListener((dialog, item, view1, position) -> {
                })
                .setOnCancelListener(DialogPlus::dismiss)
                .create();
    }


    public void getCashbackWallet(){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid", SharedPreferenceHelper.getInstance(getActivity()).getString(Constants.USERID));
        Call<CashbackWalletResponse> call= RestClient.get().getCashbackMyWallet(SharedPreferenceHelper.getInstance(getActivity()).getAuthToken(),hashMap);
        call.enqueue(new Callback<CashbackWalletResponse>() {
            @Override
            public void onResponse(@NonNull Call<CashbackWalletResponse> call, @NonNull Response<CashbackWalletResponse> response) {
                swipeRefreshLayout.setRefreshing(false);
                final CashbackWalletResponse cashbackWalletResponse=response.body();
                cashbackWalletResponse.setId("1");
                if(response.code()==200){
                    if(cashbackWalletResponse.getStatus()){


                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                               // realm.delete(CashbackWalletResponse.class);
                                realm.copyToRealmOrUpdate(cashbackWalletResponse);
                            }
                        });
                    }else{

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                               // realm.delete(MyWalletResponse.class);
                                realm.copyToRealmOrUpdate(cashbackWalletResponse);
                            }
                        });
                    }
                }else if(response.code()==401){
                    AppUtils.logout(getActivity());
                }else{
                    View parentLayout = getActivity().findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<CashbackWalletResponse> call, @NonNull Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                View parentLayout = getActivity().findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
                t.printStackTrace();

            }
        });
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onRefresh() {
        getCashbackWallet();
    }
}
