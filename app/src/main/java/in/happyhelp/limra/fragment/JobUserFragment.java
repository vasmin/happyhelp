package in.happyhelp.limra.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.R;
import in.happyhelp.limra.adapter.AppliedJobAdapter;

public class JobUserFragment extends Fragment {

    AppliedJobAdapter appliedJobAdapter;


    @BindView(R.id.appliedjobrecycler)
    RecyclerView appliedJobRecycler;

    @BindView(R.id.savedjob)
    RecyclerView savedJobRecycler;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_company_user, container, false);
        ButterKnife.bind(view,getActivity());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
