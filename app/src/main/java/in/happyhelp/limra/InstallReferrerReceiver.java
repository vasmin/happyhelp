package in.happyhelp.limra;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import in.happyhelp.limra.data.SharedPreferenceHelper;

public class InstallReferrerReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent == null) {
            return;
        }

        String referrerId = intent.getStringExtra("referrer");
        if(referrerId!=null && !referrerId.equals("")) {
            if(referrerId.contains("HH")) {
                SharedPreferenceHelper.getInstance(context).putString(Constants.PLAYREFERALCODE, referrerId);
            }
        }

        Toast.makeText(context, "Referral Code Is :"+referrerId, Toast.LENGTH_SHORT).show();
        Log.e("Data","Referral Code"+referrerId);
        if (referrerId == null) {

            return;
        }
    }
}
