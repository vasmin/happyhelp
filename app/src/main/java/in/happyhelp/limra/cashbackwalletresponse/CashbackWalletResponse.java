
package in.happyhelp.limra.cashbackwalletresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CashbackWalletResponse extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    private String id;

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private RealmList<CashbackWallet> data = null;

    @SerializedName("cashback_wallet_total")
    @Expose
    private double cashbackWalletTotal;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public RealmList<CashbackWallet> getData() {
        return data;
    }

    public void setData(RealmList<CashbackWallet> data) {
        this.data = data;
    }

    public double getCashbackWalletTotal() {
        return cashbackWalletTotal;
    }

    public void setCashbackWalletTotal(double cashbackWalletTotal) {
        this.cashbackWalletTotal = cashbackWalletTotal;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
