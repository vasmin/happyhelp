package in.happyhelp.limra.shopping;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.property.PropertyDetailsActivity;
import in.happyhelp.limra.adapter.shopping.ShoppingAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.model.ServiceModel;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.categoryresponse.Category;
import in.happyhelp.limra.shoppingresponse.categoryresponse.ShoppingCategoryResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShoppingActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    LinearLayoutManager linearLayoutManager;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    ShoppingAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);





       /* GridLayoutManager gridLayoutManager = new GridLayoutManager(this, , GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);*/

        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        adapter=new ShoppingAdapter(getApplicationContext(), new ShoppingAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(Category d, View view) throws ParseException {

            }
        });

        getCategory();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void getCategory(){
        Call<ShoppingCategoryResponse> call= RestClient.get(RestClient.SHOPPING_BASE_URL).getShoppingCategory(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<ShoppingCategoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<ShoppingCategoryResponse> call, @NonNull Response<ShoppingCategoryResponse> response) {
                ShoppingCategoryResponse shoppingCategoryResponse=response.body();
                if(response.code()==200){
                    if(shoppingCategoryResponse.getStatus()){

                        adapter.setData(shoppingCategoryResponse.getCategories());
                        recyclerView.setAdapter(adapter);
                    }else{

                    }

                }else if(response.code()==401){
                    AppUtils.logout(ShoppingActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<ShoppingCategoryResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });

    }

    @Override
    public void onRefresh() {
        getCategory();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(ShoppingActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(ShoppingActivity.this,Id);

            }
        });
    }
}
