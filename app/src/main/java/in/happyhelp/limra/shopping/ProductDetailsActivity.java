package in.happyhelp.limra.shopping;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.LoginActivity;
import in.happyhelp.limra.activity.response.bannerresponse.Banner;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.adapter.shopping.ProductAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.model.shopping.CartItem;
import in.happyhelp.limra.model.shopping.ProductType;
import in.happyhelp.limra.model.shopping.WishList;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.homeresponse.Product;
import in.happyhelp.limra.shoppingresponse.productdetailresponse.ProductDetailResponse;
import in.happyhelp.limra.shoppingresponse.productdetailresponse.ProductDetails;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmModel;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailsActivity extends AppCompatActivity {

    @BindView(R.id.slider)
    SliderLayout slider;
    List<String> listFooter=new ArrayList<>();

    TextView textCartItemCount;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.linear)
    LinearLayout linearLayout;

    @BindView(R.id.pincode)
    EditText pincode;

    DialogPlus dialogPlus;

    ProgressDialog progressBar;

    @BindView(R.id.price)
    TextView price;

    @BindView(R.id.discountprice)
    TextView discountPrice;

    @BindView(R.id.delivery)
    TextView delivery;

    @BindView(R.id.check)
    CardView check;

    @BindView(R.id.share)
    ImageView share;

    @BindView(R.id.like)
    ImageView like;

    @BindView(R.id.stock)
    TextView stock;

    @BindView(R.id.available)
    TextView available;

    @BindView(R.id.size)
    Spinner sizeSpinner;

    @BindView(R.id.color)
    Spinner colorSpinner;

    boolean isLike=false;
    Realm realm;
    String size="Select Size";
    String color="Select Color";

    String Description,Specification,ReturnPolicy="";

    @BindView(R.id.description)
    TextView description;

    @BindView(R.id.specification)
    TextView specification;

    @BindView(R.id.returnpolicy)
    TextView returnPolicy;

    @BindView(R.id.detail)
    TextView details;

    @BindView(R.id.addcart)
    TextView addtocart;

    @BindView(R.id.buy)
    TextView buy;

    int baseamount=0;
    int counter = 1;
    int amount=0;
    @BindView(R.id.count)
    TextView count;

    @BindView(R.id.increment)
    ImageView increment;

    @BindView(R.id.decrement)
    ImageView decrement;

    @BindView(R.id.sizelinear)
    RelativeLayout sizelinear;

    @BindView(R.id.colorlinear)
    RelativeLayout cololinear;

    List<String> sizeList=new ArrayList<>();
    List<String> colorList=new ArrayList<>();
    boolean isDesc=true;
    boolean isSpecification,isReturn=false;
    ArraySpinnerAdapter sizeAdapter,colorAdapter;
    String pId="";
    ProductDetails product;

    @BindView(R.id.taxtype)
    TextView taxtype;

    @BindView(R.id.imageView)
            ImageView imageView;

    boolean isImageFitToScreen;
    boolean isbuy=false;
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        realm= RealmHelper.getRealmInstance();


        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addWhishlist(pId);
            }
        });
        setupBadge();
        sizeAdapter=new ArraySpinnerAdapter(ProductDetailsActivity.this);
        colorAdapter=new ArraySpinnerAdapter(ProductDetailsActivity.this);

        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);


        imageView.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                imageView.buildDrawingCache();
                Bitmap bitmap = imageView.getDrawingCache();

                Intent intent = new Intent(ProductDetailsActivity.this, ProductImageActivity.class);
                intent.putExtra("BitmapImage", bitmap);
                startActivity(intent);
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out Product at: "+RestClient.SHOPPING_BASE_URL+"productdetails?Pid="+pId);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        increment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(baseamount>1){
                    counter++;
                    count.setText(String.valueOf(counter));
                    amount = counter*baseamount;
                }
            }
        });

        decrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(baseamount>1 && counter > 1){
                    counter--;
                    count.setText(String.valueOf(counter));
                    amount = amount-baseamount;
                }
            }
        });


        sizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(sizeList.size()>i) {
                    size = String.valueOf(sizeList.get(i));

                    final RealmResults<ProductType> productTypes1=realm.where(ProductType.class).equalTo("size",size).findAllAsync();
                    productTypes1.addChangeListener(new RealmChangeListener<RealmResults<ProductType>>() {
                        @Override
                        public void onChange(RealmResults<ProductType> productTypes) {
                            colorList.clear();
                            colorList.add("Select Color");
                            for(int i=0;i<productTypes.size();i++){
                                colorList.add(productTypes1.get(i).getColor());
                            }
                            colorAdapter.setData(colorList);
                            colorSpinner.setAdapter(colorAdapter);

                            if(colorList.size()>1){

                                cololinear.setVisibility(View.VISIBLE);
                                colorSpinner.setEnabled(true);
                            }else
                            {

                                cololinear.setVisibility(View.GONE);

                            }
                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        colorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(colorList.size()>i) {
                    color = String.valueOf(colorList.get(i));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(pincode.getText().toString())) {
                    pincode.setError("Enter pincode ");
                    return;
                }

                if(sizeList.size()>1 && size.equals("Select Size")){
                    Toast.makeText(ProductDetailsActivity.this, "Please Select Size", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(colorList.size()>1 && color.equals("Select Color")){
                    Toast.makeText(ProductDetailsActivity.this, "Please Select color", Toast.LENGTH_SHORT).show();
                    return;
                }


                //isbuy=true;
                addToCart();
                setupBadge();
            }
        });

        
        addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (TextUtils.isEmpty(pincode.getText().toString())) {
                    pincode.setError("Enter pincode ");
                    return;
                }


                if(sizeList.size()>1 && size.equals("Select Size")){
                    Toast.makeText(ProductDetailsActivity.this, "Please Select Size", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(colorList.size()>1 && color.equals("Select Color")){
                    Toast.makeText(ProductDetailsActivity.this, "Please Select color", Toast.LENGTH_SHORT).show();
                    return;
                }



                isbuy=false;
                addToCart();
                setupBadge();
            }
        });

        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDesc=true;
                isSpecification=false;
                isReturn=false;
                setData();
            }
        });

        specification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDesc=false;
                isSpecification=true;
                isReturn=false;
                setData();
            }
        });

        returnPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDesc=false;
                isSpecification=false;
                isReturn=true;
                setData();
            }
        });

        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(pincode.getText().toString())) {
                    pincode.setError("Enter pincode ");
                    Toast.makeText(ProductDetailsActivity.this, "Enter pincode ", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(pincode.getText().length()!=6){
                    pincode.setError("Enter valid pincode ");
                    Toast.makeText(ProductDetailsActivity.this, "Enter valid pincode ", Toast.LENGTH_SHORT).show();
                    return;
                }

                checkPincode();
            }
        });

        Intent intent = getIntent();
        if(intent.getData()!=null) {
            Uri uri = intent.getData();
            pId = uri.getQueryParameter(Constants.PID);
            if (!pId.equals("")) {
                getProductDetails(pId);
            }
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            pId = extras.getString(Constants.PID);
            getProductDetails(pId);
        }

        if(isLike){
            like.setImageDrawable(getDrawable(R.drawable.like));
        }else{
            like.setImageDrawable(getDrawable(R.drawable.dislike));
        }

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        slider.setPresetTransformer(com.daimajia.slider.library.SliderLayout.Transformer.Accordion);
        slider.setPresetIndicator(com.daimajia.slider.library.SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }


    public void fullImageDialogue(final String url){
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.item_image, null);
        dialogPlus = DialogPlus.newDialog(ProductDetailsActivity.this)
                .setContentHolder(new ViewHolder(view))
                .setGravity(Gravity.CENTER)
                .setInAnimation(R.anim.abc_fade_in)
                .setMargin(50, 20, 50, 50)
                .setOutAnimation(R.anim.abc_fade_out)
                .setOverlayBackgroundResource(Color.TRANSPARENT)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogPlus dialog) {
                        dialog.dismiss();
                    }
                })
                .create();
        final ImageView imageView= (ImageView) dialogPlus.findViewById(R.id.imageView);

        dialogPlus.show();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Glide.with(getApplicationContext())
                        .load(url)
                        .into(imageView);
            }
        });


        Toast.makeText(this, "Show", Toast.LENGTH_SHORT).show();
    }

    public void addToCart(){
        progressBar.show();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                CartItem cartItem = new CartItem();
                cartItem.setPid(pId);
                cartItem.setSku(product.getSku());
                cartItem.setImage(product.getImage());
                int price=product.getDiscountprice()*counter;
                cartItem.setTotalAmt(String.valueOf(price));
                cartItem.setColor(color);
                cartItem.setSize(size);
                cartItem.setDiscount(product.getDiscount());
                cartItem.setDiscountprice(String.valueOf(product.getDiscountprice()));
                cartItem.setShippingcharges(product.getShippingCharges());
                cartItem.setTax(product.getTax());
                cartItem.setName(product.getName());
                cartItem.setPrice(String.valueOf(product.getDiscountprice()));
                cartItem.setQty(String.valueOf(counter));

                realm.copyToRealmOrUpdate(cartItem);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, "Product is added to cart successfully", Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                        .show();
                getProductDetails(pId);

                if(isbuy){
                    Intent intent=new Intent(ProductDetailsActivity.this,CartActivity.class);
                    startActivity(intent);
                }
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(@NonNull Throwable error) {
                progressBar.dismiss();
                error.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, " Failure to add to cart", Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                        .show();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.home_shopping, menu);
        final MenuItem menuItem = menu.findItem(R.id.cart);

        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

        setupBadge();

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.cart) {
            Intent intent=new Intent(ProductDetailsActivity.this,CartActivity.class);
            startActivity(intent);
            return true;
        }
        else if(id==R.id.wish){
            Intent intent=new Intent(ProductDetailsActivity.this,WishActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupBadge() {

        if (textCartItemCount != null) {
            RealmResults<CartItem> cartItems=realm.where(CartItem.class).findAllAsync();
            cartItems.addChangeListener(new RealmChangeListener<RealmResults<CartItem>>() {
                @Override
                public void onChange(RealmResults<CartItem> cartItems) {
                    if (cartItems.size() == 0) {
                        if (textCartItemCount.getVisibility() != View.GONE) {
                            textCartItemCount.setVisibility(View.GONE);
                        }
                    }
                    else {
                        textCartItemCount.setText(String.valueOf(Math.min(cartItems.size(), 99)));
                        if (textCartItemCount.getVisibility() != View.VISIBLE) {
                            textCartItemCount.setVisibility(View.VISIBLE);
                        }
                    }
                }

            });

        }
    }

    public void setData(){
        if(isDesc){
            details.setText(Description);
            description.setBackground(getDrawable(R.drawable.borderbackground));
            specification.setBackground(getDrawable(R.drawable.background));
            returnPolicy.setBackground(getDrawable(R.drawable.background));
        }else if(isSpecification){

            details.setText(Html.fromHtml(Specification));
            description.setBackground(getDrawable(R.drawable.background));
            specification.setBackground(getDrawable(R.drawable.borderbackground));
            returnPolicy.setBackground(getDrawable(R.drawable.background));

        }else if(isReturn){

            details.setText(ReturnPolicy);
            description.setBackground(getDrawable(R.drawable.background));
            specification.setBackground(getDrawable(R.drawable.background));
            returnPolicy.setBackground(getDrawable(R.drawable.borderbackground));

        }
    }
    public void checkPincode(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("pincode",pincode.getText().toString());
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get(RestClient.SHOPPING_BASE_URL).checkPincode(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()) {
                        available.setVisibility(View.VISIBLE);
                        available.setText("Available");
                        available.setTextColor(Color.GREEN);
                        View parentLayout = findViewById(android.R.id.content);
                        linearLayout.setVisibility(View.VISIBLE);
                        Snackbar.make(parentLayout, "Delivery available", Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();
                    }else{
                        available.setVisibility(View.VISIBLE);
                        available.setText("Not available");
                        available.setTextColor(Color.RED);
                        linearLayout.setVisibility(View.GONE);

                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, "Sorry no delivery available", Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(ProductDetailsActivity.this);
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void addWhishlist(final String id){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("id",id);
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get(RestClient.SHOPPING_BASE_URL).addWishList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                WishList wishList=new WishList();
                                wishList.setPid(product.getId());
                                wishList.setImage(product.getImage());
                                realm.copyToRealmOrUpdate(wishList);
                            }
                        });
                        setupBadge();
                        getProductDetails(id);

                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }else{
                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(ProductDetailsActivity.this);
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void getProductDetails(final String pId){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",pId);
        Call<ProductDetailResponse> call= RestClient.get(RestClient.SHOPPING_BASE_URL).getProductDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<ProductDetailResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<ProductDetailResponse> call, @NonNull Response<ProductDetailResponse> response) {
                final ProductDetailResponse productDetailResponse=response.body();
                if(response.code()==200) {
                    Log.d("response","response"+response);
                    if (productDetailResponse.getStatus()){

                        product=productDetailResponse.getProductDetails();
                        name.setText(product.getName());
                        stock.setText(product.getStockname());
                        discountPrice.setText("\u20B9 "+product.getDiscountprice()+"");
                        price.setText((Html.fromHtml("<strike>"+"\u20B9 "+product.getPrice()+"</strike>")));
                        price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        baseamount= Integer.parseInt(product.getPrice());
                        if(isDesc){
                            details.setText(product.getDescription());
                            description.setBackground(getDrawable(R.drawable.borderbackground));
                        }

                        if(product.getTaxType().equals("0")){
                            taxtype.setText("( Include tax )");
                        }else{
                            taxtype.setText("( Exclude tax )");
                        }



                        final WishList wishList=realm.where(WishList.class).equalTo("pid",pId).findFirstAsync();
                        wishList.addChangeListener(new RealmChangeListener<RealmModel>() {
                            @Override
                            public void onChange(RealmModel realmModel) {
                                if(wishList!=null && wishList.isValid() && wishList.isLoaded()){
                                    isLike=true;
                                    if(isLike){
                                        like.setImageDrawable(getDrawable(R.drawable.like));
                                    }else{
                                        like.setImageDrawable(getDrawable(R.drawable.dislike));
                                    }
                                }else{
                                    like.setImageDrawable(getDrawable(R.drawable.dislike));
                                }
                            }
                        });



                        Description=product.getDescription();
                        Specification=product.getDetail();
                        ReturnPolicy=product.getReturnPolicy();


                        final List<ProductType> productTypes=new ArrayList<>();
                        for(int j=0;j<product.getVariant().size();j++){
                            ProductType productType=new ProductType();
                            productType.setId(String.valueOf(j));
                            productType.setSize(product.getVariant().get(j).getVariant().get(0));
                            productType.setColor(product.getVariant().get(j).getVariant().get(1));
                            productTypes.add(productType);
                        }

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(ProductType.class);
                                realm.copyToRealmOrUpdate(productTypes);
                            }
                        });


                        final RealmResults<ProductType> productTypes1=realm.where(ProductType.class).findAllAsync();
                        productTypes1.addChangeListener(new RealmChangeListener<RealmResults<ProductType>>() {
                            @Override
                            public void onChange(@NonNull RealmResults<ProductType> productTypes) {
                                sizeList.clear();
                                colorList.clear();
                                sizeList.add("Select Size");
                                colorList.add("Select Color");
                                for(int i=0;i<productTypes.size();i++){
                                    if(!sizeList.contains(productTypes1.get(i).getSize()))
                                    {
                                        sizeList.add(productTypes1.get(i).getSize());
                                    }

                                }


                                sizeAdapter.setData(sizeList);
                                sizeSpinner.setAdapter(sizeAdapter);



                                if(sizeList.size()>1){
                                    sizelinear.setVisibility(View.VISIBLE);
                                    cololinear.setVisibility(View.VISIBLE);
                                    colorSpinner.setEnabled(false);
                                }else
                                {
                                    sizelinear.setVisibility(View.GONE);
                                    cololinear.setVisibility(View.GONE);
                                   // colorSpinner.setEnabled(false);

                                }

                            }
                        });

                        colorAdapter.setData(colorList);
                        colorSpinner.setAdapter(colorAdapter);



                        for(int i=0;i<productDetailResponse.getProductDetails().getImages().size();i++){
                            listFooter.clear();
                            listFooter.addAll(productDetailResponse.getProductDetails().getImages());
                        }

                        for(int i = 0; i<listFooter.size();i ++) {

                            DefaultSliderView defaultSliderView = new DefaultSliderView(getApplicationContext());
                            final int finalI = i;
                            defaultSliderView.image(RestClient.base_image_url+listFooter.get(i))
                                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                        @Override
                                        public void onSliderClick(BaseSliderView slider) {
                                            String BannnerName=listFooter.get(finalI);
                                            Intent intent=new Intent(ProductDetailsActivity.this,ProductImageActivity.class);
                                            intent.putExtra(Constants.IMAGE,RestClient.base_image_url+listFooter.get(finalI));
                                            startActivity(intent);
                                          // fullImageDialogue(RestClient.base_image_url+listFooter.get(finalI));

                                        }
                                    });

                            slider.addSlider(defaultSliderView);

                        }

                        Glide.with(getApplicationContext()).load(RestClient.base_image_url+listFooter.get(0))
                                .into(imageView);

                    }else{

                    }
                }else if(response.code()==401){
                    AppUtils.logout(ProductDetailsActivity.this);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ProductDetailResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupBadge();
        EventBus.getDefault().unregister(this);
    }



    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(ProductDetailsActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(ProductDetailsActivity.this,Id);

            }
        });
    }
}
