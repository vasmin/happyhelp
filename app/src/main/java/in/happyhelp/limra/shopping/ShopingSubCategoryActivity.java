package in.happyhelp.limra.shopping;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.adapter.shopping.HomeProductAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.model.shopping.SubCat;
import in.happyhelp.limra.model.shopping.SubSubCat;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.subcategoryresponse.ShoppingSubCategoryResponse;
import in.happyhelp.limra.shoppingresponse.subcategoryresponse.SubCategoryResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopingSubCategoryActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    String shopId="";

    HomeProductAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shoping_sub_category);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        AppUtils.isNetworkConnectionAvailable(this);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            shopId = extras.getString(Constants.SHOPID);
            getSubCategory(shopId);
        }


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView.ItemAnimator animator = recyclerView.getItemAnimator();
        if (animator instanceof DefaultItemAnimator) {
            ((DefaultItemAnimator) animator).setSupportsChangeAnimations(false);
        }

        recyclerView.setLayoutManager(layoutManager);




    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void getSubCategory(final String shopId){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",shopId);
        Call<SubCategoryResponse> call= RestClient.get(RestClient.SHOPPING_BASE_URL).getShoppingSubCategory(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<SubCategoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<SubCategoryResponse> call, @NonNull Response<SubCategoryResponse> response) {
                SubCategoryResponse shoppingSubCategoryResponse=response.body();
                if(response.code()==200){
                    if(shoppingSubCategoryResponse.getStatus()){
                        List<SubCat> subcat=new ArrayList<>();

                        for(int i=0;i<shoppingSubCategoryResponse.getSubcategories().size();i++){
                            List<SubSubCat> subsubcat=new ArrayList<>();
                            for(int j=0;j<shoppingSubCategoryResponse.getSubcategories().get(i).getSubsubcategories().size();j++){
                                SubSubCat subsubCat=new SubSubCat(shoppingSubCategoryResponse.getSubcategories().get(i).getSubsubcategories().get(j).getId(),shoppingSubCategoryResponse.getSubcategories().get(i).getSubsubcategories().get(j).getName(),true);
                                subsubcat.add(subsubCat);
                            }
                            SubCat subCat=new SubCat(shoppingSubCategoryResponse.getSubcategories().get(i).getName(),subsubcat,1);

                            subcat.add(subCat);
                        }


                        adapter = new HomeProductAdapter(getApplicationContext(),subcat);
                        recyclerView.setAdapter(adapter);
                        if(shoppingSubCategoryResponse.getSubcategories().size()==0){
                          /*  Intent intent=new Intent(ShopingSubCategoryActivity.this,ShopingProductActivity.class);
                            startActivity(intent);*/
                        }else{

                        }
                    }else{

                    }
                }else if(response.code()==401){
                    AppUtils.logout(ShopingSubCategoryActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<SubCategoryResponse> call, @NonNull Throwable t) {
                t.printStackTrace();

            }
        });
    }


    @Override
    public void onRefresh() {
        getSubCategory(shopId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(ShopingSubCategoryActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(ShopingSubCategoryActivity.this,Id);

            }
        });
    }
}
