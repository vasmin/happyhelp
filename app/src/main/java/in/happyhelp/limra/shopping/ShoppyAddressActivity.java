package in.happyhelp.limra.shopping;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.MyProfileActivity;
import in.happyhelp.limra.activity.response.Response;
import in.happyhelp.limra.activity.response.statecityresponse.City;
import in.happyhelp.limra.activity.response.statecityresponse.State;
import in.happyhelp.limra.adapter.ArraySpinnerAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.addressdetails.AddressResponse;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmObject;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;

public class ShoppyAddressActivity extends AppCompatActivity {



    @BindView(R.id.state)
    Spinner stateSpinner;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.pincode)
    EditText pincode;

    @BindView(R.id.address1)
    EditText address1;

    @BindView(R.id.address2)
    EditText address2;

    @BindView(R.id.cityspinner)
    Spinner citySpinner;

    @BindView(R.id.locality)
    EditText locality;

    RealmResults<State> statesList;
    Realm realm;
    boolean ismycity=true;

    List<String> listCity=new ArrayList<>();
    List<String> listState=new ArrayList<>();
    ArraySpinnerAdapter stateSpinnerAdapter,citySpinnerAdapter;

    String stateName,cityName;

    String addressId="";

    @BindView(R.id.submit)
    TextView submit;

    boolean isUpdate=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shoppy_address);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ButterKnife.bind(this);
        realm= RealmHelper.getRealmInstance();

        stateSpinnerAdapter =new ArraySpinnerAdapter(ShoppyAddressActivity.this);
        citySpinnerAdapter =new ArraySpinnerAdapter(ShoppyAddressActivity.this);

        statesList=realm.where(State.class).findAllAsync();
        statesList.addChangeListener(new RealmChangeListener<RealmResults<State>>() {
            @Override
            public void onChange(RealmResults<State> states) {
                listState.clear();
                for(int i=0;i<statesList.size();i++){
                    listState.add(statesList.get(i).getState());
                }
                stateSpinnerAdapter.setData(listState);
                stateSpinner.setAdapter(stateSpinnerAdapter);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(TextUtils.isEmpty(name.getText().toString())){
                    name.setError("Enter Name ");
                    return;
                }

                if(TextUtils.isEmpty(address1.getText().toString())){
                    address1.setError("Enter address1 ");
                    return;
                }

                if(TextUtils.isEmpty(address2.getText().toString())){
                    address2.setError("Enter address2 ");
                    return;
                }

                if(TextUtils.isEmpty(locality.getText().toString())){
                    locality.setError("Enter locality ");
                    return;
                }

                if(TextUtils.isEmpty(pincode.getText().toString())){
                    pincode.setError("Enter pincode ");
                    return;
                }

                if(pincode.getText().length()!=6){
                    pincode.setError("Enter valid pincode ");
                    Toast.makeText(ShoppyAddressActivity.this, "Enter valid pincode ", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(stateName.equals("Select State")){
                    Toast.makeText(getApplicationContext(),"Select State",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(cityName.equals("Select City")){
                    Toast.makeText(getApplicationContext(),"Select City",Toast.LENGTH_SHORT).show();
                    return;
                }



                addAddress();
            }
        });

        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stateName = String.valueOf(listState.get(i));
                final RealmResults<City> cityRealmResults = realm.where(City.class).equalTo("state", stateName).findAllAsync();
                cityRealmResults.addChangeListener(new RealmChangeListener<RealmResults<City>>() {
                    @Override
                    public void onChange(@NonNull RealmResults<City> cities) {
                        listCity.clear();
                        listCity.add("Select City");
                        if (cityRealmResults.size() > 0) {
                            for (int i = 0; i < cityRealmResults.size(); i++) {
                                if (!listCity.contains(cities.get(i).getCity())) {
                                    listCity.add(cities.get(i).getCity());
                                }
                            }
                            citySpinnerAdapter.setData(listCity);
                            citySpinner.setAdapter(citySpinnerAdapter);
                        }
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(listCity.size()>i) {
                    cityName = String.valueOf(listCity.get(i));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            addressId = extras.getString(Constants.ADDRESSSID);
            getAddressDetails();

        }


    }

    public void getAddressDetails(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",addressId);
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<AddressResponse> call=RestClient.get(RestClient.SHOPPING_BASE_URL).getAddressDetails(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<AddressResponse>() {
            @Override
            public void onResponse(Call<AddressResponse> call, retrofit2.Response<AddressResponse> response) {
                AddressResponse addressResponse=response.body();
                if(response.code()==200){

                    if(addressResponse.getStatus()) {
                        isUpdate=true;
                        submit.setText("Update Address");
                        address1.setText(addressResponse.getAddressDetails().getAddress1());
                        address2.setText(addressResponse.getAddressDetails().getAddress2());
                        name.setText(addressResponse.getAddressDetails().getName());
                        locality.setText(addressResponse.getAddressDetails().getLocality());
                        pincode.setText(addressResponse.getAddressDetails().getPincode());

                        for(int i=0;i<listState.size();i++){
                            if(listState.get(i).equals(addressResponse.getAddressDetails().getState())){
                                stateSpinner.setSelection(i);
                            }
                        }


                        for(int i=0;i<listCity.size();i++){
                            if(listCity.get(i).equals(addressResponse.getAddressDetails().getCity())){
                                citySpinner.setSelection(i);
                            }
                        }

                    }else{

                    }
                }else if(response.code()==401){
                    AppUtils.logout(ShoppyAddressActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(Call<AddressResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    public void addAddress(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("city",cityName);
        hashMap.put("name",name.getText().toString());
        hashMap.put("pincode",pincode.getText().toString());
        hashMap.put("state",stateName);
        hashMap.put("address1",address1.getText().toString());
        hashMap.put("address2",address2.getText().toString());
        hashMap.put("locality",locality.getText().toString());
        hashMap.put("type","home");
        if(isUpdate) {
            hashMap.put("id", addressId);
        }
        Call<Response> call= RestClient.get(RestClient.SHOPPING_BASE_URL).addAddress(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {
                Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        new AlertDialog.Builder(ShoppyAddressActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(response1.getMessage())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        onBackPressed();
                                    }
                                })
                                .show();
                    }else{
                        new AlertDialog.Builder(ShoppyAddressActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(response1.getMessage())
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        onBackPressed();
                                    }
                                })
                                .show();
                    }

                }else if(response.code()==401){
                    AppUtils.logout(ShoppyAddressActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(ShoppyAddressActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(ShoppyAddressActivity.this,Id);

            }
        });
    }


}
