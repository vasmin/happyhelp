package in.happyhelp.limra.shopping;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.adapter.shopping.ProductAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.model.shopping.WishList;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.categoryresponse.Category;
import in.happyhelp.limra.shoppingresponse.productresponse.Product;
import in.happyhelp.limra.shoppingresponse.productresponse.ProductResponse;
import in.happyhelp.limra.shoppingresponse.wishlistresponse.WishListResponse;
import in.happyhelp.limra.shoppingresponse.wishlistresponse.Wishlist;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopingProductActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.data)
    TextView data;

    String subCat="";
    ProductAdapter adapter;
    Realm realm;
    List<WishList> list=new ArrayList<>();
    List<Product> products=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shoping_product);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);

        realm= RealmHelper.getRealmInstance();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            subCat = extras.getString(Constants.SUBCAT);
            getSubCategoryProduct(subCat);
        }
        getWishList();
        GridLayoutManager gridLayoutManager=new GridLayoutManager(getApplicationContext(), 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);

        adapter=new ProductAdapter(getApplicationContext(), new ProductAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(final Product d, View view) throws ParseException {

                ImageView like=view.findViewById(R.id.like);
                like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(d.getWishlist().equals("1")){
                            removefromWishLits(d.getId());
                        }else{
                            addWhishlist(d);
                        }

                    }
                });

                ImageView image=view.findViewById(R.id.image);
                LinearLayout linearLayout=view.findViewById(R.id.linear);

                linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(ShopingProductActivity.this,ProductDetailsActivity.class);
                        intent.putExtra(Constants.PID, d.getId());
                        startActivity(intent);
                    }
                });

                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(ShopingProductActivity.this,ProductDetailsActivity.class);
                        intent.putExtra(Constants.PID, d.getId());
                        startActivity(intent);
                    }
                });
            }
        });


    }

    public void getWishList(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("page","1");
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<WishListResponse> call= RestClient.get(RestClient.SHOPPING_BASE_URL).getWishList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<WishListResponse>() {
            @Override
            public void onResponse(@NonNull Call<WishListResponse> call, @NonNull Response<WishListResponse> response) {
                final WishListResponse wishListResponse=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(wishListResponse.getStatus()){
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(WishList.class);
                                for(int i=0;i<wishListResponse.getWishlists().size();i++){
                                    WishList wishList=new WishList();
                                    Wishlist wishlist=wishListResponse.getWishlists().get(i);
                                    wishList.setDate(wishlist.getDate());
                                    wishList.setDiscount(wishlist.getDiscount());
                                    wishList.setDiscountprice(wishlist.getDiscountprice());
                                    wishList.setImage(wishlist.getImage());
                                    wishList.setId(wishlist.getId());
                                    wishList.setPid(wishlist.getPid());
                                    wishList.setName(wishList.getDate());
                                    wishList.setUid(wishlist.getUid());
                                    list.add(wishList);
                                }
                                realm.copyToRealmOrUpdate(list);
                            }
                        });
                    }else{

                    }

                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<WishListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void removefromWishLits(String id){

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("pid",id);
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get(RestClient.SHOPPING_BASE_URL).removeWishList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {

                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        getSubCategoryProduct(subCat);
                        getWishList();
                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }else{
                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();

                    }
                }else if(response.code()==401){
                    AppUtils.logout(ShopingProductActivity.this);
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();

                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
                t.printStackTrace();
            }
        });
    }



    public void addWhishlist(final Product product){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("id",product.getId());
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get(RestClient.SHOPPING_BASE_URL).addWishList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {

                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){


                        getSubCategoryProduct(subCat);
                        getWishList();
                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }else{
                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();

            }
        });
    }

    public void getSubCategoryProduct(String subCat){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",subCat);
        hashMap.put("type","subsubcategory");
        hashMap.put("price","");
        hashMap.put("page","1");
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<ProductResponse> call= RestClient.get(RestClient.SHOPPING_BASE_URL).getProducts(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProductResponse> call, @NonNull Response<ProductResponse> response) {
                final ProductResponse productResponse=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(productResponse.getStatus()){
                        products.clear();
                        products.addAll(productResponse.getProducts());
                        adapter.setData(products);
                        recyclerView.setAdapter(adapter);

                        if(productResponse.getProducts().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }

                    }else{

                    }
                }else if(response.code()==401){
                    AppUtils.logout(ShopingProductActivity.this);
                }else{

                }

            }

            @Override
            public void onFailure(@NonNull Call<ProductResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSubCategoryProduct(subCat);
        EventBus.getDefault().unregister(this);
    }


    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(ShopingProductActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(ShopingProductActivity.this,Id);

            }
        });
    }

    @Override
    public void onRefresh() {
        getSubCategoryProduct(subCat);
    }
}
