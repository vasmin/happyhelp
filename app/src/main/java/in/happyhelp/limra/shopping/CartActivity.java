package in.happyhelp.limra.shopping;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.ConfirmationActivity;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import in.happyhelp.limra.activity.service.OrderDetailsActivity;
import in.happyhelp.limra.adapter.shopping.CartAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.model.shopping.CartItem;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.defaultaddress.DefaultAddressResponse;
import in.happyhelp.limra.shoppingresponse.placeordermodel.Address;
import in.happyhelp.limra.shoppingresponse.placeordermodel.Amount;
import in.happyhelp.limra.shoppingresponse.placeordermodel.PlaceOrder;
import in.happyhelp.limra.shoppingresponse.placeordermodel.Product;
import in.happyhelp.limra.shoppingresponse.placeordermodel.Promo;
import in.happyhelp.limra.shoppingresponse.placeordermodel.Wallet;
import in.happyhelp.limra.shoppingresponse.promodata.PromoData;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.data)
    TextView data;

    @BindView(R.id.changeaddress)
    TextView changeAddress;

    Realm realm;

    RealmResults<CartItem> cartItems;

    @BindView(R.id.adddress)
    TextView addressTxt;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.mobile)
    TextView mobile;

    @BindView(R.id.email)
    TextView email;

    @BindView(R.id.addresscard)
    CardView addressCard;

    @BindView(R.id.pay)
    TextView pay;
    int postwalletAmt=0;

    @BindView(R.id.totalcard)
    CardView totalCard;

    @BindView(R.id.subtotal)
    TextView subTotal;

    @BindView(R.id.shipping)
    TextView shippingCharges;

    @BindView(R.id.tax)
    TextView tax;

    @BindView(R.id.total)
    TextView grandTotal;

    @BindView(R.id.noaddress)
    TextView noAddress;

    @BindView(R.id.address)
    LinearLayout defaultAddress;

    CartAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    @BindView(R.id.promocode)
    LinearLayout promoCode;

    @BindView(R.id.promo)
    TextView promo;

    @BindView(R.id.wallet)
    TextView wallet;

    EditText promocode;
    TextView apply,cancel;

    float result=0;
    @BindView(R.id.cash)
    RadioButton cashRb;

    @BindView(R.id.online)
    RadioButton onlineRb;

    @BindView(R.id.walletamt)
            TextView walletAmt;

    @BindView(R.id.walletlinear)
            LinearLayout walletLinear;
    String productId="";
    in.happyhelp.limra.shoppingresponse.defaultaddress.Address address;
    int grandtotal=0;
    int walletlimit=0;
    DialogPlus dialogPlus;
    ProgressDialog progressBar;
    int promoAmt=0;
    float walletAmount=0;

    boolean isPromo=false;
    boolean isWallet=false;
    RealmResults<UserData> userData;
    @BindView(R.id.promocodehave)
    TextView promocodeHave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        realm= RealmHelper.getRealmInstance();

        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);


        linearLayoutManager = new LinearLayoutManager(CartActivity.this, LinearLayoutManager.VERTICAL, false);
        getDefaultAddress();
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        adapter=new CartAdapter(getApplicationContext(), new CartAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(final CartItem d, View view) throws ParseException {


                ImageView increment=view.findViewById(R.id.increment);
                ImageView decrement=view.findViewById(R.id.decrement);
                final TextView count=view.findViewById(R.id.count);

                increment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int counter= Integer.parseInt(d.getQty());
                        int baseamount= Integer.parseInt(d.getPrice());

                        if(baseamount>1){
                            counter++;
                            count.setText(String.valueOf(counter));

                            final int finalCounter = counter;
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(@NonNull Realm realm) {
                                    d.setQty(String.valueOf(finalCounter));
                                    realm.copyToRealmOrUpdate(d);
                                }
                            });
                            int amount = counter*baseamount;

                        }
                    }
                });

                decrement.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int counter= Integer.parseInt(d.getQty());
                        int baseamount= Integer.parseInt(d.getPrice());

                        if(baseamount>1 && counter > 1){
                            counter--;
                            count.setText(String.valueOf(counter));
                            final int finalCounter = counter;
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(@NonNull Realm realm) {
                                    d.setQty(String.valueOf(finalCounter));
                                    realm.copyToRealmOrUpdate(d);
                                }
                            });
                            //  int amount = amount-baseamount;
                        }
                    }
                });

                ImageView delete=view.findViewById(R.id.delete);
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                               d.deleteFromRealm();
                               adapter.notifyDataSetChanged();
                            }
                        });
                    }
                });
            }
        });


        promocodeHave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPromo){
                    isPromo=false;
                    setPromoCode();
                }else {
                    promoDialogue();
                }
            }
        });

        wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(walletAmount>0) {
                    if (isWallet) {
                        isWallet=false;
                        usedWallet();
                    } else {
                        isWallet=true;
                        usedWallet();
                    }
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout,"Sorry you cant use wallet", Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }
        });


        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(cashRb.isChecked()){
                    placeOrder("Cash Payment");
                }else{
                    callInstamojoPay(address.getEmail(),address.getMobile(), String.valueOf(grandtotal), address.getName());
                }

            }
        });

        changeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(CartActivity.this,AddresslistActivity.class);
                startActivity(intent);
            }
        });


        userData=realm.where(UserData.class).findAllAsync();
        userData.addChangeListener(new RealmChangeListener<RealmResults<UserData>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onChange(@NonNull RealmResults<UserData> userData) {

                if (userData.size() > 0) {
                    wallet.setText("Use Wallet (\u20B9 "+String.valueOf(userData.get(0).getWallet()+")"));
                    walletAmount= (float) userData.get(0).getWallet();
                    walletlimit= Integer.parseInt(userData.get(0).getWallet_limit());
                }

                if (userData.size() == 0) {
                    wallet.setText("0");
                }
            }
        });
        setData();
    }

    public void promoDialogue(){
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.item_promocode, null);
        dialogPlus = DialogPlus.newDialog(CartActivity.this)
                .setContentHolder(new ViewHolder(view))
                .setGravity(Gravity.CENTER)
                .setCancelable(false)
                .setInAnimation(R.anim.abc_fade_in)
                .setMargin(50, 20, 50, 50)
                .setOutAnimation(R.anim.abc_fade_out)
                .setOverlayBackgroundResource(Color.TRANSPARENT)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogPlus dialog) {
                        dialog.dismiss();
                    }
                })
                .create();
        promocode= (EditText) dialogPlus.findViewById(R.id.promocode);
        apply= (TextView) dialogPlus.findViewById(R.id.apply);
        cancel= (TextView) dialogPlus.findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPlus.dismiss();
            }
        });

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(promocode.getText().toString())){
                    promocode.setError(" Enter promocode ");
                    return;
                }
                applyPromoCode();
            }
        });
        dialogPlus.show();
    }

    public void applyPromoCode(){
        PromoData promoData=new PromoData();
        promoData.setAmount(String.valueOf(grandtotal));
        promoData.setCode(promocode.getText().toString());
        List<in.happyhelp.limra.shoppingresponse.promodata.Product> products=new ArrayList<>();

        for(int i=0;i<cartItems.size();i++){
            CartItem cartItem=cartItems.get(i);
            in.happyhelp.limra.shoppingresponse.promodata.Product product=new in.happyhelp.limra.shoppingresponse.promodata.Product();
            product.setId(cartItem.getPid());
            product.setDiscount(cartItem.getDiscount());
            product.setDiscountprice(Integer.valueOf(cartItem.getDiscountprice()));
            product.setImage(cartItem.getImage());
            product.setPrice(cartItem.getPrice());
            product.setName(cartItem.getName());
            product.setQuantity(Integer.valueOf(cartItem.getQty()));
            product.setShippingCharges(cartItem.getShippingcharges());
            product.setSku(cartItem.getSku());
            product.setSize(cartItem.getSize());
            product.setTax(cartItem.getTax());
            product.setTotalprice(Integer.valueOf(cartItem.getTotalAmt()));


            products.add(product);


        }
        promoData.setProducts(products);
        promoData.setUserid(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());


       /* HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("code",promocode.getText().toString());
        hashMap.put("amount", String.valueOf(grandtotal));
        hashMap.put("products",productId);*/
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get(RestClient.SHOPPING_BASE_URL).applyPrmoCode(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),promoData);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(dialogPlus!=null) {
                    dialogPlus.dismiss();
                }

                if(response.code()==200){
                    if(response1.getStatus()) {
                        promoCode.setVisibility(View.VISIBLE);
                        promoAmt=response1.getPromo_discount();
                        grandtotal=grandtotal-promoAmt;
                        grandTotal.setText(String.valueOf(grandtotal));
                        promo.setText(String.valueOf(response1.getPromo_discount()));
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout,response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();
                        isPromo=true;
                        setPromoCode();
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout,response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();
                    }
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                if(dialogPlus!=null) {
                    dialogPlus.dismiss();
                }
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    public void usedWallet(){
        if(isWallet){
            walletLinear.setVisibility(View.VISIBLE);
            wallet.setText("Remove Wallet Amount");
           // float walletamt=((grandtotal* 100) / walletlimit);
            result =  (grandtotal* walletlimit) / 100;
            @SuppressLint("DefaultLocale") String subtotalamt = String.format("%.02f", result);
            if(walletAmount>result) {
                walletAmt.setText(subtotalamt);
                grandtotal = (int) (grandtotal - result);
                postwalletAmt= (int) result;
            }else{
                grandtotal=(int)(grandtotal-walletAmount);
                walletAmt.setText(String.valueOf(walletAmount));
                postwalletAmt= (int) walletAmount;
            }
        }else{
            walletLinear.setVisibility(View.GONE);
            grandtotal= (int) (grandtotal+result);
            result = ( grandtotal* walletlimit) / 100;
            //float walletamt=((grandtotal* 100) / walletlimit);
            @SuppressLint("DefaultLocale") String subtotalamt = String.format("%.02f", result);
            walletAmt.setText(subtotalamt);

           // grandtotal= (int) (grandtotal+result);
            if(userData.size()>0) {
                wallet.setText("Use Wallet (\u20B9 " + String.valueOf(userData.get(0).getWallet() + ")"));
            }else{
                wallet.setText("Use Wallet(\u20B9 0)");
            }
        }
    }

    @SuppressLint("SetTextI18n")
    public void setPromoCode(){
        if(isPromo) {
            promocodeHave.setText("Remove Promocode");
        }else{
            promocodeHave.setText("Have  Promocode ?");
            grandtotal=grandtotal+promoAmt;
            grandTotal.setText(String.valueOf(grandtotal));
            promoCode.setVisibility(View.GONE);
        }
    }

    private void callInstamojoPay(String email, String phone, String amount, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", "Online Shopping");
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }
    InstapayListener listener;
    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                Log.e("TransactionID",response);
                String[] separated = response.split("orderId=");
                String last=separated[1]; // this will contain " they taste good"
                //Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                Log.e("String",last);
                String[] paymentId = last.split(":");
                String payment=paymentId[0];
                Log.e("String",payment);
                placeOrder(payment);
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }

    public void placeOrder(String paymentId){
        progressBar.show();
        List<Product> products=new ArrayList<>();
        for(int i=0;i<cartItems.size();i++){
            CartItem cartItem=cartItems.get(i);
            Product product=new Product();
            product.setDiscount(cartItem.getDiscount());
            product.setDiscountprice(cartItem.getDiscountprice());
            product.setId(cartItem.getPid());
            product.setImage(cartItem.getImage());
            product.setName(cartItem.getName());
            product.setPrice(cartItem.getPrice());
            product.setQuantity(cartItem.getQty());
            product.setShippingCharges(cartItem.getShippingcharges());
            product.setSku(cartItem.getSku());
            product.setTax(cartItem.getTax());
            product.setTotalprice(cartItem.getTotalAmt());


            if(!cartItem.getColor().equals("Select Color")){
                product.setColor(cartItem.getColor());
            }

            if(!cartItem.getSize().equals("Select Size")){
                product.setSize(cartItem.getSize());
            }

            products.add(product);
        }

        PlaceOrder placeOrder=new PlaceOrder();
        placeOrder.setUserid(SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        in.happyhelp.limra.shoppingresponse.placeordermodel.Address addressdata=new Address();
        addressdata.setAddress1(address.getAddress1());
        addressdata.setAddress2(address.getAddress2());
        addressdata.setCity(address.getCity());
        addressdata.setEmail(address.getEmail());
        addressdata.setId(address.getId());
        addressdata.setName(address.getName());
        addressdata.setLocality(address.getLocality());
        addressdata.setPincode(address.getPincode());
        addressdata.setMobile(address.getMobile());
        addressdata.setState(address.getState());
        addressdata.setPrefered(address.getPrefered());



        Amount amount=new Amount();
        amount.setDiscount("0");
        amount.setGrandtotal(String.valueOf(grandtotal));
        amount.setPaymentId(paymentId);
        if(cashRb.isChecked()) {
            amount.setPaymentMode("cod");
        }else{
            amount.setPaymentMode("online");
        }

        amount.setDiscount("0");
        amount.setShippingCharges(shippingCharges.getText().toString());
        amount.setTax(tax.getText().toString());
        amount.setTotalQty("0");
        amount.setSubtotal(subTotal.getText().toString());

        Promo promo=new Promo();
        promo.setAmount(0);
        promo.setCode("");

        Wallet wallet=new Wallet();
        wallet.setAmount(postwalletAmt);


        placeOrder.setAddress(addressdata);
        placeOrder.setAmount(amount);
        placeOrder.setProducts(products);
        placeOrder.setPromo(promo);
        placeOrder.setWallet(wallet);

        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get(RestClient.SHOPPING_BASE_URL).placeOrder(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),placeOrder);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
                progressBar.dismiss();
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){

                    if(response1.getStatus()) {

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(@NonNull Realm realm) {
                                realm.delete(CartItem.class);
                            }
                        });

                        Intent intent = new Intent(CartActivity.this, ConfirmationActivity.class);
                        startActivity(intent);
                        finish();

                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(CartActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }

    public void setData(){
        cartItems=realm.where(CartItem.class).findAllAsync();
        cartItems.addChangeListener(new RealmChangeListener<RealmResults<CartItem>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onChange(@NonNull RealmResults<CartItem> cartItems) {
                adapter.setData(cartItems);
                recyclerView.setAdapter(adapter);

                int subtotal=0;
                int Tax=0;
                int shipingcharge=0;

                for(int i=0;i<cartItems.size();i++){
                    CartItem cartItem=cartItems.get(i);
                    int qty= Integer.parseInt(cartItem.getQty());
                    int price= Integer.parseInt(cartItem.getDiscountprice());
                    int tax= Integer.parseInt(cartItem.getTax());
                    int shipingCharge= Integer.parseInt(cartItem.getShippingcharges());
                    productId=productId+","+cartItem.getPid();
                    Tax=Tax+tax;
                    shipingcharge=shipingcharge+shipingCharge;
                    subtotal=subtotal+qty*price+tax;
                }

                grandtotal=Tax+shipingcharge+subtotal;
                grandTotal.setText(String.valueOf(grandtotal));
                subTotal.setText(String.valueOf(subtotal));
                tax.setText(String.valueOf(Tax));
                shippingCharges.setText(String.valueOf(shipingcharge));

                if(cartItems.size()==0){
                    data.setVisibility(View.VISIBLE);
                    totalCard.setVisibility(View.GONE);
                    pay.setVisibility(View.GONE);
                }else {
                    data.setVisibility(View.GONE);
                    totalCard.setVisibility(View.VISIBLE);
                    pay.setVisibility(View.VISIBLE);
                }
            }
        });

       /* final Address address=realm.where(Address.class).equalTo("prefered",true).findFirstAsync();
        address.addChangeListener(new RealmChangeListener<RealmModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onChange(RealmModel realmModel) {
                if(address!=null && address.isLoaded() && address.isValid()){
                    addressCard.setVisibility(View.VISIBLE);
                    addressTxt.setText(address.getAddress1()+" "+address.getAddress2()+" "+address.getState()+" "+address.getCity()+" "+address.getLocality()+" "+address.getPincode());
                    name.setText(address.getName());
                }else{

                }
            }
        });*/
    }

    public void getDefaultAddress(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<DefaultAddressResponse> call= RestClient.get(RestClient.SHOPPING_BASE_URL).getDefaultAddress(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<DefaultAddressResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<DefaultAddressResponse> call, @NonNull Response<DefaultAddressResponse> response) {
                DefaultAddressResponse defaultAddressResponse=response.body();
                if(response.code()==200){
                    if(defaultAddressResponse.getStatus()){

                        address=defaultAddressResponse.getAddress();

                        if (address.getAddress1()!=null && address.getAddress2()!=null && address.getState()!=null && address.getCity()!=null && address.getLocality()!=null && address.getPincode()!=null)
                        {
                            addressTxt.setText(address.getAddress1()+" "+address.getAddress2()+" "+address.getState()+" "+address.getCity()+" "+address.getLocality()+" "+address.getPincode());

                        }
                        else
                        {
                            addressTxt.setText("Please add address first.");
                        }
                       // addressTxt.setText(address.getAddress1()+" "+address.getAddress2()+" "+address.getState()+" "+address.getCity()+" "+address.getLocality()+" "+address.getPincode());
                        name.setText(address.getName());
                        mobile.setText(address.getMobile());
                        email.setText(address.getEmail());

                        if (address.getAddress1()!=null && address.getAddress2()!=null && address.getState()!=null && address.getCity()!=null && address.getLocality()!=null && address.getPincode()!=null)
                        {
                            changeAddress.setText("Change Address");
                            noAddress.setVisibility(View.GONE);
                            defaultAddress.setVisibility(View.VISIBLE);
                        }else{
                            changeAddress.setText("add Address");
                            noAddress.setVisibility(View.VISIBLE);
                            defaultAddress.setVisibility(View.GONE);
                        }
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }

                }else if(response.code()==401){
                    AppUtils.logout(CartActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<DefaultAddressResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        getDefaultAddress();
        EventBus.getDefault().unregister(this);
    }




    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(CartActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(CartActivity.this,Id);

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
