package in.happyhelp.limra.shopping;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.BuildConfig;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.PdfDocumentAdapter;
import in.happyhelp.limra.PrintJobMonitorService;
import in.happyhelp.limra.R;
import in.happyhelp.limra.adapter.shopping.MyOrderAdapter;
import in.happyhelp.limra.adapter.shopping.MyProductAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.model.shopping.CartItem;
import in.happyhelp.limra.model.shopping.WishList;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.orderdetailresponse.OrderDetailsResponse;
import in.happyhelp.limra.shoppingresponse.orderdetailresponse.Orderdetails;
import in.happyhelp.limra.shoppingresponse.orderdetailresponse.Product;
import io.realm.RealmChangeListener;
import io.realm.RealmModel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShoppingOrderDetailActivity extends AppCompatActivity {

    String orderId="";

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.toname)
    TextView toName;

    @BindView(R.id.address)
    TextView address;

    @BindView(R.id.toaddress)
    TextView toAddress;

    @BindView(R.id.orderid)
    TextView orderid;

    @BindView(R.id.mobile)
    TextView mobile;

    @BindView(R.id.tomobile)
    TextView toMobile;

    @BindView(R.id.email)
    TextView email;

    @BindView(R.id.toemail)
    TextView toEmail;

    @BindView(R.id.status)
    TextView status;

    @BindView(R.id.paymentstatus)
    TextView paymentStatus;

    @BindView(R.id.paymentmode)
    TextView paymentMode;

    @BindView(R.id.subtotal)
    TextView subTotal;

    @BindView(R.id.shipping)
    TextView shipping;

    @BindView(R.id.tax)
    TextView tax;

    @BindView(R.id.grandtotal)
    TextView grandTotal;

    @BindView(R.id.cancel)
    TextView cancel;

    @BindView(R.id.date)
    TextView date;
    ProgressDialog progressBar;
    DialogPlus dialogPlus;
    EditText reason;
    TextView ok,cancelDialoge;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.download)
            TextView download;

    MyProductAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    String filepath="";
    private PrintManager mgr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_order_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ButterKnife.bind(this);
        progressBar=new ProgressDialog(this);
        progressBar.setTitle("Processing...");
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(true);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        mgr=(PrintManager) getSystemService(PRINT_SERVICE);
        adapter=new MyProductAdapter(getApplicationContext(), new MyProductAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(final Product d, View view) throws ParseException {


                TextView returnProduct=view.findViewById(R.id.returnproduct);
                returnProduct.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new AlertDialog.Builder(ShoppingOrderDetailActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(R.string.app_name)
                                .setMessage("Are you sure Do you want to return this Product ")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        returnProduct(orderId,d.getPid());
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();



                    }
                });
            }
        });
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            orderId = extras.getString(Constants.ORDERID);
            getOrderDetails();

        }

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadThermalCPV();
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(ShoppingOrderDetailActivity.this)
                        .setIcon(R.drawable.logo)
                        .setTitle(R.string.app_name)
                        .setMessage("Are you sure Do you want to cancel this Order ")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                cancelOrder(orderId);
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();


            }
        });




    }

    public void downloadThermalCPV(){
        progressBar.show();
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("oid",orderId);

        Call<ResponseBody> call=RestClient.get(RestClient.SHOPPING_BASE_URL).downloadInvoice(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, final Response<ResponseBody> response) {
                ResponseBody responseBody=response.body();
                progressBar.dismiss();
                if(response.code()==200){

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            boolean writtenToDisk = writeResponseBodyToDisk( response.body(),"Invoice");

                            if(writtenToDisk){

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        File file = new File(filepath);

                                        print(file.getAbsolutePath(),
                                                new PdfDocumentAdapter(getApplicationContext(),file.getAbsolutePath()),
                                                new PrintAttributes.Builder().build());


                                    }
                                });
                            }


                            return null;
                        }
                    }.execute();



                }else if(response.code()==401){
                    AppUtils.logout(ShoppingOrderDetailActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.dismiss();
                Toast.makeText(getApplicationContext(), "File downloading failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private PrintJob print(String name, PrintDocumentAdapter adapter,
                           PrintAttributes attrs) {
        startService(new Intent(getApplicationContext(), PrintJobMonitorService.class));

        return(mgr.print(name, adapter, attrs));
    }

    private boolean writeResponseBodyToDisk(ResponseBody body,String filename) {
        try {
            // todo password the file location/name according to your needs
            // File futureStudioIconFile = new File("Smart shopper"+ File.separator + System.currentTimeMillis()+"Invoice.pdf");
            // File futureStudioIconFile=new File(Environment.getExternalStorageDirectory()+Constants.imagePath+System.currentTimeMillis()+"Invoice.pdf");


            File futureStudioIconFile=new File(Environment.getExternalStorageDirectory()+Constants.pdfPath+filename);
            if(!futureStudioIconFile.exists()){
                futureStudioIconFile.mkdirs();
            }

            filepath=futureStudioIconFile.getAbsolutePath()+"/"+System.currentTimeMillis()+"_invoices.pdf";


            if(!futureStudioIconFile.exists()){
                futureStudioIconFile.mkdir();
            }

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(filepath);


                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public  void cancelOrder(final String id) {
        LayoutInflater inflater = getLayoutInflater();
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.item_cancel_order, null);
        dialogPlus = DialogPlus.newDialog(ShoppingOrderDetailActivity.this)
                .setContentHolder(new ViewHolder(view))
                .setGravity(Gravity.CENTER)
                .setCancelable(false)
                .setInAnimation(R.anim.abc_fade_in)
                .setMargin(50, 20, 50, 50)
                .setOutAnimation(R.anim.abc_fade_out)
                .setOverlayBackgroundResource(Color.TRANSPARENT)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogPlus dialog) {
                        dialog.dismiss();
                    }
                })
                .create();
        reason= (EditText) dialogPlus.findViewById(R.id.reason);
        ok= (TextView) dialogPlus.findViewById(R.id.ok);
        cancelDialoge= (TextView) dialogPlus.findViewById(R.id.cancel);

        cancelDialoge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPlus.dismiss();
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(reason.getText().toString())){
                    reason.setError(" Enter reason ");
                    return;
                }
                cancelOrderApi(id);
            }
        });



        dialogPlus.show();


    }

    public  void returnProduct(final String orderId, final String productId) {
        LayoutInflater inflater = getLayoutInflater();
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.item_return_order, null);
        dialogPlus = DialogPlus.newDialog(ShoppingOrderDetailActivity.this)
                .setContentHolder(new ViewHolder(view))
                .setGravity(Gravity.CENTER)
                .setCancelable(false)
                .setInAnimation(R.anim.abc_fade_in)
                .setMargin(50, 20, 50, 50)
                .setOutAnimation(R.anim.abc_fade_out)
                .setOverlayBackgroundResource(Color.TRANSPARENT)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogPlus dialog) {
                        dialog.dismiss();
                    }
                })
                .create();
        reason= (EditText) dialogPlus.findViewById(R.id.reason);
        ok= (TextView) dialogPlus.findViewById(R.id.ok);
        cancelDialoge= (TextView) dialogPlus.findViewById(R.id.cancel);

        cancelDialoge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPlus.dismiss();
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(reason.getText().toString())){
                    reason.setError(" Enter reason ");
                    return;
                }
                returnOrderApi(orderId,productId);
            }
        });



        dialogPlus.show();


    }



    public void returnOrderApi(String orderId,String productId){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("order_id",orderId);
        hashMap.put("product_id",productId);
        hashMap.put("reason",reason.getText().toString());
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get(RestClient.SHOPPING_BASE_URL).returnOrder(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                            getOrderDetails();
                            View parentLayout = findViewById(android.R.id.content);
                            Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                    .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                    .show();
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(ShoppingOrderDetailActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                        .show();

            }
        });
    }



    public void cancelOrderApi(String id){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("id",id);
        hashMap.put("reason",reason.getText().toString());
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get(RestClient.SHOPPING_BASE_URL).cancelOrder(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        if(dialogPlus!=null) {
                            dialogPlus.dismiss();
                            getOrderDetails();
                            View parentLayout = findViewById(android.R.id.content);
                            Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                    .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                    .show();
                        }
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(ShoppingOrderDetailActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                        .show();

            }
        });
    }


    public void getOrderDetails(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("id",orderId);
        Call<OrderDetailsResponse> call= RestClient.get(RestClient.SHOPPING_BASE_URL).getOrderDetail(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<OrderDetailsResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<OrderDetailsResponse> call, @NonNull Response<OrderDetailsResponse> response) {
                OrderDetailsResponse orderDetailsResponse=response.body();
                if(response.code()==200){
                    if(orderDetailsResponse.getStatus()){
                        Orderdetails orderdetails=orderDetailsResponse.getOrderdetails();

                        if(orderdetails!=null) {

                            if(orderdetails.getVendor()!=null) {
                                name.setText(orderdetails.getVendor().getName());
                                mobile.setText(orderdetails.getVendor().getMobile());
                                email.setText(orderdetails.getVendor().getEmail());
                                orderid.setText("HH00" + orderdetails.getVendor().getId());
                                address.setText(orderdetails.getVendor().getAddress());
                            }

                            if(orderdetails.getPaymentStatus()!=null)
                            if (orderdetails.getPaymentStatus().equals("0")) {
                                paymentStatus.setText(" Unpaid ");
                            } else if (orderdetails.getPaymentStatus().equals("1")) {
                                paymentStatus.setText(" Paid ");
                            }  else {
                                paymentStatus.setText(" Unpaid ");
                            }


                            if (orderdetails.getStatus().equals("0")) {
                                status.setText(" Processing");
                                status.setTextColor(Color.RED);
                            } else if (orderdetails.getStatus().equals("1")) {
                                status.setText(" Shipped");
                                status.setTextColor(Color.parseColor("#8BC34A"));
                                cancel.setVisibility(View.GONE);
                            } else if (orderdetails.getStatus().equals("2")) {
                                status.setText(" Delivered");
                                status.setTextColor(Color.parseColor("#8BC34A"));
                                cancel.setVisibility(View.GONE);
                            } else if (orderdetails.getStatus().equals("3")) {
                                status.setText(" Order Failed");
                                status.setTextColor(Color.RED);
                                cancel.setVisibility(View.GONE);
                            } else if (orderdetails.getStatus().equals("4")) {
                                status.setText(" Cancelled");
                                status.setTextColor(Color.RED);
                                cancel.setVisibility(View.GONE);
                            } else {
                                status.setText(" Processing");
                                status.setTextColor(Color.parseColor("#FFEB3B"));
                            }


                            adapter.setData(orderdetails.getProducts());
                            recyclerView.setAdapter(adapter);

                            paymentMode.setText(orderdetails.getPaymentMode());
                            toName.setText(orderdetails.getName());
                            toAddress.setText(orderdetails.getAddress1() +" "+ orderdetails.getAddress2()  +" "+orderdetails.getState() +" "+ orderdetails.getCity() );
                            toMobile.setText(orderdetails.getMobile());
                            toEmail.setText(orderdetails.getEmail());

                            date.setText(orderdetails.getDate());
                            grandTotal.setText(String.valueOf(orderdetails.getGrandTotal()));
                            subTotal.setText(String.valueOf(orderdetails.getSubTotal()));
                            tax.setText(String.valueOf(orderdetails.getTax()));
                            shipping.setText(String.valueOf(orderdetails.getShippingCharges()));

                        }


                    }else{

                    }
                }else if(response.code()==401){
                    AppUtils.logout(ShoppingOrderDetailActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderDetailsResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(ShoppingOrderDetailActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(ShoppingOrderDetailActivity.this,Id);

            }
        });
    }

}
