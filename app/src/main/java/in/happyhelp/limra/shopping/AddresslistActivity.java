package in.happyhelp.limra.shopping;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.ads.AdsActivity;
import in.happyhelp.limra.adapter.shopping.AddressAdapter;
import in.happyhelp.limra.adapter.shopping.AddressListAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.addressresponse.Address;
import in.happyhelp.limra.shoppingresponse.addressresponse.AddressListResponse;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddresslistActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.addaddress)
    FloatingActionButton addaddress;

    @BindView(R.id.addresslist)
    RecyclerView addresslist;

    @BindView(R.id.addressframe)
    FrameLayout frameLayout;

    String Id="";

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    AddressListAdapter addressAdapter;
    RealmResults<Address> addresses;
    Realm realm;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addresslist);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        realm = RealmHelper.getRealmInstance();
        ButterKnife.bind(this);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        addressAdapter = new AddressListAdapter(getApplicationContext(), new AddressListAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(final Address d, View view) throws ParseException {

                ImageView delete=view.findViewById(R.id.delete);
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new AlertDialog.Builder(AddresslistActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(R.string.app_name)
                                .setMessage("Are you sure Do you want to delete address ?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        deleteAddress(d.getId());
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();


                    }
                });


                ImageView edit=view.findViewById(R.id.edit);
                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new AlertDialog.Builder(AddresslistActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(R.string.app_name)
                                .setMessage("Are you sure Do you want to Edit address ?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                       Intent intent=new Intent(AddresslistActivity.this,ShoppyAddressActivity.class);
                                       intent.putExtra(Constants.ADDRESSSID,d.getId());
                                       startActivity(intent);
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();


                    }
                });

                LinearLayout linearLayout=view.findViewById(R.id.linear);
                linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setDefaultAddress(d.getId());
                    }
                });




                /*for(int i=0;i<addresses.size();i++){
                    if(d.getId().equals(addresses.get(i).getId())){
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                if(d.isSelected()){
                                    d.setSelected(false);
                                    realm.copyToRealmOrUpdate(d);
                                    addressAdapter.notifyDataSetChanged();

                                }else{
                                    d.setSelected(true);
                                    realm.copyToRealmOrUpdate(d);
                                    addressAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }else{
                        final Address address=addresses.get(i);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                address.setSelected(false);
                                realm.copyToRealmOrUpdate(address);
                                addressAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                }*/


            }
        });

        linearLayoutManager = new LinearLayoutManager(AddresslistActivity.this, LinearLayoutManager.VERTICAL, false);

        addresslist.setLayoutManager(linearLayoutManager);
        addresslist.setHasFixedSize(true);

        addresses = realm.where(Address.class).findAllAsync();
        addresses.addChangeListener(new RealmChangeListener<RealmResults<Address>>() {
            @Override
            public void onChange(RealmResults<Address> element) {

                addressAdapter.setdata(realm.copyFromRealm(addresses));
                postAndNotifyAdapter(new Handler(Looper.myLooper()), addresslist, addressAdapter);
            }
        });

        addresslist.setAdapter(addressAdapter);

        addaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddresslistActivity.this, ShoppyAddressActivity.class);
                startActivity(intent);
            }
        });

        getAddressList();

    }





    @Override
    protected void onResume() {
        super.onResume();
        getAddressList();
        EventBus.getDefault().register(this);
    }
    public void setDefaultAddress(String id){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("id",id);
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get(RestClient.SHOPPING_BASE_URL).setDefaultAddress(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                            getAddressList();
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();
                    }else if(response.code()==401){
                        AppUtils.logout(AddresslistActivity.this);
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();
                    }


                }else if(response.code()==401){
                    AppUtils.logout(AddresslistActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                        .show();
            }
        });
    }


    public void deleteAddress(String id){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("id",id);
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get(RestClient.SHOPPING_BASE_URL).deleteAddress(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {
                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        getAddressList();
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();
                    }else{
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                .show();
                    }


                }else if(response.code()==401){
                    AppUtils.logout(AddresslistActivity.this);
                }else{
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                        .show();
            }
        });
    }


    public void getAddressList(){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        //hashMap.put("userid","31");

        Call<AddressListResponse> call= RestClient.get(RestClient.SHOPPING_BASE_URL).getAddressList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<AddressListResponse>() {
            @Override
            public void onResponse(@NonNull Call<AddressListResponse> call, @NonNull Response<AddressListResponse> response) {
                final AddressListResponse addressListResponse=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(addressListResponse.getStatus()){
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(Address.class);
                                realm.copyToRealmOrUpdate(addressListResponse.getAddresses());
                            }
                        });
                    }else{

                    }
                }else if(response.code()==401){
                    AppUtils.logout(AddresslistActivity.this);
                }else{

                }

            }

            @Override
            public void onFailure(@NonNull Call<AddressListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }



    protected void postAndNotifyAdapter(final Handler handler, final RecyclerView recyclerView, final RecyclerView.Adapter<RecyclerView.ViewHolder> adapter) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (!recyclerView.isComputingLayout()) {
                    adapter.notifyDataSetChanged();
                } else {
                    postAndNotifyAdapter(handler, recyclerView, adapter);
                }
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }






    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(AddresslistActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(AddresslistActivity.this,Id);

            }
        });
    }

    @Override
    public void onRefresh() {
        getAddressList();
    }
}

