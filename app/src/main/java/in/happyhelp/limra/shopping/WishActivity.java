package in.happyhelp.limra.shopping;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.ads.AdsListActivity;
import in.happyhelp.limra.adapter.shopping.WishAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.model.shopping.WishList;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.wishlistresponse.WishListResponse;
import in.happyhelp.limra.shoppingresponse.wishlistresponse.Wishlist;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    int page=1;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.data)
    TextView data;

    WishAdapter adapter;

    List<WishList> list=new ArrayList<>();
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        realm= RealmHelper.getRealmInstance();
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);
        GridLayoutManager gridLayoutManager=new GridLayoutManager(getApplicationContext(), 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);

        adapter=new WishAdapter(getApplicationContext(), new WishAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(final Wishlist d, View view) throws ParseException {

                final ImageView remove=view.findViewById(R.id.remove);
                remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new AlertDialog.Builder(WishActivity.this)
                                .setIcon(R.drawable.logo)
                                .setTitle(R.string.app_name)
                                .setMessage("Are you sure Do you want to remove from wishlist ?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        removefromWishLits(d.getId());
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();


                    }
                });

                ImageView image=view.findViewById(R.id.image);
                LinearLayout linearLayout=view.findViewById(R.id.linear);
                linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(WishActivity.this,ProductDetailsActivity.class);
                        intent.putExtra(Constants.PID, d.getPid());
                        startActivity(intent);
                    }
                });

                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(WishActivity.this,ProductDetailsActivity.class);
                        intent.putExtra(Constants.PID, d.getPid());
                        startActivity(intent);
                    }
                });
            }
        });

        getWishList();
    }

    public void removefromWishLits(String id){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("id",id);
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get(RestClient.SHOPPING_BASE_URL).removeWishList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {

                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        getWishList();

                    }else{

                    }
                }else if(response.code()==401){
                    AppUtils.logout(WishActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {

            }
        });
    }

    public void getWishList(){
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("page",String.valueOf(page));
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<WishListResponse> call= RestClient.get(RestClient.SHOPPING_BASE_URL).getWishList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<WishListResponse>() {
            @Override
            public void onResponse(@NonNull Call<WishListResponse> call, @NonNull Response<WishListResponse> response) {
                final WishListResponse wishListResponse=response.body();
                swipeRefreshLayout.setRefreshing(false);
                if(response.code()==200){
                    if(wishListResponse.getStatus()){

                        adapter.setData(wishListResponse.getWishlists());
                        recyclerView.setAdapter(adapter);

                        if(wishListResponse.getWishlists().size()==0){
                            data.setVisibility(View.VISIBLE);
                        }else{
                            data.setVisibility(View.GONE);
                        }

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(WishList.class);
                                for(int i=0;i<wishListResponse.getWishlists().size();i++){
                                    WishList wishList=new WishList();
                                    Wishlist wishlist=wishListResponse.getWishlists().get(i);
                                    wishList.setDate(wishlist.getDate());
                                    wishList.setDiscount(wishlist.getDiscount());
                                    wishList.setDiscountprice(wishlist.getDiscountprice());
                                    wishList.setImage(wishlist.getImage());
                                    wishList.setId(wishlist.getId());
                                    wishList.setPid(wishlist.getPid());
                                    wishList.setName(wishList.getDate());
                                    wishList.setUid(wishlist.getUid());
                                    list.add(wishList);
                                }
                                realm.copyToRealmOrUpdate(list);
                            }
                        });
                    }else{

                    }

                }else if(response.code()==401){
                    AppUtils.logout(WishActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<WishListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }



    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(WishActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(WishActivity.this,Id);

            }
        });
    }

    @Override
    public void onRefresh() {
        getWishList();
    }
}
