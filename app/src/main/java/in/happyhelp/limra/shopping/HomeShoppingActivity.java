package in.happyhelp.limra.shopping;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.happyhelp.limra.Constants;
import in.happyhelp.limra.R;
import in.happyhelp.limra.activity.ConfirmationActivity;
import in.happyhelp.limra.activity.property.PropertyDetailsActivity;
import in.happyhelp.limra.activity.response.loginresponse.UserData;
import in.happyhelp.limra.adapter.shopping.ProductAdapter;
import in.happyhelp.limra.adapter.shopping.ShoppingAdapter;
import in.happyhelp.limra.data.AppUtils;
import in.happyhelp.limra.data.RealmHelper;
import in.happyhelp.limra.data.SharedPreferenceHelper;
import in.happyhelp.limra.model.NotificationEvent;
import in.happyhelp.limra.model.ServiceModel;
import in.happyhelp.limra.model.shopping.CartItem;
import in.happyhelp.limra.model.shopping.WishList;
import in.happyhelp.limra.network.RestClient;
import in.happyhelp.limra.shoppingresponse.addressresponse.Address;
import in.happyhelp.limra.shoppingresponse.addressresponse.AddressListResponse;
import in.happyhelp.limra.shoppingresponse.bannerresponse.ShoppingBannerResponse;
import in.happyhelp.limra.shoppingresponse.categoryresponse.Category;
import in.happyhelp.limra.shoppingresponse.categoryresponse.ShoppingCategoryResponse;
import in.happyhelp.limra.shoppingresponse.homeresponse.HomeScreenResponse;
import in.happyhelp.limra.shoppingresponse.productresponse.Product;
import in.happyhelp.limra.shoppingresponse.productresponse.ProductResponse;
import in.happyhelp.limra.shoppingresponse.subcategoryresponse.ShoppingSubCategoryResponse;
import in.happyhelp.limra.shoppingresponse.wishlistresponse.WishListResponse;
import in.happyhelp.limra.shoppingresponse.wishlistresponse.Wishlist;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeShoppingActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    LinearLayoutManager linearLayoutManager;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    ShoppingAdapter adapter;
    TextView textCartItemCount;
    @BindView(R.id.slider)
    SliderLayout slider;
    Realm realm;
    List<Product> products = new ArrayList<>();
    @BindView(R.id.product)
    RecyclerView productRecycler;
    ProductAdapter productAdapter;

    List<String> listBanner = new ArrayList<>();
    LinearLayout address, myorder, cart, setting;
    TextView name;
    List<WishList> list=new ArrayList<>();
    GridLayoutManager gridLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_shopping);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        realm = RealmHelper.getRealmInstance();

        gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2, GridLayoutManager.VERTICAL, false);
        int mNoOfColumns = AppUtils.calculateNoOfColumns(getApplicationContext(),120);
        gridLayoutManager = new GridLayoutManager(this, mNoOfColumns);
        productRecycler.setLayoutManager(gridLayoutManager);
        productRecycler.setHasFixedSize(true);

        productAdapter = new ProductAdapter(getApplicationContext(), new ProductAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(final Product d, View view) throws ParseException {

                Log.e("Main","CLICK");
                ImageView like=view.findViewById(R.id.like);
                like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(d.getWishlist().equals("1")){
                            removefromWishLits(d.getId());
                        }else{
                           // addWhishlist(d);
                        }

                    }
                });

                ImageView image=view.findViewById(R.id.image);
                LinearLayout linearLayout=view.findViewById(R.id.linear);
                linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(HomeShoppingActivity.this,ProductDetailsActivity.class);
                        intent.putExtra(Constants.PID, d.getId());
                        startActivity(intent);
                    }
                });

                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(HomeShoppingActivity.this,ProductDetailsActivity.class);
                        intent.putExtra(Constants.PID, d.getId());
                        startActivity(intent);
                    }
                });


                Intent intent=new Intent(HomeShoppingActivity.this,ProductDetailsActivity.class);
                intent.putExtra(Constants.PID, d.getId());
                startActivity(intent);


            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.inflateHeaderView(R.layout.nav_header_home_shopping);
        address = header.findViewById(R.id.address);
        myorder = header.findViewById(R.id.myorder);
        cart = header.findViewById(R.id.menu_cart);
        name = header.findViewById(R.id.name);

        setupBadge();

        RealmResults<UserData> userData = realm.where(UserData.class).findAllAsync();
        userData.addChangeListener(new RealmChangeListener<RealmResults<UserData>>() {
            @Override
            public void onChange(@NonNull RealmResults<UserData> userData) {
                if (userData.size() > 0) {
                    name.setText(userData.get(0).getName());
                }

            }
        });

        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = (new Intent(getApplicationContext(), AddresslistActivity.class));
                intent.putExtra(Constants.VISIBLE, false);
                startActivity(intent);
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = (new Intent(getApplicationContext(), CartActivity.class));
                startActivity(intent);
            }
        });


        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        adapter = new ShoppingAdapter(getApplicationContext(), new ShoppingAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(Category d, View view) throws ParseException {

                Intent intent = new Intent(HomeShoppingActivity.this, ShopingSubCategoryActivity.class);
                intent.putExtra(Constants.SHOPID, d.getId());
                startActivity(intent);

            }
        });
        getWishList();
        getCategory();
        getShoppingBanner();
        getHomeRandomProduct();


        slider.movePrevPosition(false);
        slider.setPresetTransformer(com.daimajia.slider.library.SliderLayout.Transformer.Accordion);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(3000);
        slider.startAutoCycle();

    }


    public  int calculateNoOfColumns(Context context, float columnWidthDp) { // For example columnWidthdp=180
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (screenWidthDp / columnWidthDp + 0.5); // +0.5 for correct rounding to int.
        return noOfColumns;
    }

    public void removefromWishLits(String id){

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("pid",id);
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get(RestClient.SHOPPING_BASE_URL).removeWishList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, Response<in.happyhelp.limra.activity.response.Response> response) {

                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){
                        getHomeRandomProduct();
                        getWishList();
                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }else{
                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();

                    }
                }else if(response.code()==401){
                    AppUtils.logout(HomeShoppingActivity.this);
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();

                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();
                t.printStackTrace();
            }
        });
    }


    public void addWhishlist(String id){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        hashMap.put("id",id);
        Call<in.happyhelp.limra.activity.response.Response> call=RestClient.get(RestClient.SHOPPING_BASE_URL).addWishList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<in.happyhelp.limra.activity.response.Response>() {
            @Override
            public void onResponse(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Response<in.happyhelp.limra.activity.response.Response> response) {

                in.happyhelp.limra.activity.response.Response response1=response.body();
                if(response.code()==200){
                    if(response1.getStatus()){

                        getHomeRandomProduct();
                        getWishList();
                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }else{
                        View parentLayout =findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, response1.getMessage(), Snackbar.LENGTH_LONG)
                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                                .show();
                    }
                }else if(response.code()==401){
                    AppUtils.logout(HomeShoppingActivity.this);
                }else{
                    View parentLayout =findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, Constants.SOMTHING_WENT_WRONG, Snackbar.LENGTH_LONG)
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.happyhelp.limra.activity.response.Response> call, @NonNull Throwable t) {
                t.printStackTrace();
                View parentLayout =findViewById(android.R.id.content);
                Snackbar.make(parentLayout, Constants.SERVERTIMEOUT, Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                        .show();

            }
        });
    }

    public void getCategory() {
        Call<ShoppingCategoryResponse> call = RestClient.get(RestClient.SHOPPING_BASE_URL).getShoppingCategory(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken());
        call.enqueue(new Callback<ShoppingCategoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<ShoppingCategoryResponse> call, @NonNull Response<ShoppingCategoryResponse> response) {
                ShoppingCategoryResponse shoppingCategoryResponse = response.body();
                if (response.code() == 200) {
                    if (shoppingCategoryResponse.getStatus()) {

                        adapter.setData(shoppingCategoryResponse.getCategories());
                        recyclerView.setAdapter(adapter);
                    } else {
                        adapter.setData(shoppingCategoryResponse.getCategories());
                        recyclerView.setAdapter(adapter);
                    }

                }else if(response.code()==401){
                    AppUtils.logout(HomeShoppingActivity.this);
                } else {

                }
            }

            @Override
            public void onFailure(@NonNull Call<ShoppingCategoryResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void getHomeRandomProduct() {
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<ProductResponse> call = RestClient.get(RestClient.SHOPPING_BASE_URL).getHomeBanner(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProductResponse> call, @NonNull Response<ProductResponse> response) {
                ProductResponse productResponse=response.body();
                Log.e("sgsfgfg","sfgsfg"+productResponse);
                if(response.code()==200){
                    if(productResponse.getStatus()){
                        productAdapter.setData(productResponse.getProducts());
                        productRecycler.setAdapter(productAdapter);
                        products.clear();
                        products.addAll(productResponse.getProducts());

                    }else{

                    }
                }else if(response.code()==401){
                    AppUtils.logout(HomeShoppingActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<ProductResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
}

    public void getWishList(){
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("page","1");
        hashMap.put("userid",SharedPreferenceHelper.getInstance(getApplicationContext()).getString(Constants.USERID));
        Call<WishListResponse> call= RestClient.get(RestClient.SHOPPING_BASE_URL).getWishList(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<WishListResponse>() {
            @Override
            public void onResponse(@NonNull Call<WishListResponse> call, @NonNull Response<WishListResponse> response) {
                final WishListResponse wishListResponse=response.body();
                if(response.code()==200){
                    if(wishListResponse.getStatus()){


                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(WishList.class);
                                for(int i=0;i<wishListResponse.getWishlists().size();i++){
                                    WishList wishList=new WishList();
                                    Wishlist wishlist=wishListResponse.getWishlists().get(i);
                                    wishList.setDate(wishlist.getDate());
                                    wishList.setDiscount(wishlist.getDiscount());
                                    wishList.setDiscountprice(wishlist.getDiscountprice());
                                    wishList.setImage(wishlist.getImage());
                                    wishList.setId(wishlist.getId());
                                    wishList.setPid(wishlist.getPid());
                                    wishList.setName(wishList.getDate());
                                    wishList.setUid(wishlist.getUid());
                                    list.add(wishList);
                                }
                                realm.copyToRealmOrUpdate(list);
                            }
                        });
                    }else{

                    }

                }else if(response.code()==401){
                    AppUtils.logout(HomeShoppingActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<WishListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getShoppingBanner(){
        HashMap<String,String> hashMap=new HashMap<>();
        Call<ShoppingBannerResponse> call=RestClient.get(RestClient.SHOPPING_BASE_URL).getEcommerceBanner(SharedPreferenceHelper.getInstance(getApplicationContext()).getAuthToken(),hashMap);
        call.enqueue(new Callback<ShoppingBannerResponse>() {
            @Override
            public void onResponse(@NonNull Call<ShoppingBannerResponse> call, @NonNull Response<ShoppingBannerResponse> response) {

                ShoppingBannerResponse shoppingBannerResponse=response.body();

                if(response.code()==200){
                    if(shoppingBannerResponse.getStatus()){
                        listBanner.clear();
                        for(int i=0;i<shoppingBannerResponse.getBanners().size();i++){
                            listBanner.add(shoppingBannerResponse.getBanners().get(i).getImage());
                        }


                        for (int i = 0; i < listBanner.size(); i++) {
                            DefaultSliderView defaultSliderView = new DefaultSliderView(getApplicationContext());
                            final int finalI = i;
                            defaultSliderView.image(RestClient.base_image_url + listBanner.get(i))
                                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                        @Override
                                        public void onSliderClick(BaseSliderView slider) {
                                            String BannnerName = listBanner.get(finalI);
                            /*JobCategory banners=realm.where(JobCategory.class).equalTo("image",BannnerName).findFirst();
                            String url = banners.getLink();

                            if(!url.equals("")) {
                                Intent i = new Intent(Intent.ACTION_VIEW,
                                        Uri.parse(url));
                                startActivity(i);
                            }*/
                                        }
                                    });
                            slider.addSlider(defaultSliderView);
                        }
                    }else{

                    }
                }else if(response.code()==401){
                    AppUtils.logout(HomeShoppingActivity.this);
                }else{

                }
            }

            @Override
            public void onFailure(@NonNull Call<ShoppingBannerResponse> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_shopping, menu);

        final MenuItem menuItem = menu.findItem(R.id.cart);

        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

        setupBadge();

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupBadge();
        getWishList();
        getCategory();
        getShoppingBanner();
        getHomeRandomProduct();
        EventBus.getDefault().unregister(this);
    }






    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(final NotificationEvent event) {
        // do something to the notification
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String Id="";
                Id=event.getBody();
                Log.e("Service ID",Id);
                AppUtils.setDialog(HomeShoppingActivity.this,Id);
                AppUtils.getMyServiceEnquiryDetail(HomeShoppingActivity.this,Id);

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.cart) {
            Intent intent=new Intent(HomeShoppingActivity.this,CartActivity.class);
            startActivity(intent);
        }else if(id==R.id.wish){
            Intent intent=new Intent(HomeShoppingActivity.this,WishActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupBadge() {

        if (textCartItemCount != null) {
            RealmResults<CartItem> cartItems=realm.where(CartItem.class).findAllAsync();
            cartItems.addChangeListener(new RealmChangeListener<RealmResults<CartItem>>() {
                @Override
                public void onChange(RealmResults<CartItem> cartItems) {
                    if (cartItems.size() == 0) {
                        if (textCartItemCount.getVisibility() != View.GONE) {
                            textCartItemCount.setVisibility(View.GONE);
                        }
                    }
                    else {
                        textCartItemCount.setText(String.valueOf(Math.min(cartItems.size(), 99)));
                        if (textCartItemCount.getVisibility() != View.VISIBLE) {
                            textCartItemCount.setVisibility(View.VISIBLE);
                        }
                    }
                }

            });

        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.myorder) {
            Intent intent=new Intent(HomeShoppingActivity.this,MyOrdersActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
