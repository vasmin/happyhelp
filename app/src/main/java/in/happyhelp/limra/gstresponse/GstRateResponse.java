
package in.happyhelp.limra.gstresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GstRateResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("gst")
    @Expose
    private List<Gst> gst = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Gst> getGst() {
        return gst;
    }

    public void setGst(List<Gst> gst) {
        this.gst = gst;
    }

}
